import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { DefaultComponent, WithNavComponent } from './container/layouts';

import {
  CandidateComponent,
  EmployerComponent,
  LoginComponent,
  UserTypeComponent,
  ResetPasswordComponent,
  ForgotPasswordComponent,
  AccountVerifiedComponent,
  LinkInvalidComponent,
  ReVerifyComponent,
  RedirectComponent,
  AdminAuthLoginComponent
} from './views/auth';

import { LoadingComponent } from './views/loading/loading.component';

import { NotFoundComponent, InternalServerErrorComponent } from './views/errors';

import { AuthGuard, NoAuthGuard } from './guards';
import { TermsAndConditionsEmployerComponent } from './views/terms-and-conditions-employer/terms-and-conditions-employer.component';
import { TermsAndConditionsCandidateComponent } from './views/terms-and-conditions-candidate/terms-and-conditions-candidate.component';
// import {AdminAuthLoginComponent} from './'
const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'login',
  //   pathMatch: 'full'
  // },
  {
    path: '',
    redirectTo: 'loading',
    pathMatch: 'full'
  },
  // {
  //   path: '',
  //   redirectTo: routeName,
  //   pathMatch: 'full'
  // },
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: 'loading',
        canActivate: [AuthGuard],
        component: LoadingComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Loading'
        }
      },
    ]
  },
  {
    path: '',
    component: WithNavComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Login'
        }
      },
      {
        path: 'redirect',
        component: RedirectComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Redirect'
        }
      },
      {
        path: 'signup/candidate',
        component: CandidateComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: true,
          hasHamburger: false,
          isEditable: [],
          title: 'Signup'
        }
      },
      {
        path: 'signup/employer',
        component: EmployerComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: true,
          hasHamburger: false,
          isEditable: [],
          title: 'Signup'
        }
      },
      {
        path: 'user-type',
        component: UserTypeComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'User Type'
        }
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: true,
          hasHamburger: false,
          isEditable: [],
          title: 'Forgot Password'
        }
      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Reset Password Otp'
        }
      },
      {
        path: 'reset-password/:token',
        component: ResetPasswordComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Reset Password'
        }
      },
      {
        path: 'resend-link',
        component: ReVerifyComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Re-Verify Account'
        }
      }, 
      
      //================================
      {
        path: 'terms-and-conditions-for-employer',
        component: TermsAndConditionsEmployerComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Terms and Conditions'
        }
      }, 

      {
        path: 'terms-and-conditions-for-candidate',
        component: TermsAndConditionsCandidateComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Terms and Conditions'
        }
      }, 

      //==================================
      {
        path: 'account-verified',
        component: AccountVerifiedComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Account Verified'
        }
      },
      {
        path: 'invalid-link',
        component: LinkInvalidComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Invalid Link'
        }
      },
      {
        path: 'Admin-access-to-emp',
        component: AdminAuthLoginComponent,
        canActivate: [NoAuthGuard],
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Admin access'
        }
      },
      {
        path: '404',
        component: NotFoundComponent,
        data: {
          hasBackBtn: true,
          hasHamburger: false,
          isEditable: [],
          title: '404 Not Found'
        }
      },
      {
        path: '500',
        component: InternalServerErrorComponent,
        data: {
          hasBackBtn: true,
          hasHamburger: false,
          isEditable: [],
          title: '500 Internal Server Error'
        }
      },
      // Load Projects
      {
        path: 'candidate',
        canActivate: [AuthGuard],
        loadChildren: '../../projects/candidate/src/app/app.module#CandidateAppModule'
      },
      {
        path: 'employer',
        canActivate: [AuthGuard],
        loadChildren: '../../projects/employer/src/app/app.module#EmployerAppModule'
      },
    ]
  },
  // Wild Card Routes
  {
      path: '**',
      redirectTo: '404',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
