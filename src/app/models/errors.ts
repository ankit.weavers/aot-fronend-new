export const Erros = {
  fullname: [
    { type: 'required', message: 'Name is required' }
  ],
  firstname: [
    { type: 'required', message: 'First name is required' }
  ],
  lastname: [
    { type: 'required', message: 'Last name is required' }
  ],
  phone: [
    { type: 'required', message: 'Phone is required' },
    { type: 'validCountryPhone', message: 'Phone incorrect for the country selected' },
    { type: 'pattern', message: 'Phone is required' }
    // { type: 'pattern', message: '' }

  ],
  country: [
    { type: 'required', message: 'Country is required' },
  ],
  employerType: [
    { type: 'required', message: 'Employer Type is required' },
  ],
  email: [
    { type: 'required', message: 'Email is required' },
    { type: 'email', message: 'Enter a valid email' },
    // { type: 'pattern', message: 'Enter a valid email or phone' }
  ],
  emailOrPhone: [
    { type: 'required', message: 'Email or Phone is required' },
    { type: 'pattern', message: 'Enter a valid email or phone' }
  ],

  emailPhone: [
    { type: 'required', message: 'Email or Phone is required' },
    { type: 'pattern', message: 'Enter a valid email or phone' }
  ],
  confirmEmailOrPhone: [
    { type: 'required', message: 'Confirmation of Email or Phone is required' },
    { type: 'notEqual', message: 'Email or Phone mismatch' }
  ],
  // email: {
  //   required: 'Email is required',
  //   email: 'Enter a valid email'
  // },
  confirmPassword: [
    { type: 'required', message: 'Confirm password is required' },
    { type: 'notEqual', message: 'Password mismatch' }
  ],
  oldPassword: [
    { type: 'required', message: 'Old password is required' }
  ],
  password: [
    { type: 'required', message: 'Password is required' },
    { type: 'pattern', message: 'Please enter a valid Password.' }
  ],
  newPassword: [
    { type: 'required', message: 'New Password is required' },
    { type: 'pattern', message: 'Please enter a valid Password.' }
  ],
  terms: [
    { type: 'required', message: 'You must accept terms and conditions' }
  ],
  companyName: [
    { type: 'required', message: 'Company Name is required' }
  ],
  companyDescription: [
    { type: 'required', message: 'Company Description is required' }
  ],
  brandName: [
    { type: 'required', message: 'Brand Name is required' }
  ],
  companyNo: [
    { type: 'required', message: 'Company Number is required' },
    { type: 'pattern', message: 'Company Number is required"' }
  ],
  vatNo: [
    { type: 'required', message: 'Vat Number is required' }
  ],
  capacity: [
    { type: 'required', message: 'Capacity is required' },
    { type: 'pattern', message: 'Only numbers are allowed' }
  ],
  addressOne: [
    { type: 'required', message: 'Primary Address is required' }
  ],
  city: [
    { type: 'required', message: 'City is required' }
  ],
  countryCode: [
    { type: 'required', message: 'Country is required' }
  ],
  postCode: [
    { type: 'required', message: 'Post Code is required' }
  ],
  industry: [
    { type: 'required', message: 'Industry is required' }
  ],
  jobRole: [
    { type: 'required', message: 'Job Role is required' }
  ],
  jobType: [
    { type: 'required', message: 'Job Type is required' }
  ],
  payType: [
    { type: 'required', message: 'PayType is required' }
  ],
  pay: [
    { type: 'required', message: 'Pay is required' },
    { type: 'pattern', message: 'Enter a valid Pay' }
  ],
  no_of_stuffs: [
    { type: 'required', message: 'Number of Stuff is required' },
    { type: 'pattern', message: 'Enter a valid Number of Stuff ' }

  ],
  searchControl: [
    { type: 'required', message: 'Must provide a location' }
  ],
  shift: [
    { type: 'required', message: 'Shift is required ' }
  ],
  startDate: [
    { type: 'required', message: 'Start Date is required' }
  ],
  endDate: [
    { type: 'required', message: 'End Date is required' }
  ],
  dateRangeControl: [
    { type: 'required', message: 'Must provide Date Range' }
  ],
  startTime: [
    { type: 'required', message: 'Start Time is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  endTime: [
    { type: 'required', message: 'End Time is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  startHour: [
    { type: 'required', message: 'Start Time Hour is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  endHour: [
    { type: 'required', message: 'End Time Hour is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  startMinute: [
    { type: 'required', message: 'Start Time Minute is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  endMinute: [
    { type: 'required', message: 'End Time Minute is required' },
    // { type: 'max', message: 'Max Start Time is 24:59' }
  ],
  skillDetails: [
    { type: 'required', message: 'Skill Details is required' }
  ],
  skillDescription: [
    { type: 'required', message: 'Description is required' }
  ],
  ratePerHour: [
    { type: 'required', message: 'Rate Per Hour is required' },
    { type: 'pattern', message: 'Enter a valid Rate Per Hour' }
  ],
  ni: [
    { type: 'required', message: 'NI is required' },
    { type: 'pattern', message: 'Enter a valid NI' }
  ],
  totalHour: [
    { type: 'required', message: 'Total Hour is required' },
    { type: 'pattern', message: 'Enter a valid Hour' }
  ],
  vat: [
    { type: 'required', message: 'VAT is required' },
    { type: 'pattern', message: 'Enter a valid VAT' }
  ],
  fee: [
    { type: 'required', message: 'Fee is required' },
    { type: 'pattern', message: 'Enter a valid Fee' }
  ],
  totalPayment: [
    { type: 'required', message: 'Total Payment is required' },
    { type: 'pattern', message: 'Enter a valid Payment' }
  ],
  onlyNumber: [
    { type: 'pattern', message: 'Please Enter Valid Digits' }
  ],
  dob: [
    { type: 'required', message: 'DOB is required' }
  ],
  relation: [
    { type: 'required', message: 'Relation is required' }
  ],
  location: [
    { type: 'required', message: 'Please provide a location in order to search jobs.' }
  ],
  Position:[
    {type: 'required', message: 'Position is required'}
  ],
  Address_Details: [
    {type : 'required', message: 'Address is required'}
  ],
    Duties: [
    { type: 'required', message: 'Duties is required' }
  ],
  // vat:[
  //   { type: 'required', message: 'VAT is required' }
  // ],
  // vat:[
  //   { type: 'required', message: 'VAT is required' }
  // ],
  // vat:[
  //   { type: 'required', message: 'VAT is required' }
  // ],
};
