// class
export interface Chatroom {
    chatroomId:string;
    employerId:string;
    employerName:string;
    employerProfilePic:string;
    candidateId:string;
    candidateName:string;
    candidateProfilePic:string;
    candidateUnreadCount: number;
    employerUnreadCount: number;
    lastMessageEntry:string;
}

// export class Chatroom {
//     details: any = {
//         id: String,
//         createdAt: Date,
//         unreadMessageCount: Number,
//     };
//     // messages: any = [
//     //     {
//     //         attachmentLink:String,
//     //         isRead:Boolean,
//     //         message:String,
//     //         senderId:String,
//     //         receiverId:String,
//     //     }
//     // ];
// }