export interface Company {

    'CompanyName': string;
    'CompanyDescription':string;
    'BrandName': string;
    'CompanyNo': string;
    'VatNo': string;
    'Capacity': string;
  }
  