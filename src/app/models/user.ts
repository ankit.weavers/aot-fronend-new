export interface User {
  'fname': string;
  'mname': string;
  'lname': string;
  'email': string;
  'password': string;
  'confirmPassword': string;
  'signedUpVia': string;
  'tocken': string;
}
