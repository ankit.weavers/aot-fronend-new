import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AuthenticationService, GlobalActionsService } from '../../../services';
import { MatSnackBar,MatSlideToggleChange } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'aot-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('expandCollapse', [
      state('open', style({
        height: '*',
        margin: '*',
        padding: '*',
        visibility: 'visible',
        opacity: '1'
      })),
      state('close', style({
        height: '0px',
        margin: '0px',
        padding: '0px',
        visibility: 'hidden',
        opacity: '0'
      })),
      transition('open <=> close', animate(300))
    ])
  ]
})
export class SidebarComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

  public allNavItems: any;
  private userData: any;
  private persanolData: any;
  public userType: any;
  public openCloseAnim: string = 'open';
  checked : boolean;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public snackBar: MatSnackBar,
    private globalactionservice: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.persanolData = JSON.parse(localStorage.getItem('personalData'));
    this.globalactionservice.profilePicStatus.subscribe(response => {
      this.userData = JSON.parse(localStorage.getItem('currentUser'));

    })

    this.userType = (this.userData && Object.keys(this.userData).length) ? this.userData.userType.toLowerCase() : '';
    this.allNavItems = {
      candidate: [
        {
          name: 'Profile',
          link: '',
          icon: 'assets/images/sidenav-ic-1.png',
          children: [
            {
              name: 'Edit Profile',
              link: '/candidate/profile/edit',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'View Profile',
              link: '/candidate/profile',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'Change Password',
              link: '/candidate/profile/change-password',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
          ],
          click: 'openSubMenus',
          hover: '',
          state: 'close'
        },
        {
          name: 'Dashboard',
          link: '',
          icon: 'assets/images/sidenav-ic-2.png',
          children: [
            {
              name: 'Jobs',
              link: '/candidate/dashboard/job-offers',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'Status',
              link: '/candidate/dashboard/status',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'Tracking',
              link: '/candidate/dashboard/time-tracking',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'Metrics',
              link: '/candidate/dashboard/metrics',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
          ],
          click: 'openSubMenus',
          hover: '',
          state: 'close'
        },
        {
          name: 'Apply for a Job',
          link: '',
          icon: 'assets/images/sidenav-ic-3.png',
          children: [
            {
              name: 'Refined Search',
              link: '/candidate/job/search',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            {
              name: 'Appointment',
              link: '/candidate/job/appointments',
              icon: '',
              children: [],
              click: '',
              hover: '',
              state: 'close'
            },
            // {
            //   name: 'Job History',
            //   link: '/candidate/job/job-history',
            //   icon: '',
            //   children: [],
            //   click: '',
            //   hover: '',
            //   state: 'close'
            // },
          ],
          click: 'openSubMenus',
          hover: '',
          state: 'close'
        },
        {
          name: 'Messages',
          link: '/candidate/profile/contacts',
          icon: 'assets/images/sidenav-emp-ic-5.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Ratings',
          link: '/candidate/profile/ratings',
          icon: 'assets/images/ic-star-sidebar.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Alerts',
          link: '/candidate/profile/notifications',
          icon: 'assets/images/sidenav-ic-9.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Contact Us',
          link: '/candidate/contact-us',
          icon: 'assets/images/sidenav-ic-5.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'About Us',
          link: '/candidate/about-us',
          icon: 'assets/images/sidenav-ic-6.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Privacy Policy',
          link: '/candidate/privacy-policy',
          icon: 'assets/images/sidenav-ic-7.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Terms And Conditions',
          link: '/candidate/terms-and-conditions',
          icon: 'assets/images/sidenav-emp-ic-2.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },        
        {
          name: 'Faq',
          link: '/candidate/faq',
          icon: 'assets/images/sidenav-ic-8.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
      
        // {
        //   name: 'Logout',
        //   link: '',
        //   icon: 'assets/images/sidenav-ic-10.png',
        //   children: [],
        //   click: 'logOut',
        //   hover: '',
        //   state: 'close'
        // },
      ],
      employer: [
        {
          name: 'Dashboard',
          link: '/employer/dashboard',
          icon: 'assets/images/sidenav-emp-ic-1.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Profile',
          link: '/employer/profile',
          icon: 'assets/images/sidenav-ic-1.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Change Password',
          link: '/employer/profile/change-password',
          icon: 'assets/images/sidenav-emp-ic-3.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Job Post',
          link: '/employer/job/post',
          icon: 'assets/images/sidenav-emp-ic-4.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Messages',
          // link: '/employer/profile/messages',
          link: '/employer/profile/contacts',
          icon: 'assets/images/sidenav-emp-ic-5.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Alerts',
          link: '/employer/profile/notifications',
          icon: 'assets/images/sidenav-ic-9.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Seleckt Staff',
          link: '/employer/all-candidate', //applicants
          icon: 'assets/images/candidate-ic.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Rate Workers',
          link: '/employer/dashboard/employed-candidates', //applicants
          icon: 'assets/images/ic-star-sidebar.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Favourite Workers',
          link: '/employer/fav-candidate', //applicants
          icon: 'assets/images/ic-thumb-sidebar.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
     
        {
          name: 'Payment Status',
          link: '/employer/job/payment-status',
          icon: 'assets/images/sidenav-emp-ic-7.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Contact Us',
          link: '/employer/contact-us',
          icon: 'assets/images/sidenav-ic-5.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'About Us',
          link: '/employer/about-us',
          icon: 'assets/images/sidenav-ic-6.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Privacy Policy',
          link: '/employer/privacy-policy',
          icon: 'assets/images/sidenav-ic-7.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Faq',
          link: '/employer/faq',
          icon: 'assets/images/sidenav-ic-8.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        {
          name: 'Terms And Conditions',
          link: '/employer/terms-and-conditions',
          icon: 'assets/images/sidenav-emp-ic-2.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },              
        {
          name: 'Template',
          link: '/employer/job/job-templates',
          icon: 'assets/images/sidenav-emp-ic-8.png',
          children: [],
          click: '',
          hover: '',
          state: 'close'
        },
        // {
        //   name: 'Sign Out',
        //   link: '',
        //   icon: 'assets/images/sidenav-emp-ic-9.png',
        //   children: [],
        //   click: 'logOut',
        //   hover: '',
        //   state: 'close'
        // },
      ]
    };
    if(this.userData.userType =="CANDIDATE") {
    this.getPushNotificatio();

    }
  }

  sliderOption(event: MatSlideToggleChange) {
   let value = JSON.parse(localStorage.getItem('currentUser')); 
    // console.log candidateId
    this.checked = event.checked;
    let payload;
    if(this.checked){
      payload = {
        candidateId: value.candidateId,
        isPushNotificationStatus: "true"
      }
  
    }
    else {
      payload = {
        candidateId: value.candidateId,
        isPushNotificationStatus: "false"
      }
    }

    this.globalactionservice.setPushnotificationStatus(payload).pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result =>{
      console.log(result);
    })
    console.log('checked',this.checked);

  }

  getPushNotificatio() {
   let value = JSON.parse(localStorage.getItem('currentUser')); 
   this.globalactionservice.getPushnotificationStatus({candidateId:value.candidateId}).pipe(takeUntil(this.onDestroyUnSubscribe))
   .subscribe(result =>{
     this.checked = result['details'][0].isPushNotificationStatus;
     console.log(result);
   })

  }

  sideNavItemClick(event, functionName) {
    // console.log('okkk');
    if (functionName === 'logOut') {
      this.logOut();
    }
  }
  logOut() {
    this.authenticationService.logOut();
    localStorage.removeItem('donotdelete');
    localStorage.removeItem('JobDetails');
    localStorage.removeItem('uploadedFile');
    localStorage.removeItem('Payment');
    localStorage.removeItem('JobSkills');
    localStorage.removeItem('JobLocation');
    localStorage.removeItem('Datetime');
    localStorage.removeItem('SaveTimeJobPost');
    localStorage.removeItem('allJobs');
    localStorage.removeItem('personalData');
    // localStorage.removeItem('currentUser');
    // this.router.navigate(['/login']);
  }
  expandClose(event, navItem) {
    navItem.state = (navItem.state === 'open') ? 'close' : 'open';
  }
}
