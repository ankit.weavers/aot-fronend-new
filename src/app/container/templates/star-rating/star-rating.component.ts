import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'aot-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {

  // public rateValue: any = '0';
  @Input('rateValue') rateValue: string;
  @Output() rateValueOutput = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  rateValueChanged($event) {
    this.rateValueOutput.emit($event.target.value);
  }
}
