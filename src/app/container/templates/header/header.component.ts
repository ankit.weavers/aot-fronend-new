import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, UrlTree, UrlSegmentGroup, PRIMARY_OUTLET, UrlSegment,NavigationEnd,NavigationStart } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { CustomPopupService } from '../../../services';
import { Event as NavigationEvent } from "@angular/router";
import { filter } from 'rxjs/operators';
@Component({
  selector: 'aot-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public newPageTitle: string = "Seleckt";
  public showHeader: boolean = true;
  public hasBackBtn: boolean = false;
  public hasHamburger: boolean = false;
  public isEditable: boolean = false;
  public currentUrl: any;
  public userData: any;
  @ViewChild('mainNavSidenav') mainNavSidenav: TemplateRef<any>;
  donotdelete: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private location: Location,
    public customPopupService: CustomPopupService
  ) {

    this.userData = JSON.parse(localStorage.getItem('currentUser'));

    // url segment
    const urlTree: UrlTree = router.parseUrl(this.router.url);
    const urlSGroup: UrlSegmentGroup = urlTree.root.children[PRIMARY_OUTLET];
    const urlSegment: UrlSegment[] = urlSGroup.segments;
    // url segment

    // check if the user role is same as the url hit so as to lock change in role in url
    if (this.userData && this.userData.userType.toLowerCase() !== urlSegment[0].path) {
      // console.log(" no redirection , cannot switch url  ", this.router.url);
      this.router.navigate([`/${this.userData.userType.toLowerCase()}`]);
    }

    // router.events
		// 	.pipe(
		// 		// The "events" stream contains all the navigation events. For this demo,
		// 		// though, we only care about the NavigationStart event as it contains
		// 		// information about what initiated the navigation sequence.
		// 		filter(
		// 			( event: NavigationEvent ) => {
 
		// 				return( event instanceof NavigationStart );
 
		// 			}
		// 		)
		// 	)
		// 	.subscribe(
		// 		( event: NavigationStart ) => {
 
		// 			console.group( "NavigationStart Event" );
		// 			// Every navigation sequence is given a unique ID. Even "popstate"
		// 			// navigations are really just "roll forward" navigations that get
    //       // a new, unique ID.
    //       console.log("event",event);
		// 			console.log( "navigation id:", event.id );
		// 			console.log( "route:", event.url );
		// 			// The "navigationTrigger" will be one of:
		// 			// --
		// 			// - imperative (ie, user clicked a link).
		// 			// - popstate (ie, browser controlled change such as Back button).
		// 			// - hashchange
		// 			// --
		// 			// NOTE: I am not sure what triggers the "hashchange" type.
		// 			console.log( "trigger:", event.navigationTrigger );
 
		// 			// This "restoredState" property is defined when the navigation
		// 			// event is triggered by a "popstate" event (ex, back / forward
		// 			// buttons). It will contain the ID of the earlier navigation event
		// 			// to which the browser is returning.
		// 			// --
		// 			// CAUTION: This ID may not be part of the current page rendering.
		// 			// This value is pulled out of the browser; and, may exist across
		// 			// page refreshes.
		// 			if ( event.restoredState ) {
 
		// 				console.warn(
		// 					"restoring navigation id:",
		// 					event.restoredState.navigationId
		// 				);
 
		// 			}
 
		// 			console.groupEnd();
 
		// 		}
		// 	)
		// ;

    router.events.subscribe(val => {
					// console.log( "route:", val.url );
      // Get loggedin User data
      if(val instanceof NavigationEnd) {
        this.userData = JSON.parse(localStorage.getItem('currentUser'));
        // Close Side Nav
        this.closeCustomSidenav('mainNavSidenav');
        // Fetch and Show Title of the Page
        if (this.route.snapshot.children[0].data.title) {
          // console.log('f');
          // console.log('f', this.route.snapshot.children[0]);
          this.setTitles(this.route.snapshot.children[0]);
        } else if (this.route.snapshot.children[0].children[0].data.title) {
          // console.log('s');
          // console.log('s', this.route.snapshot.children[0].children[0]);
          this.setTitles(this.route.snapshot.children[0].children[0]);
        } else {
          // console.log('t');
          // console.log('t', this.route.snapshot.children[0].children[0].children[0]);
          this.setTitles(this.route.snapshot.children[0].children[0].children[0]);
        }
  
        // Check if localstorage has JOB related data and navigated url differs from 'JOB' then remove it
        // if (this.route.snapshot.children[0].url.toString() !== 'payments') {
        //   if (localStorage.getItem('filterData')) {
        //     localStorage.removeItem('filterData');
        //   }
        // }
      }
 
    });

  }

  ngOnInit() {
  }

  public setTitles(newTitle: any) {
    console.log( "newTitle", newTitle.data.title);
    this.donotdelete = localStorage.getItem('donotdelete');

    // if(newTitle.data.title == 'Job Summary') {
    //   window.onpopstate = function (e) { 
    //     // window.history.forward(); 
    //     let r = confirm("Are you want to discard your change?")
    //     console.log('r',r);
    //     if(r == true) {
    //     window.history.forward(); 

    //       return false;
    //     }
    //     else {
    //      this.location.back()
    //     }
    //   }
    // }
    //   if(newTitle.data.title == 'Job Edit') {
    //     window.onpopstate = function (e) { 
    //       window.history.forward(); 
    //     }
    // }
    this.currentUrl = newTitle;
    this.titleService.setTitle(newTitle.data.title);
    this.newPageTitle = newTitle.data.title;
    this.showHeader = (newTitle.routeConfig.path === 'get-started') ? false : true;
    this.hasBackBtn = newTitle.data.hasBackBtn;
    this.hasHamburger = newTitle.data.hasHamburger;
    this.isEditable = newTitle.data.isEditable;
  }

  public gotoEditPage() {
    // console.log(this.currentUrl.data.isEditable);
    this.router.navigate([`/${this.userData.userType.toLowerCase()}/${this.currentUrl.data.isEditable[0]}`]);
  }
  // Sidenav Open
  openCustomSidenav(id: string) {
    this.customPopupService.open(id);
  }
  // Sidenav Close
  closeCustomSidenav(id: string) {
    this.customPopupService.close(id);
  }

}
