import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'aot-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  constructor(
  	private router: Router,
    private route: ActivatedRoute,
    private titleService: Title
  ) { }

  ngOnInit() {
  	this.route.url.subscribe(() => {
      console.log(this.route.snapshot);
      if (this.route.snapshot.children[0].data.title) {
        this.setTitle(this.route.snapshot.children[0].data.title);
      } else {
        this.setTitle(this.route.snapshot.children[0].children[0].data.title);
      }
    });
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}
