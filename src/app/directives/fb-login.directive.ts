import { Directive, ElementRef, Output, EventEmitter, HostListener, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// RxJs
import { Observable, of, fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

// Models
import { SocialLogin } from '../models';
import { AuthenticationService } from '../services';

@Directive({
  selector: '[aotFbLogin]'
})
export class FbLoginDirective implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  private user: SocialUser;
  private loggedIn: boolean;
  @Output()
  public fbLoginInitialized = new EventEmitter();
  constructor(
    private _elementRef: ElementRef,
    private activatedRoute: ActivatedRoute,
    public router: Router,
    public snackBar: MatSnackBar,
    private authService: AuthService,
    private authenticationService: AuthenticationService
  ) { }

  @HostListener('click', ['$event.target'])
  public onClick(targetElement) {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(fbData => {
      const fbDataFormatted = {};
      fbDataFormatted['signedUpVia'] = 'facebook';
      fbDataFormatted['signedUpObj'] = fbData;
      this.authenticationService.socialUserLogin(fbDataFormatted as SocialLogin)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        userData => {
        
          if (userData['isError']) {
            this.fbLoginInitialized.emit(fbDataFormatted);
          } else {
            localStorage.setItem('currentUser', JSON.stringify(userData['details']));

            const usrType = (userData['details'].userType).toLowerCase();
            this.router.navigate([`/${usrType}/dashboard`]);
          
            this.snackBar.open(userData['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection and try again!', 'Ok', {
          
          });
        }
      );
    });
  }

  ngOnInit() {
    this.authService.authState
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      (user) => {
        this.user = user;
        this.loggedIn = (user != null);
      }
    );
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }
}
