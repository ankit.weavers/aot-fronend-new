import { FormControl, FormGroup, NgForm, FormGroupDirective } from '@angular/forms';
import { AbstractControl, ValidatorFn } from '@angular/forms';

export function EmailOrPhoneValidator(controlName: string, matchingControlName: string) {
  // static checkEmailOrPhone(controlName: string, matchingControlName: string) {
    // console.log(controlName, matchingControlName);
    // return null;
  // }
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.notEqual) {
        // return if another validator has already found an error on the matchingControl
        return;
    }

    // set error on matchingControl if validation fails
    if ((control.value).toLowerCase() !== (matchingControl.value).toLowerCase()) {
        matchingControl.setErrors({ notEqual: true });
    } else {
        matchingControl.setErrors(null);
    }
  };
}
