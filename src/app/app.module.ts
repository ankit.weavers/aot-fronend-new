import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
// Interceptors
import { httpInterceptorProviders } from './interceptors';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
// AGM(Angular Map)
import { AgmCoreModule } from '@agm/core';



// Bootstrap
import {
  ModalModule,
  TooltipModule ,
  BsDatepickerModule ,
  TimepickerModule,
  PaginationModule
} from 'ngx-bootstrap';


import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


// import { SharedModule } from './modules/shared.module';
import { CoreModule } from './modules/core.module';

import { AppComponent } from './app.component';

// // Projects Import
// import { CandidateMainModule } from '../../projects/candidate/src/app/app.module';
// import { EmployerMainModule } from '../../projects/employer/src/app/app.module';

// firebase imports
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { MessagingService } from "./services/messaging.service";
import { TermsAndConditionsCandidateComponent } from './views/terms-and-conditions-candidate/terms-and-conditions-candidate.component';
import { TermsAndConditionsEmployerComponent } from './views/terms-and-conditions-employer/terms-and-conditions-employer.component';
import { AdminAuthLoginComponent } from './views/auth/admin-auth-login/admin-auth-login.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';




@NgModule({
  declarations: [
    AppComponent,
    TermsAndConditionsCandidateComponent,
    TermsAndConditionsEmployerComponent,
    AdminAuthLoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // SharedModule,
    CoreModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PaginationModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    // AGM
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjOIcLbaHp8ixAIs1qlWkZIAu2vHb0uGk',
      libraries: ['places']
    }),
    //firebase
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
    // Projects
    // CandidateMainModule.forRoot(),
    // EmployerMainModule.forRoot()
  ],
  providers: [
    Title,
    // Interceptors
    httpInterceptorProviders,
    MessagingService,
    AsyncPipe,
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
          duration: 5000,
         
      }
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
