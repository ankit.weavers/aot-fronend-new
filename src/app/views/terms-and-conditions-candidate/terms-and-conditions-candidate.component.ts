import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'aot-terms-and-conditions-candidate',
  templateUrl: './terms-and-conditions-candidate.component.html',
  styleUrls: ['./terms-and-conditions-candidate.component.scss']
})
export class TermsAndConditionsCandidateComponent implements OnInit {

  conditions;
  private onDestroyUnSubscribe = new Subject<void>();

  constructor(
    private globalActionsService: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.getTermsAndConditions();
  }

  getTermsAndConditions() {
    this.globalActionsService.getTermsConditions({ userType: 'CANDIDATE' })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(typeof result === "object" && (result !== null) && (!result['isError'] )){
            if(result['details'] !== null){
              this.conditions =  result['details'].termsCondition[0].conditionDetails;
            }
          }
        });
  }

}



