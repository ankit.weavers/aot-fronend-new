import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessagingService } from "../../services/messaging.service";
@Component({
  selector: 'aot-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  private userData: any;
  constructor(
    private router: Router,
    private messagingService: MessagingService) {
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    if (this.userData) {
      let userData = this.userData;
      let userTypeId = userData[`${userData.userType.toLowerCase()}Id`];
      this.messagingService.requestPermission(userTypeId, userData.userType.toUpperCase())
      this.messagingService.receiveMessage()
      setTimeout(() => {
        this.router.navigate([`/${this.userData.userType.toLowerCase()}/dashboard`]);
      }, 500);
    } else {
      this.router.navigate(['/login']);
    }
  }

}