import { Component, OnInit, TemplateRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, } from '@angular/router';

// RxJs
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../../services';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'aot-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss'],
})
export class RedirectComponent implements OnInit, OnDestroy {

  @ViewChild('resendLink') resendLink: TemplateRef<any>;
  private onDestroyUnSubscribe = new Subject<void>();
  private code: string;
  public userType: string = 'CANDIDATE';
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private authenticationService: AuthenticationService,
    public snackBar: MatSnackBar,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.code = params['code'];
      const state = params['state'];
    });
  }

  ngOnInit() {
    this.code ? this.getLinkedInAccessToken() : null;
  }

  ngOnDestroy() {
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  getLinkedInAccessToken() {
    let code = { code: this.code, signedUpVia: 'linkedin' }
    this.authenticationService.getLinkedInAccessToken(code, this.userType)
      .subscribe(
        result => {
          if (!result['isError']) {
            let userData = result['details'];
            userData.token  = result['token'];
            localStorage.setItem('currentUser', JSON.stringify(userData));
            const usrType = (result['details'].userType).toLowerCase();

            this.router.navigate([`/candidate/dashboard`]);
            this.snackBar.open(result['message'], 'Got it!', {
          
            });
          } else {
            if (!result['isVerified'] && result['statuscode'] !== 1009) {
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
}
