import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  // Country,
  // UsernameValidator,
  PasswordValidator,
  EmailOrPhoneValidator
  // ParentErrorStateMatcher,
  // PhoneValidator
} from '../../../../validators';
import { AuthenticationService, GlobalActionsService } from '../../../../services';
import { User, SocialLogin, Erros } from '../../../../models';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'aot-employer',
  templateUrl: './employer.component.html',
  styleUrls: ['./employer.component.scss']
})
export class EmployerComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

  public allCountries: any[] = [];
  public allCities: any[] = [];

  public signupForm = this.formBuilder.group({
    emailOrPhone:  ['', [
      Validators.required,
      // Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]{2,3}$|^[1-9]+[0-9]{9}$')
      // Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^[1-9-\+-]+[0-9]{9,11}$/)
      Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^0\d{10}$/)
      
    ]],
    confirmEmailOrPhone:  ['',Validators.required],
    // emailOrMobile: new FormGroup({
    //   email: new FormControl(null, [
    //     Validators.required,
    //     Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$|^[1-9]+[0-9]{9}$')
    //   ]),
    //   confirmEmail: new FormControl(null, [Validators.required, Validators.pattern('^[1-9]+[0-9]*$')]),
    // }),
    password:  ['', [
        Validators.required,
        Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
      ]
    ],
    confirmPassword:  ['', Validators.required],
    countryCode:  ['', Validators.required],
    empType:  ['', Validators.required],
    // signedUpVia:  ['', Validators.required],
    agreeTerms: [false, Validators.requiredTrue],
  }, {validators: [
      PasswordValidator('password', 'confirmPassword'),
      EmailOrPhoneValidator('emailOrPhone', 'confirmEmailOrPhone')
     ]}
  );
  public userType: string = 'EMPLOYER';
  private socialDataFormatted: object = {
    signedUpVia: '',
    signedUpObj: {}
  };

  public errors: any = Erros;
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private authenticationService: AuthenticationService,
    private globalActionsService: GlobalActionsService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  checksignedUpVia(email: any) {

     const regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
     const regexMobile = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
     if (regexEmail.test(email)) {
        return 'email';
     }
     if (regexMobile.test(email)) {
      return 'phone';
     }
  }

  ngOnInit() {
    this.getAllCountries();
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  signup() {
    console.log(this.signupForm);
    const signedUpVia = this.checksignedUpVia(this.signupForm.value.emailOrPhone);
    console.log('....after', signedUpVia);
    this.signupForm.value['signedUpVia'] = signedUpVia ? signedUpVia.toString() : '';
    this.signupForm.value['email'] = this.signupForm.value.emailOrPhone ? this.signupForm.value.emailOrPhone.toString() : '';
    // console.log('signedUpVia************', signedUpVia);
    // console.log('==================', this.signupForm.value);
    if (this.signupForm.valid) {

      let signUpData = this.signupForm.value
      this.signupForm.reset();
      
      this.authenticationService.signup(signUpData as User, this.userType)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result------Employer----sign up', result);
          if (result && !result['isError']) {
            console.log('s', result);
            // localStorage.setItem('currentUser', JSON.stringify(result['details']));
            this.router.navigate(['/login']);
            // this.errorLogin = false;
            // let messageSuccess =  'Congratulations ! You have sucessfully registered on our platform and a member of staff will be in touch shortly. ';
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {
      
      });
    }
  }


  socialSignup(event) {
    // socialUserSignUp
    if (this.userType) {
      this.socialDataFormatted = event;
      console.log(this.socialDataFormatted);
      this.authenticationService.socialUserSignUp(this.socialDataFormatted as SocialLogin, this.userType)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result -------------------', result)
          if (!result['isError']) {
            localStorage.setItem('currentUser', JSON.stringify(result['details']));
            const usrType = (result['details'].userType).toLowerCase();
            this.router.navigate([`/${usrType}/dashboard`]);
            // this.errorLogin = false;
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
          
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
          // console.log(error);
        }
      );
    } else {
      this.snackBar.open('Please select User Type.', 'Got it!', {
    
      });
    }
  }

  getAllCountries() {
    this.globalActionsService.getCountry()
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && !result['isError']) {
          //   this.router.navigate(['/edit']);
          // this.errorLogin = false;
          this.allCountries = result['details'];
          console.log(this.allCountries);
        } else {
          // this.errorLogin = true;
          console.log('e', result);
          this.snackBar.open(result['message'], 'Got it!', {
           
          });
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
        });
      }
    );
  }
  getAllCities(countryId) {
    console.log(countryId.value);
    this.globalActionsService.getCity({country_id: countryId.value})
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && !result['isError']) {
          //   this.router.navigate(['/edit']);
          // this.errorLogin = false;
          this.allCities = result['details'];
          console.log(this.allCities);
        } else {
          this.snackBar.open(result['message'], 'Got it!', {
          
          });
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
    );
  }
}
