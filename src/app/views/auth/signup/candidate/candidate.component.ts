import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  // Country,
  // UsernameValidator,
  PasswordValidator,
  EmailOrPhoneValidator
  // ParentErrorStateMatcher,
  // PhoneValidator
} from '../../../../validators';
import { AuthenticationService } from '../../../../services';
import { User, SocialLogin, Erros } from '../../../../models';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'aot-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.scss']
})
export class CandidateComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  public signupForm: FormGroup;
  // public parentErrorStateMatcher = new ParentErrorStateMatcher();
  public userType: string = 'CANDIDATE';
  private socialDataFormatted: object = {
    signedUpVia: '',
    signedUpObj: {}
  };
  public errors: any = Erros;

  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      fname: ['', Validators.required],
      mname: [''],
      lname: ['', Validators.required],
      // email: ['', [Validators.required, Validators.email]],
      emailOrPhone: ['', [
        Validators.required,
        // Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^[1-9-\+-]+[0-9]{9,11}$/)
      Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^0\d{10}$/)
        
      ]],
      password: ['', [
        // Validators.minLength(8),
        Validators.required,
        Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
      ]
      ],
      confirmPassword: ['', Validators.required],
      // matchingPasswords: this.matchingPasswordsGroup,
      agreeTerms: [false, Validators.requiredTrue],
    }, { validator: PasswordValidator('password', 'confirmPassword') });
    console.log(this.signupForm);
  }

  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  signup() {
    this.signupForm.value['signedUpVia'] = 'email';
    console.log(this.signupForm);
    // if (this.signupForm.valid && (this.signupForm.value.password === this.signupForm.value.confirmPassword)) {
    if (this.signupForm.valid) {

      let signUpData = this.signupForm.value
      this.signupForm.reset();

      this.authenticationService.signup(signUpData as User, this.userType)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            console.log('result----------sign up', result);
            if (result && !result['isError']) {
              console.log('s', result);
              // localStorage.setItem('currentUser', JSON.stringify(result['details']));
              this.router.navigate(['/login']);
              // this.errorLogin = false;
              this.snackBar.open(result['message'], 'Got it!', {
             
              });
            } else {
              // this.errorLogin = true;
              console.log('e', result);
              this.snackBar.open(result['message'], 'Got it!', {
             
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              
            });
          }
        );
    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {
      
      });
    }
  }

  socialSignup(event) {
    // socialUserSignUp
    if (this.userType) {
      this.socialDataFormatted = event;
      console.log(this.socialDataFormatted);
      this.authenticationService.socialUserSignUp(this.socialDataFormatted as SocialLogin, this.userType)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            console.log('result -------------------', result)
            if (!result['isError']) {
              localStorage.setItem('currentUser', JSON.stringify(result['details']));
              const usrType = (result['details'].userType).toLowerCase();
              this.router.navigate([`/${usrType}/dashboard`]);
              // this.errorLogin = false;
              this.snackBar.open(result['message'], 'Got it!', {
             
              });
            } else {
              // this.errorLogin = true;
              console.log('e', result);
              this.snackBar.open(result['message'], 'Got it!', {
              
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
            });
            // console.log(error);
          }
        );
    } else {
      this.snackBar.open('Please select User Type.', 'Got it!', {
     
      });
    }
  }

}
