import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  // Country,
  // UsernameValidator,
  PasswordValidator,
  // ParentErrorStateMatcher,
  // PhoneValidator
} from '../../../validators'; // Models
// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../../services';
import { User, SocialLogin, Erros } from '../../../models';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'aot-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  public resetPasswordForm: FormGroup;
  public resetPasswordToken: string;
  public errors: any = Erros;
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', [
          Validators.required,
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
        ]
      ],
      confirmPassword: ['', Validators.required],
    }, {validator: PasswordValidator('password', 'confirmPassword')});
    // Token param value
    this.activatedRoute.params.subscribe( params =>
      this.resetPasswordToken = params.token
    );
    console.log(this.resetPasswordToken);
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  onSubmitResetPasswordForm() {
    console.log(this.resetPasswordForm);
    if (this.resetPasswordForm.valid) {
      // this.authenticationService.resetPassword
      this.resetPasswordForm.value['tocken'] = this.resetPasswordToken;
      console.log(this.resetPasswordForm.value);

      let resetPasswordData = this.resetPasswordForm.value
      this.resetPasswordForm.reset();

      this.authenticationService.resetPassword(resetPasswordData as User)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result------Employer----sign up', result);
          if (result && !result['isError']) {
            console.log('s', result);
            this.router.navigate(['/login']);
            // this.errorLogin = false;
            const messageSuccess =  'Congratulations ! You have sucessfully changed your password. Please login with Your new password.';
            this.snackBar.open(messageSuccess, 'Got it!', {
             
            });

          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
    } else {
      this.snackBar.open('Please fill in all the required fields', 'Got it!', {
      
      });
    }
  }

}
