import { Component, OnInit, TemplateRef, OnDestroy, ViewChild,Output,EventEmitter } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
// RxJs
import { Observable, Subject, from } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';

import { AuthenticationService } from '../../../services/'; // Models
// import { User, SocialLogin, Erros, Cities, States } from '../../../models';
import { User, SocialLogin, Erros } from '../../../models';
import { MatSnackBar } from '@angular/material';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedInLoginProvider
} from 'angularx-social-login';



@Component({
  selector: 'aot-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  @ViewChild('resendLink') resendLink: TemplateRef<any>;
  private onDestroyUnSubscribe = new Subject<void>();
  private userEmail: any;
  private user: SocialUser;
  private loggedIn: boolean;
  private code: string;

  private socialDataFormatted: object = {
    signedUpVia: '',
    signedUpObj: {}
  };
  
  public userType: string = 'CANDIDATE';
  modalRef: BsModalRef;
  public loginForm = this.formBuilder.group({
    emailOrPhone: ['', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^\d{10,11}$/)
    ]],
    password: ['', Validators.required],
  });
  public notificationTest: string = '';
  public errors: any = Erros;
  // public cities: any = Cities;
  // public states: any = States;
  public pCities: any;

  @Output()
  public fbLoginInitialized = new EventEmitter();
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    private authService: AuthService,

  ) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.code = params['code'];
      const state = params['state'];
    });
  }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
    this.code ? this.getLinkedInAccessToken() : null;

  }

  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  openModal(template: TemplateRef<any>, classname) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: classname })
    );
  }

  togglTwoModal(selector: TemplateRef<any>, classname) {
    this.modalRef.hide();
    // setTimeout(() => {
    // this.modalRef = this.modalService.show(selector);
    this.openModal(selector, classname);
    // }, 500);
  }

  // signInWithGoogle(): void {
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(gpData => {
  //     console.log(gpData);
  //   });
  // }
  // signInWithFB(): void {
  //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(fbData => {
  //     console.log(fbData);
  //   });
  // }
  // signInWithLinkedIn(): void {
  //   this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID).then(lnData => {
  //     console.log(lnData);
  //   });
  // }
  // signOut(): void {
  //   this.authService.signOut().then(signoutData => {
  //     console.log(signoutData);
  //   });;
  // }

  login() {
    if (this.loginForm.valid) {

      let loginData = this.loginForm.value
      this.loginForm.reset();
    
      this.authenticationService.login(loginData as User)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (!result['isError']) {
              

              localStorage.setItem('currentUser', JSON.stringify(result['details']));
              // console.warn('userType==='+JSON.parse(localStorage.getItem('currentUser')).userType)
              // console.warn('employerType==='+JSON.parse(localStorage.getItem('currentUser')).employerType)
              // SETUP FOR THE FCM MESSAGEING
              let usrType = (result['details'].userType).toLowerCase();
              if(usrType=='employer'){
                this.router.navigate(['/employer/job/post'])
                     this.snackBar.open(result['message'], 'Got it!', {
                    
                  });
                     return false;
              }else{
                if(usrType=='candidate') {
                  this.router.navigate([`/${usrType}/job/search`]);
                  this.snackBar.open(result['message'], 'Got it!', {
                   
                  });
                }
           
            }

            } else {
              if (!result['isVerified'] &&  result['statuscode'] !== 1009) {
                this.modalRef = this.modalService.show(this.resendLink);
                console.log("go",result['isVerified']);
          
              } else {

                this.snackBar.open(result['message'], 'Got it!', {
           
                });
              }
            }
            
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
            });
          }
        );

    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {
   
      });
    }
  }

  forgotPasswordSubmit(forGotPasswordEmail, notificationModal) {

    const emailValidate = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$/;
    if (forGotPasswordEmail.value.match(emailValidate)) {
     
      const verificationData = {
        email: forGotPasswordEmail.value
      };
      this.authenticationService.sendVerificationLink(verificationData as User)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (!result['isError']) {
              this.notificationTest = 'Please check your registered email to reset your password.';
              this.togglTwoModal(notificationModal, 'modal-sm');
              this.userEmail = '';
            } else {
              this.notificationTest = 'User not found. Please try again with another email.';
              this.userEmail = '';
              this.togglTwoModal(notificationModal, 'modal-sm');
            }
          },
          error => {
            this.modalRef.hide();
            this.userEmail = '';
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
        
          }
        );
    } else {
      forGotPasswordEmail.value = '';
      
      this.snackBar.open('Please give a proper email.', 'Got it!', {
      
      });
    }
  }

  openSelectUserPopupLinkedIn($event, selectUserTypeLinkedIn, modalClass) {
    this.openModal(selectUserTypeLinkedIn, modalClass);
    this.socialDataFormatted = $event;
  }

  // Open Resend Link Modal
  openResendLinkModal(resendLink: TemplateRef<any>) {
    this.modalRef = this.modalService.show(resendLink);
  }
  getLinkedInAccessToken() {
    let code = { code: this.code, signedUpVia: 'linkedin' }
    this.authenticationService.getLinkedInAccessToken(code, this.userType)
      // .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          

          if (!result['isError']) {
            localStorage.setItem('currentUser', JSON.stringify(result['details']));
            const usrType = (result['details'].userType).toLowerCase();
           
            this.router.navigate([`/candidate/dashboard`]);
            this.snackBar.open(result['message'], 'Got it!', {
            
            });

          } else {
            if (!result['isVerified'] && result['statuscode'] !== 1009) {
              this.modalRef = this.modalService.show(this.resendLink);
            } else {

              this.snackBar.open(result['message'], 'Got it!', {
              
              });
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
          
        }
      );

  }
  openSelectUserPopup($event, userTypeModal, modalClass) {
    this.openModal(userTypeModal, modalClass);
    this.socialDataFormatted = $event;
  }
  socialSignUp(userTypeSelect) {
    this.modalRef.hide();
    console.log('user type',this.userType);
    if (this.userType) {
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(fbData => {
          const fbDataFormatted = {};
          fbDataFormatted['signedUpVia'] = 'facebook';
          fbDataFormatted['signedUpObj'] = fbData;
          this.authenticationService.socialUserSignUp(fbDataFormatted as SocialLogin,this.userType)
          .pipe(takeUntil(this.onDestroyUnSubscribe))
          .subscribe(
            result => {
            
              if (result['isError']) {
                console.log('isrror true')
                this.fbLoginInitialized.emit(fbDataFormatted);
              } else {
                let details = result['details'];
                details.token = result['token'];
                console.log('details',details)
                localStorage.setItem('currentUser', JSON.stringify(details));
                // SETUP FOR THE FCM MESSAGEING
                let usrType = (result['details'].userType).toLowerCase();
                if(usrType=='employer'){
                  this.router.navigate(['/employer/job/post'])
                       this.snackBar.open(result['message'], 'Got it!', {
                       
                    });
                       return false;
                }else{
                  if(usrType=='candidate') {
                    this.router.navigate([`/${usrType}/job/search`]);
                    this.snackBar.open(result['message'], 'Got it!', {
                    
                    });
                  }
              }
              }
            },
            error => {
              this.snackBar.open('Please Check Your Network Connection and try again!', 'Ok', {
              
              });
            }
          );
        });








    } else {
      this.snackBar.open('Please select User Type.', 'Got it!', {
       
      });
    }
  }



}


