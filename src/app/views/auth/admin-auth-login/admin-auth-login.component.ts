import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User, SocialLogin, Erros } from '../../../models';
import { AuthenticationService } from '../../../services/'; // Models
import { Observable, Subject, from } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MatSnackBar } from '@angular/material';







@Component({
  selector: 'aot-admin-auth-login',
  templateUrl: './admin-auth-login.component.html',
  styleUrls: ['./admin-auth-login.component.scss']
})
export class AdminAuthLoginComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

 public loginForm : FormGroup;
 public errors: any = Erros;

  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    public snackBar: MatSnackBar,



  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      emailOrPhone: ['', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^0\d{10}$/)
      ]],
      password: ['', Validators.required],
      emailPhone: ['', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]{2,3}$|^0\d{10}$/)
      ]],
    });


  }
  login() {
    let payload ={
      EmailId :this.loginForm.value.emailOrPhone,
      password : this.loginForm.value.password,
      employerEmailId : this.loginForm.value.emailPhone
    }
    this.authenticationService.adminAccess(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result=>{
      if(!result['isError']) {
        let value = 'value'
        localStorage.setItem('currentUser', JSON.stringify(result['details']));
        localStorage.setItem('adminAccValue',value);
                this.router.navigate(['/employer/dashboard/job-posted'])
                     this.snackBar.open('Login Successful', 'Got it!', {
                  
                  });

      }
      else {
        this.snackBar.open(result['message'], 'Got it!', {
        
        });
      }

    },
    error => {
      this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
      });
    }
    
    );
  }

}
