import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services';
import { User, Erros } from '../../../models';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'aot-re-verify',
  templateUrl: './re-verify.component.html',
  styleUrls: ['./re-verify.component.scss']
})
export class ReVerifyComponent implements OnInit {
  optionType = "";
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  phoneFormControl = new FormControl('', [Validators.required, Validators.pattern(/^[1-9-\+-]+[0-9]{9,11}$/)]);
  private onDestroyUnSubscribe = new Subject<void>();
  public errors: any = Erros;
  constructor(
    public router: Router,
    private authenticationService: AuthenticationService,
    public snackBar: MatSnackBar,    
  ) {
    
   }

  ngOnInit() {
  }

  selectOption(optionType) {
    this.optionType = optionType;
    if (this.optionType === "mobile") {
      this.emailFormControl.reset();
    }
    if (this.optionType === "email") {
      this.phoneFormControl.reset();
    }
  }

  reVerify() {

    let verificationData: Object;

    if (this.optionType === "email" && this.emailFormControl.value) {

      verificationData = {
        email: this.emailFormControl.value
      };
      this.emailFormControl.reset();

      this.authenticationService.resendLink(verificationData as User)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (!result['isError']) {
              this.router.navigate(['/login']);
              this.snackBar.open('Please check your registered email to verify your account', 'Got it!', {
               
              });
            } else {
              this.snackBar.open('User not found. Please try again with another email', 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              
            });
          }
        );
    }

    if (this.optionType === "mobile" && this.phoneFormControl.value) {
      verificationData = {
        phone: this.phoneFormControl.value
      };
      this.phoneFormControl.reset();
    }
  }
}
