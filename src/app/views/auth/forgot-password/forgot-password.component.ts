import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services';
import { User, Erros } from '../../../models';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'aot-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  optionType = "";
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  phoneFormControl = new FormControl('', [Validators.required, Validators.pattern(/^[1-9-\+-]+[0-9]{9,11}$/)]);
  private onDestroyUnSubscribe = new Subject<void>();
  public errors: any = Erros;

  constructor(
    private authenticationService: AuthenticationService,
    public snackBar: MatSnackBar,

  ) {

  }

  ngOnInit() { }

  selectOption(optionType) {
    this.optionType = optionType;
    if (this.optionType === "mobile") {
      this.emailFormControl.reset();
    }
    if (this.optionType === "email") {
      this.phoneFormControl.reset();
    }
  }

  doForgotPassword() {

    let verificationData: Object;

    if (this.optionType === "email" && this.emailFormControl.value) {

      verificationData = {
        email: this.emailFormControl.value
      };
      this.emailFormControl.reset(); //to restrict more than one submit

      this.authenticationService.sendVerificationLink(verificationData as User)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            // console.log('result', result);
            if (!result['isError']) {
              this.snackBar.open('Please check your registered email to reset your password', 'Got it!', {
                
              });
            } else {
              this.snackBar.open('User not found! Please try again with another email', 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
            // console.log(error);
          }
        );
    }

    if (this.optionType === "mobile" && this.phoneFormControl.value) {
      console.log(' call api value');
      verificationData = {
        phone: this.phoneFormControl.value
      };

      this.phoneFormControl.reset();  //to restrict more than one submit

      console.log(' verificationData', verificationData);
    }

  }

}
