import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'aot-terms-and-conditions-employer',
  templateUrl: './terms-and-conditions-employer.component.html',
  styleUrls: ['./terms-and-conditions-employer.component.scss']
})
export class TermsAndConditionsEmployerComponent implements OnInit {

  conditions;
  private onDestroyUnSubscribe = new Subject<void>();

  constructor(
    private globalActionsService: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.getTermsAndConditions();
  }

  getTermsAndConditions() {
    
    this.globalActionsService.getTermsConditions({ userType: 'EMPLOYER' })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(typeof result === "object" && (result !== null) && (!result['isError'] )){
            if(result['details'] !== null){
              this.conditions =  result['details'].termsCondition[0].conditionDetails;
            }
          }
        });
    }

}



