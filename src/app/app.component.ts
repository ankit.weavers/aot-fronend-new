import { Component } from '@angular/core';

@Component({
  selector: 'aot-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'seleckt';
}
