import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
// Models
import { User, SocialLogin } from '../models';
// Handle error
import { HandleErrorService } from './handle-error.service';
// Server Link
import { environment } from '../../environments/environment';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { url } from 'inspector';
import { Key } from 'protractor';
const bucket = new S3(
  environment.s3.bucket

);

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class GlobalActionsService {

  private profilePicChanged = new BehaviorSubject<any>(null)
  profilePicStatus = this.profilePicChanged.asObservable();

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  // Country
  getCountry() {
    const apiUrl = `${environment.SERVER_URL}/common/get-countries`;
    return this.http.post(apiUrl, '', httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getCountry'))
      );
  }
  // City
  getCity(countryId) {
    const apiUrl = `${environment.SERVER_URL}/common/get-cities`;
    return this.http.post(apiUrl, countryId, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getCity'))
      );
  }
// unique city
getCityNew(countryId) {
  const apiUrl = `${environment.SERVER_URL}/common/get-cities-distinct`;
  return this.http.post(apiUrl, countryId, httpOptions)
    .pipe(
      map(response => response),
      catchError(this.handleErrorService.handleError('getCity'))
    );
}

  // Upload image
  uploadFile(formData, urlParam, folderName, imageType) {

    const filename = `${urlParam.toLowerCase()}/${folderName}/${imageType}_${formData.name.replace(/[^a-z0-9.]/gi, '_').toLowerCase()}`;
    // fetch(filename,
    //   {
    //     mode:'no-cors'
    // })
    const params = {
      Bucket: environment.s3.bucketName,
      Key: filename,
      Body: formData,
      ACL: environment.s3.acl,
      // mode:'no-cors'
      // ContentType: files[i]['type']
    };

    const fileUploadPromise = new Promise((resolve, reject) => {
   
      bucket.upload(params, (err, data) => {
        console.log("params",params);
        
        resolve(data);
        console.log("data",data);
      });
    });
    console.log("fileUploadPromise",fileUploadPromise);

    return fileUploadPromise;
  }

  deleteFile(imageUrl){
    var params = {
      Bucket:  environment.s3.bucketName,
      Key: imageUrl,
    };
    const fileUploadPromise = new Promise((resolve, reject) => {
      bucket.deleteObject(params, (err, data) => {
        if (err) console.log(err)     
        else console.log("Successfully deleted myBucket/myKey");   
        resolve(data);
      });
    });
    return fileUploadPromise;
  }

  //profile pic change detection
  _setProfilePic(profilePic: any) {
    this.profilePicChanged.next(profilePic);
  }

  //Industry
  getIndustry() {
    const apiUrl = `${environment.SERVER_URL}/common/get-industry`;
    return this.http.post(apiUrl, '', httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getIndustry'))
      );
  }

  fetchIndustry(payload) {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/fetch-industry-for-employer`;
    return this.http.post(apiUrl, payload, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('fetchIndustry'))
      );
  }

  // Role
  getRole(industryId) {
    const apiUrl = `${environment.SERVER_URL}/common/get-role`;
    return this.http.post(apiUrl, industryId, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getRole'))
      );
  }
  getSkills(payload) {
    const apiUrl = `${environment.SERVER_URL}/common/get-role`;
    return this.http.post(apiUrl, payload, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getSkills'))
        );
}

  // Skills By Job Role Id
  getJobSkillById(jobRoleIds) {
    const apiUrl = `${environment.SERVER_URL}/admin/get-rolewise-skill`;
    return this.http.post(apiUrl, jobRoleIds, httpOptions).pipe(
      map(response => response),
      catchError(this.handleErrorService.handleError('getJobSkillById'))
    );
  }
//for unique skill
  getJobuniqueSkillById(jobRoleIds) {
    const apiUrl = `${environment.SERVER_URL}/admin/get-distinct-skill-on-role-basis`;
    return this.http.post(apiUrl, jobRoleIds, httpOptions).pipe(
      map(response => response),
      catchError(this.handleErrorService.handleError('getJobuniqueSkillById'))
    );
  }

  fetchSkill(jobRoleIds){
    const apiUrl = `${environment.SERVER_URL}/admin/fetch-rolewise-skill`;
    return this.http.post(apiUrl, jobRoleIds, httpOptions).pipe(
      map(response => response),
      catchError(this.handleErrorService.handleError('getJobSkillById'))
    ); 
  }
  // Skills
  getAllSlots() {
    const apiUrl = `${environment.SERVER_URL}/common/get-all-slots`;
    return this.http.post(apiUrl, '', httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getAllSlots'))
      );
  }

  // Languages
  getLanguage() {
    const apiUrl = `${environment.SERVER_URL}/common/get-language`;
    return this.http.post(apiUrl, '', httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getLanguage'))
      );
  }

  // Terms And Conditions
  getTermsConditions(userType) {
    const apiUrl = `${environment.SERVER_URL}/common/get-condition`;
    return this.http.post(apiUrl, userType, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getTermsConditions'))
      );
  }

  //Get about us 
  getAboutUsDetails(userType) {
    const apiUrl = `${environment.SERVER_URL}/common/get-aboutus`;
    return this.http.post(apiUrl, userType, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getAboutUsDetails'))
      );
  }

   //Get about us 
   getPrivacyPolicyDetails(userType) {
    const apiUrl = `${environment.SERVER_URL}/common/get-policy`;
    return this.http.post(apiUrl, userType, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getPrivacyPolicyDetails'))
      );
  }

   //Get about us 
   getFaqDetails(userType) {
    const apiUrl = `${environment.SERVER_URL}/admin/get-faq`;
    return this.http.post(apiUrl, userType, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getFaqDetails'))
      );
  }

   //Get contact us 
   getContactUsDetails(userType) {
    const apiUrl = `${environment.SERVER_URL}/common/get-contactus`;
    return this.http.post(apiUrl, userType, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getContactUsDetails'))
      );
  }
//set push notification status
  setPushnotificationStatus(payload) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/set-push-status`;
    return this.http.post(apiUrl, payload, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('setPushnotificationStatus'))
      );
  }

  getPushnotificationStatus(payload) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-push-status`;
    return this.http.post(apiUrl, payload, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getPushnotificationStatus'))
      );
  }

}
