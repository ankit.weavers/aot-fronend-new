import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError, Subject, from } from 'rxjs';
// import 'rxjs/add/observable/from';
import { catchError, map, tap } from 'rxjs/operators';
// Models
import { User, SocialLogin } from '../models';
// Handle error
import { HandleErrorService } from './handle-error.service';
// Server Link
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { fromFetch } from 'rxjs/fetch';
// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',

  })
};
const accessTokenHttpHeader = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
  })
};
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public userLoggedIn = new Subject();
  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService,
    private router: Router
  ) { }

  public getToken(): string {
    if (localStorage.currentUser && Object.keys(localStorage.currentUser).length) {
      return JSON.parse(localStorage.getItem('currentUser')).token;
    } else {
      return '';
    }
  }
  // Gather all new Messages
  getUserData(newUserLoggedIn) {
    this.userLoggedIn.next(newUserLoggedIn);
  }
  // Reset Subject
  resetUserData() {
    this.userLoggedIn.next({
      status: 0,
      data: []
    });
  }
  // login
  login(userData: User): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/common/signin`;
    console.log('userData', userData);

    return this.http.post<User>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('login'))
      );
  }
  adminAccess(userData) {
    const apiUrl = `${environment.SERVER_URL}/common/authenticate-admin`;
    console.log('userData', userData);

    return this.http.post(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('adminAccess'))
      );
  }
  // // Signup
  // signup(userData: User): Observable<User> {
  //   const apiUrl = `${environment.SERVER_URL}/candidate/signup`;
  //   return this.http.post<User>(apiUrl, userData, httpOptions)
  //   .pipe(
  //       map(response => response),
  //       catchError(this.handleErrorService.handleError<User>('signup'))
  //   );
  // }

  // Signup
  signup(userData: User, urlParam: string): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/${urlParam.toLowerCase()}/signup`;
    return this.http.post<User>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('signup'))
      );
  }
  // Social login
  socialUserLogin(userData: SocialLogin): Observable<SocialLogin> {
    const apiUrl = `${environment.SERVER_URL}/common/check`;
    console.log('userData', userData);

    return this.http.post<SocialLogin>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<SocialLogin>('sociallogin'))
      );
  }
  // Social Signup
  socialUserSignUp(userData: SocialLogin, urlParam: string): Observable<SocialLogin> {
    const apiUrl = `${environment.SERVER_URL}/${urlParam.toLowerCase()}/signup-via-social`;
    return this.http.post<SocialLogin>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<SocialLogin>('socialsignup'))
      );
  }
  getLinkedInAccessToken(code,userTypeSelect: string) {
    const apiUrl = `${environment.SERVER_URL}/${userTypeSelect.toLowerCase()}/signup-via-social`
    return this.http.post(apiUrl, code, httpOptions)
      .pipe(
        map(response => response),
        catchError(
          catchError(this.handleErrorService.handleError('getLinkedInAccessToken'))
        )
      );
  }
  // Forgot password send link
  sendVerificationLink(userData: User): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/common/send-link`;
    return this.http.post<User>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('sendVerificationLink'))
      );
  }
  // Reset Password
  resetPassword(formData): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/common/change-password`;
    return this.http.post<User>(apiUrl, formData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('resetPassword'))
      );
  }
  // Forgot password send link
  resendLink(userData: User): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/common/resend-link`;
    return this.http.post<User>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('sendVerificationLink'))
      );
  }

  // Logout
  // logOut() {
  //   const apiUrl = `${environment.SERVER_URL}/common/logout`;
  //   const localCurrentUserdata = JSON.parse(localStorage.getItem('currentUser'));
  //   const loggedInUserdata = {
  //     // api_key: localCurrentUserdata.api_key,
  //     userId: localCurrentUserdata.user_id
  //   };
  //   return this.http.post<User>(apiUrl, loggedInUserdata, httpOptions)
  //   .pipe(
  //       map(response => response),
  //       catchError(this.handleErrorService.handleError<User>('logOut'))
  //   );
  // }
  // Check user loggedin
  isUserLoggedIn(): Observable<boolean> {
    // if (localStorage.currentUser && Object.keys(localStorage.currentUser).length) {
    // const localCurrentUserdata = JSON.parse(localStorage.getItem('currentUser'));
    // console.log(localCurrentUserdata);
    const apiUrl = `${environment.SERVER_URL}/common/verify-token`;
    return this.http.post(apiUrl, '')
      .pipe(
        map(response => response['isError']),
        catchError(
          (err) => {
            return of(false);
          }
        )
      );
    // return true;
    // } else {
    //   return false;
    // }
  }

  logOut() {
    let variable = localStorage.getItem('adminAccValue')
    if(variable) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('adminAccValue');
      localStorage.clear();
      this.router.navigate(['/Admin-access-to-emp']);
        console.log('logoutAdmin')

    }
    else {
      localStorage.removeItem('currentUser');
      localStorage.clear();
      this.router.navigate(['/login']);
      console.log('logout')

    }

  }
}
