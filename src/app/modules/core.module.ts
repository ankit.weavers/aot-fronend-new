import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { MatNativeDateModule } from '@angular/material';
// import { MaterialModule } from './material.module';
// import { RouterModule } from '@angular/router';

// import { CarouselModule } from 'ngx-owl-carousel-o';

// import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
// import {
//   GoogleLoginProvider,
//   FacebookLoginProvider,
//   LinkedInLoginProvider,
//   LoginOpt
// } from 'angularx-social-login';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from './shared.module';

import { DefaultComponent, WithNavComponent } from '../container/layouts';
import { HeaderComponent, SidebarComponent } from '../container/templates';

import {
  CandidateComponent,
  EmployerComponent,
  LoginComponent,
  RedirectComponent,
  UserTypeComponent,
  ResetPasswordComponent,
  ForgotPasswordComponent,
  AccountVerifiedComponent,
  LinkInvalidComponent,
  ReVerifyComponent
} from '../views/auth';

import { NotFoundComponent, InternalServerErrorComponent } from '../views/errors';

// const fbLoginOptions: LoginOpt = {
//   // scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
//   scope: 'email',
//   return_scopes: true,
//   enable_profile_selector: true
// }; // https://developers.facebook.com/docs/reference/javascript/FB.login/v2.11
// const googleLoginOptions: LoginOpt = {
//   scope: 'profile email'
// }; // https://developers.google.com/api-client-library/javascript/reference/referencedocs#gapiauth2clientconfig

// const config = new AuthServiceConfig([
//   // {
//   //   id: GoogleLoginProvider.PROVIDER_ID,
//   //   provider: new GoogleLoginProvider('Google-OAuth-Client-Id', googleLoginOptions)
//   // },
//   // {
//   //   id: FacebookLoginProvider.PROVIDER_ID,
//   //   provider: new FacebookLoginProvider('Facebook-App-Id', fbLoginOptions)
//   // },
//   {
//     id: GoogleLoginProvider.PROVIDER_ID,
//     provider: new GoogleLoginProvider('1016994148249-cvijokf575kprvsj6899n1ms5a213uu4.apps.googleusercontent.com')
//   },
//   {
//     id: FacebookLoginProvider.PROVIDER_ID,
//     provider: new FacebookLoginProvider('561602290896109')
//   },
//   {
//     id: LinkedInLoginProvider.PROVIDER_ID,
//     provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
//   }
// ]);

// export function provideConfig() {
//   return config;
// }
@NgModule({
  declarations: [
    // Layouts
    DefaultComponent,
    WithNavComponent,
    // Templates
    HeaderComponent,
    // FooterComponent,
    SidebarComponent,
    // Components
    CandidateComponent,
    EmployerComponent,
    LoginComponent,
    RedirectComponent,
    UserTypeComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    NotFoundComponent,
    InternalServerErrorComponent,
    AccountVerifiedComponent,
    LinkInvalidComponent,
    ReVerifyComponent
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule
    // CommonModule,
    // BrowserModule,
    // FormsModule,
    // ReactiveFormsModule,
    // RouterModule,
    // MatNativeDateModule,
    // MaterialModule,
    // CarouselModule,
    // SocialLoginModule
  ],
  exports: [
    // MatNativeDateModule,
    // MaterialModule,
    // SocialLoginModule,
    // Components
    // DefaultComponent,
    // WithNavComponent,
    // HeaderComponent,
    // SidebarComponent,
    // NotFoundComponent,
    // InternalServerErrorComponent,
    // UserTypeComponent,
    // ResetPasswordComponent,
  ],
  providers: [
    // {
    //   provide: AuthServiceConfig,
    //   useFactory: provideConfig
    // }
  ]
})
export class CoreModule { }
