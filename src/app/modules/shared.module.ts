import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { MatNativeDateModule } from '@angular/material';
import { MaterialModule } from './material.module';

import { CarouselModule } from 'ngx-owl-carousel-o';
// Bootstrap
import { TooltipModule, BsDatepickerModule, TimepickerModule, PaginationModule } from 'ngx-bootstrap';

import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedInLoginProvider,
  LoginOpt
} from 'angularx-social-login';
// NgSelect
import { NgSelectModule } from '@ng-select/ng-select';
// AGM(Angular Map)
import { AgmCoreModule } from '@agm/core';
import {DpDatePickerModule} from 'ng2-date-picker';
import { FbLoginDirective } from '../directives/fb-login.directive';
import { LoadingComponent } from '../views/loading/loading.component';
import { CustomPopupComponent } from '../container/templates';
import { StarRatingComponent } from '../container/templates';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const fbLoginOptions: LoginOpt = {
  // scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
  scope: 'email',
  return_scopes: true,
  enable_profile_selector: true
}; // https://developers.facebook.com/docs/reference/javascript/FB.login/v2.11
const googleLoginOptions: LoginOpt = {
  scope: 'profile email'
}; // https://developers.google.com/api-client-library/javascript/reference/referencedocs#gapiauth2clientconfig

const config = new AuthServiceConfig([
  // {
  //   id: GoogleLoginProvider.PROVIDER_ID,
  //   provider: new GoogleLoginProvider('Google-OAuth-Client-Id', googleLoginOptions)
  // },
  // {
  //   id: FacebookLoginProvider.PROVIDER_ID,
  //   provider: new FacebookLoginProvider('Facebook-App-Id', fbLoginOptions)
  // },
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('1016994148249-cvijokf575kprvsj6899n1ms5a213uu4.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    // provider: new FacebookLoginProvider('2755195954574969')
    provider: new FacebookLoginProvider('2556333507806083')

  },
  {
    id: LinkedInLoginProvider.PROVIDER_ID,
    provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
  }
]);
export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
    // Directives
    FbLoginDirective,
    // Components
    LoadingComponent,
    CustomPopupComponent,
    StarRatingComponent
  ],
  imports: [
    DpDatePickerModule,
    // BrowserAnimationsModule,
    CommonModule,
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatNativeDateModule,
    MaterialModule,
    CarouselModule,
    // Bootstrap
    TooltipModule,
    BsDatepickerModule,
    TimepickerModule,
    PaginationModule,
    SocialLoginModule,
    // NgSelect
    NgSelectModule,
    // AGM
    AgmCoreModule,
    NgbModule
  ],
  exports: [
    DpDatePickerModule,
    NgbModule,
    // Modules
    // BrowserAnimationsModule,
    CommonModule,
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatNativeDateModule,
    MaterialModule,
    CarouselModule,
    // Bootstrap
    TooltipModule,
    BsDatepickerModule,
    TimepickerModule,
    PaginationModule,
    SocialLoginModule,
    // NgSelect
    NgSelectModule,
    // AGM
    AgmCoreModule,
    // Directives
    FbLoginDirective,
    // Components
    LoadingComponent,
    CustomPopupComponent,
    StarRatingComponent
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ]
})
export class SharedModule { }
