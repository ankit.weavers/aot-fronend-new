import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthenticationService } from './../services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private router: Router, private authenticationService: AuthenticationService, private location: Location) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // return true;
    // console.log(this.location.path());
    // console.log(this.authenticationService.isUserLoggedIn());
    // if (this.authenticationService.isUserLoggedIn()) {
    //   // logged in so return true
    //   return true;
    // } else {
    //   localStorage.removeItem('currentUser');
    //   this.router.navigate(['/login']);
    //   return false;
    // }
    return this.authenticationService.isUserLoggedIn().pipe(
      map(
        e => {
          if (!e) {
            // console.log(e);
            return true;
          } else {
            // localStorage.removeItem('currentUser');
            // this.router.navigate(['/login']);
            this.authenticationService.logOut();
            return false;
          }
        }
      ),
      catchError(
        (err) => {
          // localStorage.removeItem('currentUser');
          // this.router.navigate(['/login']);
          // return of(false);
          return of(true);
        }
      )
    );
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    // return true;
    return this.authenticationService.isUserLoggedIn().pipe(
      map(
        e => {
          if (!e) {
            return true;
          } else {
            // localStorage.removeItem('currentUser');
            // this.router.navigate(['/login']);
            this.authenticationService.logOut();
            console.log(e);
            return false;
          }
        }
      ),
      catchError(
        (err) => {
          // localStorage.removeItem('currentUser');
          // this.router.navigate(['/login']);
          // return of(false);
          return of(true);
        }
      )
    );
  }
}
