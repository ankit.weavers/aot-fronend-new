import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthenticationService } from './../services';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // return true;
    // if (!this.authenticationService.isUserLoggedIn()) {
    //   // logged in so return true
    //   console.log(this.authenticationService.isUserLoggedIn());
    //   return true;
    // } else {
    //   this.router.navigate(['']);
    //   return false;
    // }
    return this.authenticationService.isUserLoggedIn().pipe(
      map(
        e => {
          if (e) {
            return true;
          } else {
            // localStorage.removeItem('currentUser');
            this.router.navigate(['']);
            return false;
          }
        }
      ),
      catchError(
        (err) => {
          // localStorage.removeItem('currentUser');
          this.router.navigate(['']);
          return of(false);
        }
      )
    );
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    // return true;
    return this.authenticationService.isUserLoggedIn().pipe(
      map(
        e => {
          if (e) {
            return true;
          } else {
            // localStorage.removeItem('currentUser');
            this.router.navigate(['']);
            return false;
          }
        }
      ),
      catchError(
        (err) => {
          // localStorage.removeItem('currentUser');
          this.router.navigate(['']);
          return of(false);
        }
      )
    );
  }
}
