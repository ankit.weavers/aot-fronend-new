import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ContentType } from './content-type-interceptor.service';
import { LoggingInterceptor } from './logging-interceptor.service';
import { LoaderInterceptor } from './loader-interceptor.service';
import { TokenInterceptor } from './token-interceptor.service';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  // { provide: HTTP_INTERCEPTORS, useClass: ContentType, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
];
