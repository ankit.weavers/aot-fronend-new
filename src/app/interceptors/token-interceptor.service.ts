import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services';
import { MatSnackBar } from '@angular/material';

@Injectable()
// export class ContentType implements TokenInerceptorService {
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    public authenticationService: AuthenticationService,
    public snackBar: MatSnackBar
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    const reqModified = req.clone({
      headers : req.headers.append(
        'authorization', `Bearer ${this.authenticationService.getToken()}`
      )
    });
    // return next.handle(reqModified);
    return next.handle(reqModified).pipe(
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // Check Auth token if it is Valid else logout
            if (event.body.isError && event.body.statuscode === 504) {
              this.authenticationService.logOut();
            }
          }
          return event;
      }),
      catchError((error: any) => {
          // let data = {};
          // console.log('error.error ', error.error);
          // console.log('error.res ', error.response);

          // data = {
          //     reason: error && error.error.response ? error.error.response.message  : '',
          //     status: error.status
          // };
          // console.log('Error in interceptor ', data);
          // if(data['status'] == 404){
          //     console.log('navigate');
          //     this.authService.logout()
          // }
          console.log(error);
          return throwError(error);
      })
    );
  }

}

