// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json` // region: 'eu-west-1',.

export const environment = {
  production: false,
  // SERVER_URL: 'http://localhost:3132/api',
//  SERVER_URL: 'http://192.168.0.37:3132/api',
  SERVER_URL: 'https://api.letery.com/api',
  // SERVER_URL:'http://192.168.0.35:3132/api',

  //old one
  // s3: {
  //   bucketName : 'aot-ara',
  //   acl : 'public-read',
  //   bucket: {
  //       accessKeyId: 'AKIAZSCF3NTNV6EWRTPR',
  //       secretAccessKey: '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',
  //       region: 'us-east-2'

  //       // region: 'us-west-1'

  //       // accessKeyId: 'AKIAU3HJ4MBHASHX4ZMP',
  //       // secretAccessKey: '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',
  //       // region: 'us-east-2'
  //   },
  // },

  //new one
  s3: {
    bucketName : 'aot-ara',
    acl : 'public-read',
    bucket: {
        accessKeyId: 'AKIAZSCF3NTN6ZJVXRUT',
        secretAccessKey: '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',
        region: 'us-east-2'
  
    },
  },
  firebase: {
    apiKey: "AIzaSyB64T_nkkUbn0U2yMSh218La1odCCS2TaU",
    authDomain: "aot-seleckt.firebaseapp.com",
    databaseURL: "https://aot-seleckt.firebaseio.com",
    projectId: "aot-seleckt",
    storageBucket: "aot-seleckt.appspot.com",
    messagingSenderId: "878332196325",
    appId: "1:878332196325:web:1864e51db5db0fe3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
