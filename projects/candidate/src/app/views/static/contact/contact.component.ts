import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CandidateContactService } from '../../../services';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'candidate-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  emailId;
  supportNumber;

  constructor(
    private router: Router,
    private candidatecontactService: CandidateContactService,
    public snackBar: MatSnackBar,    
  ) { }

  ngOnInit() {
    var an = this;
    an.getContactDetails();
    
  }
  // Fetch contact details

  getContactDetails(){
    //get industries
    var an = this;
    an.candidatecontactService.getCandidateContactDetails()
      .pipe()
      .subscribe(
        result => {
          if (result && !result['isError']) {
           an.emailId = result['details'].emailId;
           an.supportNumber = result['details'].supportNo;
          } 
        },
        error => {
          an.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );   
  }

}
