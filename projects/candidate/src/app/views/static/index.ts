export { AboutComponent } from './about/about.component';
export { FaqComponent } from './faq/faq.component';
export { PrivacyComponent } from './privacy/privacy.component';
export { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
export { ContactComponent } from './contact/contact.component';
export { ReportComponent } from './report/report.component';
export { ReportProblemComponent } from './report-problem/report-problem.component';