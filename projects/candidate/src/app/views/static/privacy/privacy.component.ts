import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'candidate-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})

export class PrivacyComponent implements OnInit {

  policies;
  private onDestroyUnSubscribe = new Subject<void>();

  constructor(
    private globalActionsService: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.getPrivacyPolicy();
  }

  getPrivacyPolicy() {
    this.globalActionsService.getPrivacyPolicyDetails({ userType: JSON.parse(localStorage.getItem('currentUser')).userType })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(typeof result === "object" && (result !== null) && (!result['isError'] )){
            if(result['details'] !== null){
              this.policies =  result['details'].privacyPolicy[0].PolicyDetails;
            }
          }
        });
  }

}


