import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticRoutingModule } from './static-routing.module';

import { SharedModule } from 'src/app/modules/shared.module';
import {
  AboutComponent,
  FaqComponent,
  PrivacyComponent,
  TermsAndConditionsComponent,
  ContactComponent,
  ReportComponent,
  ReportProblemComponent
} from './';

@NgModule({
  declarations: [
    AboutComponent,
    FaqComponent,
    PrivacyComponent,
    TermsAndConditionsComponent,
    ContactComponent,
    ReportComponent,
    ReportProblemComponent
  ],
  imports: [
    CommonModule,
    StaticRoutingModule,
    SharedModule
  ]
})
export class StaticModule { }
