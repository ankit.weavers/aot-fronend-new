import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  AboutComponent,
  FaqComponent,
  PrivacyComponent,
  TermsAndConditionsComponent,
  ContactComponent,
  ReportComponent,
  ReportProblemComponent
} from './';

const routes: Routes = [
  {
    path: 'about-us',
    component: AboutComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'About Us'
    }
  },
  {
    path: 'faq',
    component: FaqComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'FAQ'
    }
  },
  {
    path: 'privacy-policy',
    component: PrivacyComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Privacy Policy'
    }
  },
  {
    path: 'terms-and-conditions',
    component: TermsAndConditionsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Terms And Conditions'
    }
  },
  {
    path: 'contact-us',
    component: ContactComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Contact Us'
    }
  },
  {
    path: 'report',
    component: ReportComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Report'
    }
  },
  {
    path: 'report-problem',
    component: ReportProblemComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Report Problem'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule { }
