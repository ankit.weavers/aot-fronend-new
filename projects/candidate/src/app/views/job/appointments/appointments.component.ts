import { Component, OnInit } from '@angular/core';
import { CandidateJobService } from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

declare var google: any;

@Component({
  selector: 'candidate-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allActiveJobList = [];
  lng: number;
  lat: number;
  public zoom: number = 12;
  totalItems;
  itemsPerPage;
  pageNo;
  totalNoOfPages:number =6;
  dataFetching = false;
  showRole:boolean = false;
  showArraow:boolean = false;
  showapplies:boolean = false;
  showArrowApllied :boolean = false;

  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getAllActiveJobsList();
  }

    // DOWN ARROW //
    showMoreRole() {
      this.showRole = true;
      this.showArraow = true;
    }
  
    // UP ARROW //
    showLessRole() {
      this.showRole = false;
      this.showArraow = false;
    }

  // get job details
  getAllActiveJobsList() {
    this.candidatejobservice.getAllActiveJobDetails({ candidateId: this.userDetails.candidateId,   pageno: this.pageNo,
      perpage:this.itemsPerPage })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details'][0]) {
            if (result['details']) {
              this.allActiveJobList = result['details'][0].results;
              let obj =result['details'][0];
              if(Object.entries(obj).length!==0)
              {
                this.totalItems = obj.total ? obj.total : 0;
                // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }else{
                this.dataFetching = false;
                this.snackBar.open('No Data Found. Please try again!','Got it!',{
                
                });
              }
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  //open candidate attendanve
  viewCandidateAttendance(candidateId, jobId, roleId, employerId){
    let JobDetailsById = {
      jobId: jobId,
      roleId: roleId,
      candidateId: candidateId,
      employerId: employerId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/candidate/job/attendance']);
  }

  //open candidate tracking page for check in - check out
  viewCandidateTrackingPage(candidateId, jobId, roleId, employerId) {
    let JobDetailsById = {
      jobId: jobId,
      roleId: roleId,
      candidateId: candidateId,
      employerId: employerId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/candidate/job/tracking']);
  }
  getPaginateData(event)
  {
    this.pageNo = event.page;
    this.allActiveJobList = [];
    this.dataFetching = true;
    this.getAllActiveJobsList();

  }
}
