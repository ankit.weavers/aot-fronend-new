
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CandidateJobService } from 'projects/candidate/src/app/services';
import { MatSnackBar } from '@angular/material';
import { Subject, Observable, Observer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MapsAPILoader } from '@agm/core';

import * as moment from 'moment';
declare var google: any;

@Component({
  selector: 'candidate-onsite',
  templateUrl: './onsite.component.html',
  styleUrls: ['./onsite.component.scss']
})

export class OnsiteComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  jobId;
  roleId;
  employerId;
  isCheckIn: boolean = false;
  lng: number;
  lat: number;
  geocoder;
  offsitesLocation;
  checkout: boolean = false;
  isLoading: boolean = false;
  showpopUp : boolean =false;

  public today: Date = new Date();
  modalRef: BsModalRef;
  dataInsert = [{
    checkIns: '',
    checkOuts: '',
  }];
  @ViewChild('informationPopUp') modalTemplate: TemplateRef<any>;
  @ViewChild('informationPopUps') modalTemplates: TemplateRef<any>;

  informationMessage;
  todaysAttendanceList = [];
  checkinVariable = [];
  time = new Observable<string>((observer: Observer<string>) => {
    setInterval(() => {
      observer.next(new Date().toString());
    }, 1000);
  });
  isTracking: boolean = true;
  onsiteLockAttendanceTracking: boolean = false;

  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private modalService: BsModalService,
    private mapsAPILoader: MapsAPILoader,

  ) {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsById'));
    this.jobId = jobDetailsData.jobId;
    this.roleId = jobDetailsData.roleId;
    this.employerId = jobDetailsData.employerId;
  }

  ngOnInit() {
    this.checkCandidateAttendanceStatus(); //create attendance for first check-in by candidate
    this.getTodaysAttendanceList(); //get attendance list for today
    this.trackCandidateLocation(); //track candidate for automatic checkout if location doesnt match the job location 

  }

  // candidate onsite checkin 
  checkCandidateAttendanceStatus() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId
    }
    this.candidatejobservice.checkAttendanceStatusById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {

            console.log("get attendance check", this.isCheckIn, result['details'].lastJobType, typeof result['details'].lastJobType, result);
            if (result['details']) {
              if (result['details'].lastJobType === "OFFSITE") {
                this.onsiteLockAttendanceTracking = true;
                this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
                this.informationMessage = 'You are already checked in from your offsite location!';
              }
              else {
                this.isCheckIn = result['details'].isCheckedIn;
              }

              if (result['details'].lastJobType === "") {
                //check if today is a working day or not
                this.checkTodayIfWorkingDay();
              }

            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  //check if today is a working day or not
  checkTodayIfWorkingDay() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
      date: new Date().toISOString().slice(0, 10) //"2018-02-22"
    }
    this.candidatejobservice.checkTodaysWorkingStatus(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log(" checkTodayIfWorkingDay result", result, result['details'].isWorkingDay);
          if (result && !result['isError'] && result['details'] && !result['details'].isWorkingDay) {
            this.onsiteLockAttendanceTracking = true;
            this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
            this.informationMessage = result['message'];
          }

          if (!this.isCheckIn && result['details'].isWorkingDay && !this.showpopUp) {
            console.log('ngonitcheckin', this.isCheckIn);
            this.modalRef = this.modalService.show(this.modalTemplates, { backdrop: 'static', keyboard: false });

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
  checkinfun() {
    this.findMe();
    this.modalRef.hide();
  }

  //get today's check-in , check-out list
  getTodaysAttendanceList() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
      date: this.today.toISOString().substring(0, 10)
    }
    this.candidatejobservice.getTodaysAttendance(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            this.todaysAttendanceList = result['onSiteArray'];
            console.log('today', this.todaysAttendanceList);
            // this.todaysAttendanceList.forEach(element => {
            //   if(element.checkIn){
            //  let values =  String((element.checkIn.toString().split('.')[0].length == 1? '0':''  ) + element.checkIn.toString().split('.')[0]) + '.' + String(element.checkIn.toString().split('.')[1]+(element.checkIn.toString().split('.')[1].length == 1? '0':''));
            //     let chckoutVariable = String((element.checkOut.toString().split('.')[0].length == 1? '0':''  ) + element.checkOut.toString().split('.')[0]) + '.' + String(element.checkOut.toString().split('.')[1]+(element.checkOut.toString().split('.')[1].length == 1? '0':''));


            //  console.log('con',values);
            //  console.log('con2',chckoutVariable)




            //     console.log("chkValue",this.dataInsert);





            //   }
            //   console.log("checkout",element.checkOut);

            //   console.log("checkIn",element.checkIn);


            // });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );
  }

  //get candidate's current location details
  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          console.log('position', position);

          this.mapsAPILoader.load().then(() => {
            this.geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(this.lat, this.lng)
            let currentClassInstance = this;
            this.geocoder.geocode({ 'location': latlng }, function (results, status) {
              console.log(results[0]);
              // if(status === "OK"){

              currentClassInstance.offsitesLocation = results[0].formatted_address;

              // }
            });
          })
          setTimeout(()=>{
            let that = this;
            if (!that.isCheckIn) {
              that.candidateOnsiteCheckIn(); //call check in api
            }
            else {
              that.candidateOnsiteCheckOut(); //call check out api
            }
          },2000)
     
        },
        error => {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              return "User denied the request for Geolocation."
            case error.POSITION_UNAVAILABLE:
              return "Location information is unavailable."
            case error.TIMEOUT:
              return "The request to get user location timed out."
          }
        }
      );
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  //call checkIn api
  candidateOnsiteCheckIn() {
    if (this.lat && this.lng) {
      this.isLoading = true;
      let requiredData = {
        candidateId: this.userDetails.candidateId,
        jobId: this.jobId,
        roleId: this.roleId,
        employerId: this.employerId,
        destinationLat: this.lat,
        destinationLong: this.lng,
        destinationName: this.offsitesLocation,

        // destinationLat: 22.5517568,
        // destinationLong: 88.3531776,
        // checkinTime: moment().format("HH:mm"),
        checkinTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
        // checkinTime: "09.10",

        date: this.today.toISOString().substring(0, 10)
      }
      console.log('datetotal', String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()));
      console.log('datehour', String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()));
      console.log('datemin', String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()));
      console.log('dateOnlyMint', new Date().getMinutes());
      console.log('dateonlyHour', new Date().getHours());


      this.candidatejobservice.candidateOnsiteCheckIn(requiredData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            this.isLoading = false;
            if (result && !result['isError']) {
              this.checkout = false;
              this.isCheckIn = true;
              this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
              this.informationMessage = result['message'];
              this.getTodaysAttendanceList();
            }
            else {
              this.isCheckIn = false;
              if(result['message'] == "Check can only be done 15 mins before your start time" && result['statuscode'] == 1033) {
                this.snackBar.open('Check-In can only be done 15 minutes before your start time','Got it!',{

                })
              }
              else {
                this.snackBar.open(result['message'], 'Got it!', {
             
                });
              }
             
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
            });
          }
        );
    } else {
      this.snackBar.open('Please Enter Your Offsite Location', 'Got it!', {
       
      });
    }
  }

  //candidate onsite check out
  candidateOnsiteCheckOut() {

    if (this.lat && this.lng) {
      this.isLoading = true;
      let requiredData = {
        candidateId: this.userDetails.candidateId,
        jobId: this.jobId,
        roleId: this.roleId,
        employerId: this.employerId,
        checkOutTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
        date: this.today.toISOString().substring(0, 10),
        destinationLat: this.lat,
        destinationLong: this.lng,
        checkoutlocationName: this.offsitesLocation,
      }

      this.candidatejobservice.candidateOnsiteCheckOut(requiredData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            this.isLoading = false;
            if (result && !result['isError']) {
              this.checkout = true;
              this.isCheckIn = false;
              this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
              this.informationMessage = result['message'];
              this.showpopUp = true;

              this.checkCandidateAttendanceStatus();
              this.getTodaysAttendanceList();
            }
            else {
              this.isCheckIn = false;
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    }

  }

  //trackCandidateLocation
  trackCandidateLocation() {

    setInterval(() => {
      if (navigator.geolocation && this.isCheckIn) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
            console.log('position 2', position);

            let requiredData = {
              candidateId: this.userDetails.candidateId,
              jobId: this.jobId,
              roleId: this.roleId,
              employerId: this.employerId,
              // "destinationLat": 15.3412, //test demo value for radius out of range
              // "destinationLong":31.1471, //test demo value for radius out of range
              destinationLat: this.lat,  //dynamic 
              destinationLong: this.lng, //dynamic
              // destinationLat: 22.5517568, //test demo value for radius within range
              //destinationLong: 88.3531776,  //test demo value for radius within range
              checkOutTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
              date: this.today.toISOString().substring(0, 10)
            }

            this.candidatejobservice.candidateAutomaticCheckOut(requiredData)
              .pipe(takeUntil(this.onDestroyUnSubscribe))
              .subscribe(
                result => {
                  this.isLoading = false;
                  if (result && !result['isError'] && result['details']) {
                    this.isCheckIn = false;
                    this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
                    this.informationMessage = result['message'];
                      this.showpopUp = true;

                    this.checkCandidateAttendanceStatus();

                    this.getTodaysAttendanceList();
                  }
                },
                error => {
                  this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
                   
                  });
                }
              );
          })
      }
    }, 300000) //called after each 5 minutes

  }

}


