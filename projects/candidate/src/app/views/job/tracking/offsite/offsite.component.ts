import { Component, OnInit, ViewChild, ElementRef, NgZone, TemplateRef } from '@angular/core';
import { CandidateJobService } from 'projects/candidate/src/app/services';
import { MatSnackBar } from '@angular/material';
import { Subject, Observable, Observer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MapsAPILoader } from '@agm/core';
import { FormControl, Validators } from '@angular/forms';
import { Erros } from 'src/app/models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

declare var google: any;

@Component({
  selector: 'candidate-offsite',
  templateUrl: './offsite.component.html',
  styleUrls: ['./offsite.component.scss']
})
export class OffsiteComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  jobId;
  roleId;
  locationTrac = [];
  lng: number;
  lat: number;
  public searchControl: FormControl;
  public zoom: number;
  @ViewChild("search")
  public searchElementRef: ElementRef;
  public errors: any = Erros;
  public today: Date = new Date();
  isCheckIn: boolean = false;
  offsiteLocation;
  offsitesLocation;
  interval;
  employerId;
  geocoder;
  showpopUp : boolean =false;
  checkout: boolean = false;
  isLoading: boolean = false;
  modalRef: BsModalRef;
  google_map_position;
  @ViewChild('informationPopUp') modalTemplate: TemplateRef<any>;
  @ViewChild('informationPopUps') modalTemplates: TemplateRef<any>;

  informationMessage;
  todaysAttendanceList = [];
  offsiteLockAttendanceTracking: boolean = false;


  time = new Observable<string>((observer: Observer<string>) => {
    setInterval(() => {
      observer.next(new Date().toString());
    }, 1000);
  });

  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private modalService: BsModalService,
  ) {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsById'));
    this.jobId = jobDetailsData.jobId;
    this.roleId = jobDetailsData.roleId;
    this.employerId = jobDetailsData.employerId;
  }

  ngOnInit() {

    this.setCurrentPosition();

    this.checkCandidateAttendanceStatus(); //create attendance for first check-in by candidate
    this.getTodaysAttendanceList(); //get attendance list for today

    //set google maps defaults
    this.zoom = 4;
    //create search FormControl
    // this.searchControl = new FormControl('', Validators.required);
    //set current position
    // this.mapsAPILoader.load().then(()=>{


    // });
    //load Places Autocomplete
    // this.mapsAPILoader.load().then(() => {
    //   let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
    //     types: []
    //   });
    //   autocomplete.addListener("place_changed", () => {
    //     this.ngZone.run(() => {
      
    //       let place: typeof google.maps.places.PlaceResult = autocomplete.getPlace();
         

      
    //       if (place.geometry === undefined || place.geometry === null) {
    //         return;
    //       }
        
    //       this.lat = place.geometry.location.lat();
    //       this.lng = place.geometry.location.lng();
    //       this.zoom = 12;
    //       this.offsiteLocation = place.name;
    //     });
    //   });
    // });
  }

  // candidate offsite checkin 
  checkCandidateAttendanceStatus() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId
    }
    this.candidatejobservice.checkAttendanceStatusById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.checkout = false;
            console.log("get attendance check", this.isCheckIn, result['details'].lastJobType, typeof result['details'].lastJobType, result);
            if (result['details']) {
              if (result['details'].lastJobType === "ONSITE") {
                this.offsiteLockAttendanceTracking = true;
                this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
                this.informationMessage = 'You are already checked in from your onsite location!';
              }
              else {
                this.isCheckIn = result['details'].isCheckedIn;
                console.log('check kro real',this.isCheckIn);
              }

              if (result['details'].lastJobType === "") {
                //check if today is a working day or not
                this.checkTodayIfWorkingDay();
              }
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  //setting current map position 
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log('positio',position);
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
    // this.mapsAPILoader.load().then(() => {
    //   this.google_map_position = new google.maps.Res( this.lat, this.lng );
    //   console.log('google_map_position',this.google_map_position);
    // })
    this.mapsAPILoader.load().then(() =>{
    this.geocoder = new google.maps.Geocoder;
    var latlng = new google.maps.LatLng(this.lat, this.lng)
    let currentClassInstance = this;
    this.geocoder.geocode({'location': latlng}, function(results, status){
      console.log(results[0]);
      // if(status === "OK"){

        currentClassInstance.offsitesLocation = results[0].formatted_address;

      // }
    });
  })
    console.log("name",this.offsitesLocation);



      });
   

    }

  }

  //check if today is a working day or not
  checkTodayIfWorkingDay() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
      date: new Date().toISOString().slice(0, 10) //"2018-02-22"
    }
    this.candidatejobservice.checkTodaysWorkingStatus(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log(" checkTodayIfWorkingDay result", result, result['details'].isWorkingDay);
          if (result && !result['isError'] && result['details'] && !result['details'].isWorkingDay) {
            this.offsiteLockAttendanceTracking = true;
            this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
            this.informationMessage = result['message'];
          }

          if (!this.isCheckIn && result['details'].isWorkingDay && !this.showpopUp) {
            console.log('ngonitcheckin', this.isCheckIn);
            this.modalRef = this.modalService.show(this.modalTemplates, { backdrop: 'static', keyboard: false });

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  checkinfun() {
    setTimeout(()=>{
      let that = this;
      that.candidateOffsiteCheckIn();

    },1000)
    this.modalRef.hide();
  }

  //get today's check-in , check-out list
  getTodaysAttendanceList() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
      date: this.today.toISOString().substring(0, 10)
    }
    this.candidatejobservice.getTodaysAttendance(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            this.todaysAttendanceList = result['offSiteArray'];
            this.locationTrac = result['details'].offSite

            console.log('attendance',this.isCheckIn);
            // this.locationTrac.forEach(element => {
            //   console.log('element',element)
            // });
            for (let i =0; i<this.locationTrac.length;i++){
              if(this.locationTrac[i].checkOut == 0 && this.isCheckIn && !this.checkout) {
                console.log('idssssssss',this.locationTrac[i].offSideId);
                this.trackCandidateLocation(this.locationTrac[i].offSideId)
              }
              // if(this.checkout && !this.isCheckIn){
              //   console.log('hjcfjdahfjkdwdwd');
              //   this.trackCandidateLocation(this.locationTrac[i].offSideId);
              // }
            }
            console.log('today',this.todaysAttendanceList);

            let lastArrayData = this.todaysAttendanceList[this.todaysAttendanceList.length - 1];
            if (lastArrayData && !lastArrayData.checkOut && lastArrayData.checkIn) {
              // this.searchControl.setValue(lastArrayData.destinationName);
              this.lat = lastArrayData.geolocation.coordinates[1];
              this.lng = lastArrayData.geolocation.coordinates[0];
              this.offsitesLocation = lastArrayData.destinationName;
              console.log('offSite',this.offsitesLocation);
              this.zoom = 12;
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );
  }

  //candidate offsite check in
  candidateOffsiteCheckIn() {
    this.isLoading = true;

    setTimeout(()=>{
      console.log('name',this.offsitesLocation);
      // this.offsiteLocation = this.offsitesLocation
      console.log('otherName',this.offsiteLocation);
      if (this.lat && this.lng) {
        let requiredData = {
          candidateId: this.userDetails.candidateId,
          jobId: this.jobId,
          roleId: this.roleId,
          employerId: this.employerId,
          destinationName: this.offsitesLocation,
          destinationLat: this.lat,
          destinationLong: this.lng,
          checkinTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
          date: this.today.toISOString().substring(0, 10)
        }
        console.log('req',requiredData);
  
        this.candidatejobservice.candidateOffsiteCheckIn(requiredData)
          .pipe(takeUntil(this.onDestroyUnSubscribe))
          .subscribe(
            result => {
              this.isLoading = false;
              if (result && !result['isError']) {
              this.checkout = false;
  
                this.isCheckIn = true;
  
                console.log('check kro checkin',this.isCheckIn);
  
                this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
                this.informationMessage = result['message'];
                this.getTodaysAttendanceList();
                let lastArrayData = this.todaysAttendanceList[this.todaysAttendanceList.length - 1];
                if (lastArrayData && !lastArrayData.checkOut && lastArrayData.checkIn) {
                  // this.searchControl.setValue(lastArrayData.destinationName);
                this.offsitesLocation = lastArrayData.destinationName;
                console.log('offSite2',this.offsitesLocation);
  
                  this.lat = lastArrayData.geolocation.coordinates[1];
                  this.lng = lastArrayData.geolocation.coordinates[0];
                  this.zoom = 12;
                }
              }
              else {
                this.isCheckIn = false;
                if(result['message'] == "Check can only be done 15 mins before your start time" && result['statuscode'] == 1033) {
                  this.snackBar.open('Check-In can only be done 15 minutes before your start time','Got it!',{
  
                  })
                }
                else {
                  this.snackBar.open(result['message'], 'Got it!', {
               
                  });
                }
              }
            },
            error => {
              this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
              });
            }
          );
  
      } else {
        this.snackBar.open('Please Enter Your Offsite Location', 'Got it!', {
       
        });
      }
    },2000)


  }

  //candidate offsite check out
  candidateOffsiteCheckOut() {
    this.isLoading = true;

    setTimeout(()=>{
      let requiredData = {
        candidateId: this.userDetails.candidateId,
        jobId: this.jobId,
        roleId: this.roleId,
        employerId: this.employerId,
        checkoutlocationName: this.offsitesLocation,
        destinationLat: this.lat,
        destinationLong: this.lng,
        checkOutTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
        //  checkOutTime: "23.50",
        date: this.today.toISOString().substring(0, 10)
      }
      this.candidatejobservice.candidateOffsiteCheckOut(requiredData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            this.isLoading = false;
            if (result && !result['isError']) {
              // this.trackCandidateLocation(true);
              this.checkout = true;
  
              this.isCheckIn = false;
              console.log('check kro checkout',this.isCheckIn);
  
              this.modalRef = this.modalService.show(this.modalTemplate, { backdrop: 'static', keyboard: false });
              this.informationMessage = result['message'];
              this.getTodaysAttendanceList();
              if(this.checkout) {
                clearInterval(this.interval);
              }
            }
            else {
              this.isCheckIn = true;
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
        );
    },2000)
 
  }

  trackCandidateLocation(offSiteId) {
    
    // if(this.isCheckIn) {

   this.interval = setInterval(() => {
    //  console.log('checkin location tracking',this.isCheckIn);
      if (navigator.geolocation && this.isCheckIn) {
        console.log('checkin');
        let requiredData = {
          candidateId: this.userDetails.candidateId,
          jobId: this.jobId,
          roleId: this.roleId,
          employerId: this.employerId,
          destinationName: this.offsitesLocation,
          offSideId: offSiteId,
          destinationLat: this.lat,
          destinationLong: this.lng,
          checkinTime: String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
          //  checkOutTime: "23.50",
          date: this.today.toISOString().substring(0, 10)
        }
        this.candidatejobservice.locationTracInsert(requiredData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            this.showpopUp = true;
            this.checkCandidateAttendanceStatus();

          })
   
      }
    }, 900000) //called after each 15 minutes
  // }

  }

}
