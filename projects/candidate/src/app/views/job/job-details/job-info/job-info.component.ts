import { Component, OnInit } from '@angular/core';
import { CandidateJobService } from '../../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'candidate-job-info',
  templateUrl: './job-info.component.html',
  styleUrls: ['./job-info.component.scss']
})
export class JobInfoComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  roleId;
  jobId;
  jobInfo;
  employerId;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  // jobStatus = 'apply';
  jobDetailsData: any;
  isApplied: Boolean = false;
  isOffered: Boolean = false;
  isAccepted: Boolean = false;
  isDeclined: Boolean = false;
  isHired: Boolean = false;
  profileCheck: any;
  messages: any;
  checkValue: boolean;
  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.jobDetailsData = JSON.parse(localStorage.getItem('jobDetailsData'));
    this.getJobDetailsById(this.jobDetailsData);
    this.checkCandidate();
  }

  // check candidate profile //
  checkCandidate() {
    this.candidatejobservice._checkCandidate({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(result => {
        if (result && !result['isError']) {
          this.profileCheck = result['details'].isAuthenticated
          this.messages = result['details'].message

        }
        else {
          this.snackBar.open(result['message'], 'Got it!', {

          })
          this.messages = result['details'].message
        }
      })
  }

  // get job details
  getJobDetailsById(data) {
    let jobData = {
      candidateId: this.userDetails.candidateId,
      roleId: data.roleId,
      jobId: data.jobId,
    };
    this.candidatejobservice.getJobDetailsById(jobData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobInfo = result['details'].jobInfo;
            this.employerId = result['details'].generalInfo.employerId;

            console.log("jobInfo", this.jobInfo);

            //set job status info
            if (result['details'].generalInfo.isApplied && !result['details'].generalInfo.isHired) {
              this.isApplied = true;
            }
            if (result['details'].generalInfo.isOffered && !result['details'].generalInfo.isHired) {
              this.isOffered = true;
            }
            if (result['details'].generalInfo.isAccepted && !result['details'].generalInfo.isHired) {
              this.isAccepted = true;
            }
            if (result['details'].generalInfo.isDeclined && !result['details'].generalInfo.isHired) {
              this.isDeclined = true;
            }
            //check if candidate is already hired job  
            if (
              result['details'].generalInfo &&
              result['details'].generalInfo.isOffered &&
              result['details'].generalInfo.isAccepted &&
              result['details'].generalInfo.isHired
            ) {
              this.isHired = true;
            }

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }

  //apply selected job
  jobApply() {

    this.snackBar.open('Please wait for sometime..', 'Got it!', {

    });
    if (this.profileCheck == "True") {
      this.stricktoApply(this.jobDetailsData);
      setTimeout(() => {
        if (this.checkValue) {
          const applyData = {
            candidateId: this.userDetails.candidateId,
            // roleId: this.roleId,
            // employerId: this.employerId,
            // jobId: this.jobId,
            ...this.jobDetailsData
          };

          this.candidatejobservice.applyJobById(applyData)
            .pipe(takeUntil(this.onDestroyUnSubscribe))
            .subscribe(
              result => {
                if (result && !result['isError']) {
                  //  this.isApplied = true;
                  this.snackBar.open(result['message'], 'Got it!', {

                  });
                  this.getJobDetailsById(this.jobDetailsData);
                }
                else {
                  this.snackBar.open(result['message'], 'Got it!', {

                  });
                }
              },
              error => {
                this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

                });
              }
            );
        }
        else {
          this.snackBar.open('you are not available for this job', 'Got it!', {

          })
        }
      }, 2000);
    }



    else {
      this.snackBar.open(this.messages, 'Got it!', {

      })
    }
  }

  stricktoApply(data) {
    this.candidatejobservice.checkaotOfferAll({ candidateId: this.userDetails.candidateId, jobId: data.jobId.toString(),roleId:data.roleId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(result => {
        if (result && !result['isError']) {
          this.checkValue = result['details'].isCandidateAvailable;
          console.log('checkvalue1', this.checkValue);

        }
      })
  }


  //accept / decline job offer
  changeStatusOfJob(changeJobstatusTo) {
    this.snackBar.open('Please wait for sometime..', 'Got it!', {

    });

    if (this.profileCheck == "True") {

      if (changeJobstatusTo === 'accept') {
        this.stricktoApply(this.jobDetailsData);
        setTimeout(() => {
          if (this.checkValue) {
            let acceptFormData = {
              candidateId: this.userDetails.candidateId,
              // employerId: this.employerId,
              // jobId: this.jobId,
              // roleId: this.roleId,
              ...this.jobDetailsData
            }
            this.candidatejobservice.acceptJobById(acceptFormData)
              .pipe(takeUntil(this.onDestroyUnSubscribe))
              .subscribe(
                result => {
                  if (result && !result['isError']) {
                    this.snackBar.open('You have accepted this job offer', 'Got it!', {

                    });
                    this.getJobDetailsById(this.jobDetailsData);
                  } else {
                    this.snackBar.open(result['message'], 'Got it!', {

                    });
                  }
                },
                error => {
                  this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

                  });
                }
              );
          }
          else {
            this.snackBar.open('you are not available for this job', 'Got it!', {

            })
          }
        }, 2000);
      }
      else if (changeJobstatusTo === 'decline') {

        let declinefrom;
        if (!this.isDeclined && !this.isAccepted) {
          declinefrom = '1';
        }
        else if (!this.isDeclined && this.isAccepted) {
          declinefrom = '2';

        }

        let declineFormData = {
          candidateId: this.userDetails.candidateId,
          declinefrom: declinefrom,
          // employerId: this.employerId,
          // jobId: this.jobId,
          // roleId: this.roleId,
          ...this.jobDetailsData,
        }
        this.candidatejobservice.declineJobById(declineFormData)
          .pipe(takeUntil(this.onDestroyUnSubscribe))
          .subscribe(
            result => {
              if (result && !result['isError']) {
                this.snackBar.open('You have declined this job offer', 'Got it!', {

                });
                this.getJobDetailsById(this.jobDetailsData);
              } else {
                this.snackBar.open(result['message'], 'Got it!', {

                });
              }
            },
            error => {
              this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

              });
            }
          );
      }
    }
    else {
      this.snackBar.open(this.messages, 'Got it!', {

      })
    }

  }

  // reject job after getting hired
  rejectJob() {
  }

}
