import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CalendarMonthViewBeforeRenderEvent, CalendarMonthViewDay, CalendarEvent } from 'angular-calendar';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { CandidateJobService } from '../../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'candidate-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.scss']
})
export class AttendenceComponent implements OnInit {

  jobId;
  roleId;
  private onDestroyUnSubscribe = new Subject<void>();
  bsRangeValue = [];
  timeValue;

  @Input() viewDate: Date = new Date();
  @Input() view: string = 'month';
  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
  currentDate: Date;
  eventList = []
 
  selectedShiftList = [];
  @Output()
  eventClicked = new EventEmitter<{
    event: CalendarEvent;
  }>();
  viewDateDetails: boolean = false;
  events:any;
  refresh:any;
  // @Output()
  // eventClicked = new EventEmitter<{
  //   event: CalendarEvent;
  // }>();
  // viewDateDetails: boolean = false;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  jobDetails: any;
  // public refresh:any;

  constructor(public snackBar: MatSnackBar,
    private candidatejobservice: CandidateJobService,
    private router: Router,
  ) { }

  ngOnInit() {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsById'));
    this.jobId = jobDetailsData.jobId;
    this.roleId = jobDetailsData.roleId;
    if (this.jobId && this.roleId) {
      this.getAttendanceDetails();
    }
  }

  // get JobPosted List
  getAttendanceDetails() {
    let requiredData = {
      candidateId: this.userDetails.candidateId,
      jobId: this.jobId,
      roleId: this.roleId
    }

    this.candidatejobservice.getAttendanceDetailsById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobDetails = result['details']
            this.timeValue = result['totaltimeForCheckIn']
            let workingStartDate = new Date(result['details'].workingDays[0].dateInTimestampFormat);
            let workingEndDate = new Date(result['details'].workingDays[result['details'].workingDays.length - 1].dateInTimestampFormat);

            this.arrangeDateWiseEvents(workingStartDate, workingEndDate);

            // preparing all events from all working days
            result['details'].workingDays.map(
              (element) => {
                if (element.totalOnsiteCheckinTimeInMns >= 0) {
                  let totCheckinTimeInMins =element.totalOnsiteCheckinTimeInMns + element.totaloffSiteCheckinTimeInMns;
                   let totCheckinHrs = Math.floor(totCheckinTimeInMins / 60);
                  let totCheckinMins = totCheckinTimeInMins % 60;
                  let totTimeCheckin = totCheckinHrs + "h" + totCheckinMins + "m";
                  let currentDateString = new Date(element.dateInTimestampFormat).getDate() + '-' + (new Date(element.dateInTimestampFormat).getMonth() + 1) + '-' + new Date(element.dateInTimestampFormat).getFullYear();
                  console.log('currentDateString',currentDateString);
                  this.eventList[currentDateString] = [{
                 
                    title: totTimeCheckin,
                    // color: colors.yellow,
                    start: new Date(element.dateInTimestampFormat),
                    // meta: {
                    //   type: 'warning',
                    //   // initial: shiftType.charAt(0),
                    // }
                  }]
                }
              }
            );
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
         
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  //arrange dates and events
  arrangeDateWiseEvents(startDate, endDate) {
    // const dates = [];
    // Strip hours minutes seconds etc.
    let currentDate = new Date(
      startDate.getFullYear(),
      startDate.getMonth(),
      startDate.getDate()
    );
    while (currentDate <= endDate) {
      // dates.push(currentDate);
      this.bsRangeValue.push(currentDate);
      currentDate = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate() + 1, // Will increase month if over range
      );
    }
    // return dates;
  }


  //set values before rendering angular calendar
  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
    setTimeout(
      () => {
        for (var i = 0; i < this.bsRangeValue.length; i++) {
          renderEvent.body.forEach(day => {
            //check if days have events 
            if (day.date.getDate() === this.bsRangeValue[i].getDate() && (day.date.getMonth() + 1) === (this.bsRangeValue[i].getMonth() + 1) && day.date.getFullYear() === this.bsRangeValue[i].getFullYear()) {
              // day.cssClass = 'bg-pink';
              // day.backgroundColor = "#ad2121";
              let eventDate = day.date.getDate() + '-' + (day.date.getMonth() + 1) + '-' + day.date.getFullYear();
              console.log('eventDate',eventDate);
              if (this.eventList[eventDate]) {
                day.events = this.eventList[eventDate];
                day.meta = this.eventList[eventDate][0].title
                day.badgeTotal = this.eventList[eventDate].length;
              }
            }
          });
        }
      }, 3000);
  }

  // RefreshShift() {
  //   this.viewDateDetails = false;
  // }

  // dayClicked(day: CalendarMonthViewDay): void {
  //   console.log("dayClicked day", day);
  // }

  dayClicked(day: CalendarMonthViewDay): void {
    console.log("day:",day);
    // this.viewDateDetails = true;
    // this.selectedShiftList = [];
    // day.cssClass = 'bg-pink';
    this.currentDate = day.date;
    let eventDate = day.date.getDate() + '-' + (day.date.getMonth() + 1) + '-' + day.date.getFullYear();
    let eventlist = this.eventList[eventDate]
    // let eventListDate = this.eventList[eventDate][0].start.getDate() + '-' + (this.eventList[eventDate][0].start.getMonth() + 1) + '-' + this.eventList[eventDate][0].start.getFullYear();
    console.log(this.eventList[eventDate],eventDate);
    if(eventlist != undefined) {
      if(this.eventList[eventDate][0].start.getDate() + '-' + (this.eventList[eventDate][0].start.getMonth() + 1) + '-' + this.eventList[eventDate][0].start.getFullYear() == eventDate) {
        localStorage.setItem('jobDetailsData', JSON.stringify({
          employerId: this.jobDetails.employerId,
          jobId: this.jobDetails.jobId,  //toString()
          roleId: this.jobDetails.roleId,
        }));
        this.router.navigate(['/candidate/job/job-details']);
      }
    }
    // if (day.events.length) {
    //   for (var i = 0; i < day.events.length; i++) {
    //     for (var j = 0; j < this.allShiftList.length; j++) {
    //       //show shifts when events are present, accordingly
    //       if (day.events[i].title === this.allShiftList[j].shift) {
    //         this.selectedShiftList.push(this.allShiftList[j])
    //       }
    //     }
    //   }
    // }
  }

}

