import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobRoutingModule } from './job-routing.module';
import { SharedModule } from 'src/app/modules/shared.module';
import {
  SearchComponent,
  SearchResultComponent,
  AppointmentsComponent,
  AttendenceComponent,
  TrackingComponent,
  OnsiteComponent,
  OffsiteComponent,
  // JobHistoryComponent,
  JobDetailsComponent,
  JobInfoComponent,
  LocationComponent
} from './';
import { CalendarModule } from 'angular-calendar';

@NgModule({
  declarations: [
    SearchComponent,
    SearchResultComponent,
    AppointmentsComponent,
    AttendenceComponent,
    TrackingComponent,
    OnsiteComponent,
    OffsiteComponent,
    // JobHistoryComponent,
    JobDetailsComponent,
    JobInfoComponent,
    LocationComponent
  ],
  imports: [
    CommonModule,
    JobRoutingModule,
    SharedModule,
    CalendarModule
  ]
})
export class JobModule { }
