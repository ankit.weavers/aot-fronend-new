import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  SearchComponent,
  SearchResultComponent,
  AppointmentsComponent,
  AttendenceComponent,
  TrackingComponent,
  OnsiteComponent,
  OffsiteComponent,
  // JobHistoryComponent,
  JobDetailsComponent,
  JobInfoComponent,
  LocationComponent
} from './';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'search',
    component: SearchComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Job Search'
    }
  },
  {
    path: 'search-result',
    component: SearchResultComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Search Result'
    }
  },
  {
    path: 'appointments',
    component: AppointmentsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Appointments'
    }
  },
  {
    path: 'attendance',
    component: AttendenceComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Attendance'
    }
  },
  {
    path: 'tracking',
    component: TrackingComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Tracking'
    },
    children: [
      /**
       * If url param not needed then turn this on
       */
      // {
      //   path: '',
      //   component: OnsiteComponent,
      //   data: {
      //     title: 'Onsite'
      //   }
      // },
      /**
       * If url param not needed then turn this off
       */
      {
        path: '',
        redirectTo: 'onsite',
        pathMatch: 'full'
      },
      {
        path: 'onsite',
        component: OnsiteComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Onsite'
        }
      },
      {
        path: 'offsite',
        component: OffsiteComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Offsite'
        }
      }
    ]
  },
  // {
  //   path: 'job-history',
  //   component: JobHistoryComponent,
  //   data: {
  //     hasBackBtn: false,
  //     hasHamburger: true,
  //     isEditable: [],
  //     title: 'Job History'
  //   }
  // },
  {
    path: 'job-details',
    component: JobDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Details'
    },
    children: [

      /**
       * If url param not needed then turn this on
       */
      // {
      //   path: '',
      //   component: JobInfoComponent,
      //   data: {
      //     title: 'Job Info'
      //   }
      // },
      /**
       * If url param not needed then turn this off
       */
      {
        path: '',
        redirectTo: 'job-info',
        pathMatch: 'full'
      },
      {
        path: 'job-info',
        component: JobInfoComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Job Info'
        }
      },
      {
        path: 'location',
        component: LocationComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Location'
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }
