import { Component, OnInit } from '@angular/core';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'candidate-job-history',
  templateUrl: './job-history.component.html',
  styleUrls: ['./job-history.component.scss']
})
export class JobHistoryComponent implements OnInit {
  bsConfig: Partial<BsDatepickerConfig>;

  constructor() { }

  ngOnInit() {
  }

}
