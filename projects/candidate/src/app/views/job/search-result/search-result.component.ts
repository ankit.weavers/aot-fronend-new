import { Component, OnInit, ViewChild, ElementRef, OnDestroy, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
// AGM
import { MapsAPILoader } from '@agm/core';
// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar } from '@angular/material';
import { CandidateJobService } from '../../../services';
declare var google: any;

@Component({
  selector: 'candidate-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  userData: any;
  sessionData: any;
  searchedData: any;
  checkValue:boolean;
  dataNearByFetching = false;
  dataOutSideFetching = false;

  maxPaginationSize:number = 4;

  nearbyPageNo: number = 1;
  nearbyPerPage: number = 2;
  nearbyTotal: number = 0;
  nearbyTotalNoOfPages: number = 0;

  outsidePageNo: number = 1;
  outsidePerPage: number = 2;
  outsideTotal: number = 0;
  outsideTotalNoOfPages: number = 0;
  profileCheck:any
  messages:any
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    // public globalactionservice: GlobalActionsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public candidateJobService: CandidateJobService
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
    this.sessionData = JSON.parse(localStorage.getItem('jobSearchData'));
    const searchData = this.sessionData;
    this.sessionData.candidateId = this.userData.candidateId;
    console.log(this.sessionData);
    this.mapsAPILoader.load().then(() => {
      this.dataNearByFetching = true;
      this.dataOutSideFetching = true;
      this.searchJob(this.sessionData);
    });
    this.checkCandidate();
  }

  ngOnDestroy() {
    // UnSubscribe Subscriptions
    // localStorage.removeItem('jobSearchData');
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  // check candidate profile //
  checkCandidate() {
    this.candidateJobService._checkCandidate({candidateId:this.userData.candidateId})
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result=>{
      if(result && !result['isError']) {
      this.profileCheck = result['details'].isAuthenticated
      this.messages = result['details'].message

      }
      else {
        this.snackBar.open(result['message'],'Got it!',{
            
        })
        this.messages = result['details'].message
      }
    })
  }

  // Get Job
  searchJob(searchdData) {
    searchdData.nearbypageno = this.nearbyPageNo;
    searchdData.nearbyperpage = this.nearbyPerPage;
    searchdData.outsidepageno = this.outsidePageNo;
    searchdData.outsideperpage = this.outsidePerPage;
    this.candidateJobService.getSearchedJob(searchdData)
    .pipe(takeUntil(this.onDestroyUnSubscribe)).subscribe(
      response => {
        console.log(response);
        if (response && !response['isError']) {
          this.searchedData = response; // All Jobs
          
          this.nearbyTotalNoOfPages = this.searchedData.nearByJobs ? this.searchedData.nearByJobs.noofpage : 1;
          this.nearbyTotal = this.searchedData.nearByJobs ? this.searchedData.nearByJobs.total : 0;
          
          this.outsideTotalNoOfPages = this.searchedData.outSideJobs ? this.searchedData.outSideJobs.noofpage : 1;
          this.outsideTotal = this.searchedData.outSideJobs ? this.searchedData.outSideJobs.total : 0;
          
          this.dataNearByFetching = false;
          this.dataOutSideFetching = false;

        } else {
          this.dataNearByFetching = false;
          this.dataOutSideFetching = false;
          this.snackBar.open('No data found! Please try again', 'Got it!', {
           
          });
        }
      },
      error => {
        this.dataNearByFetching = false;
        this.dataOutSideFetching = false;
        this.snackBar.open('Please Check Your Network Connection and try again.', 'Got it!', {
          
        });
      }
    );
  }

  getPaginatedDataNearby(event) {
    this.nearbyPageNo = event.page;
    this.dataNearByFetching = true;
    console.log(event);
    this.searchedData.nearByJobs.results = [];
    this.searchJob(this.sessionData);
  }

  getPaginatedDataOutside(event) {
    this.outsidePageNo = event.page;
    this.dataOutSideFetching = true;
    console.log(event);
    this.searchedData.outSideJobs.results = [];
    this.searchJob(this.sessionData);
  }

  stricktoApply(data) {
    this.candidateJobService.checkaotOfferAll({candidateId:this.userData.candidateId,jobId:data.jobId.toString(),roleId:data.jobDetails.roleWiseAction.roleId})
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result=>{
      if(result && !result['isError']) {
        this.checkValue = result['details'].isCandidateAvailable;
        console.log('checkvalue1',this.checkValue);
        this.applyJob(data);

      }
    })
  }


  goToJobDetails(data) {
    localStorage.setItem('jobDetailsData', JSON.stringify({
      roleId: data.jobDetails.roleWiseAction.roleId,
      jobId: data.jobId.toString(),
      employerId: data.employerId
    }));
    this.router.navigate(['/candidate/job/job-details']);
    console.log(data);
  }
  applyJob(data) {
    // this.stricktoApply(data.jobId.toString())
    // setTimeout(()=>{
      console.log('checkvalue25',this.checkValue);
    if(this.profileCheck == "True") {
      if(this.checkValue) {
        const applyData = {
          candidateId: this.userData.candidateId,
          roleId: data.jobDetails.roleWiseAction.roleId,
          jobId: data.jobId.toString(),
          employerId: data.employerId
        };
        console.log(applyData);
        this.candidateJobService.applyJobById(applyData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              data.applied = true;
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
              this.searchJob(this.sessionData);
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
      }
      else {
        this.snackBar.open('you are not available for this job','Got it!',{

        })
      }
    }
    else {
      this.snackBar.open(this.messages,'Got it!',{

      })
    }
    // },2000)
 
  }
  // setting current map position
  getFormattedJobLocation(data) {

    if (navigator.geolocation) {
      const geocoder = new google.maps.Geocoder();
      // console.log(data.location.coordinates[1], data.location.coordinates[0]);
      const latlng = new google.maps.LatLng(data.location.coordinates[1], data.location.coordinates[0]);
      // console.log(latlng);
      geocoder.geocode({ 'location': latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          // let rsltAdrComponent = results[0].address_components;
          // let resultLength = rsltAdrComponent.length;
          if (results[0] != null) {
            // let add = results[0].formatted_address;
            console.log(results[0].formatted_address);
            data.formattedAddress = results[0].formatted_address;
            console.log(this.searchedData);
            // let value = add.split(",");
            // let count = value.length;
            // this.address = rsltAdrComponent[resultLength - 8].short_name;
          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

}
