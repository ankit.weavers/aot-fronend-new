import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
// AGM
import { MapsAPILoader } from '@agm/core';
// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GlobalActionsService } from 'src/app/services';
import { Erros } from 'src/app/models';
import { MatSnackBar } from '@angular/material';
import { CandidateJobService } from '../../../services';
import * as moment from 'moment';
declare var google: any;

@Component({
  selector: 'candidate-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  industryList = [];
  jobRoleList = [];
  skillList = [];
  candidateLat: any;
  candidateLong: any;
  public zoom: number;
  locationName: any;
  formattedSearchData: any = {};
  jobTypeList = [
    { value: 'FULLTIME', name: 'Full Time' },
    { value: 'PARTTIME', name: 'Part Time' },
    { value: 'CASUSAL', name: 'Casual' },
    { value: 'TEMPORARY', name: 'Temporary' }
  ];
  payTypeList = [
    {value: 'Hourly', name: 'Hourly'},
    // {value: 'MONTHLY', name: 'Monthly'}
  ];
  userData: any;
  industryName: '';
  public errors: any = Erros;
  public jobSearchForm: FormGroup;
  @ViewChild('locationInput') public searchElementRef: ElementRef;
  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public globalactionservice: GlobalActionsService,
    public candidateJobService: CandidateJobService
  ) { }

  ngOnInit() {
    // const getValue = this.candidateJobService.getJobDetails;
    this.jobSearchForm = this.formBuilder.group({
      industryId: [''],
      roleId: [''],
      skillValues: [''],
      location: ['', Validators.required],
      jobType: [''],
      payType: [''],
      duration: ['', Validators.pattern(/^[1-9-]+[0-9]*$/)],
      hours: ['', Validators.pattern(/^[1-9-]+[0-9]*$/)],
      presentDate:[moment(new Date()).format('MM/DD/YYYY')],
      presentTime:[String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes())],
      localTimeZone:[Intl.DateTimeFormat().resolvedOptions().timeZone]
    });
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
    // this.sessionData = JSON.parse(localStorage.getItem('jobSearchData'));
    this.candidateLat = this.userData.geolocation.coordinates ? this.userData.geolocation.coordinates[1] : '';
    this.candidateLong = this.userData.geolocation.coordinates ? this.userData.geolocation.coordinates[0] : '';

    this.getIndustryList();
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      if (this.userData.geolocation.coordinates) {
        this.getFormattedJobLocation();
      }
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: []
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          // let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          const place: typeof google.maps.places.PlaceResult = autocomplete.getPlace();
          //  let place = autocomplete.getPlace();
          if (!place.place_id) {
            window.alert('Please select an option from the dropdown list.');
            return;
          }
          this.locationName = place.name;
          console.log(this.locationName);
          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // set latitude, longitude and zoom
          this.candidateLat = place.geometry.location.lat();
          this.candidateLong = place.geometry.location.lng();
          console.log(this.candidateLat, this.candidateLong);
          this.zoom = 12;
        });
      });
    });
  }

  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }
  // setting current map position
  getFormattedJobLocation() {

    if (navigator.geolocation) {
      const geocoder = new google.maps.Geocoder();
      // console.log(data.location.coordinates[1], data.location.coordinates[0]);
      const latlng = new google.maps.LatLng(this.userData.geolocation.coordinates[1], this.userData.geolocation.coordinates[0]);
      // console.log(latlng);
      geocoder.geocode({ 'location': latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          // let rsltAdrComponent = results[0].address_components;
          // let resultLength = rsltAdrComponent.length;
          if (results[0] != null) {
            // let add = results[0].formatted_address;
            console.log(results[0].formatted_address);
            this.userData.formattedAddress = results[0].formatted_address;
            this.locationName = this.userData.formattedAddress;
            this.jobSearchForm.patchValue({
              location: this.userData.formattedAddress
            });
            // let value = add.split(",");
            // let count = value.length;
            //  console.log("locationInfo", this.locationInfo)
            // this.address = rsltAdrComponent[resultLength - 8].short_name;
          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

  resetValue() {
    this.locationName = '';
    this.candidateLat = '';
    this.candidateLong = '';
  }
  // get Industry List
  getIndustryList() {
    // get industries
    this.globalactionservice.getIndustry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.industryList = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  // get Job Role List
  getJobRoleList(industryId) {
    this.globalactionservice.getRole({industryId :industryId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobRoleList = result['details'];

            console.log("jobRoleList" , this.jobRoleList);

          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  // get Skill List
  // getSkillsList(roleIds) {
  //   this.skillList = [];
  //   this.globalactionservice.getJobSkillById({ jobRole: roleIds })
  //     .pipe(takeUntil(this.onDestroyUnSubscribe))
  //     .subscribe(
  //       result => {
  //         if (result && !result['isError']) {

  //           result['details'].map(role => {
  //             this.skillList = [...this.skillList, ...role.details];
  //           });
  //         } else {
  //           this.snackBar.open(result['message'], 'Got it!', {
             
  //           });
  //         }
  //       },
  //       error => {
  //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
  //         });
  //       }
  //     );
  // }

  getSkillsList(roleIds) {
    this.skillList = [];
    this.globalactionservice.getJobuniqueSkillById({ jobRole: roleIds })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {

            this.skillList = result['details']
           
           
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  // industry selected
  selectJobIndustry(industryId) {
    console.log("industryId",industryId);
    this.jobRoleList = [];
    this.skillList = [];
    this.jobSearchForm.controls.roleId.setValue('');  // reseting job role on changing job industry
    this.jobSearchForm.controls.skillValues.setValue('');
      // reseting job role on changing job industry
    if (industryId) {
      this.getJobRoleList(industryId);
    }
  }

  getSkills(event) {
    console.log(event, this.jobSearchForm.value.roleId);
    this.getSkillsList(this.jobSearchForm.value.roleId);
  }

  searchJob() {
    console.log(this.jobSearchForm, 'this.formattedSearchData', this.formattedSearchData);
    this.formattedSearchData = {};
    if (this.jobSearchForm.valid) {
      for (const valueItem in this.jobSearchForm.value) {
        if (
          (
            (typeof(this.jobSearchForm.value[valueItem]) !== 'object') &&
            (valueItem === 'location') &&
            this.jobSearchForm.value[valueItem] &&
            this.locationName
          )
        ) {
          this.formattedSearchData.candidateLat = this.candidateLat;
          this.formattedSearchData.candidateLong = this.candidateLong;
          this.formattedSearchData.locationName = this.locationName;
        } else if (
          ((typeof(this.jobSearchForm.value[valueItem]) !== 'object') && this.jobSearchForm.value[valueItem]) ||
          ((typeof(this.jobSearchForm.value[valueItem]) === 'object') && typeof(this.jobSearchForm.value[valueItem]).length)
        ) {
          console.log("valueItem", this.jobSearchForm.value[valueItem]);
          const element = this.jobSearchForm.value[valueItem];
          this.formattedSearchData[valueItem] = this.jobSearchForm.value[valueItem];
        }
      }
      if (Object.keys(this.formattedSearchData).length) {
        console.log(this.formattedSearchData);
        localStorage.setItem('jobSearchData', JSON.stringify(this.formattedSearchData));
        if(this.candidateLat && this.candidateLong) {
        this.router.navigate(['/candidate/job/search-result']);

        }
        else {
          this.snackBar.open('Please select an option from the dropdown list of location','Got it!', {

          });
        }
      }

    } else {
      this.snackBar.open('Please provide your location', 'Got it!', {
       
      });
    }
  }

}
