import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'candidate-introductory',
  templateUrl: './introductory.component.html',
  styleUrls: ['./introductory.component.scss']
})
export class IntroductoryComponent implements OnInit {

  customOptions: any = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    autoplay:true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }
  constructor() { }

  ngOnInit() {
  }

}
