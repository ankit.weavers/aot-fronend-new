import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  ProfileComponent,
  EditProfileComponent,
  PersonalDetailsComponent,
  WorkExperienceComponent,
  LanguagesComponent,
  ProximityComponent,
  JobRolesComponent,
  UploadFilesComponent,
  AvailabilityComponent,
  DaysOffComponent,
  ChangePasswordComponent,
  NotificationsComponent,
  MessagesComponent,
  RatingComponent,
  ContactListComponent
} from '.';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: ['profile/edit'],
      title: 'Profile'
    }
  },
  {
    path: 'edit',
    component: EditProfileComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Edit Profile'
    }
  },
  {
    path: 'edit/personal-details',
    component: PersonalDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Personal Details'
    }
  },
  {
    path: 'edit/work-experience',
    component: WorkExperienceComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Work Experience'
    }
  },
  {
    path: 'edit/languages',
    component: LanguagesComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Languages'
    }
  },
  {
    path: 'edit/proximity',
    component: ProximityComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Proximity'
    }
  },
  {
    path: 'edit/job-roles',
    component: JobRolesComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Roles'
    }
  },
  {
    path: 'edit/upload-files',
    component: UploadFilesComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Upload Files'
    }
  },
  {
    path: 'edit/availability',
    component: AvailabilityComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Availability'
    }
  },
  {
    path: 'edit/days-off',
    component: DaysOffComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Days Off'
    }
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Change Password'
    }
  },
  {
    path: 'notifications',
    component: NotificationsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Notifications'
    }
  },
  {
    path: 'contacts',
    component: ContactListComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Contacts'
    }
  },
  {
    path: 'messages',
    component: MessagesComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Messages'
    }
  },
  {
    path: 'ratings',
    component: RatingComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Ratings'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
