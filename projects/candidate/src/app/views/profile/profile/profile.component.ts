import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService } from '../../../services';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'candidate-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();

  public personalDetailsList: any;
  workExpList = [];
  languageList = [];
  proximityList: any;
  jobRoles = [];
  industryName = [];
  uploadedFilesList: any;
  availability = [];
  daysOffList = [];
  skills = [];
  rating:any;
  public stars: any[] = new Array(5);
  showRole:boolean= false;
  showArraow: boolean= false;
  showapplies:boolean = false;
  showArrowApllied:boolean = false;

  constructor(
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
    private router: Router,
    private fb: FormBuilder, ) { }

  ngOnInit() {
    this.getProfileDetails();
  }

  // get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result['details'] && !result['isError']) {
            //set value into form
            if (result['details'].dob ) {
              let date = result['details'].dob.split("T")[0].split("-");
              let day = date[2];
              let month = date[1];
              let year = date[0];
              this.personalDetailsList = {
                email: result['details'].EmailId,
                // dob: day + "-" + month + "-" + year,
                dob: result['details'].dob,
                address: result['details'].location.address,
                postCode: result['details'].location.postCode,
                city: result['details'].location.city.name,
                country: result['details'].location.country.name,
                secondaryEmailId: result['details'].secondaryEmailId,
                kinContactNo: result['details']['kinDetails'].kinContactNo,
                kinName: result['details'].kinDetails.kinName,
                relationWithKin: result['details'].kinDetails.relationWithKin,
                contactNo:result['details'].contactNo,
                profilePic:result['details'].profilePic,
                FullName: result['details'].fname + " " + result['details'].lname,
                email_address: result['details'].email_address,
                home_address: result['details'].home_address
              }
            }

            if (result['details'].workExperience) {
              this.workExpList = result['details'].workExperience.experiences
            }
            if (result['details'].rating) {
              this.rating = result['details'].rating
              console.log("rating",this.rating);
            }

            if (result['details'].language) {
              this.languageList = result['details'].language
            }

            if (result['details'].geolocation && result['details'].distance) {
              this.proximityList = {
                distance: (result['details'].distance)/1609.34,
                geolocation: result['details'].geolocation
              }
            }

            if (result['details'].payloadSkill) {
              this.jobRoles = result['details'].payloadSkill.map(({ jobRoleName }) => jobRoleName);
              console.log("jobRoles",this.jobRoles);
            }
            if(result['details'].payloadSkill){
              this.skills = result['details'].payloadSkill.map(({ skillDetails }) => skillDetails);
              console.log("skills",this.skills);
            }
            if(result['details'].payloadSkill){
              this.industryName = result['details'].payloadSkill
              console.log("skills",this.skills);
            }

            if (result['details'].docs || result['details'].cv || result['details'].video) {
              this.uploadedFilesList = {
                cv: result['details'].cv,
                docs: result['details'].docs,
                video: result['details'].video
              }
            }

            if (result['details'].availability) {
              this.availability = result['details'].availability
            }

            if (result['details'].dayoff) {
              this.daysOffList = result['details'].dayoff
            }

         
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  // findSkill(skillDetails) {
  //   // console.log(skillDetails);
  //   // let comma;
  //   for(let i = 0;i<skillDetails.length;i++) {
  //     // if(i>0) {
  //     //   comma = ',';
  //     // }
  //     // let value = i>0 ? skillDetails[i].skillName + ',' :  skillDetails[i].skillName
  //     // console.log()
  //     return skillDetails[i].skillName;
  //   }
  // }

    // DOWN ARROW //
    showMoreRole() {
      this.showRole = true;
      this.showArraow = true;
    }
  
    // UP ARROW //
    showLessRole() {
      this.showRole = false;
      this.showArraow = false;
    }

        // DOWN ARROW //
        showapplidMoreRole() {
          this.showapplies = true;
          this.showArrowApllied = true;
        }
      
        // UP ARROW //
        showappliedLessRole() {
          this.showapplies = false;
          this.showArrowApllied = false;
        }

}