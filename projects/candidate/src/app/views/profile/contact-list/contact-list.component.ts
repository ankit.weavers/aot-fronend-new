import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'candidate-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  candidateId = JSON.parse(localStorage.getItem('currentUser')).candidateId;
  contactList = [];

  constructor(private firestoreDB: AngularFirestore, private router: Router) { }

  ngOnInit() { this.getEmployerContacts(); }

  getEmployerContacts() {
    this.firestoreDB.collection('chatrooms', ref => ref.where('candidateId', '==', this.candidateId))
      .snapshotChanges().subscribe(
        docSnapshot => {
          this.contactList = [];
          docSnapshot.map(element => {
            this.contactList.push(element.payload.doc.data())
          });
          console.log("this.contactList", this.contactList);
        }
      )
  }

  //open chat room
  openChat(employerId, chatroomId) {
    let jobDetailsData = {
      employerId: employerId,
      chatroomId: chatroomId
    }
    localStorage.setItem('jobDetailsData', JSON.stringify(jobDetailsData));
    this.router.navigate(['/candidate/profile/messages'])
  }

}
