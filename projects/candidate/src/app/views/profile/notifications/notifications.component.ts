import { Component, OnInit } from '@angular/core';
import { CandidateJobService } from '../../../services';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'candidate-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  private userData =JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allNotification:any;
  totalItems;
  itemsPerPage;
  totalNoOfPages;
  pageNo;
  dataFetching =false;
  // noti=[];
  

  constructor(    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
this.notifications();
  }
  notifications(){
    let payload={
      candidateId:this.userData.candidateId,
      pageno: this.pageNo,
      perpage:this.itemsPerPage
    }
this.candidatejobservice.getNotificationDetails(payload)
.pipe(takeUntil(this.onDestroyUnSubscribe))
.subscribe(
  result => {
    if (result && !result['isError'] && result['detail'][0]) {
  
    this.allNotification =result['detail'][0];
    // if(result['details']==null){
    //   this.allNotification={};
    // }
    let obj =result['detail'][0];
    if(Object.entries(obj).length !==0){
      this.totalItems = obj.total ? obj.total : 0;
      this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
      this.dataFetching = false;
    }

     
      
    }else {
      this.dataFetching = false;
      this.snackBar.open('No data found! Please try again', 'Got it!', {
       
      });
    }
    error => {
      this.dataFetching = false;
      this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
       
      });
    }
    }

  
  );
} 
deleteNotifications(notificationId){
  let payload={
    candidateId:this.userData.candidateId,
    notificationId:notificationId
  }
this.candidatejobservice.deleteNotificationDetails(payload)
.pipe(takeUntil(this.onDestroyUnSubscribe))
.subscribe(
result => {
  if (result && !result['isError']){
    this.snackBar.open('Notification successfuly deleted','Got it!',{
     
    });
    this.ngOnInit();
  }
}

  
);
}
getPaginateData(event){
  this.pageNo = event.page;
  this.allNotification = [];
  this.dataFetching = true;
  this.notifications();
}
} 

