import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatNativeDateModule } from '@angular/material';
import { MaterialModule } from 'src/app/modules/material.module';

import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from 'src/app/modules/shared.module';

import { AgmCoreModule } from '@agm/core';

import {
  ProfileComponent,
  EditProfileComponent,
  PersonalDetailsComponent,
  WorkExperienceComponent,
  LanguagesComponent,
  ProximityComponent,
  JobRolesComponent,
  UploadFilesComponent,
  AvailabilityComponent,
  DaysOffComponent,
  ChangePasswordComponent,
  NotificationsComponent,
  MessagesComponent,
  RatingComponent,
  ContactListComponent
} from '.';

@NgModule({
  declarations: [
    ProfileComponent,
    EditProfileComponent,
    PersonalDetailsComponent,
    WorkExperienceComponent,
    LanguagesComponent,
    ProximityComponent,
    JobRolesComponent,
    UploadFilesComponent,
    AvailabilityComponent,
    DaysOffComponent,
    ChangePasswordComponent,
    NotificationsComponent,
    MessagesComponent,
    RatingComponent,
    ContactListComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MatNativeDateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjOIcLbaHp8ixAIs1qlWkZIAu2vHb0uGk',
      libraries: ["places"]
    })
  ]
})
export class ProfileModule { }
