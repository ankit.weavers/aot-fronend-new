import { Component, OnInit } from '@angular/core';
import { CandidateJobService } from '../../../services';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'candidate-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})

export class RatingComponent implements OnInit {

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allRatings:any;
  public stars: any[] = new Array(5);
  userData: any;
  sessionData: any;
  searchedData: any;
  pageNo: number = 1;
  perPage: number = 5;
  total: number = 0;
  totalNoOfPages: number = 0;
  dataFetching = false;
  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }


  ngOnInit() {
   // this.dataFetching = true;
   this.dataFetching = false;
    this.getAllRatingList();
  }

  // get Ratings
  getAllRatingList() {

    let ratingPayload={
      candidateId: this.userDetails.candidateId,
      pageno: this.pageNo,
      perpage: this.perPage
    }

    this.candidatejobservice.getAllRatingDetails(ratingPayload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            if (result['details']) {
              this.allRatings = result['details'].allRatings;
              let obj = result['details'].allRatings;
              if (Object.entries(obj).length !== 0) {
                this.total = obj.total ? obj.total : 0;
                this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }
            }
          }else {
            this.dataFetching = false;
            this.snackBar.open('No data found! Please try again', 'Got it!', {
             
            });
          }
        },
        error => {
          this.dataFetching = false;
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
  getPaginatedData(event) {

    this.pageNo = event.page;
    this.dataFetching = true;
    this.allRatings = [];
    this.getAllRatingList();
  }
}
