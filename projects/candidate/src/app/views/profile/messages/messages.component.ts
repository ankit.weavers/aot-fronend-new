import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { Employer, Candidate, Chatroom } from 'src/app/models';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'employer-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})

/* check the chatroom for the employer and candidate pair, if no chatroomId is found,
   create "employer" and "candidate" account in firestore and insert data accordingly,
   if yes , update the message list  */

export class MessagesComponent implements OnInit, OnDestroy {

  employerId;
  candidateId;
  currentChatroomId;
  messageList = [];
  textMessageToSend = new FormControl('');
  employerUnreadCount: number = 0;
  employerName;
  employerProfilePic;

  //file upload variables
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  isUpload: Boolean = false;
  fileType;
  uploadedFileURL;

  constructor(private firestoreDB: AngularFirestore, private firestoreStorage: AngularFireStorage, public snackBar: MatSnackBar, private router: Router) {

    this.candidateId = JSON.parse(localStorage.getItem('currentUser')).candidateId;
    this.employerId = JSON.parse(localStorage.getItem('jobDetailsData')).employerId;
    this.currentChatroomId = JSON.parse(localStorage.getItem('jobDetailsData')).chatroomId;
    this.getChatroomDetails();  //get chat room details for matched employer-candidate pair
  }

  ngOnInit() {
    //reset candidate unread counter
    this.firestoreDB.firestore.collection('chatrooms').doc(this.currentChatroomId).update({ candidateUnreadCount: 0 });
  }

  //get chatroom details
  getChatroomDetails() {

    this.messageList = [];

    //get unread count details
    this.firestoreDB.collection('chatrooms').doc(this.currentChatroomId).snapshotChanges().subscribe(
      details => {
        if (details.payload.exists) {
          let allDetails: any = details.payload.data();
          this.employerName = allDetails.employerName;
          this.employerProfilePic = allDetails.employerProfilePic;
          this.employerUnreadCount = allDetails.employerUnreadCount;
        }
      });

    //get messages
    this.firestoreDB.collection('chatrooms').doc(this.currentChatroomId).collection('messages').snapshotChanges()
      .subscribe(
        success => {

          if (success) {
            this.messageList = [];
            success.map(res => {
              this.messageList.push(res.payload.doc.data())
            });
            this.isUpload = false;
          }
        },
        error => {
          this.snackBar.open('Something went wrong, please refresh', 'Got it!', {
         
          });
        });
  }

  //upload file
  upload(event) {

    this.textMessageToSend.reset();

    if (event.target.files[0]) {

      let filetype = event.target.files[0].type;

      if (filetype === 'image/jpeg' || filetype === 'image/jpg' || filetype === 'image/png') {
        this.fileType = 'image';
        this.isUpload = true;
      }
      else if (filetype === 'text/plain' || filetype === 'application/pdf') {
        this.fileType = 'document';
        this.isUpload = true;
      }
      else if (event.target.files[0].size < 5 * 1024 * 1024) {
        this.snackBar.open('Please upload file size up to 5MB', 'Got it!', {
         
        });
        this.isUpload = false;
      }
      else {
        this.snackBar.open('Please Upload Files With .jpeg , .jpg , .png , .txt , .pdf ', 'Got it!', {
         
        });
        this.isUpload = false;
      }

      if (this.isUpload) {
        const randomId = Math.random().toString(36).substring(2);// create a random id
        this.ref = this.firestoreStorage.ref(randomId);// create a reference to the storage bucket location
        this.task = this.ref.put(event.target.files[0]);// the put method creates an AngularFireUploadTask and kicks off the upload
        this.uploadProgress = this.task.percentageChanges();
        this.task.snapshotChanges().pipe(
          finalize(() => {
            let downloadedURL = this.ref.getDownloadURL();
            downloadedURL.subscribe(url => {
              // let downloadURL: Observable<string> = url;
              this.uploadedFileURL = url;
              this.sendMessage();
            });
          })
        ).subscribe();
      }
    }

  }

  //create chat room
  // createChatroom() {
  //   let chatroom: Chatroom = {
  //     chatroomId: this.currentChatroomId,
  //     employerId: this.employerId,
  //     employerName: "string",
  //     employerProfilePic: "string",
  //     candidateId: this.candidateId,
  //     candidateName: "string",
  //     candidateProfilePic: JSON.parse(localStorage.getItem('currentUser')).profilePic,
  //     candidateUnreadCount: 0,
  //     employerUnreadCount: 0,
  //     lastMessageEntry: '',
  //   }
  //   return new Promise<any>((resolve, reject) => {
  //     this.firestoreDB.collection('chatrooms').doc(this.currentChatroomId).set(chatroom);
  //     resolve();
  //   });
  // }

  //send message to employer
  sendMessage() {

    let toUploadfileType = {};
    let doUpload: Boolean = false;

    //check if valid message exists or not 
    if (this.textMessageToSend.value) {// && typeof this.uploadedFileURL == 'undefined'
      toUploadfileType = {
        text: this.textMessageToSend.value,
      }
      doUpload = true;
    }
    else if (!this.textMessageToSend.value && this.uploadedFileURL && this.fileType === 'image') {
      toUploadfileType = {
        imageUrl: this.uploadedFileURL,
      }
      doUpload = true;
    }
    else if (!this.textMessageToSend.value && this.uploadedFileURL && this.fileType === 'document') {
      toUploadfileType = {
        attachmentUrl: this.uploadedFileURL,
      }
      doUpload = true;
    }
    else {
      this.snackBar.open('Please Write A Message !! ', 'Got it!', {
       
      });
    }

    if (doUpload) {

      this.textMessageToSend.reset();
      this.uploadedFileURL = '';

      let docId = Math.floor(Date.now() / 1000);
      let messageTime = Date.now();
      let messageObject = {
        messageId: docId,
        senderId: this.candidateId,
        receiverId: this.employerId,
        // isRead: false,
        createdAt: messageTime,
        ...toUploadfileType
      }

      //increase unread counter for employer
      this.employerUnreadCount = this.employerUnreadCount + 1;

      // save & update message in chatroom
      this.firestoreDB.firestore.collection('chatrooms').doc(this.currentChatroomId).collection('messages').doc(docId.toString()).set(messageObject)
        .then(() => {
          //update the unread counter for employer
          this.firestoreDB.firestore.collection('chatrooms').doc(this.currentChatroomId).update({
            employerUnreadCount: this.employerUnreadCount,
            lastMessageEntry: messageTime
          })
        })
        .catch(error => {
          this.snackBar.open('Something went wrong. Please try again  ', 'Got it!', {
            
          });
        });
    }

  }

  ngOnDestroy() {
    //reset employer unread counter
    this.firestoreDB.firestore.collection('chatrooms').doc(this.currentChatroomId).update({ candidateUnreadCount: 0 });
  }

}
