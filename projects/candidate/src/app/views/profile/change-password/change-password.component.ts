import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Validators, FormBuilder } from '@angular/forms';
import { PasswordValidator } from 'src/app/validators';
import { Erros } from 'src/app/models';
import { Router } from '@angular/router';
import { CandidateAuthenticationService } from '../../../services';
import { MatSnackBar } from '@angular/material';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'candidate-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})

export class ChangePasswordComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  private storedDetails: any;
  public changePasswordForm = this.formBuilder.group({
    oldPassword: ['', Validators.required],
    newPassword: ['', [
      Validators.required,
      Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
    ]
    ],
    confirmPassword: ['', Validators.required]
  }, { validator: PasswordValidator('newPassword', 'confirmPassword') });
  public errors: any = Erros;
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private candidateauthenticationService: CandidateAuthenticationService,
    public snackBar: MatSnackBar
  ) {
    this.storedDetails = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  changePassword() {
    if (this.changePasswordForm.valid) {
      //   this.changePasswordForm.value['candidateId']  = this.storedDetails['candidateId'];

      let changePasswordData = {
        email: this.storedDetails['EmailId'],
        oldpassword: this.changePasswordForm.value.oldPassword,
        newpassword: this.changePasswordForm.value.newPassword,
      }
      this.changePasswordForm.reset();

      this.candidateauthenticationService.changePasswordFromProfile(changePasswordData)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              const messageSuccess = 'Password changed successfully.';
              this.snackBar.open(messageSuccess, 'Got it!', {
              
              });
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    } else {
      this.snackBar.open('Please fill in all the required fields', 'Got it!', {
       
      });
    }
  }

}
