import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService } from '../../../../services';
import { Router } from '@angular/router';
import { Erros } from 'src/app/models';
import { BsDatepickerConfig, BsDaterangepickerConfig } from 'ngx-bootstrap/datepicker';
import * as moment from 'moment';
@Component({
  selector: 'candidate-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {
  bsConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
  personalDetailsEditForm: FormGroup;
  startMinDate = new Date();
  private onDestroyUnSubscribe = new Subject<void>();
  public allCountries: any[] = [];
  public allCities: any[] = [];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  public errors: any = Erros;
  savedVal: '';
  todayDatecolor = 'today';
  data: any;
  countryVariable: any;
  selectedPayballUser: any;
  country: any
  variable: boolean = false;
  cityId;
  public allLanguages: any[] = [];
  // languagesEditForm: FormGroup;
  selectedLanguagesList = [];
  callfunttion: boolean = false;
  parttenn = "^([A-Za-z][A-Ha-hJ-Yj-y]?[0-9][A-Za-z0-9]? ?[0-9][A-Za-z]{2}|[Gg][Ii][Rr] ?0[Aa]{2})$"
  // private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  constructor(
    private fb: FormBuilder,
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.bsConfig.customTodayClass = this.todayDatecolor;
    this.bsConfig.dateInputFormat = 'DD/MM/YYYY';

    this.personalDetailsEditForm = this.fb.group({
      dob: ['', Validators.required],
      address: ['', Validators.required],
      postCode: ['', Validators.required],
      contactNo: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
      city: ['', Validators.required],
      country: ['', Validators.required],
      EmailId: ['', [Validators.required, Validators.email]],
      kinName: ['', Validators.required],
      relationWithKin: ['', Validators.required],
      kinContactNo: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      language: ['', Validators.required],
      email_address: ['', [Validators.required, Validators.email]],
      home_address: ['', Validators.required],

      // lang.rank: ['', Validators.required]
      // langrank2: ['', Validators.required],
      // langrank3: ['', Validators.required],
      // langrank4: ['', Validators.required]


    });
    // this.data=JSON.parse(localStorage.getItem('currentUser'));
    // if(this.data){
    //   this.personalDetailsEditForm.patchValue({
    //     firstname : this.data.fname,
    //     lastname : this.data.lname
    //   })
    // }
    this.getAllLanguages();
    this.getAllCountries();
    this.getProfileDetails();
  }

  //get all countries
  getAllCountries() {

    this.globalActionsService.getCountry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allCountries = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );


  }
  saveLanguages() {
    let allRanksAvailable = true;
    this.selectedLanguagesList.map(item => {
      if (!item.rank) {
        allRanksAvailable = false;
      }
    });
    if (allRanksAvailable) {
      let languageObject = {
        candidateId: this.userDetails.candidateId,
        languages: this.selectedLanguagesList
      };
      console.log("payload", languageObject);
      this.candidateEditProfileService.postLangugageDetails({ languageObj: languageObject })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.callfunttion = false;
              this.saveProfileEditDetails();
              // this.snackBar.open('LANGUAGES EXPERIENCE UPDATED', 'Got it!', {
              //   duration: 3000,
              // });
              //show alert and navigate to candidate profile component
              // this.router.navigate(['/candidate/profile/edit']);
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
            });
          }
        );

    } else {
      this.snackBar.open('Please select your spoken language and rate your level of fluency', 'Got it!', {
      
      });
      return false;
    }
  }
  showSelectedLanguage() {
    const selectedLanguages = this.selectedLanguagesList;
    this.selectedLanguagesList = this.personalDetailsEditForm.value.language;
    if (this.selectedLanguagesList.length > 0) {
      this.callfunttion = true;

    } else {
      this.callfunttion = false;

    }
    selectedLanguages.map(item => {
      this.selectedLanguagesList.filter(filterItem => {
        if ((item.name === filterItem.name) && item.rank) {
          filterItem.rank = item.rank;
        }
      });
    });
    console.log("showSelectedLanguage", selectedLanguages)
  }

  //selectRank for the selected language
  selectRank(index, rankType) {
    this.selectedLanguagesList[index].rank = rankType;
    this.callfunttion = true;
    console.log("selectRank", this.selectedLanguagesList[index].rank);

    // this.saveLanguages();
  }

  //get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {
            this.savedVal = result['details'].language;
            // this.personalDetailsEditForm.get('language').setValue(result['details'].language);
            this.personalDetailsEditForm.patchValue({
              language: result['details'].language
            });
            // this.personalDetailsEditForm.get('langrank1').setValue(result['details'].language);

            this.showSelectedLanguage()
            //set value into form
            if (result['details'].location) {
              this.personalDetailsEditForm.controls.address.setValue(result['details'].location.address)
              this.personalDetailsEditForm.controls.postCode.setValue(result['details'].location.postCode)

              if (result['details'].location.city) {
                this.getAllCities(result['details'].location.city.country_id)

                this.personalDetailsEditForm.controls.city.setValue(result['details'].location.city.name)
                this.cityId = result['details'].location.city.cityId
              }
              if (result['details'].location.country) {
                this.personalDetailsEditForm.controls.country.setValue(result['details'].location.country.countryId)
              }
            }
            if (result['details'].kinDetails) {
              this.personalDetailsEditForm.controls.kinName.setValue(result['details'].kinDetails.kinName)
              this.personalDetailsEditForm.controls.relationWithKin.setValue(result['details'].kinDetails.relationWithKin)
              this.personalDetailsEditForm.controls.kinContactNo.setValue(result['details'].kinDetails.kinContactNo)
            }
            // const date = new Date();
            let detDate ;
            // if(moment(result['details'].dob).format('MM-DD-YYYY')) {
            //   console.log('neww')
            //   detDate = moment(result['details'].dob).format('MM-DD-YYYY')
            // }
          if(result['details'].dob) {
            detDate = moment(result['details'].dob, 'DD/MM/YYYY').format('DD/MM/YYYY')
            console.log('det',detDate);
          
          this.personalDetailsEditForm.controls.dob.setValue(detDate);
          }
            
            this.personalDetailsEditForm.controls.contactNo.setValue(result['details'].contactNo)
            this.personalDetailsEditForm.controls.EmailId.setValue(result['details'].EmailId)
            this.personalDetailsEditForm.controls.firstname.setValue(result['details'].fname)
            this.personalDetailsEditForm.controls.lastname.setValue(result['details'].lname)
            this.personalDetailsEditForm.controls.email_address.setValue(result['details'].email_address)
            this.personalDetailsEditForm.controls.home_address.setValue(result['details'].home_address)


          } else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  //get all cities
  getAllCities(countryId) {
    this.personalDetailsEditForm.controls.city.setValue('');
    // this.allCities = [];
    this.countryVariable = countryId;
    this.globalActionsService.getCityNew({ country_id: countryId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allCities = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }
  selectedCountry() {
    this.country = this.personalDetailsEditForm.value.country;
    console.log('selectedRoles', this.country.toString());
    this.getAllCities(this.country.toString());

  }
  sendInvitation(organization: any) {
    console.log(organization);
    this.variable = true
    // this.selecktPayball.push(organization.email);
    this.selectedPayballUser.push(organization.cityId)
    console.log("email", this.selectedPayballUser);
    // this.personal_referral_code = organization.personal_referral_code;
    // this.firstFormGroup.patchValue({
    //   referralCode : organization.first_name + " " + organization.last_name
    // })
  }
  selectedJobRoles() {
    this.variable = true
    this.selectedPayballUser = this.personalDetailsEditForm.value.city;
    console.log('selectedRoles', this.selectedPayballUser.cityId);

    // console.log('skills',this.skills)
    //  this.getRoleSkills();
  }
  getAllLanguages() {
    this.globalActionsService.getLanguage()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allLanguages = result['details'];
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  getAllCitiess(event: any) {

    if (event.length < 1) {
      this.selectedPayballUser = [];
      console.log("test", this.selectedPayballUser);
      return false;
    }

    let payload = {
      country_id: this.countryVariable,
      name: event
    }

    this.globalActionsService.getCityNew(payload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allCities = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );
  }

  saveProfileEditDetails() {

    if (this.callfunttion == true) {
      this.saveLanguages();
      return false;
    }
    let city;
    if (this.variable) {
      city = this.selectedPayballUser.cityId
    }
    else {
      city = this.cityId
    }
    // let dateValue = this.personalDetailsEditForm.value.dob;
    // let momentValue =moment(dateValue).format('DD-MM-YYYY') ;
    let dataValuess;
    // if (momentValue == "Invalid date") {
    
    //   dataValuess = this.personalDetailsEditForm.value.dob
    // }
  
    // else {
    //   dataValuess = moment(dateValue).format('DD-MM-YYYY')
    // }
    // console.log('moment',momentValue);
    // console.log('dateValue',dateValue);
    // console.log('dataValuess',dataValuess);


    let profileObject = {
      candidateId: this.userDetails.candidateId,
      dob: moment(this.personalDetailsEditForm.value.dob, 'DD/MM/YYYY').format('DD/MM/YYYY'),
      address: this.personalDetailsEditForm.value.address,
      postCode: this.personalDetailsEditForm.value.postCode,
      contactNo: this.personalDetailsEditForm.value.contactNo,
      city: city,
      country: this.personalDetailsEditForm.value.country.toString(),
      EmailId: this.personalDetailsEditForm.value.EmailId,
      kinName: this.personalDetailsEditForm.value.kinName,
      relationWithKin: this.personalDetailsEditForm.value.relationWithKin,
      kinContactNo: this.personalDetailsEditForm.value.kinContactNo,
      firstname: this.personalDetailsEditForm.value.firstname,
      lastname: this.personalDetailsEditForm.value.lastname,
      email_address: this.personalDetailsEditForm.value.email_address,
      home_address: this.personalDetailsEditForm.value.home_address,
    }

    // this.personalDetailsEditForm.reset();

    this.candidateEditProfileService.postEditProfileDetails(profileObject)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {
            this.snackBar.open('PERSONAL DETAILS UPDATED', 'Got it!', {
           
            });
            localStorage.setItem('personalData', JSON.stringify({ firstname: this.personalDetailsEditForm.value.firstname, lastname: this.personalDetailsEditForm.value.lastname }));
            localStorage.setItem("persanalaDetails", 'deatils');
            //show alert and navigate to job-post component
            this.personalDetailsEditForm.reset();

            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
         
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );

  }

}
