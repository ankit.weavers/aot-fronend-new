import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CandidateEditProfileService } from '../../../../services';
import { Erros } from 'src/app/models';
import { BsDatepickerConfig, BsDaterangepickerConfig } from 'ngx-bootstrap/datepicker';
import * as moment from 'moment';
@Component({
  selector: 'candidate-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.scss']
})
export class WorkExperienceComponent implements OnInit {
  bsConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
  workExpEditForm: FormGroup;
  private onDestroyUnSubscribe = new Subject<void>();
  industryList = [];
  jobRoleList = [];
  endMaxDate = new Date();
  endMinDate: Date;
  valueBoolean : Boolean = true;
  isStartDateSelected: Boolean = false;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  workExpList = [];
  public errors: any = Erros;
  todayDatecolor = 'today'
  constructor(
    private fb: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar,
    public globalactionservice: GlobalActionsService,
    private candidateEditProfileService: CandidateEditProfileService,

  ) {

    this.workExpEditForm = this.fb.group({
      industryId: ['', Validators.required],
      jobRole: ['', Validators.required],
      description: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      companyName: ['', Validators.required],
      referenceName: ['', Validators.required],
      referenceContact: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
      referenceEmailAddress: ['', [Validators.required, Validators.email]],
      referencePosition: ['', Validators.required],
      referenceAddressDetails: ['', Validators.required]
    });

  }

  ngOnInit() {
    this.bsConfig.customTodayClass = this.todayDatecolor;
    this.bsConfig.dateInputFormat = 'DD/MM/YYYY';
    this.getIndustryList();
    this.getProfileDetails();
    console.log('ngn dele', this.workExpList.length)
  }

  addAnotherWorkExp() {
    if (this.workExpEditForm.valid) {
 
  
      let workLists = {
        companyName: this.workExpEditForm.value.companyName,
        description: this.workExpEditForm.value.description,
        endDate: moment(this.workExpEditForm.value.endDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
        industryId: this.workExpEditForm.value.industryId,
        jobRole: this.workExpEditForm.value.jobRole,
        referenceAddressDetails: this.workExpEditForm.value.referenceAddressDetails,
        referenceContact: this.workExpEditForm.value.referenceContact,
        referenceEmailAddress: this.workExpEditForm.value.referenceEmailAddress,
        referenceName: this.workExpEditForm.value.referenceName,
        referencePosition: this.workExpEditForm.value.referencePosition,
        startDate: moment(this.workExpEditForm.value.startDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
      };
  
      this.workExpList.push(workLists);
      this.workExpEditForm.reset();
    }
  }

  getIndustryList() {
    //get industries
    this.globalactionservice.getIndustry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.industryList = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  //get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result['details'].workExperience && !result['isError']) {
            //set value into form 
            this.workExpList = result['details'].workExperience.experiences;
            console.log('getProfileDetails', this.workExpList)
            if (this.workExpList.length) {
              this.workExpList.map(
                item => {
                  if (item.roleDetails) {
                    item.jobRole = item.roleDetails.jobRoleId
                  }
                }
              )
            }
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );

  }


  //industry selected
  selectJobIndustry(industryId) {
    this.jobRoleList = [];
    // this.workExpEditForm.controls.jobRole.setValue('')  //reseting job role on changing job industry
    this.getJobRoleList(industryId)
  }

  //get Job Role List
  getJobRoleList(industryId) {

    this.globalactionservice.getRole({ industryId: [industryId] })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobRoleList = result['details'];
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  //get start date value to validate end date
  selectEndDate() {
    this.isStartDateSelected = true;
    this.endMinDate = this.workExpEditForm.value.startDate;
  }

  //edit work exp 
  editWorkExp(index) {

    if (this.workExpList[index]) {
      let industryId;
      let jobRoleId;
      if (this.workExpList[index].roleDetails) {
        industryId = this.workExpList[index].roleDetails.industryId;
        jobRoleId = this.workExpList[index].roleDetails.jobRoleId;
      }
      else {
        industryId = this.workExpList[index].industryId;
        jobRoleId = this.workExpList[index].jobRole;
      }
      this.workExpEditForm.get('industryId').setValue(industryId);
      this.selectJobIndustry(industryId);
      this.workExpEditForm.get('jobRole').setValue(jobRoleId);
      this.workExpEditForm.get('description').setValue(this.workExpList[index].description);
      this.valueBoolean = false;
      // let date = new Date()
      // let dateMoment = moment(date).format('DD-MM-YYYY')
      // let enddate = new Date()
      // let enddateMoment = moment(enddate).format('DD-MM-YYYY')

      // this.workExpEditForm.get('startDate').setValue(this.workExpList[index].startDate);
      // this.workExpEditForm.get('endDate').setValue(this.workExpList[index].endDate);
      this.workExpEditForm.get('startDate').setValue(moment(this.workExpList[index].startDate,'DD/MM/YYYY').format('DD/MM/YYYY'));
      this.workExpEditForm.get('endDate').setValue(moment(this.workExpList[index].endDate, 'DD/MM/YYYY').format('DD/MM/YYYY'));
      this.workExpEditForm.get('companyName').setValue(this.workExpList[index].companyName);
      this.workExpEditForm.get('referenceName').setValue(this.workExpList[index].referenceName);
      this.workExpEditForm.get('referenceContact').setValue(this.workExpList[index].referenceContact);
      this.workExpEditForm.get('referenceEmailAddress').setValue(this.workExpList[index].referenceEmailAddress);
      this.workExpEditForm.get('referencePosition').setValue(this.workExpList[index].referencePosition);
      this.workExpEditForm.get('referenceAddressDetails').setValue(this.workExpList[index].referenceAddressDetails);
      //edit and remove from list
      this.workExpList.splice(index, 1);
    }
  }

  //delete work exp
  deleteWorkExp(expId: any, index) {
    if (this.workExpList[index]) {
      this.workExpList.splice(index, 1);
      console.log('after dele', this.workExpList.length)
    }

    this.candidateEditProfileService.deleteWorkExp({ "candidateId": this.userDetails.candidateId, "expId": expId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            // this.snackBar.open(result['message'], 'Got it!', {
            //   duration: 3000,
            // });
          } else {
            // this.snackBar.open(result['message'], 'Got it!', {
            //   duration: 3000,
            // });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
       
          });
        }

      );
  }

  //copy work exp
  copyWorkExp(index) {
    if (this.workExpList[index]) {
      let industryId;
      let jobRoleId;
      if (this.workExpList[index].roleDetails) {
        industryId = this.workExpList[index].roleDetails.industryId;
        jobRoleId = this.workExpList[index].roleDetails.jobRoleId;
      }
      else {
        industryId = this.workExpList[index].industryId;
        jobRoleId = this.workExpList[index].jobRole;
      }
      this.workExpEditForm.get('industryId').setValue(industryId);
      this.selectJobIndustry(industryId);
      this.valueBoolean = false;
      this.workExpEditForm.get('jobRole').setValue(jobRoleId);
      this.workExpEditForm.get('description').setValue(this.workExpList[index].description);
      this.workExpEditForm.get('startDate').setValue(moment(this.workExpList[index].startDate,'DD/MM/YYYY').format('DD/MM/YYYY'));
      this.workExpEditForm.get('endDate').setValue(moment(this.workExpList[index].endDate, 'DD/MM/YYYY').format('DD/MM/YYYY'));
      this.workExpEditForm.get('companyName').setValue(this.workExpList[index].companyName);
      this.workExpEditForm.get('referenceName').setValue(this.workExpList[index].referenceName);
      this.workExpEditForm.get('referenceContact').setValue(this.workExpList[index].referenceContact);
      this.workExpEditForm.get('referenceEmailAddress').setValue(this.workExpList[index].referenceEmailAddress);
      this.workExpEditForm.get('referencePosition').setValue(this.workExpList[index].referencePosition);
      this.workExpEditForm.get('referenceAddressDetails').setValue(this.workExpList[index].referenceAddressDetails);

      // //copy object and push new
      // if(!this.workExpEditForm.valid){
      //   this.workExpList.splice(index, 1);
      // }
    }
  }

  // saveCandidateWorkExp
  saveCandidateWorkExp() {
    

    let workLists = {
      companyName: this.workExpEditForm.value.companyName,
      description: this.workExpEditForm.value.description,
      endDate: moment(this.workExpEditForm.value.endDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
      industryId: this.workExpEditForm.value.industryId,
      jobRole: this.workExpEditForm.value.jobRole,
      referenceAddressDetails: this.workExpEditForm.value.referenceAddressDetails,
      referenceContact: this.workExpEditForm.value.referenceContact,
      referenceEmailAddress: this.workExpEditForm.value.referenceEmailAddress,
      referenceName: this.workExpEditForm.value.referenceName,
      referencePosition: this.workExpEditForm.value.referencePosition,
      startDate: moment(this.workExpEditForm.value.startDate, 'DD/MM/YYYY').format('DD/MM/YYYY'),
    };

    let workList = [];
    if (this.workExpEditForm.valid && this.workExpList.length === 0) {
      workList.push(workLists);
      console.log('worklist',workList);
    }
    else if (this.workExpList.length) {

      this.workExpList.push(workLists)
      workList = this.workExpList; 
      console.log('worklisthjgvjhgyjhg',workList);

    }
    this.workExpEditForm.reset();
    // console.log('save',this.workExpList);
    // console.log('look',workList);
    let workListArr  = workList.filter(value => Object.keys(value).length !== 0);

console.log('worklist',workListArr);

    // if (this.workExpEditForm.valid && this.workExpList.length === 0) {
    //   workList.push(this.workExpEditForm.value);
    // }
    // else if (this.workExpList.length) {

    //   this.workExpList.push(this.workExpEditForm.value)
    //   workList = this.workExpList;
    // }
    // this.workExpEditForm.reset();
    // console.log('save', this.workExpList);
    // console.log('look', workList);
    // let workListArr = workList.filter(value => Object.keys(value).length !== 0);
    let workExpObject = {
      candidateId: this.userDetails.candidateId,
      experiences: workListArr //this.workList
    }

    this.candidateEditProfileService.postWorkExpDetails({ workObj: workExpObject })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('WORK EXPERIENCE UPDATED', 'Got it!', {
             
            });
            localStorage.setItem("workExprience", 'workExprience');

            //show alert and navigate to job-post component
            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

}