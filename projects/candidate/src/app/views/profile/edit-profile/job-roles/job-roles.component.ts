import { json } from '@angular-devkit/core';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators , FormArray} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { CandidateEditProfileService } from '../../../../services';
import { Erros } from 'src/app/models';

@Component({
  selector: 'candidate-job-roles',
  templateUrl: './job-roles.component.html',
  styleUrls: ['./job-roles.component.scss']
})
export class JobRolesComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  payloadSkill = [];
  skillDetailsObj = []
  industryList = [];
  jobRoleList = [];
  skills =[];
  skillss=[];
  skillsList=[];
  industryId =[];
  selectedRoleWiseSkill = {};
  selectedRoleWiseSkills={};
  public isSkillAvailable :boolean = false;
  public addJobRolesForm: FormGroup;
  selectedRoles = [];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  public errors: any = Erros;
  selectedSkillsPayload: string[] = [];
   public keywords = [];
  public saveJobIdRoleId =[];
  selectedPayballUser = [];
  selectIndustry = [];
  addSkillsForm: FormGroup;
  industryName = [];
  showRole:boolean = false;
  showArraow:boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar,
    public globalactionservice: GlobalActionsService,
    private candidateEditProfileService: CandidateEditProfileService,
  ) {
    this.addJobRolesForm = this.fb.group({
      industry: ['', Validators.required],
      jobRole: ['', Validators.required],
     
    });
    this.addSkillsForm = this.fb.group({
      skillDetails: new FormArray([])
    });
    // this.getRoleSkills();
  }

  ngOnInit() {

    this.getIndustryList();
    this.getProfileDetails();
  }

  // DOWN ARROW //
  showMoreRole() {
    this.showRole = true;
    this.showArraow = true;
  }

  // UP ARROW //
  showLessRole() {
    this.showRole = false;
    this.showArraow = false;
  }

  // get Industry List
  getIndustryList() {
    //get industries
    this.globalactionservice.getIndustry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.industryList = result['details'];
          } 
          // else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //     duration: 3000,
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  //get Job Role List
  getJobRoleList(industryId) {
    this.globalactionservice.getRole({ industryId: industryId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobRoleList = result['details'];
            // console.log('this.jobRoleList',this.jobRoleList[0].industryName);
            // for(let i =0; i<this.jobRoleList.length;i++){
            //   this.industryName[i] = this.jobRoleList[i].industryName;
            // }
          } 
          // else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //     duration: 3000,
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  // industry selected
  selectJobIndustry() {

    this.selectedPayballUser = this.addJobRolesForm.value.industry;
    console.log('selectedIndustry',this.selectedPayballUser);
    if(this.selectedPayballUser.length == 0) {
      this.jobRoleList = [];
      this.selectedRoles = [];
      this.skillsList=[];
      this.addJobRolesForm.controls.jobRole.setValue('');
    }


    this.getJobRoleList( this.selectedPayballUser);
    // this.skills =[];
  }

  // get selected job roles based on industry selected
  selectedJobRoles() {
    this.selectedRoles = this.addJobRolesForm.value.jobRole;
    console.log('selectedRoles',this.selectedRoles);
   
    // console.log('skills',this.skills)
     this.getRoleSkills();
  }
  getRoleSkills() {

    // let roleIds = [];
  
    // for(let i =0; i<this.selectedRoles.length; i++){
    //   roleIds[i] = this.selectedRoles[i].jobRoleId;
  
    // }
    console.log('roleIds', this.selectedRoles);
  
  

    // this.globalactionservice.getJobSkillById({jobRole:roleIds})
    //   .pipe(takeUntil(this.onDestroyUnSubscribe))
    //   .subscribe(
    //     result => {
    //       if (result && !result['isError']) {
    //         let skillsRes =  result['details'];
    //         console.log("skillsRes",typeof skillsRes);
    //         for(let i =0; i<skillsRes.length; i++){
    //           this.skills[i] = skillsRes[i];
    //           console.log('skills',this.skills);
    //         } 
    //         let skillFac =this.skills;
    //         console.log('skillFac',typeof skillFac);

    //         for(let i=0; i<skillFac.length; i++)
    //         {
    //           this.skillss = skillFac[i].details;
    //          console.log('skillss',this.skillss);
    //         } 
      

    //       }
    //     },
    //     error => {
    //       this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
    //         duration: 3000,
    //       });
    //     }
    //   );


      //get skill details by Job Role Id
      // this.globalactionservice.getJobSkillById({ jobRole: roleIds })
      //   .pipe(takeUntil(this.onDestroyUnSubscribe))
      //   .subscribe(
      //     result => {
      //       if (result && !result['isError']) {
      //         this.skillsList = result['details'];
      //         console.log('skillsList########################################', this.skillsList)

      //       } else {
      //         this.snackBar.open(result['message'], 'Got it!', {
      //           duration: 3000,
      //         });
      //       }
      //     },
      //     error => {
      //       this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
      //         duration: 3000,
      //       });
      //     }
      //   )
      this.globalactionservice.fetchSkill({ candidateId: this.userDetails.candidateId,jobRole:  this.selectedRoles })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.skillsList = result['details'];
              console.log('skillsList########################################', this.skillsList)

            } else {
              this.snackBar.open(result['message'], 'Got it!', {
              
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
            });
          }
        )




  }

  onChecklistChange(checked, sId, jobRoleId, roleName, sName,iId,roleIndex,skillIndex) {
    console.log('selected ch',this.skillsList[roleIndex].skillDetails[skillIndex].isSelected)

    this.skillsList[roleIndex].skillDetails[skillIndex].isSelected = checked
      // this.skillDetailsObj.push({
      //   skillId:sId,
      //   skillName:sName,
      // })
      // this.selectedRoleWiseSkills = {
      //   jobRoleName:roleName,
      //   jobRoleId:jobRoleId,
      //   industryId:iId,
      //   skillDetails:this.skillDetailsObj
      // }
      // this.payloadSkill[roleIndex] =  this.selectedRoleWiseSkills;
      
      console.log('skillDetails',this.skillsList);
  }

  // get candidate profile details
  getProfileDetails() {
    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] ) {

            //set value into form
            if (result['details'].payloadSkill) {
              // this.industryId = result['details'].payloadSkill;
              for(let i=0 ; i<result['details'].industryIdNameList.length; i++){
                this.industryId[i] = result['details'].industryIdNameList[i].industryId
              }
             

              console.log("industry",this.industryId);
              console.log("value",result['details'].payloadSkill[0].industryId);
              this.addJobRolesForm.get('industry').setValue(this.industryId);
              this.selectJobIndustry();
              let payloadSkill =  result['details'].payloadSkill;
              let selectedRoles = [];
              selectedRoles =  payloadSkill.map(({ jobRoleId }) => jobRoleId);
              console.log('selectedRoles',selectedRoles)

              this.addJobRolesForm.controls['jobRole'].setValue(selectedRoles);

              this.selectedJobRoles();

              // this.selectedRoles = result['details'].skillDetails;
              
              // let jobRoles = [];
              // Object.keys(result['details'].skillDetails).map(
              //   key => {
              //     jobRoles.push(result['details'].skillDetails[key].jobRoleName)
              //   }
              // )
              // this.addJobRolesForm.get('jobRole').setValue(jobRoles);
              this.skillsList = result['details'].payloadSkill;
            }
            // if(result['details'].skillids)
            // {
            //   let industryId =result['details'].skillids[0].industryId;
            //   this.addJobRolesForm.get('industry').setValue(industryId);
            //   this.selectJobIndustry(industryId);
            //   this.skills = result['details'].skillids;
            //   let jobRoles = [];
            //   Object.keys(result['detaills'].skillids).map(
            //     key =>{
            //       jobRoles.push(result['details'].skillids[key].skillName)
            //     }
            //   )
            //   this.addJobRolesForm.get('jobRole').setValue(jobRoles);
            // }
          }
          // else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //     duration: 3000,
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  //save job roles 
  saveJobRoles() {
    let roles = [];
    Object.keys(this.addJobRolesForm.value.jobRole).map(
      key => {
        roles.push(this.addJobRolesForm.value.jobRole[key].jobRoleId)
        
      }
    )
    let jobDetailsObject = {
      candidateId: this.userDetails.candidateId,
      payloadSkill: this.skillsList
  
    }

    this.addJobRolesForm.reset();
    this.selectedRoles = [];
    this.jobRoleList = [];
    this.skillsList =[];
    

    this.candidateEditProfileService.postJobRolesDetails(jobDetailsObject)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('ROLES UPDATED', 'Got it!', {
            
            });
            localStorage.setItem("roles",'roles');

            //show alert and navigate to candidate edit component
            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }
  
  onCheckSkill(checked, skillName, skillId) {
    if (checked) {
      this.keywords.push(skillName)
      this.selectedSkillsPayload.push(skillId)
      console.log("data",this.selectedSkillsPayload);
      console.log('data 2',this.keywords);
    }
    else {
      for (var i = 0; i < this.keywords.length; i++) {
        if (this.keywords[i] === skillName) {
          this.keywords.splice(i, 1);
          this.selectedSkillsPayload.splice(i, 1)
        }
      }
    }
  }
  



}
