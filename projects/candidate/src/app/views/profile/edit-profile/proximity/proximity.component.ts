import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MapsAPILoader,AgmCircle,AgmMap } from '@agm/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatSliderChange } from '@angular/material';
import { CandidateEditProfileService } from '../../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Erros } from 'src/app/models';

declare var google: any;

@Component({
  selector: 'candidate-proximity',
  templateUrl: './proximity.component.html',
  styleUrls: ['./proximity.component.scss']
})
export class ProximityComponent implements OnInit {
// @ViewChild('agm-circle') child;
@ViewChild( AgmCircle ) agmcircle ;
@ViewChild(AgmMap) agmmap;
  lng: number;
  lat: number;
  public searchControl: FormControl;
  public zoom: number;
  @ViewChild("search")
  public searchElementRef: ElementRef;
  placesss;
  disabled = false;
  fit;
  max = 30;
  editValue : boolean = false;
  min = 0;
  step = 1;
  thumbLabel = true;
  distanceValue = 1 ;
  disValue;
  locationName: "";
  circleColor;
  geocoder;
  private onDestroyUnSubscribe = new Subject<void>();
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  public errors: any = Erros;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private router: Router,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
  ) {
    this.getProfileDetails();
  }

  ngOnInit() {
    //set google maps defaults
    this.zoom = 4;
    //create search FormControl
    this.searchControl = new FormControl('', Validators.required);
    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
       
      });
      
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          // let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          let place: typeof google.maps.places.PlaceResult = autocomplete.getPlace();
          console.log(place);
          //  let place = autocomplete.getPlace();
          this.placesss = place.place_id;
          if (!place.place_id) {
            window.alert('Please select an option from the dropdown list.');
            return;
          }
          // this.locationName = place.name;
          this.locationName = place.formatted_address;
          this.editValue = true;


          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  //setting current map position 
  private setCurrentPosition() {

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;


      });

      // this.mapsAPILoader.load().then(() =>{
      //   this.geocoder = new google.maps.Geocoder;
      //   var latlng = new google.maps.LatLng(this.lat, this.lng)
      //   let currentClassInstance = this;
      //   this.geocoder.geocode({'location': latlng}, function(results, status){
      //     console.log(results[0]);
      //     // if(status === "OK"){
    
      //       currentClassInstance.locationName = results[0].formatted_address;
    
      //     // }
      //   });
      // })
    }
  }

  // // Function to call when the input is touched (when a slider is moved).
  // onTouched = () => {};
  onInputChange(event: MatSliderChange) {
    this.distanceValue = event.value;
    this.disValue = this.distanceValue * 1609.34;
    console.log("distance",this.distanceValue);
  }

  //get candidate profile details
  getProfileDetails() {
    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {
            //set value into form
            this.lng = result['details'].geolocation.coordinates[0];
            this.lat = result['details'].geolocation.coordinates[1];
            this.locationName = result['details'].geolocation.locationName
            if(this.locationName){
              this.searchControl = new FormControl(this.locationName, Validators.required);
              this.editValue = false;
            }
            else {
              this.searchControl = new FormControl("", Validators.required);
            }
            this.zoom = 12;
            this.distanceValue = (result['details'].distance)/1609.34;
            this.disValue = this.distanceValue * 1609.34;
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

  //save location details
  saveLocationDetails() {
    if(!this.locationName) {
      this.snackBar.open('location name is must','got it!',{

      });
      return false;
    }
    let jobLocation;
    // if (this.searchControl.value && !this.editValue) {
      jobLocation = {
        candidateId: this.userDetails.candidateId,
        distance: this.distanceValue * 1609.34,
  
        location: {
          type: "Point",
          locationName: this.locationName,
          coordinates: [this.lng, this.lat],
        }
        }
      // }
    // } else {
      // jobLocation = {
      //   candidateId: this.userDetails.candidateId,
      //   distance: this.distanceValue * 1609.34,
  
      //   location: {
      //     type: "Point",
      //     locationName: this.locationName,
      //     coordinates: [this.lng, this.lat],
      //   },
      // }
    // }

    this.candidateEditProfileService.postProximityDetails(jobLocation)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('LOCATION UPDATED', 'Got it!', {
             
            });
            localStorage.setItem("location",'location');

            //show alert and navigate to job-post component
            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
  }

}
