import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { CandidateEditProfileService } from '../../../../services';
import { takeUntil } from 'rxjs/operators';
import { Erros } from 'src/app/models';

@Component({
  selector: 'candidate-days-off',
  templateUrl: './days-off.component.html',
  styleUrls: ['./days-off.component.scss']
})

export class DaysOffComponent implements OnInit {

  endMaxDate = new Date();
  endMinDate: Date;
  isStartDateSelected: Boolean = false;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  daysOffForm: FormGroup;
  private onDestroyUnSubscribe = new Subject<void>();
  daysOffList = [];
  checked = false;
  dateRangeControl = new FormControl('');
  public errors: any = Erros;
  disting: boolean = false; 

  constructor(
    private fb: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
  ) {
    this.daysOffForm = this.fb.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });

  }

  ngOnInit() {
    this.getProfileDetails();
  }

  //get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result['details'].dayoff && !result['isError']) {
            //set value into form
            this.daysOffList = result['details'].dayoff;

            // if (result['details'].dayoff[0]) {
            //   this.daysOffForm.get('startDate').setValue(result['details'].dayoff[0].startDate)
            //   this.daysOffForm.get('endDate').setValue(result['details'].dayoff[0].endDate)
            // }

          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              duration: 3000,
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            duration: 3000,
          });
        }
      );
  }

  //slider option
  sliderOption(event: MatSlideToggleChange) {
    this.checked = event.checked
    if (!this.checked) {
      this.dateRangeControl.reset();
    }
    else {
      this.daysOffForm.reset();
    }
  }

  //get start date value to validate end date
  selectEndDate() {
    this.isStartDateSelected = true;
    this.endMinDate = this.daysOffForm.value.startDate;
  }

  //delete days off
  deleteDaysOff(index) {
    if (this.daysOffList[index]) {
      this.daysOffList.splice(index, 1);
    }
  }

  //saveDaysOff 
  saveDaysOff() {

    let daysOffObject = {
      candidateId: '',
      dayoff: []
    };
    daysOffObject.candidateId = this.userDetails.candidateId;

    if (this.daysOffForm.valid) {
      this.daysOffList.push(this.daysOffForm.value)
      console.log("code",this.daysOffList);

    }
    if (this.dateRangeControl.value) {
      this.disting = true;
      this.daysOffList.push({
        startDate: this.dateRangeControl.value[0],
        endDate: this.dateRangeControl.value[1],
        disting:true
      })
      console.log(this.daysOffList);
    }

    daysOffObject.dayoff = this.daysOffList

    this.daysOffForm.reset();
    this.dateRangeControl.reset();

    this.candidateEditProfileService.postDaysOffDetails(daysOffObject)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('DAYS OFF UPDATED', 'Got it!', {
              duration: 3000,
            });
            //show alert and navigate to candidate-profile component
            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              duration: 3000,
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            duration: 3000,
          });
        }
      );
  }
}
