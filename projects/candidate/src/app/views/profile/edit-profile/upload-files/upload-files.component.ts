import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CandidateEditProfileService } from '../../../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'candidate-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {

  storedDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  selectedCv = [];
  selectedVideo = [];
  selectedDocs = [];
  readyToSaveCv: any;
  readyToSaveVideo: any;
  readyToSaveDocs = [];
  uploadedCv;
  uploadedVideo;
  uploadedDocs = [];
  isUploading = false;
  isCvUploading = false;
  isVideoUploading = false;
  isDocsUploading = false;

  cvChosen = false;
  videoChosen = false;
  docChosen = false;
  // isDocsUploaded:Boolean  = false;

  constructor(
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
    private router: Router,

  ) { }


  ngOnInit() {
    this.getProfileDetails();
    console.log(this.storedDetails.userType);

  }

  // get candidate profile details
  getProfileDetails() {
    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.storedDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            //set value into form
            console.log('getProfileDetails', result['details']);
            const profileDetails = result['details'];

            // if (result['details'].docs && result['details'].cv && result['details'].video) {
            // this.uploadedFilesList = {
            //   cv: result['details'][0].cv,
            //   docs: result['details'][0].docs,
            //   video: result['details'][0].video
            // }

            // this.uploadedDocs = {
            //   docName: profileDetails.docs[0].docName,
            //   docLink: profileDetails.docs[0].docLink
            // }

            this.uploadedCv = (profileDetails && profileDetails.cv) ? profileDetails.cv : '';

            this.uploadedVideo = (profileDetails && profileDetails.video) ? profileDetails.video : '';

            this.uploadedDocs = (profileDetails && profileDetails.docs && profileDetails.docs.length) ? profileDetails.docs : [];


              // this.isDocsUploaded = true;

            console.log('uploadedCv' , this.uploadedCv);
            console.log('uploadedVideo' , this.uploadedVideo);
            console.log('uploadedDocs' , this.uploadedDocs);

            // }

          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  // upload files to S3bucket

  getSelectedFile(event, fileType, inputSelector) {
    if (event.target.files.length > 0) {
      console.log(event, inputSelector, inputSelector.value);
      const selectedFiles = event.target.files;
      // event.target.value = '';
      // console.log('All files', selectedFiles);
      // for (const file of selectedFiles) {
      // const fileExt = file.name.split('.').pop();
      // For CV
      if (fileType === 'CV') {
        this.filterFiles(selectedFiles, ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])
        .then(async (value: any) => {
          this.selectedCv = [];
          this.selectedCv.push(...value);
          const folderName = 'files/resumes';
          for (let i = 0; i < this.selectedCv.length; i++) {
            this.isCvUploading = true;
            // this.readyToSaveCv = {
            //   cvName: files[i].name,
            //   // cvLink: res['Location']
            // };
            await this.uploadDocumentFile(this.selectedCv[i], folderName, fileType)
            .then((res) => {
              console.log('uploadFileCV', res, res['Location']);
              this.readyToSaveCv = {
                cvName: res['key'].split('/')[3],
                cvLink: res['Location']
              };
              this.selectedCv[i].notSaved = false;
              this.selectedCv.splice(i, 1);
              this.cvChosen = true;
              this.isCvUploading = false;
              // this.snackBar.open(`'${files[i].name}' has been uploaded.`, 'Got it!', {
              //   duration: 5000,
              // });
            });
            i--;
          }
        });
      }
      // For Video
      if (fileType === 'Video') {
        this.filterFiles(selectedFiles, ['mp4', 'webm'])
        .then(async (value: any) => {
          this.selectedVideo = [];
          this.selectedVideo.push(...value);
          const folderName = 'files/videos';
          for (let i = 0; i < this.selectedVideo.length; i++) {
            this.isVideoUploading = true;
            // this.readyToSaveVideo = {
            //   videoName: files[i].name,
            //   // videoLink: res['Location']
            // };
            await this.uploadDocumentFile(this.selectedVideo[i], folderName, fileType)
            .then((res) => {
              console.log('uploadFileVideo', res, res['Location']);
              this.readyToSaveVideo = {
                videoName: res['key'].split('/')[3],
                videoLink: res['Location']
              };
              this.selectedVideo[i].notSaved = false;
              this.selectedVideo.splice(i, 1);
              this.videoChosen = true;
              this.isVideoUploading = false;
              // this.snackBar.open(`'${files[i].name}' has been uploaded.`, 'Got it!', {
              //   duration: 5000,
              // });
            });
            i--;
          }
        });

      }
      // For Documents
      if (fileType === 'Document') {
        this.filterFiles(selectedFiles, ['jpg', 'jpeg', 'png', 'txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])
        .then(async (value: any) => {
          this.selectedDocs.push(...value);
          console.log(this.selectedDocs);
          const folderName = 'files/documents';
          for (let i = 0; i < this.selectedDocs.length; i++) {
            this.isDocsUploading = true;
            // this.readyToSaveDocs.push({
            //   docName: files[i].name
            // });
            await this.uploadDocumentFile(this.selectedDocs[i], folderName, fileType)
            .then((res) => {
              console.log('uploadFileDoc', res, res['Location']);
              this.readyToSaveDocs.push({
                docName: res['key'].split('/')[3],
                docLink: res['Location']
              });
              this.selectedDocs[i].notSaved = false;
              this.selectedDocs.splice(i, 1);
              this.docChosen = true;
              this.isDocsUploading = false;
              this.isUploading = false;
              // this.snackBar.open(`'${files[i].name}' has been uploaded.`, 'Got it!', {
              //   duration: 5000,
              // });
            });
            i--;
          }
        });

      }
    }
  }

  uploadDocumentFile(file, folderName: string, fileType: any) {
    return this.globalActionsService.uploadFile(file, this.storedDetails.userType, folderName, fileType);
  }


  filterFiles(selectedFiles, fileExts) {
    return new Promise((resolve, reject) => {
      const filteredFiles = [];
      for (const file of selectedFiles) {
        const fileExt = file.name.split('.').pop();
        if (
          fileExts.includes(fileExt)
        ) {
          // console.log(file.name.split('.').pop());
          file.notSaved = true;
          console.log(file);
          filteredFiles.push(file);
          // this.uploadDocumentFiles(file, folderName, fileType);
        } else {
          console.log('CV', file);
          this.snackBar.open(`'${file.name}' is not a a Valid File Type. Unsupported files types will be ignored.`, 'Got it!', {
         
          });
        }
      }
      resolve(filteredFiles);
    });
  }

  deSelectFile(arrayName, index) {
    console.log(arrayName, index);
    arrayName.splice(index, 1);
    console.log(arrayName);
  }

  deleteSelectedFile(doc, arrayName, index) {
    // this.deSelectFile(arrayName, index);
    console.log(doc);
    this.candidateEditProfileService.deleteDocuments({ candidateId: this.storedDetails.candidateId, docId: doc.docId })
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        console.log(result);
        this.deSelectFile(arrayName, index);
      },
      error => {
        console.log(error);
        this.snackBar.open('Errror please try again', 'Got it!', {
      
        });
      }
    );
  }

  // addMoreDoc
  addMoreDoc() {
    console.log('addMoreDoc', this.uploadedDocs );
    // this.isDocsUploaded = false;

  }

  //save all uploaded files
  saveAllUploads() {
    console.log('saveAllUploads', this.uploadedCv, this.uploadedDocs, this.uploadedVideo);

    const uploadData = {
      candidateId: this.storedDetails.candidateId,
      // profilePic: res['Location']
      // cv: this.uploadedCv,
      // video: this.uploadedVideo,
      // documents: this.uploadedDocs
      cv: this.readyToSaveCv ? this.readyToSaveCv : this.uploadedCv,
      video: this.readyToSaveVideo ? this.readyToSaveVideo : this.uploadedVideo,
      // documents: this.readyToSaveDocs
      documents: [...this.readyToSaveDocs , ...this.uploadedDocs]
    };
    if (uploadData.documents.length == 0 || !uploadData.video ) {
      this.snackBar.open('please complete the section','got it!',{

      });
      return false;
    }

    console.log('call allS3UploadedDocs ', uploadData);

    this.candidateEditProfileService.allS3UploadedDocs(uploadData)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      (result) => {
        //    console.log('result------Employer----add company details', result)
        if (result && !result['isError']) {

          let messageSuccess = 'ALl Uploaded successfully.';
          this.snackBar.open(messageSuccess, 'Got it!', {
          
          });
          localStorage.setItem("uploadFile",'uploadFile');

          // this.getEmployerDetails();
          //show alert and navigate to candidate edit component
          this.router.navigate(['/candidate/profile/edit']);
        } else {
          // console.log('e', result);
          this.snackBar.open('Internal Server Error', 'Got it!', {
          
          });
        }
      },
      (error) => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      },
    );

  }



    // for (let i = 0; i < files.length; i++) {
        // this.isCvUploading = true;
        // this.isVideoUploading = true;
        // this.isDocsUploading = true;
    //   if (fileType === 'CV') {
    //     // this.selectedCv = '';
    //     this.readyToSaveCv = {
    //       cvName: files[i].name,
    //       // cvLink: res['Location']
    //     };
    //     files[i].notSaved = true;
    //     files.splice(i, 1);
    //     this.selectedCv  = files;
    //     this.cvChosen = true;
    //     this.isCvUploading = false;
    //   } else if (fileType === 'Video') {
    //     // this.selectedVideo = '';
    //     this.readyToSaveVideo = {
    //       videoName: files[i].name,
    //       // videoLink: res['Location']
    //     };
    //     files[i].notSaved = true;
    //     files.splice(i, 1);
    //     this.selectedVideo = files;
    //     this.videoChosen = true;
    //     this.isVideoUploading = false;
    //   } else if (fileType === 'Document') {
    //     this.readyToSaveDocs.push({
    //       docName: files[i].name
    //     });
    //     files[i].notSaved = true;
    //     files.splice(i, 1);
    //     this.selectedDocs = files;
    //     this.docChosen = true;
    //     this.isDocsUploading = false;
    //   }
    //   console.log(i);
    //   i--;
    // }
}
