import { Component, OnInit, TemplateRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CandidateEditProfileService } from '../../../../services';
import { Erros } from 'src/app/models';
import { BsDatepickerConfig, BsDaterangepickerConfig } from 'ngx-bootstrap/datepicker';
import { DAYS_OF_WEEK } from 'calendar-utils';


@Component({
  selector: 'candidate-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.scss']
})
export class AvailabilityComponent implements OnInit, AfterViewInit {

  modalRef: BsModalRef;
  private onDestroyUnSubscribe = new Subject<void>();
  endMaxDate = new Date();
  endMinDate: Date;
  isStartDateSelected: Boolean = false;
  bsConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
  bsConfigs: Partial<BsDaterangepickerConfig> = new BsDaterangepickerConfig();
  // private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  daysOffForm: FormGroup;
  // daysOffRange: FormGroup;
  // private onDestroyUnSubscribe = new Subject<void>();
  daysOffList = [];
  timeSlot:any = [];
  checked = false;
  dateRangeControl = new FormControl('');
  public errors: any = Erros;
  shiftList = [];
  days= ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
  selectedDay: string = 'Monday';
  availabilityList = [];
  selectedShiftTiming = [];
  todayDatecolor = 'today';
  value: boolean = false;
  disting :boolean= false;
  lists:any;
  endTime = "00.00";
  starttime = "00.00";
  startsixHour = "06.00";
  endsixHour = "06.00";
  selectdayss = [];
  randomDates = [];

  bsInlineValue;
  // bsInlineRangeValue:Date[];
  datesDisabled=[];
  dayss = [
    {
   slotName:"Morning"
    },
    {
     slotName:"Afternoon"
    },
       {
     slotName:"Afternoon"
    },
    {
      slotName:"Afternoon"
     },
  ];

  allSelectedTimeSlots = [
      {
        day: 'Monday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''
      },
      {
        day: 'Tuesday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
      {
        day: 'Wednesday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
      {
        day: 'Thursday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
      {
        day: 'Friday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
      {
        day: 'Saturday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
      {
        day: 'Sunday',
        Timeslot: [],
        timeSlotDetails: [],
        availabilityId:''

      },
  ];
  timeSlottable = [
    {
      day: 'Monday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ] ,
      
    
    },
    {
      day: 'Tuesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Wednesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Thursday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Friday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Saturday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },
    {
      day: 'Sunday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },
];
  // allSelectedTimeSlots: any[] = [];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  checkedDay: any;

  constructor(
    private fb: FormBuilder,
    private modalService: BsModalService,
    public globalactionservice: GlobalActionsService,
    public snackBar: MatSnackBar,
    public router: Router,
    private candidateEditProfileService: CandidateEditProfileService,
  ) { 
    // this.daysOffForm = this.fb.group({
    //   startDate: ['', Validators.required],
    //   endDate: ['', Validators.required],
    // });
  }

  ngOnInit() {
    this.bsConfig.customTodayClass = this.todayDatecolor;
    this.bsConfig.dateInputFormat = 'DD-MM-YYYY';
    this.bsConfigs.customTodayClass = this.todayDatecolor;
    this.bsConfigs.rangeInputFormat = 'DD-MM-YYYY';
    this.getShiftList();
    // this.modalService.onHidden.subscribe((reason: string) => {
    //   //  console.log(`onShown event has been fired`);
    //   // this.selectedShiftTiming = [];
    //   console.log(this.checkedDay);
    //   if (this.checkedDay) {
    //     this.checkedDay.checked = false;
    //   }
    // });
  }
  ngAfterViewInit(): void {
    
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  // get Shift List
  getShiftList() {
    this.globalactionservice.getAllSlots()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.shiftList = result['details'];
            console.log('shiftList', this.shiftList)
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
          
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        },
        () => {
          this.getProfileDetails();
        }
      );
  }

  // get candidate profile details
  getProfileDetails() {
    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && result['details'] && !result['isError']) {
          console.log('getProfileDetails', result['details'].availability);
          // set value into form
          this.availabilityList = result['details'].availability;
          this.callArray();
          // for (let i = 0 ;i<this.availabilityList.length ;i++){
          //   this.dayss[i] = this.availabilityList[i].day 
          //   this.timeSlot[i] = this.availabilityList[i].timeSlotDetails
            // for(let j = 0;j<this.dayss.length; j++){
            //   this.timeSlot[j] = this.dayss[j]
            // }
          // }

          // this.timeSlot = this.dayss[0] ;
          // this.availabilityList.map(dayss =>{
          //   this.dayss.filter(aval =>{
          //     dayss.day = aval.day
          //     dayss.timeSlotDetails.slotName = aval.slotName
          //   })
          // })
          // this.selectdayss = result['details'].availability.map(({timeSlotDetails})=>timeSlotDetails);
          console.log("avail",this.timeSlot);
          console.log("selected",this.selectdayss);
          console.log("days",this.dayss);

          this.daysOffList = result['details'].dayoff;
        } else {
          this.snackBar.open(result['message'], 'Got it!', {
          
          });
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
        });
      },
      () => {
        this.checkBoxCheckOnLoad(this.availabilityList);
      },
    );
  }
  callArray() {
  
    this.timeSlottable.forEach(element => {
     element.Timeslot.forEach(data => {
       this.availabilityList.forEach(a => {
         if(a.day === element.day) {
           a.timeSlotDetails.forEach(d => {
             if(d.slotName === data.slotName) {
               data.slotValue = true
             }
           });
         }
       });
     });
    });
  }
  onValueChange(event){
    this.datesDisabled.push(new Date(event));
    this.randomDates.push(event);
    console.log("date",this.randomDates,event);
  }
  removeDate(index){
  
    console.log("index",index);
    this.randomDates.splice(index,1);

    this.datesDisabled.splice(index,1);
  }
  sliderOption(event: MatSlideToggleChange) {
    this.checked = event.checked
    if (!this.checked) {
      this.dateRangeControl.reset();
    }
    else {
      // this.daysOffForm.reset();
      this.randomDates = [];
    }
  }
  changevalue(){
    if(this.dateRangeControl.value){
      this.value =true
    }
    else{
      this.value = false;
    }
  }

  //get start date value to validate end date
  selectEndDate() {
    this.isStartDateSelected = true;
    this.endMinDate = this.daysOffForm.value.startDate;
  }

  //delete days off
  deleteDaysOff(index,i) {
   
    if (this.daysOffList[i]) {
      this.daysOffList.splice(i, 1);
    }
 
   let payload={
    candidateId:this.userDetails.candidateId,
    dayOffId: index._id
    }
    this.candidateEditProfileService.DeleteDaysOffDetails(payload).pipe(takeUntil(this.onDestroyUnSubscribe)).subscribe(result=>{
 
    })
  }

  //saveDaysOff 
  saveDaysOff() {
    this.saveAvailability();

    let daysOffObject = {
      candidateId: '',
      dayoff: []
    };
    daysOffObject.candidateId = this.userDetails.candidateId;
    for(let i = 0;i<this.randomDates.length;i++){ 
      this.daysOffList.push({
        startDate: this.randomDates[i],
        endDate: this.randomDates[i]
      })
      console.log("days",this.daysOffList);
    }
    // if (this.daysOffForm.valid) {
    //   this.daysOffList.push(this.daysOffForm.value)
    // }
    if (this.dateRangeControl.value) {
      this.disting = true;
      this.daysOffList.push({
        startDate: this.dateRangeControl.value[0],
        endDate: this.dateRangeControl.value[1]
      })
      console.log(this.daysOffList)
    }

    daysOffObject.dayoff = this.daysOffList

    // this.daysOffForm.reset();
    this.dateRangeControl.reset();

    this.candidateEditProfileService.postDaysOffDetails(daysOffObject)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            // this.snackBar.open('DAYS OFF UPDATED', 'Got it!', {
            //   duration: 3000,
            // });
            // localStorage.setItem("daysOF","daysOF");
            localStorage.setItem("daysOF",'daysOF');
            //show alert and navigate to candidate-profile component
            // this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  checkBoxCheckOnLoad(availabilityData) {
    console.log('before', availabilityData, this.allSelectedTimeSlots);
    if (availabilityData.length) {
      availabilityData.map(selectedDay => {
        this.allSelectedTimeSlots.filter(dayOfWeek => {
          if (dayOfWeek.day === selectedDay.day) {
            dayOfWeek.Timeslot = selectedDay.Timeslot;
            dayOfWeek.timeSlotDetails = selectedDay.timeSlotDetails;
            dayOfWeek.availabilityId = selectedDay.availabilityId;
          }
        });
      });
    }
    console.log('After fetch', availabilityData, this.allSelectedTimeSlots);
    this.selectCheckBoxForDay();
  }

  // modal functionality
  selectDay(event, template: TemplateRef<any>, selectedDay, selector) {
    // console.log(event);
    if (event.target.checked) {
      this.selectedDay = selector.value;
      this.checkedDay = selector;
      this.selectCheckBoxForDay();
      console.log('On change', this.allSelectedTimeSlots, this.shiftList);
      // this.modalRef = this.modalService.show(template, {backdrop: 'static', keyboard: false});
      // this.modalService.onHidden.subscribe((reason: string) => {
      //   //  console.log(`onShown event has been fired`);
      //   this.selectedShiftTiming = []
      // });
    } else if (!event.target.checked) {
      // this.modalRef.hide();
    }
    // this.selectedDay = selectedDay;
    // this.modalRef = this.modalService.show(template);
    // this.modalService.onShown.subscribe((reason: string) => {
    //   //  console.log(`onShown event has been fired`);
    //   this.selectedShiftTiming = []
    // })
  }

  selectCheckBoxForDay() {
    this.shiftList.map(shift => {
      shift.checked = false;
    });
    if (this.allSelectedTimeSlots.length) {
      this.allSelectedTimeSlots.filter(slot => {
        console.log("slot",slot.day)
        if (slot.day === this.selectedDay) {
          slot.Timeslot.map(slotItem => {
            this.shiftList.filter(shift => {
              if (shift.id === slotItem) {
                shift.checked = true;
              }
            });
          });
        }
      });
    }
  }

  // selectShiftTime depending on the shift in modal selected
  selectShiftTime(event, shift) {
    if (event.target.checked) {
      console.log('selectShiftTime', shift);
      if (this.allSelectedTimeSlots.length) {
        console.log('Day available.', this.selectedDay, typeof this.selectedDay);
        this.allSelectedTimeSlots.map((slot, index) => {
          if ((slot.day === this.selectedDay) && (!slot.Timeslot.includes(shift.id))) {
            slot.Timeslot.push(shift.id);
            slot.timeSlotDetails.push(shift);
            console.log('add to selected days.');
          }
        });
      // } else {
      //   console.log('No day selected.');
      //   this.allSelectedTimeSlots.push({
      //     day: this.selectedDay,
      //     Timeslot: [shift.id],
      //     timeSlotDetails: [shift]
      //   });
      }

      console.log('On change', this.allSelectedTimeSlots, this.shiftList);
      
    } else if (!event.target.checked) {
      console.log('before remove from list', this.allSelectedTimeSlots);
      if (this.allSelectedTimeSlots.length) {
        this.allSelectedTimeSlots.map((slot, index) => {
          if (slot.day === this.selectedDay) {

            const fileredSlots = slot.Timeslot.filter(item => {
                console.log('slot', item, typeof item, shift.id, typeof shift.id, (item !== shift.id));
                return item !== shift.id;
            });
            const fileredSlotDetails = slot.timeSlotDetails.filter(item => {
                console.log('slot', item, typeof item, shift.id, typeof shift.id, (item !== shift.id));
                return item.id !== shift.id;
            });

            slot.Timeslot = fileredSlots;
            slot.timeSlotDetails = fileredSlotDetails;
          }
        });
      }
      console.log('after remove from list', this.allSelectedTimeSlots);
    }
  }


  // save time details from modal
  saveShiftTime() {

    if (this.selectedShiftTiming.length) {
      this.availabilityList = this.selectedShiftTiming;
    }

    console.log('saveShiftTime availabilityList', this.availabilityList)

    //  this.availabilityList.push(this.selectedShiftTiming)
    // this.availabilityList.push({
    //   day: this.selectedDay,
    //   Timeslot: this.selectedShiftTiming
    // })

    this.modalRef.hide();
    // this.selectedShiftTiming = [];

    console.log('availabilityList', this.availabilityList);

  }

  // deleteShift
  deleteShift(shift,list) {
    console.log("shift",shift);
    // console.log("list",list.availabilityId);
    console.log("list",list);

    this.allSelectedTimeSlots.map((slot, index) => {
      if (slot.day === this.selectedDay) {

        const fileredSlots = slot.Timeslot.filter(item => {
            console.log('slot', item, typeof item, shift.id, typeof shift.id, (item !== shift.id));
            return item !== shift.id;
        });
        const fileredSlotDetails = slot.timeSlotDetails.filter(item => {
            console.log('slot', item, typeof item, shift.id, typeof shift.id, (item !== shift.id));
            return item.id !== shift.id;
        });

        slot.Timeslot = fileredSlots;
        slot.timeSlotDetails = fileredSlotDetails;
      }
    });

    // if (this.availabilityList[index]) {
    //   this.availabilityList.splice(index, 1);

    
    // }
    // for(let i =0;i<this.availabilityList.length;i++){
    //   this.lists  = this.availabilityList[i].availabilityIdzzz
    // }
    console.log("this.lists",this.lists);

    let payload = {
      candidateId: this.userDetails.candidateId,
      Timeslot: shift.id,
      availabilityId:list.availabilityId
    }
    this.candidateEditProfileService.DeleteAblityDetails(payload).pipe(takeUntil(this.onDestroyUnSubscribe)).subscribe(result=>{
     
    })
    this.selectCheckBoxForDay();
  }

  // saveavailability
  saveAvailability() {
    // console.log('saveAvailability', this.availabilityList);

    // let availability = {};
    // let timeslotsArray = [];

    // Object.keys(this.availabilityList).map(
    //   key => {
    //     //    console.log('key', key, this.availabilityList[key] , this.allSelectedTimeSlots[key] );
    //     if (this.availabilityList[key].day === this.allSelectedTimeSlots[key].day) {
    //       timeslotsArray.push(this.allSelectedTimeSlots[key].Timeslot);
    //       availability[this.allSelectedTimeSlots[key].day] = timeslotsArray;
    //     }
    //   }
    // );

    //   console.log('availability', availability);

    let availabilityObject = {
      candidateId: this.userDetails.candidateId,
      availability: this.allSelectedTimeSlots.filter(item => item.Timeslot.length)
    };

    console.log(this.allSelectedTimeSlots.filter(item => item.Timeslot.length));
    

    //   console.log('availabilityObject', availabilityObject);


    this.candidateEditProfileService.postAvailabilityDetails(availabilityObject)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {
            this.snackBar.open('availability UPDATED', 'Got it!', {
         
            });
            // show alert and navigate to job-post component
            this.router.navigate(['/candidate/profile/edit']);
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );

  }

  filterAvailablity(availabilityArray) {
    // for(let i =0; i<this.allSelectedTimeSlots.length; i++){
    //   this.timeSlot[i] = this.allSelectedTimeSlots[i].timeSlotDetails
    //   console.log("timeslot",this.timeSlot[i]);

    
    // }

    // for (let i = 0; i<this.timeSlot.length; i++){
    //   this.selectdayss[i] = this.timeSlot[i].slotName
    

    // }
    // console.log("timeslot",this.selectdayss);

    // console.log("availabilityArray",availabilityArray);
    return availabilityArray.filter(item => ((item.day === this.selectedDay) && item.timeSlotDetails.length && item.availabilityId));
    // console.log("availabilityArray",availabilityArray);
    
  }
  filterAvailablitys(availabilityArray) {
    // console.log("availabilityArray",availabilityArray);
    return availabilityArray.filter(item => ((item) && item.timeSlotDetails.length)); 
    // console.log("availabilityArray",availabilityArray);
    
  }

}
