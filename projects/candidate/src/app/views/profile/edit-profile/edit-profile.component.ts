import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CandidateEditProfileService } from '../../../services';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'candidate-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  storedDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
 persanolData;
 workexp= [];
 location= [];
 upload = [];
 dayoff = [];
 jobrole = [];
 video;
 valu:boolean = false;
 profilePic;
  profileDetails;
  constructor(
    private router: Router,
    private candidateEditProfileService: CandidateEditProfileService,
    private globalActionsService: GlobalActionsService, public snackBar: MatSnackBar,
    private firestoreDB: AngularFirestore
  ) { }

  ngOnInit() {
    // this.persanolData= localStorage.getItem('persanalaDetails');
    // this.workexp= localStorage.getItem('workExprience');
    // this.location= localStorage.getItem('location');
    // this.upload= localStorage.getItem('uploadFile');
    // this.dayoff= localStorage.getItem('daysOF');
    // this.jobrole= localStorage.getItem('roles');

    console.log('data',this.persanolData, this.workexp , this.location ,this.upload  , this.dayoff, this.jobrole);

    this.getProfileDetails();
   
  }

  //get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.storedDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result['details'] && !result['isError']) {
            //set value into form
            this.profileDetails = result['details']
            this.persanolData = result['details'].contactNo;
            this.workexp = result['details'].workExperience.experiences[0];
            this.location = result['details'].geolocation.locationName;
            this.jobrole = result['details'].payloadSkill;
            this.dayoff = result['details'].dayoff;
            this.upload = result['details'].docs 
            this.video = result['details'].video
            this.profilePic = result['details'].profilePic

            // condition && this.dayoff.length>0 && this.upload.length>0
            if(this.persanolData && this.workexp &&this.location && this.jobrole.length>0){
              this.valu=true;
            }
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }


  //profile picture upload for candidate
  getSelectedImage(event) {
    if (event.target.files && event.target.files[0]) {
      let folderName = "profile"
      this.globalActionsService.uploadFile(event.target.files[0], this.storedDetails.userType, folderName, 'profile')
        .then(
          (res) => { // Success
            //   console.log('toPromise', res, res['Location']);
            const uploadData = {
              candidateId: this.storedDetails.candidateId,
              profilePic: res['Location']
            };

            this.candidateEditProfileService.profileS3ImageUpload(uploadData)
              .pipe(takeUntil(this.onDestroyUnSubscribe))
              .subscribe(
                (result) => {

                  if (result && !result['isError']) {
                    let messageSuccess = 'Image uploaded successfully.'
                    this.snackBar.open(messageSuccess, 'Got it!', {
                    
                    });

                    let candidateDetailsStored = JSON.parse(localStorage.getItem('currentUser'));
                    candidateDetailsStored.profilePic = res['Location'];
                    localStorage.setItem('currentUser', JSON.stringify(candidateDetailsStored));
                    this.globalActionsService._setProfilePic({ status: 'set' });
                    this.getProfileDetails();

                    //update candidate profile pic
                    this.firestoreDB.collection('chatrooms', ref => ref.where('candidateId', '==', this.storedDetails.candidateId)).get().subscribe(
                      docsSnapshot => {
                        if (!docsSnapshot.empty) {
                          docsSnapshot.docs.forEach(element => {
                            console.log("element", element, element.id);
                            this.firestoreDB.firestore.collection('chatrooms').doc(element.id).update({ candidateProfilePic: res['Location'] })
                          });
                        }
                      });

                  } else {
                    this.snackBar.open('Internal Server Error', 'Got it!', {
                     
                    });
                  }
                },
                (error) => {
                  this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
                  
                  });
                },
              );
          }
        );
    }
  }

  submitCandidateAllProfile() {

    if(!this.profilePic) {
      this.snackBar.open('please upload profile image','Got it!',{

      })
      return false;
    }
    
  this.router.navigate(['/candidate/profile']);
}
}
