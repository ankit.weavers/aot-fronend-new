import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService } from '../../../../services';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'candidate-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  public allLanguages: any[] = [];
  languagesEditForm: FormGroup;
  selectedLanguagesList = [];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));

  constructor(
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar,
    private candidateEditProfileService: CandidateEditProfileService,
    private router: Router,
    private fb: FormBuilder,

  ) {
    this.languagesEditForm = this.fb.group({
      language: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.getAllLanguages();
    this.getProfileDetails();
  }

  //get all countries
  getAllLanguages() {
    this.globalActionsService.getLanguage()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allLanguages = result['details'];
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
              duration: 3000,
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            duration: 3000,
          });
        }
      );
  }

  //showSelectedLanguage
  showSelectedLanguage() {
    const selectedLanguages = this.selectedLanguagesList;
    this.selectedLanguagesList = this.languagesEditForm.value.language;
    selectedLanguages.map(item => {
      this.selectedLanguagesList.filter(filterItem => {
        if ((item.name === filterItem.name) && item.rank) {
          filterItem.rank = item.rank;
        }
      });
    });
  console.log("showSelectedLanguage",selectedLanguages) 

  }

  //selectRank for the selected language
  selectRank(index, rankType) {
    this.selectedLanguagesList[index].rank = rankType;
    console.log("selectRank", this.selectedLanguagesList[index].rank );

  }

  //get candidate profile details
  getProfileDetails() {

    this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            //set value into form
            this.languagesEditForm.get('language').setValue(result['details'].language);
            this.showSelectedLanguage()

          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              duration: 3000,
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            duration: 3000,
          });
        }
      );
  }

  //save languages
  saveLanguages() {
    let allRanksAvailable = true;
    this.selectedLanguagesList.map(item => {
      if (!item.rank) {
        allRanksAvailable = false;
      }
    });
    if (allRanksAvailable) {
      let languageObject = {
        candidateId: this.userDetails.candidateId,
        languages: this.selectedLanguagesList
      };
      this.candidateEditProfileService.postLangugageDetails({ languageObj: languageObject })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.snackBar.open('LANGUAGES EXPERIENCE UPDATED', 'Got it!', {
                duration: 3000,
              });
              //show alert and navigate to candidate profile component
              this.router.navigate(['/candidate/profile/edit']);
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
                duration: 3000,
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              duration: 3000,
            });
          }
        );

    } else {
      this.snackBar.open('Please select a language rank for all selected languages.', 'Got it!', {
        duration: 3000,
      });
    }
  }

}
