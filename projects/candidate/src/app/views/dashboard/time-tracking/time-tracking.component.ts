import { Component, OnInit } from '@angular/core';
import { CandidateJobService } from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'candidate-time-tracking',
  templateUrl: './time-tracking.component.html',
  styleUrls: ['./time-tracking.component.scss']
})
export class TimeTrackingComponent implements OnInit {
  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    autoplay: false,
    autoplayHoverPause: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  };
  private onDestroyUnSubscribe = new Subject<void>();
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  allActiveJobs = [];

  constructor(
    private candidatejobservice: CandidateJobService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getAllActiveJobTrackingList();
  }

  getAllActiveJobTrackingList() {
    this.candidatejobservice.getJobTrackingDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allActiveJobs = result['details'].results.reverse();
          }
          //  else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //   
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

}
