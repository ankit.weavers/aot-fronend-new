import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'candidate-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    localStorage.removeItem('jobDetailsData')
  }

}
