import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CollapseModule } from 'ngx-bootstrap';

import { CarouselModule } from 'ngx-owl-carousel-o';

import { DashboardRoutingModule } from './dashboard-routing.module';
import {
  DashboardComponent,
  JobOffersComponent,
  JobInvitationComponent,
  MetricsComponent,
  StatusComponent,
  TimeTrackingComponent
} from './';

@NgModule({
  declarations: [
    DashboardComponent,
    JobOffersComponent,
    JobInvitationComponent,
    MetricsComponent,
    StatusComponent,
    TimeTrackingComponent
  ],
  imports: [
    CommonModule,
    CarouselModule,
    CollapseModule.forRoot(),
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
