import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  DashboardComponent,
  JobOffersComponent,
  JobInvitationComponent,
  MetricsComponent,
  StatusComponent,
  TimeTrackingComponent
} from './';
const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Dashboard'
    },
    children: [
      /**
       * If url param not needed then turn this on
       */
      // {
      //   path: '',
      //   component: JobOffersComponent,
      //   data: {
      //     title: 'Job Offers'
      //   }
      // },
      /**
       * If url param not needed then turn this off
       */
      {
        path: '',
        redirectTo: 'job-offers',
        pathMatch: 'full'
      },
      {
        path: 'job-offers',
        component: JobOffersComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Job Offers'
        }
      },
      {
        path: 'job-invitations',
        component: JobInvitationComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Job Invitations'
        }
      },
      {
        path: 'status',
        component: StatusComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Status'
        }
      },
      {
        path: 'time-tracking',
        component: TimeTrackingComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Time Tracking'
        }
      },
      {
        path: 'metrics',
        component: MetricsComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Metrics'
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
