import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService, CandidateJobService } from '../../../services';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

declare var google: any;
@Component({
  selector: 'candidate-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss']
})
export class MetricsComponent implements OnInit {
  public hideRuleContent:boolean[] = [];
  public buttonName:any = 'Expand';  
  public stars: any[] = new Array(5);
  allRatings =[];
  allRatingAvg:{};
  acceptedJobList = [];
  appliedJobList = [];
  declinedJobList = [];
  jobsTakenList = [];
  jobsOfferedNotTaken = [];
  completedJobList = [];
  ratingList = [];
  hiredJob=[];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  isCollapsed = true;
  customOptions: any = {
    loop: true,
    mouseDrag: true,
    autoHeight:true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: true,
    autoplay: false,
    autoplayHoverPause: false,
    navSpeed: 700,
    navText: ['<i class="far fa-arrow-alt-circle-left"></i>', '<i class="far fa-arrow-alt-circle-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    }
  }

  constructor(
    public snackBar: MatSnackBar,
    private candidateprofileservice: CandidateEditProfileService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private candidatejobservice: CandidateJobService,
  ) { }

  ngOnInit() {
    // this.getJobDetails();
    this.mapsAPILoader.load().then(() => {
      this.getJobDetails();
    });
    // Fetch candidates overall ratings
    this.fetchOverallRatings();
  }

  //get all job details by id
  getJobDetails() {
    this.candidateprofileservice.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            //applied jobs
            this.appliedJobList = result['details'].appliedJobs.reverse()
            //accepted jobs
            this.acceptedJobList = result['details'].acceptedJobs.reverse();
            //jobs that are offered but not taken jobs
            this.declinedJobList = result['details'].declinedJobs.reverse();
            this.hiredJob =result['details'].hiredJobs.reverse();
            // if (this.appliedJobList) {
            //   this.appliedJobList.map(data => {
            //     data.formattedAddress = 'Loading...';
            //     this.getFormattedLocation(data);
            //   });
            // }
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
          
            });
          }

          //jobs taken
          // this.jobsTakenList

          //jobs declined
          //  this.jobsHiredNotTaken =

          //job role rating list
          // this.ratingList

        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  // setting current map position
  // getFormattedLocation(data) {
  //   if (navigator.geolocation && data.location) {
  //     let geocoder = new google.maps.Geocoder();
  //     let latlng = new google.maps.LatLng(data.location[1], data.location[0]);
  //     geocoder.geocode({ 'location': latlng }, (results, status) => {
  //       if (status === google.maps.GeocoderStatus.OK) {
  //         if (results[0] != null) {
  //           data.formattedAddress = results[0].formatted_address;
  //         } else {
  //           alert('No address available!');
  //         }
  //       }
  //     });
  //   }
  // }

  //view job details
  viewJob(employerId, jobId, roleId) {
    localStorage.setItem('jobDetailsData', JSON.stringify({
      employerId: employerId,
      jobId: jobId,  //toString()
      roleId: roleId,
    }));
    this.router.navigate(['/candidate/job/job-details']);
  }

  // Fetch Overall Ratings
  fetchOverallRatings(){
    this.candidateprofileservice.getCandidateOverallRatings({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allRatings = result['details'].allRatings;
            this.allRatingAvg = result['details'].allRatingAvg;
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );    
  }
  // Toggle Rolewise Rating Colapse
  toggle(index) {
    // toggle based on index
    this.hideRuleContent[index] = !this.hideRuleContent[index];
  } 
}
