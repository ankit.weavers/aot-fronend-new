import { Component, OnInit, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService, CandidateJobService } from '../../../services';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// AGM
import { MapsAPILoader } from '@agm/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
declare var google: any;

@Component({
  selector: 'candidate-job-invitation',
  templateUrl: './job-invitation.component.html',
  styleUrls: ['./job-invitation.component.scss']
})
export class JobInvitationComponent implements OnInit {

  modalRef: BsModalRef;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allOffers = [];
  jobId;
  roleId;
  employerId;
  profileCheck: any;
  messages: any;
  checkValue: boolean;
  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    autoplay: false,
    autoplayHoverPause: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }
  constructor(
    public snackBar: MatSnackBar,
    private candidateprofileservice: CandidateEditProfileService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private candidatejobservice: CandidateJobService,
    private modalService: BsModalService,

  ) { }

  ngOnInit() {
    // this.getJobDetails();
    this.mapsAPILoader.load().then(() => {
      this.getJobDetails();
    });
    this.checkCandidate();

  }

     // check candidate profile //
     checkCandidate() {
      this.candidatejobservice._checkCandidate({candidateId: this.userDetails.candidateId})
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(result=>{
        if(result && !result['isError']) {
        this.profileCheck = result['details'].isAuthenticated
        this.messages = result['details'].message
  
        }
        else {
          this.snackBar.open(result['message'],'Got it!',{
              
          })
          this.messages = result['details'].message
        }
      })
    }


  //get all job details by id
  getJobDetails() {
    this.candidateprofileservice.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allOffers = result['details'].invitationToApplyJobs.reverse();

            // console.log("allOffers" , this.allOffers);

            // if (this.allOffers) {
            //   this.allOffers.map(data => {
            //     data.formattedAddress = 'Loading...';
            //     this.getFormattedLocation(data);
            //   });
            // }
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  // setting current map position
  // getFormattedLocation(data) {
  //   if (navigator.geolocation) {
  //     let geocoder = new google.maps.Geocoder();
  //     let latlng = new google.maps.LatLng(data.location[1], data.location[0]);
  //     geocoder.geocode({ 'location': latlng }, (results, status) => {
  //       if (status === google.maps.GeocoderStatus.OK) {
  //         if (results[0] != null) {
  //           data.formattedAddress = results[0].formatted_address;
  //         } else {
  //           alert('No address available!');
  //         }
  //       }
  //     });
  //   }
  // }

  //open modal
  openModal(template: TemplateRef<any>, employerId, jobId, roleId) {
    this.employerId = employerId;
    this.jobId = jobId;
    this.roleId = roleId;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', keyboard: false });
    // this.stricktoApply(this.jobId,this.roleId);

    // this.modalRef.hide();
  }

  stricktoApply(empId,data,roleId) {
    this.candidatejobservice.checkaotOfferAll({ candidateId: this.userDetails.candidateId, jobId: data,roleId:roleId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(result => {
        if (result && !result['isError']) {
          this.checkValue = result['details'].isCandidateAvailable;
          console.log('checkvalue1', this.checkValue);

          this.changeStatusOfJob(empId,data,roleId);
        }
      })
  }

  //accept / decline job offer
  changeStatusOfJob(employerId, jobId, roleId) {
    if (this.profileCheck == "True") {
      if (this.checkValue) {
    let acceptFormData = {
      candidateId: this.userDetails.candidateId,
      employerId: employerId,
      jobId: jobId,
      roleId: roleId,
    }
    this.candidatejobservice.acceptJobInvitation(acceptFormData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          this.snackBar.open(result['message'], 'Got it!', {
           
          });
          if (result && !result['isError']) {
            //  this.modalRef.hide();
            this.snackBar.open('You have successfully applied for this job', 'Got it!', {
             
            });
            this.router.navigate(['/candidate/dashboard/status'])
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
      }
      else {
        this.snackBar.open('you are not available for this job', 'Got it!', {

        })
      }
    }
    else {
      this.snackBar.open(this.messages, 'Got it!', {

      })
    }

  }

  //view job details
  viewJob(employerId, jobId, roleId) {
    localStorage.setItem('jobDetailsData', JSON.stringify({
      employerId: employerId,
      jobId: jobId,  //toString()
      roleId: roleId,
    }));
    this.router.navigate(['/candidate/job/job-details']);
  }

}
