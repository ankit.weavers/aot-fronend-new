import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { CandidateEditProfileService, CandidateJobService } from '../../../services';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// AGM
import { MapsAPILoader } from '@agm/core';
import * as moment from 'moment';
declare var google: any;

@Component({
  selector: 'candidate-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  acceptedJobList = [];
  hiredJobList = [];
  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    autoplay: false,
    autoplayHoverPause: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }

  constructor(
    public snackBar: MatSnackBar,
    private candidateprofileservice: CandidateEditProfileService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private candidatejobservice: CandidateJobService,
  ) { }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      this.getJobDetails();
    });
  }

  //get all job details by id
  getJobDetails() {
    this.candidateprofileservice.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.acceptedJobList = result['details'].acceptedJobs.reverse();
            this.hiredJobList = result['details'].hiredJobs.reverse();

            // if (this.acceptedJobList) {
            //   this.acceptedJobList.map(data => {
            //     data.formattedAddress = 'Loading...';
            //     this.getFormattedLocation(data);
            //   });
            // }
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  // Cancel job by Worker //
  cancelJob(jobId,roleId,i:number) {
    this.candidateprofileservice._cancelJOB({ candidateId: this.userDetails.candidateId,jobId:jobId,roleId:roleId, today:moment(new Date()).format('MM/DD/YYYY'),
    presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
    localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone })
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && !result['isError']) {
          this.hiredJobList.splice(i,0);
          this.getJobDetails()

          // if (this.acceptedJobList) {
          //   this.acceptedJobList.map(data => {
          //     data.formattedAddress = 'Loading...';
          //     this.getFormattedLocation(data);
          //   });
          // }
          this.snackBar.open('You have Canceled this job', 'Got it!', {
           
          });
        } else {
          this.snackBar.open(result['message'], 'Got it!', {
           
          });
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
        });
      }
    );
  }

  // setting current map position
  // getFormattedLocation(data) {
  //   if (navigator.geolocation) {
  //     let geocoder = new google.maps.Geocoder();
  //     let latlng = new google.maps.LatLng(data.location[1], data.location[0]);
  //     geocoder.geocode({ 'location': latlng }, (results, status) => {
  //       if (status === google.maps.GeocoderStatus.OK) {
  //         if (results[0] != null) {
  //           data.formattedAddress = results[0].formatted_address;
  //         } else {
  //           alert('No address available!');
  //         }
  //       }
  //     });
  //   }
  // }

  //accept / decline job offer
  declineJob(employerId, jobId, roleId) {

    let declineFormData = {
      candidateId: this.userDetails.candidateId,
      employerId: employerId,
      jobId: jobId,
      roleId: roleId,
      declinefrom: '2'
    }
    this.candidatejobservice.declineJobById(declineFormData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('You have declined this job offer', 'Got it!', {
              
            });
            this.router.navigate(['/candidate/dashboard/metrics'])
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );

  }

  //reject job after being hired by employer
  rejectJob(employerId, jobId, roleId) {
  }

  //view job details
  viewJob(employerId, jobId, roleId) {
    localStorage.setItem('jobDetailsData', JSON.stringify({
      employerId: employerId,
      jobId: jobId,  //toString()
      roleId: roleId,
    }));
    this.router.navigate(['/candidate/job/job-details']);
  }

}
