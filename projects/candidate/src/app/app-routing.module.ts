import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroductoryComponent } from './views/introductory/introductory.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'get-started',
    component: IntroductoryComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: false,
      isEditable: [],
      title: 'Get Started'
    }
  },
  {
    path: '',
    loadChildren: './views/static/static.module#StaticModule'
  },
  {
    path: 'dashboard',
    loadChildren: './views/dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'profile',
    loadChildren: './views/profile/profile.module#ProfileModule'
  },
  {
    path: 'job',
    loadChildren: './views/job/job.module#JobModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
