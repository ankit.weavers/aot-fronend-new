// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders } from '@angular/compiler/src/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CandidateCoreModule } from './modules/candidate-core.module';

const providers = [];
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    CandidateCoreModule,
  ],
  providers,
  bootstrap: [AppComponent]
})
export class CandidateAppModule { }

@NgModule({})
export class CandidateMainModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CandidateAppModule,
      providers
    };
  }
}
