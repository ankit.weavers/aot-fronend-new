import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/modules/shared.module';

import { DashboardModule } from '../views/dashboard/dashboard.module';
import { JobModule } from '../views/job/job.module';
import { StaticModule } from '../views/static/static.module';
import { ProfileModule } from '../views/profile/profile.module';

import { IntroductoryComponent } from '../views/introductory/introductory.component';

@NgModule({
  declarations: [
    IntroductoryComponent,
  ],
  imports: [
    SharedModule,
    DashboardModule,
    JobModule,
    StaticModule,
    ProfileModule
  ],
  exports: [
    SharedModule
  ]
})
export class CandidateCoreModule { }
