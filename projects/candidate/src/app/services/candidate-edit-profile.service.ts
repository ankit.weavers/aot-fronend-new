import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HandleErrorService } from 'src/app/services/handle-error.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
// Models
import { User, Company, CompanyAddress, CompanyContact } from 'src/app/models';

// Content Type
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

@Injectable({
    providedIn: 'root'
})
export class CandidateEditProfileService {

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService
    ) { }


    //Get profile details
    getCandidateProfileDetails(candidateId) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-candidate-details`;
        return this.http.post(apiUrl, candidateId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getCandidateProfileDetails'))
            );
    }

        //Cancel job
        _cancelJOB(payload) {
            const apiUrl = `${environment.SERVER_URL}/candidate-job/cancel-job-for-candidate`;
            return this.http.post(apiUrl, payload, httpOptions)
                .pipe(
                    map(response => response),
                    catchError(this.handleErrorService.handleError('_cancelJOB'))
                );
        }

    //profile image upload from S3 bucket
    profileS3ImageUpload(uploadData): Observable<User> {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/profile-pic-upload`;
        return this.http.post<User>(apiUrl, uploadData, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError<User>('profileS3ImageUpload'))
            );
    }

    //Post profile details
    postEditProfileDetails(editProfileDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-personal-details`;
        return this.http.post(apiUrl, editProfileDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postEditProfileDetails'))
            );
    }


    //Post profile details
    postWorkExpDetails(workExpDetails) {
        console.log('okkkk')
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-experience`;
        return this.http.post(apiUrl, workExpDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postWorkExpDetails'))
            );
    }

    //Post profile details
    postLangugageDetails(languageDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-language`;
        return this.http.post(apiUrl, languageDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postLangugageDetails'))
            );
    }

    //Post proximity details
    postProximityDetails(proximityDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-location`;
        return this.http.post(apiUrl, proximityDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postProximityDetails'))
            );
    }

    //Post job roles details
    postJobRolesDetails(jobRolesDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-roles`;
        return this.http.post(apiUrl, jobRolesDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postJobRolesDetails'))
            );
    }

    //all docs  upload from S3 bucket for candidate
    allS3UploadedDocs(uploadData): Observable<User> {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-attachments`;
        return this.http.post<User>(apiUrl, uploadData, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError<User>('allS3UploadedDocs'))
            );
    }

    //Post avaibility details
    postAvailabilityDetails(avaibilityDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-avilability`;
        return this.http.post(apiUrl, avaibilityDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postAvailabilityDetails'))
            );
    }

    // Post days off details
    postDaysOffDetails(paysOffDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/add-dayoff`;
        return this.http.post(apiUrl, paysOffDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postDaysOffDetails'))
            );
    }
   DeleteDaysOffDetails(paysOffDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/delete-day-off`;
        return this.http.post(apiUrl, paysOffDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('DeleteDaysOffDetails'))
            );
    }
    DeleteAblityDetails(paysOffDetails) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/delete-availability`;
        return this.http.post(apiUrl, paysOffDetails, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('DeleteAblityDetails'))
            );
    }
    // Delete Files
    deleteDocuments(documentDetail) {
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/delete-Doc`;
        return this.http.post(apiUrl, documentDetail, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('deleteDocuments'))
            );
    }
    // Fetch Candidate Overall Ratings
    getCandidateOverallRatings(candidateId){
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-ratings`;
        return this.http.post(apiUrl, candidateId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('deleteDocuments'))
            );        
    }
    deleteWorkExp(candidateId){
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/delete-candidate-work-exp`;
        return this.http.post(apiUrl, candidateId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('deleteWorkExp'))
            );        
    }
   

}
