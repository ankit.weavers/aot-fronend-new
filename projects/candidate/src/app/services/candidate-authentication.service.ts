import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
// Models
import { User } from 'src/app/models';
// Handle error
import { HandleErrorService } from 'src/app/services/handle-error.service';

// Server Link
import { environment } from 'src/environments/environment';

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class CandidateAuthenticationService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  // Signup
  signup(userData: User): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/candidate/signup`;
    return this.http.post<User>(apiUrl, userData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('signup'))
      );
  }


  // change Password
  changePasswordFromProfile(formData): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/candidate/profile-change-password`;
    return this.http.post<User>(apiUrl, formData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('changePasswordFromProfile'))
      );
  }
}
