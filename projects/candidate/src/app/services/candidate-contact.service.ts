import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

// Handle error
import { HandleErrorService } from 'src/app/services/handle-error.service';

// Server Link
import { environment } from 'src/environments/environment';

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})


export class CandidateContactService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService    
  ) { }

  //Get candidate contact  details
  getCandidateContactDetails() {
    const apiUrl = `${environment.SERVER_URL}/common/get-contactus`;
    return this.http.post(apiUrl, httpOptions)
    .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getCandidateContactDetails'))
    );
  }
}
