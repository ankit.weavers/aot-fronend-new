// import { environment } from './../../../../../src/environments/environment';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HandleErrorService } from 'src/app/services/handle-error.service';
import { catchError, map, tap } from 'rxjs/operators';

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class CandidateJobService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  getSearchedJob(searchData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-jobs1`;
    return this.http.post(apiUrl, searchData, httpOptions).pipe(
      map(response => response),
      catchError(this.handleErrorService.handleError('getSearchedJob'))
    );
  }
  // Get job details by id 
  getJobDetailsById(jobData) {
    //  const apiUrl = `${environment.SERVER_URL}/candidate-job/job-details`;
    const apiUrl = `${environment.SERVER_URL}/candidate-job/job-details1`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getJobDetailsById'))
      );
  }

  //Apply job by candidate
  applyJobById(applyData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/apply-jobs`;
    return this.http.post(apiUrl, applyData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('applyJobById'))
      );
  }

  //Accept job by candidate
  acceptJobById(data) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/job-accept`;
    return this.http.post(apiUrl, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('acceptJobById'))
      );
  }

    //Accept job invitation by candidate
    acceptJobInvitation(data) {
      const apiUrl = `${environment.SERVER_URL}/candidate-job/apply-invitation`;
      return this.http.post(apiUrl, data, httpOptions)
        .pipe(
          map(response => response),
          catchError(this.handleErrorService.handleError('acceptJobInvitation'))
        );
    }

  //Decline job by candidate
  declineJobById(data) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/job-decline`;
    return this.http.post(apiUrl, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('declineJobById'))
      );
  }

  checkaotOfferAll(payload) {
    const apiUrl = `${environment.SERVER_URL}/emp-job/check-candidate-presence`;
    return this.http.post(apiUrl, payload, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('checkaotOfferAll'))
        );
}

_checkCandidate(payload) {
  const apiUrl = `${environment.SERVER_URL}/candidate-job/fetch-authenticity-of-candidate`;
  return this.http.post(apiUrl, payload, httpOptions)
      .pipe(
          map(response => response),
          catchError(this.handleErrorService.handleError('_checkCandidate'))
      );
}

  // Get Active job details by id 
  getAllActiveJobDetails(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-appointments`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getAllActiveJobDetails'))
      );
  }

  // Check active job wise attendance 
  checkAttendanceStatusById(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/populate-calender-data`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('checkAttendanceStatusById'))
      );
  }

  // Check active job wise attendance 
  getTodaysAttendance(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-datewise-calender-data`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getTodaysAttendance'))
      );
  }

  //candidate onsite check-in 
  candidateOnsiteCheckIn(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/onsite-checkin`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('candidateOnsiteCheckIn'))
      );
  }

  //candidate onsite check-out 
  candidateOnsiteCheckOut(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/onsite-checkout`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('candidateOnsiteCheckOut'))
      );
  }


  //candidate onsite automatic check-out 
  candidateAutomaticCheckOut(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/automatic-checkout`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('candidateAutomaticCheckOut'))
      );
  }

  //candidate offsite check-in 
  candidateOffsiteCheckIn(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/offline-checkin`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('candidateOffsiteCheckIn'))
      );
  }

  //candidate offsite check-out 
  candidateOffsiteCheckOut(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/offline-checkout`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('candidateOffsiteCheckOut'))
      );
  }

  // Get active job wise attendance 
  getAttendanceDetailsById(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-candidatewise-attendance`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getAttendanceDetailsById'))
      );
  }

  // getJobTrackingDetails
  getJobTrackingDetails(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/get-time-tracking`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getJobTrackingDetails'))
      );
  }

  //check todays date is a working date or not
  checkTodaysWorkingStatus(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-job/check-isWorking-day`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('checkTodaysWorkingStatus'))
      );
  }

  // Get Candidate Wise Ratings
  getAllRatingDetails(jobData) {
    const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-ratings`;
    return this.http.post(apiUrl, jobData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError('getAllRatingDetails'))
      );
  }
  getNotificationDetails(jobData){
      const apiUrl =`${environment.SERVER_URL}/candidate-job/fetch-notification-for-candidate`;
      return this.http.post(apiUrl,jobData,httpOptions)
      .pipe(
        map(response => response),
      catchError(this.handleErrorService.handleError('getNotificationDetails'))
      );
  }
  deleteNotificationDetails(payload){
    const apiUrl =`${environment.SERVER_URL}/candidate-job/delete-notification-for-candidate`;
    return this.http.post(apiUrl,payload,httpOptions)
    .pipe(
      map(response => response),
    catchError(this.handleErrorService.handleError('deleteNotificationDetails'))
    );
}
locationTracInsert(payload){
  const apiUrl =`${environment.SERVER_URL}/candidate-job/offsite-tracking`;
  return this.http.post(apiUrl,payload,httpOptions)
  .pipe(
    map(response => response),
  catchError(this.handleErrorService.handleError('locationTracInsert'))
  );
}

}
