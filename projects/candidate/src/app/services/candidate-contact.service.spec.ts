import { TestBed } from '@angular/core/testing';

import { CandidateContactService } from './candidate-contact.service';

describe('CandidateContactService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CandidateContactService = TestBed.get(CandidateContactService);
    expect(service).toBeTruthy();
  });
});
