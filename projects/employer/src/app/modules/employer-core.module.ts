import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { RouterModule } from '@angular/router';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { MatNativeDateModule } from '@angular/material';
// import { MaterialModule } from 'src/app/modules/material.module';

// import { CarouselModule } from 'ngx-owl-carousel-o';
import { SharedModule } from 'src/app/modules/shared.module';

// Lazy load modules
import { UserDashboardModule } from '../views/user-dashboard/user-dashboard.module';
import { EmployerJobModule } from '../views/job/employer-job.module';
import { EmployerStaticModule } from '../views/static/employer-static.module';
import { EmployerProfileModule } from '../views/profile/employer-profile.module';
import { CandidateModule } from '../views/candidate/candidate.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { IntroductoryComponent } from '../views/introductory/introductory.component';

@NgModule({
  declarations: [
    IntroductoryComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    SharedModule,
    // Lazy load modules
    UserDashboardModule,
    EmployerJobModule,
    EmployerStaticModule,
    EmployerProfileModule,
    CandidateModule,
  ],
  exports: [
    SharedModule
  ]
})
export class EmployerCoreModule { }
