import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
// Models
import { User, Company, CompanyAddress, CompanyContact } from 'src/app/models';
// Handle error
import { HandleErrorService } from 'src/app/services/handle-error.service';

// Server Link
import { environment } from 'src/environments/environment';

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class EmployerAuthenticationService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  // Save company details
  savecompanyDetails(companyData: Company): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/add-company`;
    return this.http.post<User>(apiUrl, companyData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('savecompanyDetails'))
      );
  }

  //Save profile changes
  saveProfileChanges(profileData){
    const apiUrl = `${environment.SERVER_URL}/employer-profile/add-signup-industry`;
    return this.http.post<User>(apiUrl, profileData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('saveProfileChanges'))
      );
  }

  // Save company address
  saveCompanyAddress(companyAddressData) {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/add-address`;
    return this.http.post<User>(apiUrl, companyAddressData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('saveCompanyAddress'))
      );
  }

  // Save company contact
  saveCompanyContact(companyContactData: CompanyContact): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/add-contact`;
    return this.http.post<User>(apiUrl, companyContactData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('saveCompanyContact'))
      );
  }

  // Getting employer details
  getEmployerDetails(employerId): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/get-details`;
    return this.http.post<User>(apiUrl, employerId, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('getEmployerDetails'))
      );
  }

  //  Uploading image
  profleImageUpload(uploadData): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/profile-pic-upload`;
    return this.http.post<User>(apiUrl, uploadData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('profleImageUpload'))
      );
  }

  //profile image upload from S3 bucket
  profileS3ImageUpload(uploadData): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/pic-upload`;
    return this.http.post<User>(apiUrl, uploadData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('profileS3ImageUpload'))
      );
  }

  // change Password
  changePassword(formData): Observable<User> {
    const apiUrl = `${environment.SERVER_URL}/employer-profile/profile-change-password`;
    return this.http.post<User>(apiUrl, formData, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleErrorService.handleError<User>('changePassword'))
      );
  }


}
