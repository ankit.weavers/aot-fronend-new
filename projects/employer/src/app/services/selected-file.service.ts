import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SelectedFileService {

  // private selectedFile = new BehaviorSubject<any>(null)
  // currentFile = this.selectedFile.asObservable();

  private selectedFileArray = new BehaviorSubject<any[]>(null)
  currentFileArray = this.selectedFileArray.asObservable();

  private searchString = new BehaviorSubject<any>(null)
  searchStringStatus = this.searchString.asObservable();
 


  constructor() { }

  // _setSelectedFile(fileObj: any) {
  //   this.selectedFile.next(fileObj);
  // }

  _setSelectedFileArray(fileObj: any[]) {
    this.selectedFileArray.next(fileObj);
  }

  _setSearchStatus(searchString: any) {
    this.searchString.next(searchString);
    
  }

}
