import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HandleErrorService } from 'src/app/services/handle-error.service';
import { catchError, map, tap } from 'rxjs/operators';

// Content Type
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

@Injectable({
    providedIn: 'root'
})
export class EmployerJobService {
    JobDetails;

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService
    ) { }
    //set localstorage for employer
    setLocalStorage(key, value) {
        localStorage.setItem(key, value)
    }
    //get local storage for employer
    getLocalStorage(key: string) {
        return localStorage.getItem(key) ? localStorage.getItem(key) : null

    }
    // Set Employer Job Details
    setJobDetails(jobDetails) {
        localStorage.setItem('JobDetails', JSON.stringify(jobDetails));
    }
    // Set Employer File Details
    setFileDetails(file) {
        localStorage.setItem('uploadedFile', file)
    }

    // Get Employer File Details
    get getFileDetails() {
        let file = localStorage.getItem('uploadedFile');
        if (file) {
            return file;
        }
        else {
            return null;
        }
    }
    // Set newly created jon ids
    setNewlyCreatedJobids(jobIds) {
        console.log('jonsdvsd ')
        localStorage.setItem('jobIds', JSON.stringify(jobIds));
    }
    // Get Employer Date Time Details
    get getNewlyCreatedJobids() {
        if (localStorage.getItem('jobIds')) {
            return JSON.parse(localStorage.getItem('jobIds'));
        }
        else {
            return null;
        }
    }
    // Get Employer Job Details
    get getJobDetails() {

        if (localStorage.getItem('JobDetails')) {
            return JSON.parse(localStorage.getItem('JobDetails'));
        }
        else {
            return null;
        }
    }

    // Set Employer Job Location
    setJobLocation(jobLocationDetails) {
        localStorage.setItem('JobLocation', JSON.stringify(jobLocationDetails))
    }

    // Get Employer Location Details
    get getJobLocation() {

        if (localStorage.getItem('JobLocation')) {
            return JSON.parse(localStorage.getItem('JobLocation'));
        }
        else {
            return null;
        }
    }

    // Set Employer Skills
    setSkillDetails(jobskillDetails) {
        console.log(JSON.stringify(jobskillDetails));
        localStorage.setItem('JobSkills', JSON.stringify(jobskillDetails))
    }

    // Get Employer Skills
    get getSkillDetails() {

        if (localStorage.getItem('JobSkills')) {
            return JSON.parse(localStorage.getItem('JobSkills'));
        }
        else {
            return null;
        }
    }

    // Skills By Job Role Id
    //   getJobSkillById(jobRoleId) {

    //       const apiUrl = `${environment.SERVER_URL}/admin/get-rolewise-skill`;
    //       return this.http.post(apiUrl, jobRoleId, httpOptions)
    //           .pipe(
    //               map(response => response),
    //               catchError(this.handleErrorService.handleError('getJobSkillById'))
    //           );
    //   }

    // Set Employer Date Time Details
    setDateTimeDetails(datetimeDetails) {
        localStorage.setItem('Datetime', JSON.stringify(datetimeDetails))
    }

    // set original time to help fetch original time for initialisation
    setTimeDetails(timedetails) {
        localStorage.setItem('SaveTimeJobPost', JSON.stringify(timedetails))
    }

    // Get Employer Date Time Details
    get getDateTimeDetails() {

        if (localStorage.getItem('Datetime')) {
            return JSON.parse(localStorage.getItem('Datetime'));
        }
        else {
            return null;
        }
    }

    // Get Employer Time Details
    get getTimeDetails() {

        if (localStorage.getItem('SaveTimeJobPost')) {
            return JSON.parse(localStorage.getItem('SaveTimeJobPost'));
        }
        else {
            return null;
        }
    }



    // Set Employer Payment
    setPayment(paymentDetails) {
        localStorage.setItem('Payment', JSON.stringify(paymentDetails))
    }

    // Get Employer Payment Details
    get getPayment() {

        if (localStorage.getItem('Payment')) {
            return JSON.parse(localStorage.getItem('Payment'));
        }
        else {
            return null;
        }
    }

    // Post all job details
    postJob(job) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/post-multi-job1`;
        return this.http.post(apiUrl, job, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postJob'))
            );
    }

    //post job as template
    postJobAsTemplate(job) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/add-template`;
        return this.http.post(apiUrl, job, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('postJob'))
            );
    }

    // Edit job by particular role
    updateJob(job) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/update-job`;
        return this.http.post(apiUrl, job, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('updateJob'))
            );
    }

        // Edit job by particular role
        _checkProfile(job) {
            const apiUrl = `${environment.SERVER_URL}/emp-job/fetch-authenticity-of-employer`;
            return this.http.post(apiUrl, job, httpOptions)
                .pipe(
                    map(response => response),
                    catchError(this.handleErrorService.handleError('_checkProfile'))
                );
        }

    // Set Employer All Jobs
    setAllJobs(allJobs) {
        localStorage.setItem('allJobs', JSON.stringify(allJobs))
    }

    // Get Employer All Jobs
    get getAllJob() {

        if (localStorage.getItem('allJobs')) {
            return JSON.parse(localStorage.getItem('allJobs'));
        }
        else {
            return null;
        }
    }

    //get all job templates by employer id
    getAllJobTemplates(employerId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-all-template`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getAllJobTemplates'))
            );
    }
    //get all job template detailsby template id
    getTemplateDetailsById(templateId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-template`;
        return this.http.post(apiUrl, templateId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getTemplateDetailsById'))
            );
    }


    // Get all job details by id
    getJobsPostedById(userId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-jobs1`;
        return this.http.post(apiUrl, userId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getJobsPostedById'))
            );
    }

    //get each job details by job id
    getJobDetailsById(formData) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-apllied-candidate1`;
        return this.http.post(apiUrl, formData, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getJobDetailsById'))
            );
    }
 //cancel candiate
 _cancelJOB(formData) {
    const apiUrl = `${environment.SERVER_URL}/emp-job/cancel-job-for-candidate-by-employer`;
    return this.http.post(apiUrl, formData, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('_cancelJOB'))
        );
}

 //cancel job
 _cancelJOBfromJoblist(formData) {
    const apiUrl = `${environment.SERVER_URL}/emp-job/cancel-job-for-by-employer`;
    return this.http.post(apiUrl, formData, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('_cancelJOBfromJoblist'))
        );
}
    //get each job details by job id for Editing job
    getEditJobDetailsById(formData) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/edit-job`;
        return this.http.post(apiUrl, formData, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getEditJobDetailsById'))
            );
    }

    //get each candidate profile by candidate id
    getCandidateProfileById(jobDetailsForCandidate) {
        // const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-candidate-details`;
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-candidate-details-withstatus`;
        return this.http.post(apiUrl, jobDetailsForCandidate, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getCandidateProfileById'))
            );
    }

    getCandidateProfile(jobDetailsForCandidate) {
        // const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-candidate-details`;
        const apiUrl = `${environment.SERVER_URL}/candidate-profile/view-candidate-profile`;
        return this.http.post(apiUrl, jobDetailsForCandidate, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getCandidateProfile'))
            );
    }

    //offer job to candidate
    offerJobToCandidate(details) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/job-approve`;
        return this.http.post(apiUrl, details, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('offerJobToCandidate'))
            );
    }

    //search jobs in employee dashboard
    searchJobInDashboard(details) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/search-job-byName1`;
        return this.http.post(apiUrl, details, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('searchJobInDashboard'))
            );
    }

    // get Search History by employer
    getSearchHistory(employerId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-last-search-name`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getSearchHistory'))
            );
    }


    //hire job to candidate
    hireCandidate(details) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/hire-candidate`;
        return this.http.post(apiUrl, details, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('hireCandidate'))
            );
    }

    // get Employed Candidate List by employer
    getAllEmployedCandidateDetails(employerId) {

        const apiUrl = `${environment.SERVER_URL}/emp-job/get-employed-candidate`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getAllEmployedCandidateDetails'))
            );
    }
    getAllEmployedCandidateDetail(employerId) {

        const apiUrl = `${environment.SERVER_URL}/emp-job/get-favourite-candidate-for-emp`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getAllEmployedCandidateDetails'))
            );
    }
    //search Employed candidiate by name on applican page in employee dashboard
    searchEmployedCandidiateByNAme(details) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-employed-candidate-byname`;
        return this.http.post(apiUrl, details, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('searchJobInDashboard'))
            );
    }
    submitReview(reviewData) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/give-rating`;
        return this.http.post(apiUrl, Object.assign({}, reviewData), httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('searchJobInDashboard'))
            );
    }

    saveAsFav(employerId, candidateId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/mark-favourite`;
        let favData = Object.assign({ employerId: employerId, candidateId: candidateId });
        return this.http.post(apiUrl, Object.assign({}, favData), httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('saveAsFav'))
            );
    }
    removeFromFav(employerId, candidateId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/mark-unfavourite`;
        let favData = Object.assign({ employerId: employerId, candidateId: candidateId });
        return this.http.post(apiUrl, Object.assign({}, favData), httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('saveAsFav'))
            );
    }
    // Set Employer All Jobs
    setJobIdToGetRoles(jobIds) {
        localStorage.setItem('jobIds', jobIds)
    }
    // Edit job by particular role
    getRoles(jobIds) {
        let jobIdsObj = Object.assign({ jobId: jobIds });
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-roles-ByJobId`;
        return this.http.post(apiUrl, jobIdsObj, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getRoles'))
            );
    }

    getSkills(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-skills-ByJobroleId`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getSkills'))
            );
    }
    getSkill(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-skills-By-many-JobroleId`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getSkill'))
            );
    }
    getFilteredCandidates(payload) {
        // let staticPayload = Object.assign({ jobId: 'JOB156958698916636', roleId: 'ROLE1559285975687' });
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-choose-workers`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getFilteredCandidates'))
            );
    }
    getFilteredCandidate(payload) {
        // let staticPayload = Object.assign({ jobId: 'JOB156958698916636', roleId: 'ROLE1559285975687' });
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-choose-workers-and`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getFilteredCandidate'))
            );
    }
    getFilteredFavouriteCandidates(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getFilteredFavouriteCandidates'))
            );
    }
    getFilteredFavouriteCandidate(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite-and`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getFilteredFavouriteCandidate'))
            );
    }
    getBrowseCandidateOffers(payload) {

        const apiUrl = `${environment.SERVER_URL}/emp-job/browse-candidate-offer`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getBrowseCandidateOffers'))
            );
    }
    getBrowseCandidateOffer(payload) {

        const apiUrl = `${environment.SERVER_URL}/emp-job/browse-candidate-offer-and`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getBrowseCandidateOffer'))
            );
    }

    aotOfferAll(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-all`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('aotOfferAll'))
            );
    }
    checkaotOfferAll(payload) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/check-candidate-presence`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('checkaotOfferAll'))
            );
    }
    aotInviteCandidates(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/invite-to-apply`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('aotInviteCandidates'))
            );
    }

    getShiftName(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/determine-shiftname-logic1`;
        return this.http.post(apiUrl, payload, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getShiftName'))
            );
    }
    getOngoingJobById(employerId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-onging-jobs`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('getOngoingJobById'))
            );
    }
    searchJobInOngoingDashboard(employerId) {
        const apiUrl = `${environment.SERVER_URL}/emp-job/search-Ongoingjob-byName`;
        return this.http.post(apiUrl, employerId, httpOptions)
            .pipe(
                map(response => response),
                catchError(this.handleErrorService.handleError('searchJobInOngoingDashboard'))
            );
    }
    getCompletedJobById(employerId){
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-closed-jobs`;
        return this.http.post(apiUrl, employerId, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getCompletedJobById'))
        );
    }
    searchJobInCompletedDashboard(employerId){
        const apiUrl =`${environment.SERVER_URL}/emp-job/search-completedjob-byName`;
        return this.http.post(apiUrl, employerId, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('searchJobInCompletedDashboard'))
        );
    }
    getTaxValue(employerId){
        const apiUrl =  `${environment.SERVER_URL}/common/get-payment-breakDown-static-value`;
        return this.http.post(apiUrl, employerId, httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getTaxValue'))
        );
    }
    getEmployerContactDetail(employerId){
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-candidatelist`;
        return this.http.post(apiUrl,employerId,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getEmployerContactDetail'))
        );
    }
    getEmployerContactDetails(employerId){
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-all-candidate-for-emp`;
        return this.http.post(apiUrl,employerId,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getEmployerContactDetail'))
        );
    }
    getCandidateFillter(employerId){
        const apiUrl = `${environment.SERVER_URL}/emp-job/get-all-candidate-for-emp-for-role-and-skill`;
        return this.http.post(apiUrl,employerId,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getCandidateFillter'))
        );
    }

    pastJobs(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/fetch-completed-job-of-candidate`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('pastJobs'))
        );
    }
    getNotificationDetails(employerId){
        const apiUrl = `${environment.SERVER_URL}/emp-job/fetch-notification-for-employer`;
        return this.http.post(apiUrl,employerId,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getNotificationDetails'))
        );

    }
    deleteNotificationDetails(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/delete-notification-for-employer`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('deleteNotificationDetails'))
        );

    }
    getCheckinCheckoutData(payload){
        const apiUrl = `${environment.SERVER_URL}/candidate-job/get-time-tracking-for-date`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getCheckinCheckoutData'))
        );

    }
    getCheckinlocationTracking(payload){
        const apiUrl = `${environment.SERVER_URL}/candidate-job/fetch-office-trackin-details`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('getCheckinlocationTracking'))
        );

    }
    deleteTemp(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/delete-template`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('deleteTemp'))
        );

    }
        //Get profile details
        getCandidateProfileDetails(candidateId) {
            const apiUrl = `${environment.SERVER_URL}/candidate-profile/get-candidate-details`;
            return this.http.post(apiUrl, candidateId, httpOptions)
                .pipe(
                    map(response => response),
                    catchError(this.handleErrorService.handleError('getCandidateProfileDetails'))
                );
        }

    andtireone(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite-and-tier1`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('andtireone'))
        );

    }
    andtiretwo(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite-and-tier2`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('andtiretwo'))
        );

    }

    ortireone(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite-tier1`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('ortireone'))
        );

    }
    ortiretwo(payload){
        const apiUrl = `${environment.SERVER_URL}/emp-job/aot-offer-favourite-tier2`;
        return this.http.post(apiUrl,payload,httpOptions)
        .pipe(
            map(response => response),
            catchError(this.handleErrorService.handleError('ortiretwo'))
        );

    }
}
