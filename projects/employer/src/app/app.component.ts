import { PlatformLocation } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'employer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'employer';
  constructor(private _location:PlatformLocation) {

  }
}
