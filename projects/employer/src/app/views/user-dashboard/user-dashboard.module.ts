import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared.module';
import { CalendarModule } from 'angular-calendar';

import { UserDashboardRoutingModule } from './user-dashboard-routing.module';
import {
  DashboardComponent,
  JobsPostedComponent,
  OngoingComponent,
  CompletedComponent,
  AppointmentsComponent,
  EmployedCandidatesComponent,
  ApplicantsComponent
} from './';
import { WorkLogComponent } from './work-log/work-log.component';

@NgModule({
  declarations: [
    DashboardComponent,
    JobsPostedComponent,
    OngoingComponent,
    CompletedComponent,
    AppointmentsComponent,
    EmployedCandidatesComponent,
    ApplicantsComponent,
    WorkLogComponent
  ],
  imports: [
    CommonModule,
    UserDashboardRoutingModule,
    SharedModule,
    CalendarModule
  ]
})
export class UserDashboardModule { }
