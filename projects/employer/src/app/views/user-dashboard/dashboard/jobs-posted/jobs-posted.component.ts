import { Component, OnInit ,ViewChild,TemplateRef} from '@angular/core';
import { EmployerJobService, SelectedFileService } from '../../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { MapsAPILoader } from '@agm/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import * as moment from 'moment';
declare var google: any;

@Component({
  selector: 'employer-jobs-posted',
  templateUrl: './jobs-posted.component.html',
  styleUrls: ['./jobs-posted.component.scss']
})
export class JobsPostedComponent implements OnInit {
  @ViewChild('templateJobOffer') templateJobOffer: TemplateRef<any>;
  private onDestroyUnSubscribe = new Subject<void>();
  jobPostedList : any;
  totalItems;
  itemsPerPage;
  totalNoOfPages:number = 6;
  pageNo;
  dataFetching = false;
  
  saveAstemplatePermissionModal: BsModalRef;
  templateEvent;
  templateNameModal: BsModalRef
  public templateName: string = ""
  modalRef: BsModalRef;
  skills = [];
  skillss =[];
  jobRole = [];
  constructor(
    private employerSetGetService: EmployerJobService,
    private modalService: BsModalService,
    public employerJobService: EmployerJobService,
    public snackBar: MatSnackBar,
    private mapsAPILoader: MapsAPILoader,
    private router: Router,
    private sfs: SelectedFileService
  ) { }

  ngOnInit() {
    localStorage.removeItem('JobDetailsById');
    this.sfs.searchStringStatus.subscribe(response => {
      if (response && response.string) {
        this.getJobsSearchedList(response.string.trim());
      }
      else {
        this.mapsAPILoader.load().then(() => {
          this.getJobsPostedList();
        });
      }
    })
    // this. getFilteredCandidates();
  }

  // get JobPosted List
  getJobsPostedList() {
    let payload ={
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      today:moment(new Date()).format('MM/DD/YYYY'),
      presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
      pageno : this.pageNo,
      perpage : this.itemsPerPage,
      localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone
    }
  this.employerJobService.getJobsPostedById(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
          if (result && !result['isError'] && result['details']) {

              this.jobPostedList = result['details'];
              let obj = result['details'];
              if(Object.entries(obj).length !== 0){
                this.totalItems = obj.total ? obj.total : 0;
                // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }          
            }else {
              this.dataFetching = false;
              this.snackBar.open('No data found! Please try again', 'Got it!', {
               
              });
            }
          },
      error => {
          this.dataFetching = false;
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
    );

  }

  cancelJOB(jobId,roleId) {
    let requiredData = {
      jobId: jobId,
      roleId: roleId,
      today:moment(new Date()).format('MM/DD/YYYY'),
      presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
      localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    this.employerJobService._cancelJOBfromJoblist(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.sfs.searchStringStatus.subscribe(response => {
              if (response && response.string) {
                this.getJobsSearchedList(response.string.trim());
              }
              else {
                this.mapsAPILoader.load().then(() => {
                  this.getJobsPostedList();
                });
              }
            })
           this.snackBar.open('You have Removeed this job', 'Got it!', {
           
          });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
     
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
     
          });
        }
      );
  }

  // setting current map position
  // setCurrentPosition(data) {
  //   if (navigator.geolocation) {
  //     let geocoder = new google.maps.Geocoder();
  //     let latlng = new google.maps.LatLng(data.location.coordinates[1], data.location.coordinates[0]);
  //     geocoder.geocode({ 'location': latlng }, (results, status) => {
  //       if (status === google.maps.GeocoderStatus.OK) {
  //         if (results[0] != null) {
  //           data.formattedAddress = results[0].formatted_address;
  //         }
  //       }
  //     });
  //   }
  // }

  //open job details page and set values in local storage
  openJobDetails(jobId, roleId,jobposted) {
    let jobDetails = {
      jobId: jobId,
      roleId: roleId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(jobDetails));
    localStorage.setItem('removeButton',jobposted)
    this.router.navigate(['/employer/job-details'])
  }

  //open job post page for job editing by setting local storage
  openJobEdit(jobId, roleId){
    let jobIds = [jobId];
    this.employerSetGetService.setJobIdToGetRoles(JSON.stringify(jobIds));
    let jobDetails = {
      jobId: jobId,
      roleId: roleId
    }
    let jobEdit = 'jobEdit';
    localStorage.setItem('jobEdit',jobEdit);
    localStorage.setItem('JobDetailsById', JSON.stringify(jobDetails));
    localStorage.setItem('JobDetailsBId', JSON.stringify(jobDetails));

    this.router.navigate(['/employer/job/edit'])
  }

  //get jobs according o the searched item
  getJobsSearchedList(searchItem) {
    this.employerJobService.searchJobInDashboard(
      {
        employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
        searchText: searchItem
      }
    )
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobPostedList = result['details'];
          }
          else {
            this.snackBar.open('No Results Found', 'Got it!', {
           
            });
            //  this.getJobsPostedList();
            this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
    
  }
  findCandidates(jobId,roleId){
    let jobIds = [jobId];
    this.employerSetGetService.setJobIdToGetRoles(JSON.stringify(jobIds));

    let jobDetails = {
      jobId: jobId,
      roleId: roleId
    }
    localStorage.setItem('JobDetailsBId', JSON.stringify(jobDetails));
    this.modalRef = this.modalService.show(this.templateJobOffer);
  }

  navigateToCandidatesList(option){
    this.modalRef.hide();
    this.router.navigate(['/employer/candidate/list',option]);
    // if (option == 'option-2') {
    //   payload = Object.assign(payload, { skills: this.skillss });
    //   this.employerSetGetService.getFilteredFavouriteCandidates(payload)
    //     .pipe(takeUntil(this.onDestroyUnSubscribe))
    //     .subscribe(
    //       result => {
            
    //         if (result && !result['isError'] && result['details'] ) {
    //           // if(result['details']['TIER2']){
    //           //   this.candidates = result['details'];
    //           // }
    //           this.candidates = result['details'];
    //           this.snackBar.open(result['message'],'Got it',{
    //             duration:3000,
    //           });
          
    //     }else{
    //       this.snackBar.open('No candidates found', 'Got it!', {
    //         duration: 3000,
    //       });

    //   }
    //       },
    //       error => {
    //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
    //           duration: 3000,
    //         });
    //       }
       
    //     );
    // }
  }
 


  getPaginateData(event){
    this.pageNo = event.page;
    this.dataFetching = true;
    this.jobPostedList = [];
    this.getJobsPostedList();
  }

  // date formet //
  // dateTimeFormets(startDateTime,endDateTime) {
  //   console.log(new Date(startDateTime),new Date(endDateTime))
  //   let startHour = new Date(startDateTime).getHours();
  //   let startMintus = new Date(startDateTime).getMinutes();
  //   let endHour = new Date(endDateTime).getHours();
  //   let endMintus = new Date(endDateTime).getMinutes();

  //   return `${startHour}:${startMintus}-${endHour}:${endMintus}`
  // }

}
