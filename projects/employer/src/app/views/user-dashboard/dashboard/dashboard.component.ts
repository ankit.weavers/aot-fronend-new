import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { SelectedFileService ,EmployerJobService} from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'employer-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // @ViewChild("tref") tref: ElementRef;
  public searchGroupForm: FormGroup;
  private onDestroyUnSubscribe = new Subject<void>();
  searchHistoryList = [];
  clearSearch = new Subject();

  constructor(
    private fb: FormBuilder,
    private sfs: SelectedFileService,
    public employerJobService: EmployerJobService,
    public snackBar: MatSnackBar,

  ) {
    this.searchGroupForm = this.fb.group({
      searchedItem: '',
    });
  }

  ngOnInit() {
    this.sfs._setSearchStatus('');
    localStorage.removeItem('removeButton');
  }
//   ngAfterViewInit(): void {
//     // outputs `I am span`
//     console.log("ok",this.tref.nativeElement);
// }

  // search job by item searched
  searchJobByItem() {
  // if(this.searchGroupForm.value.searchedItem){
      this.sfs._setSearchStatus({string: this.searchGroupForm.value.searchedItem})

  // }
  }

  //get search history
  getSearchHistory(){
    this.employerJobService.getSearchHistory({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId })
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && result['details'].roleName) {
          this.searchHistoryList = result['details'].roleName;
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
      
        });
      }
    );
  }

}
