import { Component, OnInit } from '@angular/core';
import {EmployerJobService, SelectedFileService} from '../../../../services';
import { takeUntil } from 'rxjs/operators';
import {Subject } from 'rxjs';
import {MatSnackBar} from '@angular/material';
import {MapsAPILoader} from '@agm/core';
import {Router} from '@angular/router';
import {BsModalService,BsModalRef} from 'ngx-bootstrap';
import { json } from '@angular-devkit/core';
import * as moment from 'moment';

@Component({
  selector: 'employer-completed',
  templateUrl: './completed.component.html',
  styleUrls: ['./completed.component.scss']
})
export class CompletedComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();
  completedList: any;
  totalItems;
  itemsPerPage;
  pageNo;
  totalNoOfPages:number =6;
  dataFetching = false;
  constructor(
    private employerSetGetService : EmployerJobService,
    private modelService : BsModalService,
    public employerJobService :EmployerJobService,
    public snackBar : MatSnackBar,
    private mapsApiLoder : MapsAPILoader,
    private router : Router,
    private sfs : SelectedFileService
    ) {}

    ngOnInit() {
      localStorage.removeItem('JobDetailsById');
      this.sfs.searchStringStatus.subscribe(response => {
        if (response && response.string) {
          this.getSearchJonInCompleted(response.string.trim());
        }
        else {
          this.mapsApiLoder.load().then(() => {
            this.getCompletedJobPostedList();
          });
        }
      })
    }
    

  getCompletedJobPostedList(){
    let payload={
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      today:moment(new Date()).format('MM/DD/YYYY'),
      presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
      pageno: this.pageNo,
      perpage:this.itemsPerPage,
      localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone
    } 
    this.employerJobService.getCompletedJobById(payload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            this.completedList = result['details'];
            let obj =result['details'];
            if(Object.entries(obj).length!==0)
            {
              this.totalItems = obj.total ? obj.total : 0;
              // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
              this.dataFetching = false;
            }else{
              this.dataFetching = false;
              this.snackBar.open('NO DATA FOUND.Please try again!','Got it!',{
             
              });
            }
          }
        },
        error => {
          this.dataFetching = false;
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );

  }
   getSearchJonInCompleted(searchItem)
   {
  
    this.employerJobService.searchJobInCompletedDashboard(
      {
        employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
        searchText: searchItem
      }
    )
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
       
          if (result && !result['isError']) {
            this.completedList = result['details'];
          }
          else {
            this.snackBar.open('No Results Found', 'Got it!', {
            
            });
            //  this.getJobsPostedList();
            this.sfs._setSearchStatus('');
          }
          if(this.completedList.length==0){
            this.snackBar.open('No Data Found','got it',{
             
            });
          }

        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
 
  }
  getPaginateData(event){
    this.pageNo = event.page;
    this.completedList =[];
    this.dataFetching = true;
    this.getCompletedJobPostedList();
  }
  openJobDetail(jobId,roleId,completed)
  {
    let jobDetails={
      jobId : jobId,
      roleId : roleId
    }
    localStorage.setItem('JobDetailsById',JSON.stringify(jobDetails));
    localStorage.setItem('removeButton',completed)
    this.router.navigate(['/employer/job-details'])
  }
  getCompletedPaginateData(event)
  {
    this.pageNo = event.page;
    this.completedList = [];
    this.dataFetching = true;
    this.getCompletedJobPostedList();
  }
   }
  

