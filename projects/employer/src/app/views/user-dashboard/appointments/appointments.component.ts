import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CalendarMonthViewBeforeRenderEvent, CalendarMonthViewDay, CalendarEvent } from 'angular-calendar';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EmployerJobService } from '../../../services';
import { MatSnackBar } from '@angular/material';
import { start } from 'repl';

@Component({
  selector: 'employer-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {

  jobId;
  roleId;
  private onDestroyUnSubscribe = new Subject<void>();
  allShiftList;
  // selectedStartDate: Date;
  // selectedEndDate: Date;

  bsRangeValue = [];

  @Input() viewDate: Date = new Date();
  // refresh: Subject<any> = new Subject();
  @Input() view: string = 'month';
  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
  // lastSelectedShiftIndex;
  currentDate: Date;
  eventList = []
  selectedShiftList = [];
  @Output()
  eventClicked = new EventEmitter<{
    event: CalendarEvent;
  }>();
  viewDateDetails: boolean = false;
  events:any;
  refresh:any;

  constructor(public snackBar: MatSnackBar, public employerJobService: EmployerJobService) { }

  ngOnInit() {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsById'));
    this.jobId = jobDetailsData.jobId;
    this.roleId = jobDetailsData.roleId;
    this.getJobDetailsById();
  }

  // get JobPosted List
  getJobDetailsById() {
    let requiredData = {
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      jobId: this.jobId,
      roleId: this.roleId
    }
    this.employerJobService.getJobDetailsById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allShiftList = result['details'].setTime;
            // this.allShiftList[0].checked = "checked";
            // this.selectRank(0);
            // this.selectedStartDate = new Date(this.allShiftList[0].startDate);
            // this.selectedEndDate = new Date(this.allShiftList[0].endDate);
            // this.currentDate = this.selectedStartDate;

            // this.bsRangeValue = this.getDatesBetween(this.selectedStartDate, this.selectedEndDate);

            // preparing all dates from all shifts
            for (var i = 0; i < this.allShiftList.length; i++) {
              let shiftStartDate = new Date(this.allShiftList[i].startDate);
              let shiftEndDate = new Date(this.allShiftList[i].endDate);
              this.arrangeDateWiseEvents(shiftStartDate, shiftEndDate, this.allShiftList[i].shift);
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  //arrange dates and events
  arrangeDateWiseEvents(startDate, endDate, shiftType) {
    // const dates = [];
    // Strip hours minutes seconds etc.
    let currentDate = new Date(
      startDate.getFullYear(),
      startDate.getMonth(),
      startDate.getDate()
    );
    while (currentDate <= endDate) {
      // dates.push(currentDate);
      // this.bsRangeValue.push(currentDate);

      // let shiftColor;
      // if(shiftType === 'Morning' ){
      //   shiftColor=colors.yellow;
      // }
      // else  if(shiftType === 'Morning' ){
      //   shiftColor=colors.yellow;
      // }

      let currentDateString = currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear();

      if (this.eventList[currentDateString]) {

        let existingEvent = this.eventList[currentDateString];
           this.eventList[currentDateString] = [
          {
            title: shiftType,
            // color: colors.yellow,
            // color: {
            //   primary: "#ad2121",
            //   // "secondary": "#FAE3E3"
            // },
            start: currentDate,
            meta: {
              type: 'warning',
              initial: shiftType =="Full Day"?"FD":shiftType.charAt(0),
            }
          },
          ...existingEvent,
        ]
      }
      else {

        this.bsRangeValue.push(currentDate);
        console.log("date",this.bsRangeValue);

        this.eventList[currentDateString] = [{
          title: shiftType,
          // color: colors.yellow,
          start: currentDate,
          meta: {
            type: 'warning',
            initial: shiftType =="Full Day"?"FD":shiftType.charAt(0),
          }
        }]
      }
      currentDate = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate() + 1, // Will increase month if over range
      );
    }
    // this.beforeMonthViewRender(e);
    // return dates;
  }

  // Returns an array of dates between the two dates
  // getDatesBetween = (startDate, endDate) => {
  //   const dates = [];
  //   // Strip hours minutes seconds etc.
  //   let currentDate = new Date(
  //     startDate.getFullYear(),
  //     startDate.getMonth(),
  //     startDate.getDate()
  //   );
  //   while (currentDate <= endDate) {
  //     dates.push(currentDate);
  //     currentDate = new Date(
  //       currentDate.getFullYear(),
  //       currentDate.getMonth(),
  //       currentDate.getDate() + 1, // Will increase month if over range
  //     );
  //   }
  //   return dates;
  // };


  //set values before rendering angular calendar
    beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
      console.log("fine:");
      // this.arrangeDateWiseEvents(startDate, endDate, shiftType);
    setTimeout(
      () => {
        // this.bsRangeValue = this.bsRangeValue.sort((a,b)=>a.getTime()-b.getTime());
        // let firstDate = this.bsRangeValue[0].getDate() + '-' + (this.bsRangeValue[0].getMonth() + 1) + '-' + this.bsRangeValue[0].getFullYear();
        // this.selectedShiftList.push(this.allShiftList[firstDate]);
        console.log("jf");

        for (var i = 0; i < this.bsRangeValue.length; i++) {
          console.log("pool");
          renderEvent.body.forEach(day => {
            console.log("jfjfjfj");
            //check if days have events 
            if (day.date.getDate() === this.bsRangeValue[i].getDate() && (day.date.getMonth() + 1) === (this.bsRangeValue[i].getMonth() + 1) && day.date.getFullYear() === this.bsRangeValue[i].getFullYear()) {
              // day.cssClass = 'bg-pink';
              console.log("hfhgh");
              let eventDate = day.date.getDate() + '-' + (day.date.getMonth() + 1) + '-' + day.date.getFullYear();
              day.events = this.eventList[eventDate];
              // day.backgroundColor = "#ad2121";
              day.badgeTotal = this.eventList[eventDate].length;
              let customTitle = '';
              for (var k = 0; k < this.eventList[eventDate].length; k++) {
                day.meta = customTitle + this.eventList[eventDate][k].meta.initial;
                customTitle = this.eventList[eventDate][k].meta.initial + ', ';
              }
            }
          });
        }
      }, 3000);
  }

  //select
  // selectRank(index) {
  //   this.lastSelectedShiftIndex = index;
  //   this.refresh.next();
  //   this.allShiftList[index].checked = "checked";
  //   this.selectedStartDate = new Date(this.allShiftList[index].startDate);
  //   this.selectedEndDate = new Date(this.allShiftList[index].endDate);
  //   this.currentDate = this.selectedStartDate;
  //   this.bsRangeValue = this.getDatesBetween(this.selectedStartDate, this.selectedEndDate);
  // }

  RefreshShift() {
    this.viewDateDetails = false;
    // this.selectRank(this.lastSelectedShiftIndex);
  }

  dayClicked(day: CalendarMonthViewDay): void {
    console.log("day:");
    this.viewDateDetails = true;
    this.selectedShiftList = [];
    // day.cssClass = 'bg-pink';
    this.currentDate = day.date;
    let eventDate = day.date.getDate() + '-' + (day.date.getMonth() + 1) + '-' + day.date.getFullYear();

    if (day.events.length) {
      for (var i = 0; i < day.events.length; i++) {
        for (var j = 0; j < this.allShiftList.length; j++) {
          //show shifts when events are present, accordingly
          if (day.events[i].title === this.allShiftList[j].shift) {
            this.selectedShiftList.push(this.allShiftList[j])
          }
        }
      }
    }
  }

  // toggleDayHighlight(event: CalendarEvent, isHighlighted: boolean): void {
  //   // this.view.days.forEach(day => {
  //   //   if (isHighlighted && day.events.indexOf(event) > -1) {
  //   //     day.backgroundColor =
  //   //       (event.color && event.color.secondary) || '#D1E8FF';
  //   //   } else {
  //   //     delete day.backgroundColor;
  //   //   }
  //   // });
  // }

  // events: CalendarEvent[] = [
  //   {
  //     title: 'Event 1',
  //     // color: colors.yellow,
  //     start: new Date(),
  //     meta: {
  //       type: 'warning',
  //     }
  //   }
  // ];

}
