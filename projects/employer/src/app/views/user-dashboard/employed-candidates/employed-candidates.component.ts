import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { FormGroup, FormBuilder,FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MapsAPILoader } from '@agm/core';
//import { EmployerJobService } from '../../../services';
import { SelectedFileService ,EmployerJobService} from '../../../services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Erros } from 'src/app/models';

@Component({
  selector: 'employer-employed-candidates',
  templateUrl: './employed-candidates.component.html',
  styleUrls: ['./employed-candidates.component.scss']
})
export class EmployedCandidatesComponent implements OnInit {
  modalRef: BsModalRef;
  public searchGroupForm: FormGroup; 
  searchHistoryList = [];

  public roleId;
  public jobId;
  public candidateId;
  public employerId;
  public rating: any = null;
  public review: any = null;
  public errors: any = Erros;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allEmployedCandidateDetails = [];
  pageNo;
  perPage;
  total;
  totalNoOfPages:number =6;
  dataFetching = false;
  constructor(
    public employerJobService: EmployerJobService,
    private fb: FormBuilder,
    private employerjobservice: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router,
    private sfs: SelectedFileService,
    private mapsAPILoader: MapsAPILoader,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    
  ) {
    this.searchGroupForm = this.fb.group({
      searchedItem: '',
    });    
   }

  ngOnInit() {   
    this.getAllEmployedCandidates();
    this.sfs.searchStringStatus.subscribe(response => {
      if (response && response.string) {
        this.getJobsSearchedList(response.string.trim());
      }
      else {
        this.mapsAPILoader.load().then(() => {
          this.getAllEmployedCandidates();
        });
      }
    });
 
  }

    // get all employed candidate list details
    getAllEmployedCandidates() {
      this.employerjobservice.getAllEmployedCandidateDetails({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,   pageno: this.pageNo,
      perpage: this.perPage })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError'] && result['details']) {

              this.allEmployedCandidateDetails = result['details'].results;
              let obj = result['details'];
              if(Object.entries(obj).length !== 0){
                this.total = obj.total ? obj.total : 0;
                // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }          
            }else {
              this.dataFetching = false;
              this.snackBar.open('No data found! Please try again', 'Got it!', {
               
              });
            }
              
            
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              
            });
          }
        );
    }
    openJobDetails(jobId, roleId) {
      let jobDetails = {
        jobId: jobId,
        roleId: roleId
      }
      localStorage.setItem('JobDetailsById', JSON.stringify(jobDetails));
      this.router.navigate(['/employer/job-details'])
    }
  //open candidate job profile and set in local storage
  viewCandidateProfile(candidateId,jobId,roleId,rating,profilepic) {
    let JobDetailsById = {
      candidateId: candidateId,
      jobId: jobId,
      roleId: roleId,
      rating:rating,
      profilepic:profilepic
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/candidate/details']);
  }

  // search job by item searched
  searchJobByItem() {
    // if(this.searchGroupForm.value.searchedItem){
      this.sfs._setSearchStatus({string: this.searchGroupForm.value.searchedItem})
    // }
  }

  //get jobs according o the searched item
  getJobsSearchedList(searchItem) {
    this.employerJobService.searchEmployedCandidiateByNAme(
      {
        employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
        searchText: searchItem
      }
    ) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && result['details']) {
            this.allEmployedCandidateDetails = result['details'].results;
          }
          else {
            this.snackBar.open('No Results Found', 'Got it!', {
             
            });
            this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
  // Save employed candidiate as favourite
  saveAsFav(employerId,candidateId){
    this.employerJobService.saveAsFav(employerId,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.getAllEmployedCandidates();
            this.snackBar.open('You have marked this candidate as Favorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
            this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
  removeFromFav(employerId,candidateId){
    this.employerJobService.removeFromFav(employerId,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.getAllEmployedCandidates();
            this.snackBar.open('You have marked this candidate as UnFavorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
            this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }
   
  openModal(template: TemplateRef<any>,employerId,jobId,candidateId,roleId) {
    this.roleId = roleId;
    this.jobId = jobId;
    this.candidateId = candidateId;
    this.employerId = employerId;
    this.modalRef = this.modalService.show(template);
  }
  ratingValueChanged(value) {
    this.rating = value;
  }
  // save review
  saveReview(){
    if(!this.review) {
      this.snackBar.open('Please add your review','Got it!',{
       
      });
      return false;
    }
    if(!this.rating) {
      this.snackBar.open('Please give your rating','Got it!',{
      
      });
      return false;
    }

      let reviewData = [];
      reviewData['employerId']=this.employerId;
      reviewData['candidateId']=this.candidateId;
      reviewData['jobId']=this.jobId;
      reviewData['roleId']=this.roleId;
      reviewData['review']=this.review;
      reviewData['rating']=this.rating;

      this.employerJobService.submitReview(reviewData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          this.modalRef.hide();
          reviewData = [];
          this.review='';
          this.rating='';            
          if (result && !result['isError']) {
            const messageSuccess =  'Review submitted successfully.';
            this.snackBar.open(messageSuccess, 'Got it!', {

           
            });
            this.getAllEmployedCandidates();

          } else {

            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }        
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );  
  }
  getPaginatedData(event) {
    this.pageNo = event.page;
    this.dataFetching = true;
    this.allEmployedCandidateDetails = [];
    this.getAllEmployedCandidates();
  } 
}
