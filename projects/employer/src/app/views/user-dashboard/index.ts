export { DashboardComponent } from './dashboard/dashboard.component';
export { JobsPostedComponent } from './dashboard/jobs-posted/jobs-posted.component';
export { OngoingComponent } from './dashboard/ongoing/ongoing.component';
export { CompletedComponent } from './dashboard/completed/completed.component';
export { AppointmentsComponent } from './appointments/appointments.component';
export { EmployedCandidatesComponent } from './employed-candidates/employed-candidates.component';
export { ApplicantsComponent } from './applicants/applicants.component';
export {WorkLogComponent} from './work-log/work-log.component';
