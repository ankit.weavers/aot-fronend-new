import { Component, OnInit } from '@angular/core';
import { EmployerJobService } from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'employer-work-log',
  templateUrl: './work-log.component.html',
  styleUrls: ['./work-log.component.scss']
})
export class WorkLogComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();
  onSite =[];
  offSite =[];
  variable;
  value;
  checklocation = [];
  exactPayment;
  totTimeCheckin;
  candidateId;
  employerId;
  jobId;
  roleId;
  constructor(
    public employerJobService: EmployerJobService
  ) { }

  ngOnInit() {
    this.candidateId = JSON.parse(localStorage.getItem('JobDetailsById')).candidateId;
    this.jobId = JSON.parse(localStorage.getItem('JobDetailsById')).jobId;
    this.roleId = JSON.parse(localStorage.getItem('JobDetailsById')).roleId;
    this.employerId = JSON.parse(localStorage.getItem('currentUser'));
 
    this.onsiteOffsiteDeatils();
    this.locationTracting();
  }

  locationTracting() {
    let payload = {
      candidateId:this.candidateId,
      jobId:  this.jobId ,
      roleId: this.roleId,
      employerId:this.employerId.employerId,
      date: new Date().toISOString().slice(0, 10)
   }	
    this.employerJobService.getCheckinlocationTracking(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result =>{
      if(result && !result['isError']){
     
        this.checklocation = result['details'][0].OffsiteData;
        console.log('checking',this.checklocation);
      }
    })
  }
  onsiteOffsiteDeatils() {

    let payload = {
      candidateId:this.candidateId,
      jobId:  this.jobId ,
      roleId: this.roleId,
      date: new Date().toISOString().slice(0, 10)
   }	
    this.employerJobService.getCheckinCheckoutData(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result =>{
      if(result && !result['isError']){
        console.log('result',result);
        this.value = result['details']
        this.variable = result['details'].workingDays;
        this.exactPayment = result['exactPayment']

        let totCheckinTimeInMins =this.variable.totalOnsiteCheckinTimeInMns + this.variable.totaloffSiteCheckinTimeInMns;
        let totCheckinHrs = Math.floor(totCheckinTimeInMins / 60);
       let totCheckinMins = totCheckinTimeInMins % 60;
       this.totTimeCheckin = totCheckinHrs + "h" + totCheckinMins + "m";
        // this.value = result['details'].workingDays.totalonSiteTime;
        this.onSite = result['details'].workingDays.onSite;
        this.offSite = result['details'].workingDays.offSite;

      }
    })
  }

}
