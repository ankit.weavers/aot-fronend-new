import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  DashboardComponent,
  JobsPostedComponent,
  OngoingComponent,
  CompletedComponent,
  AppointmentsComponent,
  EmployedCandidatesComponent,
  ApplicantsComponent,
  WorkLogComponent
} from './';
const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Dashboard'
    },
    children: [
      {
        path: '',
        redirectTo: 'job-posted',
        pathMatch: 'full'
      },
      {
        path: 'job-posted',
        component: JobsPostedComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Job Posted'
        }
      },
      {
        path: 'ongoing',
        component: OngoingComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Ongoing'
        }
      },
      {
        path: 'completed',
        component: CompletedComponent,
        data: {
          hasBackBtn: false,
          hasHamburger: false,
          isEditable: [],
          title: 'Completed'
        }
      },
    ]
  },
  {
    path: 'appointments',
    component: AppointmentsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Appointments'
    }
  },
  {
    path: 'worklog',
    component: WorkLogComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Work Log'
    }
  },
  {
    path: 'employed-candidates',
    component: EmployedCandidatesComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Employed Worker'
    }
  },
  {
    path: 'applicants',
    component: ApplicantsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Applicants'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserDashboardRoutingModule { }
