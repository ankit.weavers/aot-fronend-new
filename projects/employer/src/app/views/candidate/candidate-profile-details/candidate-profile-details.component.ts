import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { EmployerJobService } from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// import undefined = require('firebase/empty-import');

@Component({
  selector: 'employer-candidate-profile-details',
  templateUrl: './candidate-profile-details.component.html',
  styleUrls: ['./candidate-profile-details.component.scss']
})
export class CandidateProfileDetailsComponent implements OnInit {
  generalInfo:{};
  candidateId;
  jobId;
  roleId;
  payloads:any;
  public stars: any[] = new Array(5);
  private onDestroyUnSubscribe = new Subject<void>();
  candidateDetailsList;
  workExpList = [];
  languageList = [];
  jobRoles: any =[];
  skillDetails:any =[];
  industry = [];
  skills=[];
  payload=[];
  availability = [];
  daysOffList = [];
  uploadedFilesList: any;
  isOffer: Boolean = false;
  isHire: Boolean = false;
  alreadyOffered: Boolean = false;
  alreadyHired: Boolean = false;
  isApplied:Boolean=false;
  array:any;
  isApplie;
  isOfferedd;
  isHiredd;
  isDeclined;
  isAccepted;
  lookingstatus: any;
  showRole: boolean = false;
  showArraow: boolean = false;
  showapplies: boolean = false;
  showArrowApllied: boolean = false;
  timeSlottable = [
    {
      day: 'Sunday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },
    {
      day: 'Monday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ] ,
      
    
    },
    {
      day: 'Tuesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Wednesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Thursday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Friday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Saturday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },

];
  constructor(
    public snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    public employerJobService: EmployerJobService,
  ) { }

  ngOnInit() {
    this.payloads=JSON.parse(localStorage.getItem('JobDetailsById'))
    this.candidateId = JSON.parse(localStorage.getItem('JobDetailsById')).candidateId;
    this.jobId = JSON.parse(localStorage.getItem('JobDetailsById')).jobId;
    this.roleId = JSON.parse(localStorage.getItem('JobDetailsById')).roleId;
    this.getCandidateProfile();
  }

   // DOWN ARROW //
   showMoreRole() {
    this.showRole = true;
    this.showArraow = true;
  }

  // UP ARROW //
  showLessRole() {
    this.showRole = false;
    this.showArraow = false;
  }

  // DOWN ARROW //
  showapplidMoreRole() {
    this.showapplies = true;
    this.showArrowApllied = true;
  }

  // UP ARROW //
  showappliedLessRole() {
    this.showapplies = false;
    this.showArrowApllied = false;
  }

  //get candidate details
  getCandidateProfile() {
    // let jobDetailsForCandidate;
    
    // if(jobDetailsForCandidate !=null || jobDetailsForCandidate !=undefined){
    let  jobDetailsForCandidate = {
        candidateId: this.candidateId,
        jobId: this.jobId,
        roleId: this.roleId,
    }
  // }else{
  //   jobDetailsForCandidate = {
  //     candidateId: this.candidateId,
  //     jobId: this.jobId
  // }
  // }
 

    this.employerJobService.getCandidateProfileById(jobDetailsForCandidate)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          if (result && !result['isError']) {
            this.candidateDetailsList = result['details'];
            this.lookingstatus = result['details'].isAvailable

            if (result['details'].workExperience) {
              this.workExpList = result['details'].workExperience.experiences
            }
            if (result['details'].language) {
              this.languageList = result['details'].language
            }
            // if (result['details'].selectedRole) {
            //   this.jobRoles = result['details'].selectedRole
            //   // console.log("jobRoles",this.jobRoles);

            // }
            
            // if (result['details'].payloadSkills) {

            //   // this.jobRoles = result['details'].payloadSkills[0];
            //   // console.log("job",this.jobRoles.industryDetail);
            //   // console.log("jobss",this.jobRoles);

            //   // for(let i=0;i<this.jobRoles.length;i++){
            //   //   this.payload[i]=this.jobRoles[i].payloadSkill.map(({jobRoleName}) => jobRoleName);
            //   // }
            //   console.log("jobRoles",this.payload);
            // }
            if (result['details'].payloadSkills) {

              this.jobRoles = result['details'].payloadSkills[0].industryDetail;
              this.skillDetails =result['details'].payloadSkills[0].payloadSkill
              // for(let i=0;i<this.jobRoles.length;i++){
              //   // this.industry[i]=this.jobRoles[i].industryDetail.map(({industryName}) => industryName);
              // }
              console.log("jobRoles",this.jobRoles);
            }
            // if(result['details'].payloadSkills){
            //   this.jobRoles = result['details'].payloadSkills;
            //   for(let i=0;i<this.jobRoles.length;i++){
            //     this.skills[i]=this.jobRoles[i].payloadSkill.map(({ skillDetails }) => skillDetails);
            //   }
            //   // this.skills = result['details'].payloadSkill.map(({ skillDetails }) => skillDetails);
            //   console.log("skills",this.skills);
            // }
            if (result['details'].availability) {
              this.availability = result['details'].availability;
              this.callArray();

            }
            if (result['details'].dayoff) {
              this.daysOffList = result['details'].dayoff
            }
            if (result['details'].docs || result['details'].cv || result['details'].video) {
              this.uploadedFilesList = {
                cv: result['details'].cv,
                docs: result['details'].docs,
                video: result['details'].video
              }
            }

            if (result['status'].generalInfo) {
              this.generalInfo = result['status'].generalInfo;
            }
                 //check if candidate is to be offered job  
                 if (result['status'].generalInfo && 
                 !result['status'].generalInfo.isOffered  ) {
                  this.isOffer = true;
                  this.alreadyOffered = false;
                  this.isHire = false;
                  this.alreadyHired = false;
                }
                //check if candidate is already offered job  
                if (result['status'].generalInfo && 
                result['status'].generalInfo.isOffered && 
                !result['status'].generalInfo.isAccepted && 
                !result['status'].generalInfo.isDeclined
                ) {
                  this.alreadyOffered = true;
                  this.isOffer = false;
                  this.isHire = false;
                  this.alreadyHired = false;
                }
                //check if candidate is to be hired 
                if (result['status'].generalInfo && 
                result['status'].generalInfo.isOffered && 
                result['status'].generalInfo.isAccepted && 
                !result['status'].generalInfo.isHired
                ) {
                  this.isHire = true;
                  this.isOffer = false;
                  this.alreadyOffered = false;
                  this.alreadyHired = false;
                }
                //check if candidate is already hired job  
                if (result['status'].generalInfo &&
                 result['status'].generalInfo.isOffered && 
                 result['status'].generalInfo.isAccepted && 
                 result['status'].generalInfo.isHired
                 ) {
                  this.alreadyHired = true;
                  this.isOffer = false;
                  this.alreadyOffered = false;
                  this.isHire = false;
                }
                // if(result['status'].generalInfo && !result['status'].isOffered &&
                //   result['status'].isAccepted &&
                //   !result['status'].isHired
                //   ){
                //     this.isHire = true;
                //     this.isOffer = false;
                //     this.alreadyOffered = false;
                //     this.alreadyHired = false;

                //   }
                //   if (result['status'].generalInfo &&
                //   !result['status'].generalInfo.isOffered && 
                //   result['status'].generalInfo.isAccepted && 
                //   result['status'].generalInfo.isHired
                //   ) {
                //    this.alreadyHired = true;
                //    this.isOffer = false;
                //    this.alreadyOffered = false;
                //    this.isHire = false;
                //  }

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  //offer job to candidate
  offerJobToCandidate() {

    this.snackBar.open('Please Wait for sometime..', 'Got it!', {
   
    });

    let details = {
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      candidateId: this.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
    }

    this.employerJobService.offerJobToCandidate(details)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.getCandidateProfile();
            this.snackBar.open('Your job has been successfully offered to this candidate', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        }
      )
  }

  //cancel job offer to candidate
  // cancelJobToCandidate() {
  // }

  //hire candidate
  hireCandidate() {
    this.snackBar.open('Please Wait for sometime..', 'Got it!', {
    
    });

    let details = {
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      candidateId: this.candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
    }

    this.employerJobService.hireCandidate(details)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.getCandidateProfile();

            this.snackBar.open('You have successfully selected this candidate for your job', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        }
      )
  }

  callArray() {
  
    this.timeSlottable.forEach(element => {
     element.Timeslot.forEach(data => {
       this.availability.forEach(a => {
         if(a.day === element.day) {
           a.timeSlotDetails.forEach(d => {
             if(d.slotName === data.slotName) {
               data.slotValue = true
             }
           });
         }
       });
     });
    });
  }

}
