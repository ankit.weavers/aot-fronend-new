import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { EmployerJobService } from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'employer-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})
export class CandidateProfileComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();
  public stars: any[] = new Array(5);
  payloads: any;
  workExpList = [];
  languageList = [];
  jobRoles: any;
  industry = [];
  skills = [];
  payload = [];
  availability = [];
  daysOffList = [];
  industryName = [];

  candidateDetailsList;
  uploadedFilesList: any;
  showRole: boolean = false;
  showArraow: boolean = false;
  showapplies: boolean = false;
  showArrowApllied: boolean = false;
  timeSlottable = [
    {
      day: 'Sunday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },
    {
      day: 'Monday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ] ,
      
    
    },
    {
      day: 'Tuesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Wednesday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Thursday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
      

    },
    {
      day: 'Friday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
    

    },
    {
      day: 'Saturday',
      Timeslot: [
        {
          slotName:"Morning",
          slotValue:false,
        },
        {
          slotName:"Afternoon",
          slotValue:false,
        },
        {
          slotName:"Evening",
          slotValue:false,
        },
        {
          slotName:"Night",
          slotValue:false,
        }
      ],
     

    },

];

  constructor(
    public snackBar: MatSnackBar,

    public employerJobService: EmployerJobService,
  ) { }

  ngOnInit() {
    this.payloads = JSON.parse(localStorage.getItem('Job'));


    this.viewProfile();
  }

  // DOWN ARROW //
  showMoreRole() {
    this.showRole = true;
    this.showArraow = true;
  }

  // UP ARROW //
  showLessRole() {
    this.showRole = false;
    this.showArraow = false;
  }

  // DOWN ARROW //
  showapplidMoreRole() {
    this.showapplies = true;
    this.showArrowApllied = true;
  }

  // UP ARROW //
  showappliedLessRole() {
    this.showapplies = false;
    this.showArrowApllied = false;
  }

  viewProfile() {
    // let candidateId = localStorage.getItem('Job');
    this.employerJobService.getCandidateProfileDetails({ candidateId: this.payloads.candidateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result['details'] && !result['isError']) {
            //set value into form
            if (result['details'].dob || result['details'].fname || result['details'].lname) {
              // let date = result['details'].dob.split("T")[0].split("-");
              // let day = date[2];
              // let month = date[1];
              // let year = date[0];
              this.candidateDetailsList = {
                email: result['details'].EmailId,
                dob: result['details'].dob,
                address: result['details'].location.address,
                postCode: result['details'].location.postCode,
                city: result['details'].location.city.name,
                country: result['details'].location.country.name,
                secondaryEmailId: result['details'].secondaryEmailId,
                kinContactNo: result['details']['kinDetails'].kinContactNo,
                kinName: result['details'].kinDetails.kinName,
                relationWithKin: result['details'].kinDetails.relationWithKin,
                contactNo: result['details'].contactNo,
                profilePic: result['details'].profilePic,
                fname: result['details'].fname,
                lname: result['details'].lname,
                mname: result['details'].mname,
                email_address: result['details'].email_address,
                home_address: result['details'].home_address
              }
            }

            if (result['details'].workExperience) {
              this.workExpList = result['details'].workExperience.experiences
            }
            // if (result['details'].rating) {
            //   this.rating = result['details'].rating
            //   console.log("rating",this.rating);
            // }

            if (result['details'].language) {
              this.languageList = result['details'].language
            }

            // if (result['details'].geolocation && result['details'].distance) {
            //   this.proximityList = {
            //     distance: (result['details'].distance)/1609.34,
            //     geolocation: result['details'].geolocation
            //   }
            // }

            if (result['details'].payloadSkill) {
              this.jobRoles = result['details'].payloadSkill.map(({ jobRoleName }) => jobRoleName);
              console.log("jobRoles", this.jobRoles);
            }
            if (result['details'].payloadSkill) {
              this.skills = result['details'].payloadSkill.map(({ skillDetails }) => skillDetails);
              console.log("skills", this.skills);
            }
            if (result['details'].payloadSkill) {
              this.industryName = result['details'].payloadSkill
              console.log("skills", this.skills);
            }

            if (result['details'].docs || result['details'].cv || result['details'].video) {
              this.uploadedFilesList = {
                cv: result['details'].cv,
                docs: result['details'].docs,
                video: result['details'].video
              }
            }

            if (result['details'].availability) {
              this.availability = result['details'].availability
               this.callArray();

            }

            if (result['details'].dayoff) {
              this.daysOffList = result['details'].dayoff
            }


          } else {
            this.snackBar.open(result['message'], 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      )
  }

  callArray() {
  
    this.timeSlottable.forEach(element => {
     element.Timeslot.forEach(data => {
       this.availability.forEach(a => {
         if(a.day === element.day) {
           a.timeSlotDetails.forEach(d => {
             if(d.slotName === data.slotName) {
               data.slotValue = true
             }
           });
         }
       });
     });
    });
  }




}

