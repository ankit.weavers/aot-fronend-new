
import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployerJobService } from '../../../services';
// RxJs
import { GlobalActionsService } from 'src/app/services';

import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import undefined = require('firebase/empty-import');
import { BsModalService, BsModalRef,ModalOptions } from 'ngx-bootstrap';
// import { FormGroup } from '@angular/forms';

@Component({
  selector: 'employer-candidate-profile-list',
  templateUrl: './candidate-profile-list.component.html',
  styleUrls: ['./candidate-profile-list.component.scss']
})

export class CandidateProfileListComponent implements OnInit {
  @ViewChild('templateJobOffer') templateJobOffer: TemplateRef<any>;
  @ViewChild('JobOffer') JobOffer: TemplateRef<any>;
  @ViewChild('informationPopUps') information: TemplateRef<any>;
  @ViewChild('alertpopup') alertpopup: TemplateRef<any>;
  ngbModalOptions: ModalOptions = {
    backdrop : 'static',
    keyboard : false
};
  public isSlider: boolean = false;
  public isSkillAvailable: boolean = false;
  public isAvailabilityAvailable: boolean = false;
  public isRatingAvailable: boolean = false;
  public stars: any[] = new Array(5);
  public rating: String;
  public showFilter:boolean=false;
  public inviteboolean:boolean = false ;
  paramValue: any;
  modalRef: BsModalRef;
  roles: any;
  skills=[];
  skillss=[]
  selectedSkills: string[] = [];
  selectedSkillsPayload: string[] = [];
  public keywords = [];
  public keywordss = [];
  public selectedCandidates = [];
  public selectedCandidateArr = [];
  public selectedCandidateToInviteArr = [];
  public saveJobIdRoleId: [];
  public candidates:any = [];
  public candidatesss:any = [];
  tireLength;
  rolesss =[];
  roleName=[];
  jobId=[];
  jobI;
  roleI;
  applyFilter:FormGroup;
  skillId=[];
  offer:any;
  invite:any;
  autometicOffer=[];
  offerss=[];
  totalItems;
  itemsPerPage;
  pageNo;
  totalNoOfPages:number =6;
  masage:any;
  dataFetching = false;
  private onDestroyUnSubscribe = new Subject<void>();
  text :string='Apply Filter';     
  public status : Boolean= false;  
  public status2 : Boolean =true;
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private globalActionsService: GlobalActionsService,
    private employerSetGetService: EmployerJobService,
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsBId'));
    this.jobI = jobDetailsData.jobId;
    this.roleI = jobDetailsData.roleId;
    console.log('rating',jobDetailsData );
    this.route.paramMap.subscribe(params => {
      this.paramValue = params.get("option");
    })

    this.getRolesByJobID();
    // this.getFilteredCandidates();
    this.applyFilter=this.formBuilder.group({
      selectRole:[''],
      selectSkill:['']
    })

  }

  toggleCollapsibleDiv() {
    this.isSlider ? this.isSlider = false : this.isSlider = true;
  }

  getRolesByJobID() {
    let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;

    let option = this.paramValue;
    this.employerSetGetService.getRoles(storedJobIds)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.roles = result['details'];
            this.jobId =result['details'][0].jobId;

          
            this.rolesss =result['details'][0].roleId;
            this.roleName =result['details'][0].roleName;
           
            console.log("role",this.rolesss);
            this.getRoleSkill();

              // this.aotOfferAll();
              if(this.roles.length==1){
              this.applyFilter.patchValue({
                selectRole: this.roles[0].roleName
              });
              this.getRoleSkills(this.roles);
              }



          }

        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );

    if (option == 'option-1') {

      this.isSkillAvailable = false;
      this.isAvailabilityAvailable = false;
      // this.isRatingAvailable = false;

    } else if (option == 'option-2') {

      this.isSkillAvailable = true;
      this.isAvailabilityAvailable = false;
      // this.isRatingAvailable = false;
    } else if (option == 'option-3') {

      this.isSkillAvailable = true;
      this.isAvailabilityAvailable = false;
      // this.isRatingAvailable = true;
    }
  }


  getRoleSkills(event) {
    this.skills = [];
    let payload;
    let option = this.paramValue;
    if(this.roles.length==1){

    payload = this.roles[0];

    }else{
    let index = event.target.selectedIndex - 1;

    payload = this.roles[index];
    }
   
    this.saveJobIdRoleId = payload;
    this.selectedSkillsPayload.splice(0,this.selectedSkillsPayload.length);
    console.log("jobid",this.selectedSkillsPayload);

    console.log("jobid",this.saveJobIdRoleId);
    this.keywords = [];
    // this.keywordss = [];
    this.keywordss[0] = payload['roleName'];
    this.employerSetGetService.getSkills(payload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {

            if (option != 'option-1') {
            this.skills = result['details'].skillDetails;

            
            this.applyFilter.controls.selectSkill.setValue('')  //reseting job role on changing job industry

            for(let i = 0; i<this.skills.length; i++) {
              this.selectedSkillsPayload.push(this.skills[i].skillId);
              this.keywords.push(this.skills[i].skillName);
             }
             console.log('selectedSkillsPayload',this.selectedSkillsPayload);
             console.log('keywords',this.keywords);
                
            }

         

     


          }

        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }
  getRoleSkill() {
    this.skillss = [];
   

    let payload ={
      roleId:this.roleI,
      // roleName:this.roleName,
      jobId:this.jobI
    }
    console.log("skill",payload);
   
    this.employerSetGetService.getSkills(payload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {

            this.skillss = result['details'].skillDetails;
            for(let i=0;i<this.skillss.length;i++){
            
              this.skillId[i]=this.skillss[i].skillId;
              console.log("skillId",this.skillId);
            }
            this.getFilteredCandidate();

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }


  onCheckSkill(checked, skillName, skillId) {
    if (checked) {
      this.keywords.push(skillName)
      this.selectedSkillsPayload.push(skillId)
      console.log("uiahdgd",this.selectedSkillsPayload);
      console.log("skill",this.keywords);

    }
    else {
      for (var i = 0; i < this.keywords.length; i++) {
        if (this.keywords[i] === skillName) {
          this.keywords.splice(i, 1);
          this.selectedSkillsPayload.splice(i , 1)
      console.log("skilllllll",this.selectedSkillsPayload);
      console.log("name",this.keywords);


        }
      }
    }
  }
  // onCheckRating(rating) {
  //   this.rating = rating;
  // }


  getFilteredCandidates() {
    this.status=true;
    this.status2=false;
    let option = this.paramValue;
    let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    // payload = Object.assign(payload, { skills: this.selectedSkillsPayload,rating:this.rating});

    


    if (option == 'option-1') {
      if(payload!=null || payload != undefined){
        payload = Object.assign(payload, { skills: this.selectedSkillsPayload,    pageno: this.pageNo,
          perpage:this.itemsPerPage,rating:"3"});
        console.log("payload 1",payload);
      }else{
        let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
        payload = Object.assign({roleId :roless, jobId: storedJobIds[0],rating:"3"})
        console.log("payload 2",payload);
      }
      this.employerSetGetService.getFilteredCandidates(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {

            // console.log("getFilteredCandidates", result);

            if (result && !result['isError'] && result['details']) {
              this.candidates = result['details'];
              console.log("candidtae",this.candidates);
              let obj =result['details'].TIER1;
              if(Object.entries(obj).length!==0)
              {
                this.totalItems = obj.total ? obj.total : 0;
                // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }else{
                this.dataFetching = false;
                this.snackBar.open('No data found! Please try again','Got it!',{
                
                });
              }
              // this.status=false;

              let candidate =result['details'].TIER1;
              this.offer =candidate.results;
      
                // this.modalRef = this.modalService.show(this.templateJobOffer);
                if(result['details'].TIER1.results.length >0) {
                  this.masage = 'Job offered to available workers. Notification will be sent once filled'
                  this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
                  this.tireLength = result['details'].TIER1.results
                }
              
          
              this.snackBar.open('you have successfully Offered the candidates','Got it',{
                
              });


          
        }else{
          // this.candidates.splice(0,this.candidates.length);
          this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
          });
          this.showFilter=true;
          this.candidates={};


      }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
        );
    } else if (option == 'option-2') {


      // if(payload!=null || payload != undefined){
      //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
      //     perpage:this.itemsPerPage,searchWithLocation:true});
      // }else{
      //   let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      //   payload = Object.assign({roleId :roless, jobId: storedJobIds[0],searchWithLocation:true })
      // console.log("payload 2",payload);

      // }

      this.ortireOnewithLocation();
      this.ortireTwowithLocation();
    
    } else if (option == 'option-3') {
  

      // payload = Object.assign(payload, { skills: this.selectedSkillsPayload,rating:this.rating});
      if(payload!=null || payload != undefined){
        payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
          perpage:this.itemsPerPage,rating:"3"});
      }else{
        let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
        payload = Object.assign({roleId :roless, jobId: storedJobIds[0],rating:"3" })
      console.log("payload 2",payload);

      }
    
      // if (this.selectedSkillsPayload && this.selectedSkillsPayload.length) {
      //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload});
      // }

      this.employerSetGetService.getBrowseCandidateOffers(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError'] && result['details']) {
              
                this.candidates = result['details'];
                let obj =result['details'].TIER;
                if(Object.entries(obj).length!==0)
                {
                  this.totalItems = obj.total ? obj.total : 0;
                  // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                  this.dataFetching = false;
                }else{
                  this.dataFetching = false;
                  this.snackBar.open('No data found! Please try again','Got it!',{
                
                  });
                }
              // this.status=false;

                this.snackBar.open(result['message'],'Got it',{
              
                });
            
          }else{
          // this.candidates.splice(0,this.candidates.length);
          // this.candidates.length=0;


            this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
            
            });
            this.showFilter=true;
            this.candidates={};


        }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
        );
    }
  }
  saveAsFav(employerId,candidateId){
    this.employerSetGetService.saveAsFav(employerId,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            if(this.status){
              this.getFilteredCandidates();
            }
            else{
              this.getFilteredCandidate();
            }
            this.snackBar.open('You have marked this candidate as Favorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
            // this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }
  removeFromFav(employerId,candidateId){
    this.employerSetGetService.removeFromFav(employerId,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            if(this.status){
              this.getFilteredCandidates();
            }
            else{
              this.getFilteredCandidate();
            }
            this.snackBar.open('You have marked this candidate as UnFavorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
          
            });
            // this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  getFilteredCandidate() {
    // this.status2=false;
    let option = this.paramValue;
    // let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    // payload = Object.assign(payload, { skills: this.selectedSkillsPayload,rating:this.rating});
    // if(payload!=null || payload != undefined){
    //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload});
    //   console.log("payload 1",payload);
    // }else{
    
    // }
    


    if (option == 'option-1') {
      let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],skillArrb:this.skillId,rating:"3",pageno: this.pageNo,
       perpage:this.itemsPerPage })
       console.log("payload 2",payload);
      this.employerSetGetService.getFilteredCandidate(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {

            // console.log("getFilteredCandidates", result);

            if (result && !result['isError'] && result['details']) {
              this.candidates = result['details'];
              let obj =result['details'].TIER1;
              if(Object.entries(obj).length!==0)
              {
                this.totalItems = obj.total ? obj.total : 0;
                // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                this.dataFetching = false;
              }else{
                this.dataFetching = false;
                this.snackBar.open('No data found! Please try again','Got it!',{
                  
                });
              }
              // this.status2=false;
              let candidate =result['details'].TIER1;
              this.offer =candidate.results;
              // for(let i=0;i<this.offer.length;i++){
              //   this.offerss[i]=this.offer[i].isOffered;
              // if(this.offer[i].isOffered==false){
              //   this.selectedCandidateArr[i]={
              //     jobId:  this.offer[i].jobId,
              //     employerId:  this.offer[i].employerId,
              //     candidateId:  this.offer[i].candidateId,
              //     roleId:  this.offer[i].roleId
              //   }
                // this.modalRef = this.modalService.show(this.templateJobOffer);

              // }

              // }
              // if(this.status){
              // this.aotOfferAll();
               
              // }
            
          //     if(result['details'].TIER1.results.length< 2){
          //       this.modalRef = this.modalService.show(this.templateJobOffer);

          //       // this.snackBar.open("Change the criteria through filter to show more candidate","Got it!",{

          //       // });
          //     }else{
        
          //   this.snackBar.open(result['message'],'Got it',{
          //     duration:3000,
          //   });
          // }
          if(result['details'].TIER1.results.length >0) {
            this.masage = 'Job offered to available workers. Notification will be sent once filled'
            this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
            this.tireLength = result['details'].TIER1.results
          }
          // if(result['details'].TIER1.results.length < 2){

          //   this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          //   });

          // }


          
        }else{
          this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          });

        
          this.showFilter=true;


      }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    } else if (option == 'option-2') {
      // if(payload!=null || payload != undefined){
      //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload});
      // }else{
      //   let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      //  let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],skillArrb:this.skillId,pageno: this.pageNo,
      //   perpage:this.itemsPerPage,searchWithLocation:true})
      // console.log("payload 2",payload);

      this.andtireOnewithLocation();
      this.andtireTwowithLocation();

      // }
  
    } else if (option == 'option-3') {
  

      // payload = Object.assign(payload, { skills: this.selectedSkillsPayload,rating:this.rating});
      // if(payload!=null || payload != undefined){
      //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload});
      // }else{
        let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
       let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],rating:"3",skillArrb:this.skillId,pageno: this.pageNo,
          perpage:this.itemsPerPage })
      console.log("payload 2",payload);

      // }
    
      // if (this.selectedSkillsPayload && this.selectedSkillsPayload.length) {
      //   payload = Object.assign(payload, { skills: this.selectedSkillsPayload});
      // }

      this.employerSetGetService.getBrowseCandidateOffer(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError'] && result['details']) {
              
                this.candidates = result['details'];
                let obj =result['details'].TIER;
                if(Object.entries(obj).length!==0)
                {
                  this.totalItems = obj.total ? obj.total : 0;
                  // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
                  this.dataFetching = false;
                }else{
                  this.dataFetching = false;
                  this.snackBar.open('No data found! Please try again','Got it!',{
                  
                  });
                }
                // this.status2=false
                if(result['details'].TIER.results.length < 2){
                  this.snackBar.open("Change the criteria to filter more candidates","Got it!",{
  
                  });
                }else{
          
              this.snackBar.open(result['message'],'Got it',{
               
              });
            }
            
          }else{
            this.snackBar.open("Change the criteria to filter more candidates","Got it!",{
            });
            this.showFilter=true;


        }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              
            });
          }
        );
    }
  }
  checkinfun(status:any) {
    console.log('ggg',status);
    if(status == 'Yes') {
      this.modalRef.hide();
    }
    else if(status == 'No') {
      this.modalRef.hide();
      this.router.navigate(['/employer/job/post'])
    }
    else if(status == 'OK') {
    console.log('kkkk');

      this.modalRef.hide();
      this.router.navigate(['/employer/job/post'])
    }
  }

  getPaginateData(event)
  {
    this.pageNo = event.page;
    
    // this.candidates = [];

    
    this.dataFetching = true;
    if(this.status){
  
    this.getFilteredCandidates();
   
  

    // this.status=false
    }
    if(this.status2){
    // this.candidates = [];
    this.getFilteredCandidate();
    // this.status2=false;
    }

  }

  getPaginateforTireTwo(event) {
    this.pageNo = event.page;
    
    // this.candidates = [];

    
    this.dataFetching = true;

    if(this.status){
  
      this.ortireTwowithLocation();
     
    
  
      // this.status=false
      }
      if(this.status2){
      // this.candidates = [];
      this.andtireTwowithLocation();
      // this.status2=false;
      }
  }

  getPaginateforTireOne(event) {
    this.pageNo = event.page;
    
    // this.candidates = [];

    
    this.dataFetching = true;

    if(this.status){
  
      this.ortireOnewithLocation();
     
    
  
      // this.status=false
      }
      if(this.status2){
      // this.candidates = [];
      this.andtireOnewithLocation();
      // this.status2=false;
      }
  }
              // this.aotOfferAll();


  //open candidate job profile and set in local storage
  viewCandidateProfile(candidateId, jobId, roleId,rating,profilepic,isCandidateAvailable) {
    let JobDetailsById = {
      candidateId: candidateId,
      jobId: jobId,
      roleId: roleId,
      rating:rating,
      profilepic:profilepic,
      isCandidateAvailable:isCandidateAvailable
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/candidate/details'])
  }

  selectCandidate(checked, candidateObj) {
    if (checked) {
      this.selectedCandidateArr.push({
        jobId: candidateObj.jobId,
        employerId: candidateObj.employerId,
        candidateId: candidateObj.candidateId,
        roleId: candidateObj.roleId
      })
      console.log("array",this.selectedCandidateArr);
    } else {
      for (var i = 0; i < this.selectedCandidateArr.length; i++) {
        if (this.selectedCandidateArr[i].candidateId === candidateObj.candidateId) {
          this.selectedCandidateArr.splice(i, 1);
          this.selectedCandidateArr.splice(i, 1)
        }
      }
    }
  }

  selectCandidateToInvite(checked, candidateObj) {
    if (checked) {
      this.selectedCandidateToInviteArr.push({
        jobId: candidateObj.jobId,
        employerId: candidateObj.employerId,
        candidateId: candidateObj.candidateId,
        roleId: candidateObj.roleId
      })
    } else {
      for (var i = 0; i < this.selectedCandidateToInviteArr.length; i++) {
        if (this.selectedCandidateToInviteArr[i].candidateId === candidateObj.candidateId) {
          this.selectedCandidateToInviteArr.splice(i, 1);
          this.selectedCandidateToInviteArr.splice(i, 1)
        }
      }
    }
  }

  aotOfferAll() {
    console.log('ts', this.selectedCandidateArr);
    this.employerSetGetService.aotOfferAll({ offerData: this.selectedCandidateArr })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('You have successfully offered your job to selcted candidates', 'Got it!', {
            
            });

            if(this.paramValue == 'option-3') {
              this.masage = "Job offered to selected workers. Choose more workers?"
              this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
            }
            this.selectedCandidateArr = [];
       
            if(this.status){
  
              this.getFilteredCandidates();
              }
              if(this.status2){
              this.getFilteredCandidate();
              }
            }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      )
  }
  aotOffer() {
    console.log('ts', this.selectedCandidateArr);
    this.employerSetGetService.aotOfferAll({ offerData: this.selectedCandidateArr })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('You have successfully offered your job to selcted candidates', 'Got it!', {
             
            });
            this.selectedCandidateArr = [];
            this.status=false;
            this.getFilteredCandidates();
          } 
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      )
  }
  aotInviteCandidates() {
    this.employerSetGetService.aotInviteCandidates({ inviteToApplyData: this.selectedCandidateToInviteArr })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.snackBar.open('You have successfully offered your job to selected candidates', 'Got it!', {
            
            });


            this.selectedCandidateToInviteArr = [];

            this.inviteboolean = true;
            if(this.paramValue == 'option-2') {
              this.masage = "Worker(s) invited successfully"
              this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
            }
            if(this.status){
  
              this.getFilteredCandidates();
              }
              if(this.status2){
              this.getFilteredCandidate();
              }
          } else {
            this.snackBar.open('Failed to invite workers', 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      )
  }

  andtireOnewithLocation() {
    
    let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
    let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],rating:"3",skillArrb:this.skillId,pageno: this.pageNo,
     perpage:this.itemsPerPage,searchWithLocation:true})
   console.log("payload 2",payload);
    this.employerSetGetService.andtireone(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            
            if (result && !result['isError'] ) {

              if(result['noOfAvailableCandidate'] == 0) {
                this.candidatesss =[];
                this.andtireOnewithoutLocation();
                // this.andtireTwowithoutLocation();
                return false;
              }
              this.candidatesss = result['details'];
              let obj =result['details'].TIER1;
              // let obj2 =result['details'].TIER2;
            if(Object.entries(obj).length!==0)
            {
              this.totalItems = obj.total ? obj.total : 0;
              // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
              this.dataFetching = false;
            }else{
              this.dataFetching = false;
              this.snackBar.open('No data found! Please try again','Got it!',{
               
              });
            }
         
            // this.status2=false;
              let candidate =result['details'].TIER1;
              this.offer =candidate.results;
             
              if(result['details'].TIER1.results.length>0 && !this.inviteboolean) {
                this.masage = "Job offered to favourite workers. View list or invite available workers?"
                this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
              }
             
          //     if(result['details'].TIER1.results.length < 2){
          //       this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          //       });
          //     }else{
        
          //   this.snackBar.open(result['message'],'Got it',{
          
          //   });
          // }
          
        }else{
          this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          });
          this.showFilter=true;


      }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
       
        );
  }

  andtireTwowithLocation() {
    let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
    let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],rating:"3",skillArrb:this.skillId,pageno: this.pageNo,
     perpage:this.itemsPerPage,searchWithLocation:true})
   console.log("payload 2",payload);
    this.employerSetGetService.andtiretwo(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        
        if (result && !result['isError']  ) {

          if(result['noOfAvailableCandidate'] == 0) {
            // this.andtireOnewithoutLocation();
            this.candidates = [];
            this.andtireTwowithoutLocation();
            return false;
          }
          this.candidates = result['details'];
          // let obj =result['details'].TIER1;
          let obj =result['details'].TIER2;
        if(Object.entries(obj).length!==0)
        {
          this.totalItems = obj.total ? obj.total : 0;
          // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
          this.dataFetching = false;
        }else{
          this.dataFetching = false;
          this.snackBar.open('No data found! Please try again','Got it!',{
           
          });
        }
     
        // this.status2=false;
          let candidate =result['details'].TIER2;
          this.offer =candidate.results;
         
      
         
          if(result['details'].TIER2.results.length < 2){
            this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

            });
          }else{
    
        this.snackBar.open(result['message'],'Got it',{
      
        });
      }
      
    }else{
      this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

      });
      this.showFilter=true;


  }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
   
    );

  }

  andtireOnewithoutLocation() {
    let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
       let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],rating:"3",skillArrb:this.skillId,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:false})

        this.employerSetGetService.andtireone(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            
            if (result && !result['isError'] && result['details'] ) {
              this.candidatesss = result['details'];
              let obj =result['details'].TIER1;
              // let obj2 =result['details'].TIER2;
            if(Object.entries(obj).length!==0)
            {
              this.totalItems = obj.total ? obj.total : 0;
              // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
              this.dataFetching = false;
            }else{
              this.dataFetching = false;
              this.snackBar.open('No data found! Please try again','Got it!',{
               
              });
            }
         
            // this.status2=false;
              let candidate =result['details'].TIER1;
              this.offer =candidate.results;
             
              if(result['details'].TIER1.results.length>0 && !this.inviteboolean) {
                this.masage = "Job offered to favourite workers. View list or invite available workers?"
                this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
              }
             
          //     if(result['details'].TIER1.results.length < 2){
          //       this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          //       });
          //     }else{
        
          //   this.snackBar.open(result['message'],'Got it',{
          
          //   });
          // }
          
        }else{
          this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          });
          this.showFilter=true;


      }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
       
        );

  }

  andtireTwowithoutLocation() {
    let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
       let payload = Object.assign({roleId :this.roleI, jobId: storedJobIds[0],rating:"3",skillArrb:this.skillId,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:false})

        this.employerSetGetService.andtiretwo(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            
            if (result && !result['isError'] && result['details'] ) {

              this.candidates = result['details'];
              // let obj =result['details'].TIER1;
              let obj =result['details'].TIER2;
            if(Object.entries(obj).length!==0)
            {
              this.totalItems = obj.total ? obj.total : 0;
              // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
              this.dataFetching = false;
            }else{
              this.dataFetching = false;
              this.snackBar.open('No data found! Please try again','Got it!',{
               
              });
            }
         
            // this.status2=false;
              let candidate =result['details'].TIER2;
              this.offer =candidate.results;
             
          
             
              if(result['details'].TIER2.results.length < 2){
                this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

                });
              }else{
        
            this.snackBar.open(result['message'],'Got it',{
          
            });
          }
          
        }else{
          this.snackBar.open("Change the criteria to filter more candidates","Got it!",{

          });
          this.showFilter=true;


      }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
            });
          }
       
        );

  }

  ortireOnewithLocation() {
    let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    if(payload!=null || payload != undefined){
      payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:true,rating:"3"});
    }else{
      let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      payload = Object.assign({roleId :roless, jobId: storedJobIds[0],searchWithLocation:true,rating:"3" })
    console.log("payload 2",payload);

    }
    this.employerSetGetService.ortireone(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        
        if (result && !result['isError'] ) {
          if (result['noOfAvailableCandidate'] == 0) {
            this.candidatesss =[];
            this.ortireOnewithoutLocation();
            // this.ortireTwowithoutLocation();
            return false;
          }
          this.candidatesss = result['details'];
          this.snackBar.open(result['message'],'Got it',{
    
          });
            let obj =result['details'].TIER1;
            // let obj2 =result['details'].TIER2;
          if(Object.entries(obj).length!==0)
          {
            this.totalItems = obj.total ? obj.total : 0;
            // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
            this.dataFetching = false;
          }else{
            this.dataFetching = false;
            this.snackBar.open('No data found! Please try again','Got it!',{
             
            });
          }

          // this.status=false;
          if(result['details'].TIER1.results.length>0 && !this.inviteboolean) {
            this.masage = "Job offered to favourite workers. View list or invite available workers?"
            this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
          }

          let candidate =result['details'].TIER1;
          this.offer =candidate.results;
     
      
       
      
    }else{
      // this.candidates.length=0;

      // this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
      
      // });
      this.showFilter=true;
      this.candidatesss={};



  }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
   
    );

  }

  ortireTwowithLocation() {
    let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    if(payload!=null || payload != undefined){
      payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:true,rating:"3"});
    }else{
      let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      payload = Object.assign({roleId :roless, jobId: storedJobIds[0],searchWithLocation:true,rating:"3" })
    console.log("payload 2",payload);

    }
    this.employerSetGetService.ortiretwo(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        
        if (result && !result['isError'] ) {
          if (result['noOfAvailableCandidate'] == 0) {
            // this.ortireOnewithoutLocation();
            this.candidates = [];
            this.ortireTwowithoutLocation();
            return false;
          }
          this.candidates = result['details'];
          this.snackBar.open(result['message'],'Got it',{
    
          });
            let obj =result['details'].TIER2;
            // let obj2 =result['details'].TIER2;
          if(Object.entries(obj).length!==0)
          {
            this.totalItems = obj.total ? obj.total : 0;
            // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
            this.dataFetching = false;
          }else{
            this.dataFetching = false;
            this.snackBar.open('No data found! Please try again','Got it!',{
             
            });
          }

          // this.status=false;

          let candidate =result['details'].TIER2;
          this.offer =candidate.results;
     
      
       
      
    }else{
      // this.candidates.length=0;

      this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
      
      });
      this.showFilter=true;
      this.candidates={};



  }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
   
    );
  }

  ortireOnewithoutLocation() {
    let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    if(payload!=null || payload != undefined){
      payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:false,rating:"3"});
    }else{
      let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      payload = Object.assign({roleId :roless, jobId: storedJobIds[0],searchWithLocation:false,rating:"3" })
    console.log("payload 2",payload);

    }

    this.employerSetGetService.ortireone(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        
        if (result && !result['isError'] && result['details'] ) {
 
          this.candidatesss = result['details'];
          this.snackBar.open(result['message'],'Got it',{
    
          });
            let obj =result['details'].TIER1;
            // let obj2 =result['details'].TIER2;
          if(Object.entries(obj).length!==0)
          {
            this.totalItems = obj.total ? obj.total : 0;
            // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
            this.dataFetching = false;
          }else{
            this.dataFetching = false;
            this.snackBar.open('No data found! Please try again','Got it!',{
             
            });
          }

          // this.status=false;
          if(result['details'].TIER1.results.length>0 && !this.inviteboolean) {
            this.masage = "Job offered to favourite workers. View list or invite available workers?"
            this.modalRef = this.modalService.show(this.information,this.ngbModalOptions);
          }

          let candidate =result['details'].TIER1;
          this.offer =candidate.results;
     
      
       
      
    }else{
      // this.candidates.length=0;

      // this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
      
      // });
      this.showFilter=true;
      this.candidatesss={};



  }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
   
    );

  }

  ortireTwowithoutLocation() {
    let payload = this.saveJobIdRoleId;
    let roless = this.rolesss;
    if(payload!=null || payload != undefined){
      payload = Object.assign(payload, { skills: this.selectedSkillsPayload,pageno: this.pageNo,
        perpage:this.itemsPerPage,searchWithLocation:false,rating:"3"});
    }else{
      let storedJobIds = this.employerSetGetService.getNewlyCreatedJobids;
      payload = Object.assign({roleId :roless, jobId: storedJobIds[0],searchWithLocation:false,rating:"3" })
    console.log("payload 2",payload);

    }

    this.employerSetGetService.ortiretwo(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        
        if (result && !result['isError'] && result['details'] ) {
      
          this.candidates = result['details'];
          this.snackBar.open(result['message'],'Got it',{
    
          });
            let obj =result['details'].TIER2;
            // let obj2 =result['details'].TIER2;
          if(Object.entries(obj).length!==0)
          {
            this.totalItems = obj.total ? obj.total : 0;
            // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
            this.dataFetching = false;
          }else{
            this.dataFetching = false;
            this.snackBar.open('No data found! Please try again','Got it!',{
             
            });
          }

          // this.status=false;

          let candidate =result['details'].TIER2;
          this.offer =candidate.results;
     
      
       
      
    }else{
      // this.candidates.length=0;

      this.snackBar.open('No candidates found,please Increase the Distance of Job Location', 'Got it!', {
      
      });
      this.showFilter=true;
      this.candidates={};



  }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
        });
      }
   
    );

  }

}
