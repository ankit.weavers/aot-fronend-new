import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CandidateProfileListComponent, CandidateProfileDetailsComponent,CandidateProfileComponent } from './';
const routes: Routes = [
  {
    path: 'list/:option',
    component: CandidateProfileListComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Worker List'
    }
  },
  {
    path: 'details',
    component: CandidateProfileDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Worker Details'
    }
  },
  {
    path: 'candidate-profile',
    component: CandidateProfileComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Worker Profile'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidateRoutingModule { }
