import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material';
import { MaterialModule } from 'src/app/modules/material.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CandidateRoutingModule } from './candidate-routing.module';
import { CandidateProfileListComponent, CandidateProfileDetailsComponent } from './';
import {
 
  PaginationModule
} from 'ngx-bootstrap';
import { CandidateProfileComponent } from './candidate-profile/candidate-profile.component';
@NgModule({
  declarations: [
    CandidateProfileListComponent,
    CandidateProfileDetailsComponent,
    CandidateProfileComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MatNativeDateModule,
    MaterialModule,
    ReactiveFormsModule,
    CandidateRoutingModule,
    PaginationModule.forRoot(),
  ]
})
export class CandidateModule { }
