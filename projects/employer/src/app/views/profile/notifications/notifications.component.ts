import { Component, OnInit } from '@angular/core';
import { EmployerJobService } from '../../../services';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'candidate-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  private userData =JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allNotification:any;
  notiId=[];
  totalItems;
  itemsPerPage;
  totalNoOfPages;
  pageNo;
  dataFetching =false;
  // noti=[];
  

  constructor(    private EmployerJobService: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
this.notifications();
  }
  notifications(){
    let payload={
      employerId:this.userData.employerId,
      pageNo: this.pageNo,
      perPage:this.itemsPerPage
    }
this.EmployerJobService.getNotificationDetails(payload)
.pipe(takeUntil(this.onDestroyUnSubscribe))
.subscribe(
  result => {
    if (result && !result['isError'] && result['details'][0]) {
  
    this.allNotification =result['details'][0].results;
    // if(result['details']==null){
    //   this.allNotification={};
    // }
    // this.snackBar.open(result['mmessage'],'Got it!',{
    //   duration:5000,
    // });
    // this.deleteNotification= result['details'][0].results;
    // console.log("bgjg",this.allNotification);
    // for(let i=0;i<this.allNotification.length;i++){
    //   this.notiId[i]=this.allNotification[i].notification._id;
    // }
    // console.log("id",this.notiId);
    let obj =result['details'][0];
    if(Object.entries(obj).length !==0){
      this.totalItems = obj.total ? obj.total : 0;
      this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
      this.dataFetching = false;
    }

     
      
    }else {
      this.dataFetching = false;
      this.snackBar.open('No data found! Please try again', 'Got it!', {
      
      });
    }
    error => {
      this.dataFetching = false;
      this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
      });
    }
    }

  
  );
} 
deleteNotifications(notificationId){
  let payload={
    employerId:this.userData.employerId,
    notificationId:notificationId
  }
this.EmployerJobService.deleteNotificationDetails(payload)
.pipe(takeUntil(this.onDestroyUnSubscribe))
.subscribe(
result => {
  if (result && !result['isError']){
    this.snackBar.open('Notification successfuly deleted','Got it!',{
    
    });
    this.notifications();
  }
}

  
);
}
getPaginateData(event){
  this.pageNo = event.page;
  this.allNotification = [];
  this.dataFetching = true;
  this.notifications();
}
} 

