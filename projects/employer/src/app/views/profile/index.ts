export { CompanyProfileComponent } from './company-profile/company-profile.component';
export { IndividualProfileComponent } from './individual-profile/individual-profile.component';
export { EditProfileComponent } from './edit-profile/edit-profile.component';
export { CompanyDetailsComponent } from './edit-profile/company-details/company-details.component';
export { AddressComponent } from './edit-profile/address/address.component';
export { ContactDetailsComponent } from './edit-profile/contact-details/contact-details.component';
export { ChangePasswordComponent } from './change-password/change-password.component';
export { NotificationsComponent } from './notifications/notifications.component';
export { MessagesComponent } from './messages/messages.component';
export { ContactListComponent } from './contact-list/contact-list.component';
