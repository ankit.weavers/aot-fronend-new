import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared.module';
// import { MatNativeDateModule } from '@angular/material';
// import { MaterialModule } from 'src/app/modules/material.module';

import { EmployerProfileRoutingModule } from './employer-profile-routing.module';
import {
  CompanyProfileComponent,
  IndividualProfileComponent,
  EditProfileComponent,
  CompanyDetailsComponent,
  AddressComponent,
  ContactDetailsComponent,
  ChangePasswordComponent,
  NotificationsComponent,
  MessagesComponent,
  ContactListComponent
} from './';

@NgModule({
  declarations: [
    CompanyProfileComponent,
    IndividualProfileComponent,
    EditProfileComponent,
    CompanyDetailsComponent,
    AddressComponent,
    ContactDetailsComponent,
    ChangePasswordComponent,
    NotificationsComponent,
    MessagesComponent,
    ContactListComponent
  ],
  imports: [
    // CommonModule,
    SharedModule,
    // MatNativeDateModule,
    // MaterialModule,
    EmployerProfileRoutingModule
  ]
})
export class EmployerProfileModule { }
