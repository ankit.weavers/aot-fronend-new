import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { EmployerAuthenticationService } from '../../../services';
import { GlobalActionsService } from 'src/app/services';

// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'employer-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss']
})
export class CompanyProfileComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();
  public employerDetails: any;
  public storedDetails: any;
  details : any;
  industryDetails: any;
  countryDetails : any;
  constructor(
    private employerauthenticationService: EmployerAuthenticationService,
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.storedDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.getEmployerDetails();
  }

  getEmployerDetails() {
    // let storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    this.employerauthenticationService.getEmployerDetails({ employerId: (this.storedDetails['employerId']).toString() })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.employerDetails = result['details'];
            this.details =  result['cityDetails']
            this.countryDetails  =  result['countryDetails']
            this.industryDetails   = result['industryDetails']
            // const messageSuccess = 'Company details found.'
            // this.snackBar.open(messageSuccess, 'Got it!', {
            //   duration: 3000,
            // });
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );
  }

  // profleImageUpload(changeEvent) {
  //   if (changeEvent.target.files && changeEvent.target.files[0]) {
  //     const fileReader = new FileReader();
  //     fileReader.readAsDataURL(changeEvent.target.files[0]);
  //     fileReader.onload = (event) => {
  //       const dataUrl = event.target['result'];
  //       const uploadData = {
  //         employerId: this.employerDetails.employerId,  //
  //         profilePic: dataUrl
  //       };
  //       this.employerauthenticationService.profleImageUpload(uploadData)
  //         .pipe(takeUntil(this.onDestroyUnSubscribe))
  //         .subscribe(
  //           (result) => {
  //             if (result && !result['isError']) {
  //               //   this.router.navigate(['/edit']);
  //               let messageSuccess = 'Image uploaded successfully.'
  //               this.snackBar.open(messageSuccess, 'Got it!', {
  //                 duration: 5000,
  //               });
  //               this.getEmployerDetails();
  //             } else {
  //               this.snackBar.open('Internal Server Error', 'Got it!', {
  //                 duration: 5000,
  //               });
  //             }
  //           },
  //           (error) => {
  //             this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //               duration: 5000,
  //             });
  //           },
  //         );
  //     };

  //   }
  // }

  // getSelectedImage(event) {
  //   if (event.target.files && event.target.files[0]) {

  //     let folderName = "profile"

  //     this.globalActionsService.uploadFile(event.target.files[0], this.storedDetails.userType, folderName, 'profile')
  //       .then(
  //         (res) => { // Success
  //           const uploadData = {
  //             employerId: this.employerDetails.employerId,
  //             profilePic: res['Location']
  //           };
  //           this.employerauthenticationService.profileS3ImageUpload(uploadData)
  //             .pipe(takeUntil(this.onDestroyUnSubscribe))
  //             .subscribe(
  //               (result) => {
  //                 if (result && !result['isError']) {
  //                   let messageSuccess = 'Image uploaded successfully.'
  //                   this.snackBar.open(messageSuccess, 'Got it!', {
  //                     duration: 5000,
  //                   });
  //                   this.getEmployerDetails();
  //                 } else {
  //                   this.snackBar.open('Internal Server Error', 'Got it!', {
  //                     duration: 5000,
  //                   });
  //                 }
  //               },
  //               (error) => {
  //                 this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //                   duration: 5000,
  //                 });
  //               },
  //             );
  //         }
  //       );
  //   }
  // }

}
