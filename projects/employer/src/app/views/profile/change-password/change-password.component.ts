import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  // Country,
  // UsernameValidator,
  PasswordValidator,
  // ParentErrorStateMatcher,
  // PhoneValidator
} from 'src/app/validators'; // Models

import { ActivatedRoute, Router } from '@angular/router';
import { EmployerAuthenticationService } from '../../../services';
import { User, SocialLogin, Erros } from 'src/app/models';

// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'employer-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  private onDestroyUnSubscribe = new Subject<void>();
  private storedDetails: any;
  public changePasswordForm = this.formBuilder.group({
    oldPassword: ['', Validators.required],
    newPassword: ['', [
        Validators.required,
        Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
      ]
    ],
    confirmPassword: ['', Validators.required]
  }, {validator: PasswordValidator('newPassword', 'confirmPassword')});
  public errors: any = Erros;
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private employerauthenticationService: EmployerAuthenticationService,
    public snackBar: MatSnackBar
  ) {
    this.storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    // UnSubscribe Subscriptions
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  changePassword() {
    if (this.changePasswordForm.valid) {
      this.changePasswordForm.value['employerId']  = this.storedDetails['employerId'];

      let changePasswordData = this.changePasswordForm.value
      this.changePasswordForm.reset();

      this.employerauthenticationService.changePassword(changePasswordData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result------Employer----change password ', result);
          if (result && !result['isError']) {
            console.log('s', result);
            const messageSuccess =  'Password changed successfully.';
            this.snackBar.open(messageSuccess, 'Got it!', {
           
            });
          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
    } else {
      this.snackBar.open('Please fill in all the required fields', 'Got it!', {
       
      });
    }
  }

}
