import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import {EmployerJobService} from '../../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'employer-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

  employerId = JSON.parse(localStorage.getItem('currentUser')).employerId;
  contactList = [];
  // let getJobDetails = localStorage.getItem('JobDetails')


  constructor(private firestoreDB: AngularFirestore, private router: Router, private employerSetGetService: EmployerJobService,) { }

  ngOnInit() {
    // this.contactDetails();
    this.getEmployerContacts();
  //  console.log("contact on init");
  }

  getEmployerContacts() {
    this.firestoreDB.collection('chatrooms', ref => ref.where('employerId', '==', this.employerId))
      .snapshotChanges().subscribe(
        docSnapshot => {
          this.contactList = [];
          docSnapshot.map(element => {
            this.contactList.push(element.payload.doc.data());
          });

        //  console.log("contactList" , this.contactList);

        }
      );
  }
 
  //open chat room
  openChat(candidateId, chatroomId) {
    let JobDetailsById = {
      candidateId: candidateId,
      chatroomId: chatroomId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/profile/messages']);
  }
  // contactDetails(){
  //   let payload={
  //     employerId:this.employerId
  //   }

  //   this.employerSetGetService.getEmployerContactDetail(payload)
  //   .pipe(takeUntil(this.onDestroyUnSubscribe))
  //   .subscribe(
  //     result => {
  //       if(result && !result['isError'] && result['details']){
           
  //       }
  //     });
  // }


}
