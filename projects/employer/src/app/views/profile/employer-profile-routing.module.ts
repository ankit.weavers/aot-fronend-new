import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  CompanyProfileComponent,
  IndividualProfileComponent,
  EditProfileComponent,
  CompanyDetailsComponent,
  AddressComponent,
  ContactDetailsComponent,
  ChangePasswordComponent,
  NotificationsComponent,
  MessagesComponent,
  ContactListComponent
} from './';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'company-profile',
    pathMatch: 'full'
  },
  {
    path: 'company-profile',
    component: CompanyProfileComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: ['profile/edit'],
      title: 'Company Profile'
    }
  },
  {
    path: 'domiciliary-profile',
    component: IndividualProfileComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: ['profile/edit'],
      title: 'Domiciliary Profile'
    }
  },
  {
    path: 'edit',
    component: EditProfileComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Edit Profile'
    }
  },
  {
    path: 'edit/company-details',
    component: CompanyDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Company Details'
    }
  },
  {
    path: 'edit/address',
    component: AddressComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Address'
    }
  },
  {
    path: 'edit/contact-details',
    component: ContactDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Contact Details'
    }
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Change Password'
    }
  },
  {
    path: 'notifications',
    component: NotificationsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Notifications'
    }
  },
  {
    path: 'contacts',
    component: ContactListComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Contacts'
    }
  },
  {
    path: 'messages',
    component: MessagesComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Messages'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerProfileRoutingModule { }
