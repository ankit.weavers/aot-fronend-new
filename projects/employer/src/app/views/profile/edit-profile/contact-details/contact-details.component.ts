import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { EmployerAuthenticationService } from '../../../../services';

// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';


import { CompanyContact, Erros } from 'src/app/models';
@Component({
  selector: 'employer-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

  public storedDetails: any;

  public employerDetails: any;
  public errors: any = Erros;

  public companyContactForm = this.formBuilder.group({
    // emailMobile: ['', Validators.required],
    // confirmEmailMobile: ['', Validators.required],
    // //email: ['', [Validators.required, Validators.email]],
    // password: ['', Validators.required],
    // confirmPassword: ['', Validators.required],
    // country: ['', Validators.required],
    // emprType: ['', Validators.required],
    // agreeTerms: [false, Validators.requiredTrue],
    contactName: ['', Validators.required],
    EmailId: ['', [Validators.required, Validators.email]],
    mobileNo: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
    contactTelephoneNo: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
  });


  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private employerAuthenticationService: EmployerAuthenticationService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    this.getEmployerDetails();
  }

  getEmployerDetails() {
    // alert('getting company details.');
    // let storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    // let employerId = 'EMP1556604908411';
    this.employerAuthenticationService.getEmployerDetails({employerId: (this.storedDetails['employerId']).toString()})
    // this.employerAuthenticationService.getEmployerDetails({'employerId': employerId })
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        console.log('result------Employer----details', result)
        if (result && !result['isError']) {
          console.log('s', result);
          this.employerDetails = {
            contactName : result['details'].contactName ? result['details'].contactName : '',
            EmailId : result['details'].EmailId ? result['details'].EmailId : '',
            mobileNo : result['details'].mobileNo ? result['details'].mobileNo : '',
            contactTelephoneNo : result['details'].contactTelephoneNo ? result['details'].contactTelephoneNo : '',
          };
          console.log( 'company detals---------------', this.employerDetails);
          this.companyContactForm.setValue(this.employerDetails);
        } else {
          console.log('e', result);
          this.snackBar.open(result['message'], 'Got it!', {
        
          });
        }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
      
        });
      }
    );
  }

  saveCompanyContact() {
    // alert('saving company contact.');

    // let storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    this.companyContactForm.value.employerId  = this.storedDetails['employerId'];
    if (this.companyContactForm.valid) {
      this.employerAuthenticationService.saveCompanyContact(this.companyContactForm.value as CompanyContact)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result------Employer----add company contact', result)
          if (result && !result['isError']) {
            console.log('s', result);
            this.companyContactForm.reset();
            this.router.navigate(['/employer/profile/edit']);
         //   this.router.navigate(['/edit']);
            // this.errorLogin = false;
            const messageSuccess =  'Company contact details saved successfully.';
            this.snackBar.open(messageSuccess, 'Got it!', {
             
            });
          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );
    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {
      
      });
    }
  }

}
