import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { EmployerAuthenticationService } from '../../../services';
import { GlobalActionsService } from 'src/app/services';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Erros } from 'src/app/models';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'employer-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})

export class EditProfileComponent implements OnInit {
// @ViewChild('selectIndustry') selectIndustry:ElementRef;
// @ViewChild('name') name:ElementRef;
  private onDestroyUnSubscribe = new Subject<void>();
  public employerDetails: any;
  public storedDetails: any;
  public dada :any
  public editProfileForm: FormGroup;
  industryList = [];
  public errors: any = Erros;
  image : boolean = false;

  constructor(
    private employerauthenticationService: EmployerAuthenticationService,
    private globalActionsService: GlobalActionsService, public snackBar: MatSnackBar,
    private fb: FormBuilder, public router: Router, private firestoreDB: AngularFirestore
  ) {
    this.editProfileForm = this.fb.group({
      industry: ['', Validators.required],
      fullname: ['']
    });
  }


  ngOnInit() {
    this.storedDetails = JSON.parse(localStorage.getItem('currentUser'));
    // this.dada = JSON.parse(localStorage.getItem('holdData'))
    this.getEmployerDetails();
    this.getIndustryList();
    // this.getData();
  }

   // MAKE FORMCONTROLE TOUCH //
   public markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  getEmployerDetails() {
    this.employerauthenticationService.getEmployerDetails({ employerId: (this.storedDetails['employerId']).toString() })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.employerDetails = result['details'];
            this.editProfileForm.controls.fullname.setValue(this.employerDetails.individualContactName)
            this.editProfileForm.controls.industry.setValue(this.employerDetails.industryId)
            console.log('kuch to ho',this.storedDetails.employerType);
            this.getData()

            // if(!this.employerDetails.individualContactName && this.employerDetails.industryId.length == 0 && this.storedDetails.employerType === "INDIVIDUAL") {
            //   console.log('data1')
            //   this.getData()
            // }

            // if(this.employerDetails.industryId.length == 0 && this.storedDetails.employerType != "INDIVIDUAL") {
            //   console.log('data2')
            //   this.getData()
            // }
            //   this.router.navigate(['/edit']);
            // const messageSuccess = 'Details updated.'
            // this.snackBar.open(messageSuccess, 'Got it!', {
            //   duration: 3000,
            // });
          }
          // else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //     duration: 3000,
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
          });
        }
      );
  }

  getData() {
    const eventPage1 = JSON.parse(localStorage.getItem('holdData'))
    if (typeof eventPage1 !== 'undefined' && eventPage1 !== null) {
      console.log('data3');
      this.editProfileForm.controls.fullname.setValue(eventPage1.name)
      this.editProfileForm.controls.industry.setValue(eventPage1.industry)
    }
  }

  selectIndustry() {
    const data={
            industry: this.editProfileForm.value.industry ? this.editProfileForm.value.industry:'',
            name: this.editProfileForm.value.fullname ? this.editProfileForm.value.fullname:''
          }
          localStorage.setItem('holdData',JSON.stringify(data));
  }
  name() {
    const data={
      industry: this.editProfileForm.value.industry ? this.editProfileForm.value.industry:'',
      name: this.editProfileForm.value.fullname ? this.editProfileForm.value.fullname:''
          }
          localStorage.setItem('holdData',JSON.stringify(data));
  }

  getSelectedImage(event) {
    if (event.target.files && event.target.files[0]) {

      let folderName = "profile"

      this.globalActionsService.uploadFile(event.target.files[0], this.storedDetails.userType, folderName, 'profile')
        .then(
          (res) => { // Success
            const uploadData = {
              employerId: this.employerDetails.employerId,
              profilePic: res['Location']
            };

            this.employerauthenticationService.profileS3ImageUpload(uploadData)
              .pipe(takeUntil(this.onDestroyUnSubscribe))
              .subscribe(
                (result) => {
                  if (result && !result['isError']) {
                    //   this.router.navigate(['/edit']);
                    let messageSuccess = 'Image uploaded successfully.'
                    this.snackBar.open(messageSuccess, 'Got it!', {
                    
                    });
                    this.image = true;

                    let employerDetailsStored = JSON.parse(localStorage.getItem('currentUser'));
                    employerDetailsStored.profilePic = res['Location'];
                    localStorage.setItem('currentUser', JSON.stringify(employerDetailsStored));
                    this.globalActionsService._setProfilePic({ status: 'set' });
                    this.getEmployerDetails();

                    //update employer profile pic
                    this.firestoreDB.collection('chatrooms', ref => ref.where('employerId', '==', this.employerDetails.employerId)).get().subscribe(
                      docsSnapshot => {
                        if (!docsSnapshot.empty) {
                          docsSnapshot.docs.forEach(element => {
                            this.firestoreDB.firestore.collection('chatrooms').doc(element.id).update({ employerProfilePic: res['Location'] })
                          });
                        }
                      });

                  } else {
                    this.snackBar.open('Internal Server Error', 'Got it!', {
                      
                    });
                  }
                },
                (error) => {
                  this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
                    
                  });
                },
              );
          }
        );
    }
  }

  // company() {
  //   console.log('industry',this.selectIndustry.nativeElement);
  //   if(this.selectIndustry.nativeElement) {
  //     const data={
  //       industry:this.selectIndustry.nativeElement,
  //       name:this.name.nativeElement.value ? this.name.nativeElement.value : ''
  //     }
  //     localStorage.setItem('holdData',JSON.stringify(data));
  //   this.router.navigate(['/employer/profile/edit/company-details']);

  //   } else {
  //   this.router.navigate(['/employer/profile/edit/company-details']);

  //   }
  // }

  // address() {
    

  //   console.log('industry',this.selectIndustry.nativeElement);
  //   if(this.selectIndustry.nativeElement) {
  //     const data={
  //       industry:this.selectIndustry.nativeElement,
  //       name:this.name.nativeElement.value ? this.name.nativeElement.value : ''
  //     }
  //     localStorage.setItem('holdData',JSON.stringify(data));
  //     this.router.navigate(['/employer/profile/edit/address']);

  //   }
  //   else {
  //   this.router.navigate(['/employer/profile/edit/address']);

  //   }
  // }

  // contact() {
  //   if(this.selectIndustry.nativeElement) {
  //     const data={
  //       industry:this.selectIndustry.nativeElement,
  //       name:this.name.nativeElement.value ? this.name.nativeElement.value : ''
  //     }
  //     localStorage.setItem('holdData',JSON.stringify(data));
  //     this.router.navigate(['/employer/profile/edit/contact-details']);

  //   }
  //   else {
  //     this.router.navigate(['/employer/profile/edit/contact-details']);

  //   }
  // }

  // get Industry List
  getIndustryList() {
    this.globalActionsService.getIndustry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.industryList = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  // submit form
  submitProfileDetails() {

    if (this.editProfileForm.status === 'INVALID') {
      this.markFormGroupTouched(this.editProfileForm);
      return false;
    }
    if(!this.employerDetails.profilePic) {
      this.snackBar.open('Please Upload Company logo','Got it!',{

      });
      return ;
    }
    if(!this.editProfileForm.value.industry) {
      this.snackBar.open('Please Select Industry ','Got it!',{

      });
      return;
    }
    if(!this.editProfileForm.value.fullname && this.storedDetails['employerType'] == 'INDIVIDUAL' ) {
      this.snackBar.open('Please Fill Name','Got it!',{

      });
      return;
    }
    if(this.storedDetails['employerType'] != 'INDIVIDUAL' && !this.employerDetails.companyName ) {
      this.snackBar.open('Please Fill Company Details','Got it!',{

      });
      return;
    }
        if( !this.employerDetails.address.addressLineOne ) {
      this.snackBar.open('Please Fill Address','Got it!',{

      });
      return;
    }
    if( !this.employerDetails.contactName ) {
      this.snackBar.open('Please Fill Contact Details','Got it!',{

      });
      return;
    }
    let profileObject = {
      employerId: this.employerDetails.employerId,
      individualContactName: this.editProfileForm.value.fullname,
      industryId: this.editProfileForm.value.industry
    }
    // if (this.editProfileForm.valid) {
      this.editProfileForm.reset();
      this.employerauthenticationService.saveProfileChanges(profileObject)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              //update employer name
              this.firestoreDB.collection('chatrooms', ref => ref.where('employerId', '==', this.employerDetails.employerId)).get().subscribe(
                docsSnapshot => {
                  if (!docsSnapshot.empty) {
                    docsSnapshot.docs.forEach(element => {
                      this.firestoreDB.firestore.collection('chatrooms').doc(element.id).update({ employerName: profileObject.individualContactName })
                    });
                  }
                });
              this.getEmployerDetails();
              localStorage.removeItem('holdData');
               this.router.navigate(['/employer/profile/company-profile']);
              const messageSuccess = 'Profile details saved successfully.';
              this.snackBar.open(messageSuccess, 'Got it!', {
              
              });
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    // } else {
    //   this.snackBar.open('Please fill all the required fields.', 'Got it!', {
    //     duration: 5000,
    //   });
    // }
  }

}
