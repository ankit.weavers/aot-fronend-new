import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { CompanyAddress, Erros } from 'src/app/models';
import { EmployerAuthenticationService } from '../../../../services';
import { GlobalActionsService } from 'src/app/services';

// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'employer-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})


export class AddressComponent implements OnInit {

  public errors: any = Erros;
  public storedDetails: any;
  public employerDetails: any;
  private onDestroyUnSubscribe = new Subject<void>();

  public allCountries: any[] = [];
  public allCities: any[] = [];
  countryVariable: any;
  selectedPayballUser: any;
  country: any;
  variable: boolean = false;
  cityId;
  countryId;
  parttenn = "^([A-Za-z][A-Ha-hJ-Yj-y]?[0-9][A-Za-z0-9]? ?[0-9][A-Za-z]{2}|[Gg][Ii][Rr] ?0[Aa]{2}).*[ ].*$";

  public companyAddressForm = this.formBuilder.group({
    // emailMobile: ['', Validators.required],
    // confirmEmailMobile: ['', Validators.required],
    // //email: ['', [Validators.required, Validators.email]],
    // password: ['', Validators.required],
    // confirmPassword: ['', Validators.required],
    // country: ['', Validators.required],
    // emprType: ['', Validators.required],
    // agreeTerms: [false, Validators.requiredTrue],
    addressOne: ['', Validators.required],
    addressTwo: [''],
    countryCode: ['', Validators.required],
    city: ['', Validators.required],
    postCode: ['', Validators.required]
  });


  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private employerAuthenticationService: EmployerAuthenticationService,
    private globalActionsService: GlobalActionsService,
    public snackBar: MatSnackBar
  ) { }
  ngOnInit() {
    this.storedDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.getEmployerDetails();
    this.getAllCountries();
  }

  getEmployerDetails() {
    this.employerAuthenticationService.getEmployerDetails({ employerId: (this.storedDetails['employerId']).toString() })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            if (result['details'].address) {

              if (result['details'].address.countryCode) {
                this.getAllCities(result['details'].address.countryCode);

              }
              if (result['cityDetails'].cityId) {
                this.cityId = result['cityDetails'].cityId ? result['cityDetails'].cityId : ''
                this.countryId = result['cityDetails'].country_id ? result['cityDetails'].country_id : ''
              }


              this.employerDetails = {
                addressOne: result['details'].address.addressLineOne ? result['details'].address.addressLineOne : '',
                addressTwo: result['details'].address.addressLineTwo ? result['details'].address.addressLineTwo : '',
                city: result['cityDetails'].name ? result['cityDetails'].name : '',
                countryCode: result['countryDetails'].name ? result['countryDetails'].name : '',
                postCode: result['details'].address.postCode ? result['details'].address.postCode : '',
              };

              this.companyAddressForm.setValue(this.employerDetails);
            }
          }
          // else {
          //   this.snackBar.open(result['message'], 'Got it!', {
          //     duration: 3000,
          //   });
          // }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }
  saveCompanyAddress() {
    // let city;
    // let country_Id;
    // if(this.variable) {
    let city = this.selectedPayballUser ? this.selectedPayballUser : this.cityId;
    let country_Id = this.country ? this.country : this.countryId;
    // }
    // else {
    //   city = this.cityId
    //   country_Id = this.countryId
    // }
    let payload = {
      addressOne: this.companyAddressForm.value.addressOne,
      addressTwo: this.companyAddressForm.value.addressTwo,
      city: city,
      countryCode: country_Id.toString(),
      postCode: this.companyAddressForm.value.postCode,
      employerId: this.companyAddressForm.value.employerId = this.storedDetails['employerId']
    }

    if (this.companyAddressForm.valid) {
      this.employerAuthenticationService.saveCompanyAddress(payload)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.companyAddressForm.reset();
              this.router.navigate(['/employer/profile/edit']);
              //   this.router.navigate(['/edit']);
              // this.errorLogin = false;
              const messageSuccess = 'Company address details saved successfully.';
              this.snackBar.open(messageSuccess, 'Got it!', {

              });
            } else {
              // this.errorLogin = true;
              this.snackBar.open(result['message'], 'Got it!', {

              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

            });
          }
        );
    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {

      });
    }
  }

  getAllCountries() {
    this.globalActionsService.getCountry()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            //   this.router.navigate(['/edit']);
            // this.errorLogin = false;
            this.allCountries = result['details'];
          } else {
            // this.errorLogin = true;
            this.snackBar.open(result['message'], 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }
  getAllCities(countryId) {
    this.companyAddressForm.controls.city.setValue('');
    // this.allCities = [];
    this.countryVariable = countryId;
    this.globalActionsService.getCityNew({ country_id: countryId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            //   this.router.navigate(['/edit']);
            // this.errorLogin = false;
            this.allCities = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }
  selectedCountry() {
    this.variable = true
    this.country = this.companyAddressForm.value.countryCode;
    console.log('selectedRoles', this.country.toString());
    this.getAllCities(this.country.toString());

  }
  sendInvitation(organization: any) {
    console.log(organization);
    this.variable = true;
    // this.selecktPayball.push(organization.email);
    this.selectedPayballUser.push(organization.cityId)
    console.log("email", this.selectedPayballUser);
    // this.personal_referral_code = organization.personal_referral_code;
    // this.firstFormGroup.patchValue({
    //   referralCode : organization.first_name + " " + organization.last_name
    // })
  }
  selectedJobRoles() {
    this.variable = true
    this.selectedPayballUser = this.companyAddressForm.value.city;
    console.log('selectedRoles', this.selectedPayballUser);

    // console.log('skills',this.skills)
    //  this.getRoleSkills();
  }
  getAllCitiess(event: any) {

    if (event.length < 1) {
      this.selectedPayballUser = [];
      console.log("test", this.selectedPayballUser);
      return false;
    }

    let payload = {
      country_id: this.countryVariable,
      name: event
    }

    this.globalActionsService.getCityNew(payload)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allCities = result['details'];
          } else {
            this.snackBar.open(result['message'], 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }

}
