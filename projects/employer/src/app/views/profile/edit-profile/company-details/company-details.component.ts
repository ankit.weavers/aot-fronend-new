import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { EmployerAuthenticationService } from '../../../../services';

// RxJs
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';


import { Company, Erros } from 'src/app/models';
@Component({
  selector: 'employer-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();

  public storedDetails: any;

  public employerDetails: any;
  public errors: any = Erros;

  public companyDetailsForm = this.formBuilder.group({
    companyName: ['', Validators.required],
    companyDescription: '',
    brandName: '',
    companyNo: ['', [Validators.required, Validators.pattern(/^\d{10,11}$/)]],
    vatNo: ['', ],
    capacity: ['', [
      Validators.required,
      Validators.pattern(/^[1-9-]+[0-9]*$/)
    ]],
  });

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private employerAuthenticationService: EmployerAuthenticationService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    this.getEmployerDetails();
  }

  getEmployerDetails() {
    this.employerAuthenticationService.getEmployerDetails({employerId: (this.storedDetails['employerId']).toString()})
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        console.log('result------Employer----details', result)
        if (result && !result['isError']) {
          console.log('s', result);
          this.employerDetails = {
            companyName : result['details'].companyName ? result['details'].companyName : '',
            companyDescription : result['details'].companyDescription ? result['details'].companyDescription : '',
            brandName : result['details'].brandName ? result['details'].brandName : '',
            companyNo : result['details'].companyNo ? result['details'].companyNo : '',
            vatNo : result['details'].vatNo ? result['details'].vatNo : '',
            capacity : result['details'].hiringCapacity ? result['details'].hiringCapacity : '',
          };
          console.log( 'company detals---------------', this.employerDetails);
          this.companyDetailsForm.setValue(this.employerDetails);
        } 
        // else {
        //   console.log('e', result);
        //   this.snackBar.open(result['message'], 'Got it!', {
        //     duration: 3000,
        //   });
        // }
      },
      error => {
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  
        });
      }
    );
  }


  savecompanyDetails() {
    // const storedDetails =  JSON.parse(localStorage.getItem('currentUser'));
    this.companyDetailsForm.value.employerId  = this.storedDetails['employerId'];

    if (this.companyDetailsForm.valid) {
      this.employerAuthenticationService.savecompanyDetails(this.companyDetailsForm.value as Company)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          console.log('result------Employer----add company details', result)
          if (result && !result['isError']) {
            console.log('s', result);
            this.companyDetailsForm.reset();
            this.router.navigate(['/employer/profile/edit']);
            // this.errorLogin = false;
            // this.getEmployerDetails();
            const messageSuccess =  'Company details saved successfully.'
            this.snackBar.open(messageSuccess, 'Got it!', {
              
            });
          } else {
            // this.errorLogin = true;
            console.log('e', result);
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
    } else {
      this.snackBar.open('Please fill all the required fields.', 'Got it!', {
      
      });
    }


  }

}
