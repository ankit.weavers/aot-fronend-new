import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'employer-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})

export class ContactUsComponent implements OnInit {
  contactUsDetails:any;
  private onDestroyUnSubscribe = new Subject<void>();

  constructor(
    private globalActionsService: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.getContactUsDetails();
  }
  getContactUsDetails(){
    this.globalActionsService.getContactUsDetails({ userType: JSON.parse(localStorage.getItem('currentUser')).userType })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.contactUsDetails = result['details'];
          }
    });
  }
}
