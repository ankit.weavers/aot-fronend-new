import { Component, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';

@Component({
  selector: 'employer-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})

export class AboutUsComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  aboutUsDescription;

  constructor(
    private globalservice: GlobalActionsService,
    public snackBar: MatSnackBar,
  ) { }


  ngOnInit() {
    this.getAboutUs();
  }

  //get candidate about us
  getAboutUs() {
    this.globalservice.getAboutUsDetails({ userType: 'EMPLOYER' })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(typeof result === "object" && (result !== null)  ){
            if(result['details'] !== null){
              this.aboutUsDescription = result['details'].aboutUs[0].aboutUsDetails;
            }
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }


}

