export { AboutUsComponent } from './about-us/about-us.component';
export { FaqComponent } from './faq/faq.component';
export { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
export { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
export { ContactUsComponent } from './contact-us/contact-us.component';
export { ReportProblemComponent } from './report-problem/report-problem.component';
