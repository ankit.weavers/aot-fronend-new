import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material';
import { MaterialModule } from 'src/app/modules/material.module';

import { EmployerStaticRoutingModule } from './employer-static-routing.module';
import {
  AboutUsComponent,
  FaqComponent,
  PrivacyPolicyComponent,
  TermsAndConditionsComponent,
  ContactUsComponent,
  ReportProblemComponent
} from './';

@NgModule({
  declarations: [
    AboutUsComponent,
    FaqComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    ContactUsComponent,
    ReportProblemComponent
  ],
  imports: [
    CommonModule,
    MatNativeDateModule,
    MaterialModule,
    EmployerStaticRoutingModule
  ]
})
export class EmployerStaticModule { }
