import { Component, OnInit } from '@angular/core';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'employer-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements OnInit {

  conditions;
  private onDestroyUnSubscribe = new Subject<void>();

  constructor(
    private globalActionsService: GlobalActionsService,
  ) { }

  ngOnInit() {
    this.getTermsAndConditions();
  }

  getTermsAndConditions() {
    this.globalActionsService.getTermsConditions({ userType: JSON.parse(localStorage.getItem('currentUser')).userType })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(typeof result === "object" && (result !== null) && (!result['isError'] )){
            if(result['details'] !== null){
              this.conditions =  result['details'].termsCondition[0].conditionDetails;
            }
          }
        });
    }
}
