import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AboutUsComponent,
  FaqComponent,
  PrivacyPolicyComponent,
  TermsAndConditionsComponent,
  ContactUsComponent,
  ReportProblemComponent
} from './';
const routes: Routes = [
  {
    path: 'about-us',
    component: AboutUsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'About Us'
    }
  },
  {
    path: 'faq',
    component: FaqComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'FAQ'
    }
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Privacy Policy'
    }
  },
  {
    path: 'terms-and-conditions',
    component: TermsAndConditionsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Terms And Conditions'
    }
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Contact Us'
    }
  },
  {
    path: 'report-problem',
    component: ReportProblemComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Report Problem'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerStaticRoutingModule { }
