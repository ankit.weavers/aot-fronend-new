import { Component, OnInit } from '@angular/core';
import { SelectedFileService ,EmployerJobService} from '../../../services';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
@Component({
  selector: 'employer-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent implements OnInit {
  allPaymentStatus = [];
  public searchGroupForm: FormGroup;
  searchHistoryList = [];  
  private onDestroyUnSubscribe = new Subject<void>();
  pageNo;
  perPage;
  total;
  totalNoOfPages:number =6;
  dataFetching = false;
  constructor(
    public employerJobService: EmployerJobService,
    private fb: FormBuilder,
    private employerjobservice: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router,
    private sfs: SelectedFileService,
    private mapsAPILoader: MapsAPILoader,      
  ) {
    this.searchGroupForm = this.fb.group({
      searchedItem: '',
    });        
   }

  ngOnInit() {
    var an = this;
    //call function to fetch payment status
    //this.dataFetching = false;
    an.getAllPaymentStatus();
    an.sfs.searchStringStatus.subscribe(response => {
      if (response && response.string) {
        an.getPaymentStatusSearchedList(response.string.trim());
      }
      else {
        an.mapsAPILoader.load().then(() => {
          an.getAllPaymentStatus();
        });
      }
    })      
  }

  //  get all payment status
  getAllPaymentStatus(){
    let paymentPayload={
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      pageno: this.pageNo,
      perpage: this.perPage
    }
  
    this.employerjobservice.getAllEmployedCandidateDetails(paymentPayload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if (result && !result['isError'] && result['details']) {

          this.allPaymentStatus = result['details'].results;
          let obj = result['details'];
          if(Object.entries(obj).length !== 0){
            this.total = obj.total ? obj.total : 0;
            // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
            this.dataFetching = false;
          }          
        }else {
          this.dataFetching = false;
          this.snackBar.open('No data found! Please try again', 'Got it!', {
           
          });
        }
      },
      error => {
        this.dataFetching = false;
        this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
         
        });
      }
    );
  }
  //navigate to job details page
  openJobDetails(jobId, roleId) {
    let jobDetails = {
      jobId: jobId,
      roleId: roleId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(jobDetails));
    this.router.navigate(['/employer/job-details'])
  }
    // search job by item searched
    searchJobByItem() {
      // if(this.searchGroupForm.value.searchedItem){
        this.sfs._setSearchStatus({string: this.searchGroupForm.value.searchedItem})
      // }
    }
  
    //get jobs according o the searched item
    getPaymentStatusSearchedList(searchItem) {
      this.employerJobService.searchEmployedCandidiateByNAme(
        {
          employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
          searchText: searchItem
        }
      ) 
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && result['details']) {
              this.allPaymentStatus = result['details'].results;
            }
            else {
              this.snackBar.open('No Results Found', 'Got it!', {
              
              });
              this.sfs._setSearchStatus('');
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    }
    getPaginatedData(event) {
      this.pageNo = event.page;
      this.dataFetching = true;
      this.allPaymentStatus = [];
      this.getAllPaymentStatus();
    } 
}
