import { Component, OnInit, TemplateRef } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { EmployerJobService } from '../../../services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'employer-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})

export class JobDetailsComponent implements OnInit {

  jobId;
  roleId;
  private onDestroyUnSubscribe = new Subject<void>();
  jobDetailsList;
  modalRef: BsModalRef;
  optionSelectedForCandidate = "";
  selectedCandidateId;
  candidateName;
  candidateProfilePic;
  checkVariable:any;
  showRole:boolean = false;
  showArraow:boolean = false;
  showapplies:boolean = false;
  showArrowApllied :boolean = false;

  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    public employerJobService: EmployerJobService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    let jobDetailsData = JSON.parse(localStorage.getItem('JobDetailsById'));
    this.checkVariable = localStorage.getItem('removeButton')
    this.jobId = jobDetailsData.jobId;
    this.roleId = jobDetailsData.roleId;
    this.getJobDetailsById();
  }

  // get JobPosted List
  getJobDetailsById() {
    let requiredData = {
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      jobId: this.jobId,
      roleId: this.roleId
    }
    this.employerJobService.getJobDetailsById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobDetailsList = result['details'];

            console.log("jobDetailsList", this.jobDetailsList)
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
     
          });
        }
      );
  }

  // DOWN ARROW //
  showMoreRole() {
    this.showRole = true;
    this.showArraow = true;
  }

  // UP ARROW //
  showLessRole() {
    this.showRole = false;
    this.showArraow = false;
  }

    // DOWN ARROW //
    showapplidMoreRole() {
      this.showapplies = true;
      this.showArrowApllied = true;
    }
  
    // UP ARROW //
    showappliedLessRole() {
      this.showapplies = false;
      this.showArrowApllied = false;
    }

  cancelJOB(candidateId) {
    let requiredData = {
      candidateId: candidateId,
      jobId: this.jobId,
      roleId: this.roleId,
      today:moment(new Date()).format('MM/DD/YYYY'),
      presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
      localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    this.employerJobService._cancelJOB(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
           this.getJobDetailsById()
           this.snackBar.open('You have Removeed this Candidate from job', 'Got it!', {
           
          });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
     
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
     
          });
        }
      );
  }

  //open candidate job profile and set in local storage
  viewCandidateProfile(candidateId) {
    let JobDetailsById = {
      jobId: this.jobId,
      roleId: this.roleId,
      candidateId: candidateId
    }
    localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/candidate/details'])
  }

  //open modal
  openOptionModal(template: TemplateRef<any>, candidateId, candidateName, candidateProfilePic) {
    this.selectedCandidateId = candidateId;
    this.candidateName = candidateName;
    this.candidateProfilePic = candidateProfilePic
    this.modalRef = this.modalService.show(template, { backdrop: 'static', keyboard: false });
  }

  //get selected Option
  selectOtion(selectedOption) {
    this.optionSelectedForCandidate = selectedOption;
  }

  //change job activity for candidate
  candidateAction() {

    if (this.optionSelectedForCandidate === "message") {
      this.modalRef.hide();
      let JobDetailsById = {
        jobId: this.jobId,
        roleId: this.roleId,
        candidateId: this.selectedCandidateId,
        candidateName: this.candidateName,
        candidateProfilePic: this.candidateProfilePic
      }
      localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
      this.router.navigate(['/employer/profile/messages']);
    }
    else if (this.optionSelectedForCandidate === "appointment") {
      this.modalRef.hide();
      let JobDetailsById = {
        jobId: this.jobId,
        roleId: this.roleId,
        candidateId: this.selectedCandidateId,
        candidateName: this.candidateName,
        candidateProfilePic: this.candidateProfilePic
      }
      localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
      this.router.navigate(['/employer/appointments']);
    }
    else if (this.optionSelectedForCandidate === "worklog") {
      this.modalRef.hide();
      let JobDetailsById = {
        jobId: this.jobId,
        roleId: this.roleId,
        candidateId: this.selectedCandidateId,
        candidateName: this.candidateName,
        candidateProfilePic: this.candidateProfilePic
      }
      localStorage.setItem('JobDetailsById', JSON.stringify(JobDetailsById));
      this.router.navigate(['/employer/worklog']);
    }
    else {
      this.snackBar.open('Please Select An Option !!', 'Got it!', {
      
      });
    }
  }


}
