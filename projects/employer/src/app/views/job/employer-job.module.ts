import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material';
import { MaterialModule } from 'src/app/modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployerJobRoutingModule } from './employer-job-routing.module';
import { SharedModule } from 'src/app/modules/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgmCoreModule } from '@agm/core';


import {
  JobPostComponent,
  DetailsComponent,
  LocationComponent,
  SkillsComponent,
  DateTimeComponent,
  UploadFileComponent,
  PaymentBreakdownComponent,
  JobDetailsComponent,
  JobEditComponent,
  JobSummaryComponent,
  PaymentStatusComponent,
  JobTemplatesComponent
} from './';

@NgModule({
  declarations: [
    JobPostComponent,
    DetailsComponent,
    LocationComponent,
    SkillsComponent,
    DateTimeComponent,
    UploadFileComponent,
    PaymentBreakdownComponent,
    JobDetailsComponent,
    JobEditComponent,
    JobSummaryComponent,
    PaymentStatusComponent,
    JobTemplatesComponent
  ],
  imports: [
    CommonModule,
    MatNativeDateModule,
    MaterialModule,
    EmployerJobRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgSelectModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjOIcLbaHp8ixAIs1qlWkZIAu2vHb0uGk',
      libraries: ['places']
    })

  ]
})
export class EmployerJobModule { }
