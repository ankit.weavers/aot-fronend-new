import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  JobPostComponent,
  DetailsComponent,
  LocationComponent,
  SkillsComponent,
  DateTimeComponent,
  UploadFileComponent,
  PaymentBreakdownComponent,
  JobDetailsComponent,
  JobEditComponent,
  JobSummaryComponent,
  PaymentStatusComponent,
  JobTemplatesComponent
} from './';
import { PendingChangesGuard } from './can-deactivate-guard.guard';


const routes: Routes = [
  {
    path: 'post',
    component: JobPostComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Post a Job'
    },
  },
  {
    path: 'edit',
    component: JobPostComponent,
    // canDeactivate: [PendingChangesGuard],
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Edit'
    },
  },
  {
    path: ':type/details',
    component: DetailsComponent,
    canDeactivate: [PendingChangesGuard],
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Add Job Details'
    }
  },
  {
    path: ':type/location',
    component: LocationComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Add Job Location'
    }
  },
  {
    path: ':type/skills',
    component: SkillsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Skills Required'
    }
  },
  {
    path: ':type/time',
    component: DateTimeComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Set Time'
    }
  },
  {
    path: ':type/upload-file',
    component: UploadFileComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Upload File'
    }
  },
  {
    path: ':type/payment-breakdown',
    component: PaymentBreakdownComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: false,
      isEditable: [],
      title: 'Payment Breakdown'
    }
  },

  {
    path: 'job-details',
    component: JobDetailsComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Details'
    }
  },
  {
    path: 'job-edit',
    component: JobEditComponent,
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Edit'
    }
  },
  {
    path: 'job-summary',
    component: JobSummaryComponent,
    canDeactivate: [PendingChangesGuard],
    data: {
      hasBackBtn: true,
      hasHamburger: false,
      isEditable: [],
      title: 'Job Summary'
    }
  },
  {
    path: 'payment-status',
    component: PaymentStatusComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Payment Status'
    }
  },
  {
    path: 'job-templates',
    component: JobTemplatesComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Job Templates'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerJobRoutingModule { }
