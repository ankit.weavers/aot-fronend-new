import { Component, OnInit } from '@angular/core';
import { EmployerJobService } from '../../../services';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'employer-job-templates',
  templateUrl: './job-templates.component.html',
  styleUrls: ['./job-templates.component.scss']
})
export class JobTemplatesComponent implements OnInit {

  jobId;
  roleId;
  private onDestroyUnSubscribe = new Subject<void>();
  jobTemplatesList;
  jobTemplateDetails;
  savedDateList = [];
  skillss=[];

  constructor(
    private employerSetGetService: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router,
    public employerJobService: EmployerJobService,

  ) { }
  ngOnInit() {
    this.getAllJobTemplates();
  }

  // get Job Templates List
  getAllJobTemplates() {

    this.employerJobService.getAllJobTemplates({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {

          console.log("result templates", result)
          if (result && !result['isError']) {
            this.jobTemplatesList = result['details'];
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
       
          });
        }
      );

  }
  // get Job Template Details
  getJobTemplateDetails(templateId) {

    this.employerJobService.getTemplateDetailsById({employerId: JSON.parse(localStorage.getItem('currentUser')).employerId, templateId:templateId  })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobTemplateDetails = result['details'];
            console.log("dfjs",this.jobTemplateDetails.skills[0].skillDetails)
            for(let i=0 ;i<this.jobTemplateDetails.skills[0].skillDetails.length;i++){
              console.log('++++++++++++++++++++',this.jobTemplateDetails.skills[0].skillDetails[i].skillId);
              this.skillss.push({ id: this.jobTemplateDetails.skills[0].skillDetails[i].skillId,name: this.jobTemplateDetails.skills[0].skillDetails[i].skillName });
            }
            console.log("dsadk",this.skillss);


            let jobDetailsObject = {
              industry: this.jobTemplateDetails.industry,
              jobRole: this.jobTemplateDetails.rolewiseSkills,
              jobType: this.jobTemplateDetails.jobType,
              payType: this.jobTemplateDetails.payType,
              pay:     this.jobTemplateDetails.pay,
              no_of_stuffs: this.jobTemplateDetails.noOfStuff,
              locationTrackingRadius: this.jobTemplateDetails.locationTrackingRadius,
            }


            this.employerSetGetService.setJobDetails(jobDetailsObject);

            /************************* 2.location variables **************************/
            let jobLocation = {
              location: {
                type: "Point",
                coordinates: [
                  this.jobTemplateDetails.location.coordinates[0],
                  this.jobTemplateDetails.location.coordinates[1]
                ],
              },
              distance:  this.jobTemplateDetails.distance,
              locationName: this.jobTemplateDetails.locationName,
            }

            this.employerSetGetService.setJobLocation(jobLocation);

            //**************************** 3. skill variables ****************************/

        
            let skillSet = [];
            let roleWiseActions ={};
            for (var i = 0; i <  this.jobTemplateDetails.rolewiseSkills.length; i++) {
              for (var k = 0; k <  this.jobTemplateDetails.rolewiseSkills[i].skillIds.length; k++) {
                  skillSet.push( this.jobTemplateDetails.rolewiseSkills[i].skillIds[k] );
              }
              let jobRoleId = this.jobTemplateDetails.rolewiseSkills[i].jobRoleId;
              let jobRoleName =  this.jobTemplateDetails.rolewiseSkills[i].jobRoleName;
              let roleWiseAction = {
                [jobRoleId]: {
                  roleId: jobRoleId,
                  roleName:  jobRoleName,
                  skills: this.skillss
                }
              };  
              Object.assign(roleWiseActions, roleWiseAction)
        
            }
            let setSkill = {
              roleWiseAction: roleWiseActions,
              description:  this.jobTemplateDetails.description
            };
         
           this.employerSetGetService.setSkillDetails(setSkill);

            // /************************** 4. date time variables ***************************/
            
              this.formatDate(this.jobTemplateDetails.setTime);
              // let getDateTime =localStorage.getItem('dateTime');
              // console.log("time",getDateTime);
              this.employerSetGetService.setDateTimeDetails({ dateTime:  this.savedDateList });

            // /*********************** 5. uploaded files variables ************************/

            // this.employerSetGetService.setFileDetails({ uploadedFile:  this.jobTemplateDetails.uploadFile, type: "url" });

            // /*********************** 6. payment files variables ************************/

            this.employerSetGetService.setPayment({ payment:  this.jobTemplateDetails.payment });


             this.router.navigate(['/employer/job/post']);
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
            
          });
        }
      );

  }

  deleteTemp(templateId,i) {

    this.jobTemplatesList.splice(i,1)
    
    this.employerJobService.deleteTemp({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,templateId:templateId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if(result && !result['isError']) {
            this.getAllJobTemplates();
            this.snackBar.open(result['message'],'Got it!',{

            })
          }
        })

  }


    //process or format date and time
    formatDate(dateArray) {
      this.savedDateList= dateArray;
      for (let i = 0; i < dateArray.length; i++) {
        if (typeof dateArray[i].startDate === "number") {
          this.savedDateList[i].startDate = new Date(dateArray[i].startDate);
        }
        if (typeof dateArray[i].endDate === "number") {
          this.savedDateList[i].endDate = new Date(dateArray[i].endDate);
        }
        if (typeof dateArray[i].startTime === "string") {
          this.savedDateList[i].startTime = dateArray[i].startTime
        }
        if (typeof dateArray[i].endTime === "string") {
          this.savedDateList[i].endTime =dateArray[i].endTime
        }
      }
      console.log('savedDateList', this.savedDateList);
    }
}
