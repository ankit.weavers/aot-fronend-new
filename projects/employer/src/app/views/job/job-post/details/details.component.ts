import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { EmployerJobService } from '../../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Erros } from 'src/app/models';
import { MatSliderChange } from '@angular/material';

@Component({
  selector: 'employer-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})



export class DetailsComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  industryList = [];
  industryListssssssssssss = [];

  jobRoleList = [];
  jobTypeList = [{ id: "FULLTIME", name: "Full Time" }, { id: "PARTTIME", name: "Part Time" }, { id: "HALFDAY", name: "Half Day" }, { id: "NIGHTSHIFT", name: "Night Shift" }];
  payTypeList = "Hourly";
  industryName: "";
  public errors: any = Erros;
  jobUrlType;
  public addJobDetailsForm: FormGroup;
  isApiEdit;
  max: number = 1000;
  min: number = 0;
  // showTicks = false;
  step: number = 1;
  jobRoleValueArray = [];
  thumbLabel: boolean = true;
  skillRemove: boolean = false
  distanceValue: number = 15;
  disabled: any;
  jobRoleArray = [];
  skillsRemoveBollean: boolean = false;
  skillsList = [];
  selectedSkills = [];
  jobRoleIds = [];
  selectedRoleWiseSkill = {};
  okay = localStorage.getItem('JobSkills');
  skillArray = [];
  guest = [];
  arrayValue = [];
  getJobdetailsInsial:any;
  getSkillValueInsial:any;
  showRole:boolean = false;
  showArraow:boolean = false;
  // formatLabel(value: number) {
  //   if (value >= 1000) {
  //     return Math.round(value * 1000) + 'm';
  //   }

  //   return value;

  // }

  constructor(
    private fb: FormBuilder,
    private employerJobService: EmployerJobService,
    private router: Router,
    public snackBar: MatSnackBar,
    public globalactionservice: GlobalActionsService,
    private activatedRouter: ActivatedRoute,
  ) {

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];


    if (this.jobUrlType === "edit") {
      this.isApiEdit = true;
    } else if (this.jobUrlType === "post") {
      this.isApiEdit = false;
    }

    let getValue = this.employerJobService.getJobDetails;

    if (getValue) {
      // this.guest = [...getValue.jobRole];
      this.getJobRoleList(getValue.industry);
      this.addJobDetailsForm = this.fb.group({
        industry: [{ value: getValue.industry, disabled: this.isApiEdit }, Validators.required],
        jobRole: [{ value: '', disabled: this.isApiEdit }, Validators.required],
        jobType: [getValue.jobType, Validators.required],
        // payType: [getValue.payType, Validators.required],
        pay: [getValue.pay, [Validators.required, Validators.pattern(/^[1-9]\d*(\.\d+)?$/)]],//validators.min get from api
        no_of_stuffs: [getValue.no_of_stuffs, [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        skillDescription: ['', Validators.required],
        skillDetails: new FormArray([])
        // ni:[getValue.ni,[Validators.required]],
        // vat:[getValue.vat,[Validators.required, Validators.pattern(/^[0-9]*$/)]],
        // fee:[getValue.fee,[Validators.required, Validators.pattern(/^[0-9]*$/)]]
      });
      this.distanceValue = getValue.locationTrackingRadius
      for (let i = 0; i < getValue.jobRole.length; i++) {
        this.jobRoleValueArray.push(getValue.jobRole[i].jobRoleId)
      }
      this.addJobDetailsForm.patchValue({
        jobRole: this.jobRoleValueArray
      })
      // this.addJobDetailsForm.get('jobRole').disable();
      // this.getJobRoleList(getValue.industry);

    }
    else {
      this.addJobDetailsForm = this.fb.group({
        industry: ['', Validators.required],
        jobRole: ['', Validators.required],
        jobType: ['', Validators.required],
        // payType: ['', Validators.required],
        pay: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
        no_of_stuffs: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        skillDescription: ['', Validators.required],
        skillDetails: new FormArray([])
        // ni:['',[Validators.required]],
        // vat:['',[Validators.required, Validators.pattern(/^[0-9]*$/)]],
        // fee:['',[Validators.required, Validators.pattern(/^[0-9]*$/)]]
      });
    }
  }

  ngOnInit() {
    this.getJobdetailsInsial = this.employerJobService.getJobDetails;
    this.getSkillValueInsial = this.employerJobService.getSkillDetails;
    this.getIndustryList();
  }

  canDeactivate() {
    if(this.skillsRemoveBollean) {
      // let r = confirm('WARNING: You have unsaved changes. Press Cancel to go back and save these changes, or OK to lose these changes.')
      let getJobdetails = this.employerJobService.getJobDetails;
      let getSkillValue = this.employerJobService.getSkillDetails;
      let r
      if(getJobdetails && getSkillValue) {
        let jobDetailsObject = {
          industry: this.getJobdetailsInsial.industry,
          industryName: this.getJobdetailsInsial.industryName,
          jobRole: this.getJobdetailsInsial.jobRole,
          jobType:this.getJobdetailsInsial.jobType,
          // payType: this.addJobDetailsForm.value.payType,
          payType: "Hourly",
    
          pay: this.getJobdetailsInsial.pay,
          no_of_stuffs: this.getJobdetailsInsial.no_of_stuffs,
          locationTrackingRadius: this.distanceValue,
          // ni:this.addJobDetailsForm.value.ni,
          // vat:this.addJobDetailsForm.value.vat,
          // fee:this.addJobDetailsForm.value.fee
    
        }
    
    
        // if (this.skillRemove) {
        //   localStorage.removeItem('JobSkills');
        // }
        this.employerJobService.setJobDetails(jobDetailsObject);

        let skill = {
          //  roleWiseAction: Object.values(this.selectedRoleWiseSkill),
          roleWiseAction: this.getSkillValueInsial.roleWiseAction,
          description: this.getSkillValueInsial.description
        }
        this.employerJobService.setSkillDetails(skill);
      return  true;

      }
      else {
        return false;
      }

    }
    else {
      return true;
    }
  }
// DOWN ARROW //
  showMoreRole() {
    this.showRole = true;
    this.showArraow = true;
  }

  // UP ARROW //
  showLessRole() {
    this.showRole = false;
    this.showArraow = false;
  }

  // getCondition(j) {
  //   if(this.skillsList.length > 5) {
  //     if(j<5) {

  //     }
  //   }
  //   else {

  //   }
  // }

  // get Industry List
  getIndustryList() {
    //get industries
    this.globalactionservice.fetchIndustry({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {

            this.industryList = result['details'].response.industries;

          } else {
            this.snackBar.open('Please select your industry in profile section before post a job', 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }

  //get Job Role List
  getJobRoleList(industryId) {

    this.globalactionservice.getRole({ industryId: [industryId] })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.jobRoleList = result['details'];
            let getValue = this.employerJobService.getJobDetails;
            if (getValue) {
              if (!this.skillRemove) {
                this.jobRoleSelect(this.jobRoleValueArray);

              }

            }
            // if (typeof this.addJobDetailsForm.value.jobRole === "string") {
            //   for (let index = 0; index < this.jobRoleList.length; index++) {
            //     const element = this.jobRoleList[index];
            //     if (element.jobRoleId === this.addJobDetailsForm.value.jobRole) {
            //       this.addJobDetailsForm.patchValue({
            //         jobRole: [element],
            //       });
            //     }
            //   }
            // }

          } else {
            this.snackBar.open(result['message'], 'Got it!', {

            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

          });
        }
      );
  }



  // select job role //
  jobRoleSelect(event) {
    // if(this.skillRemove) {
    //   localStorage.removeItem('JobSkills');
    // }
    this.skillArray = [...event];

    let getJobdetails = this.employerJobService.getJobDetails;
    let getSkillValue = this.employerJobService.getSkillDetails;
    if (getJobdetails && getSkillValue) {
      this.skillsRemoveBollean = true;
    }
    this.guest = this.jobRoleList.filter(e => {
      return this.skillArray.some(item => item === e.jobRoleId);
    });
    if (this.guest) {
      //get skill details by Job Role Id
      this.globalactionservice.getJobSkillById({ jobRole: event })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.skillsList = result['details'];

              // if(this.skillsList.length>5) {
              //   this.showRole = true;
              // }

              //check if value is stored in localstorage
              let getValue = this.employerJobService.getSkillDetails;
              if (getValue && !this.skillRemove) {
                this.arrayValue = [];
                Object.keys(this.skillsList).map(
                  (key) => {
                    if (getValue.roleWiseAction[this.skillsList[key].jobRoleId]) {
                      if (getValue.roleWiseAction[this.skillsList[key].jobRoleId].roleId === this.skillsList[key].jobRoleId) {
                        this.arrayValue.push({
                          [this.skillsList[key].jobRoleId]: getValue.roleWiseAction[this.skillsList[key].jobRoleId]
                        })
  
  
                      }
                    }
                
                  });
                // if (this.isApiEdit) {
                //   this.skillDescription = new FormControl(getValue.description);
                // }
                var total = this.arrayValue.reduce(
                  (accumulator, currentValue) => {
                    return { ...accumulator, ...currentValue }

                  }, {})
                let skill = {
                  //  roleWiseAction: Object.values(this.selectedRoleWiseSkill),
                  roleWiseAction: total,
                  description: getValue.description
                }

                this.employerJobService.setSkillDetails(skill);

                 getValue = this.employerJobService.getSkillDetails;
                this.addJobDetailsForm.patchValue({
                  skillDescription: getValue.description
                })

                this.selectedRoleWiseSkill = getValue.roleWiseAction;

                Object.keys(this.skillsList).map(
                  (key) => {
                    if (getValue.roleWiseAction[this.skillsList[key].jobRoleId] && this.skillsList[key].details) {
                      Object.keys(this.skillsList[key].details).map(
                        (skillRow) => {
                          for (let i = 0; i < getValue.roleWiseAction[this.skillsList[key].jobRoleId].skills.length; i++) {
                            if (this.skillsList[key].details[skillRow]['skillId'] === (getValue.roleWiseAction[this.skillsList[key].jobRoleId].skills[i].id)) {
                              this.skillsList[key].details[skillRow].checked = "checked";
                              const control = <FormArray>this.addJobDetailsForm.controls['skillDetails'];
                              control.push(new FormControl(this.skillsList[key]));

                              if (this.isApiEdit) {
                                let skillId = this.skillsList[key].details[skillRow]['skillId'];
                                let jobRoleId = this.skillsList[key].jobRoleId;
                                let roleName = this.skillsList[key].jobRoleName;
                                let skillName = this.skillsList[key].details[skillRow]['skillName'];
                              }
                            }

                          }
                          // if(!this.skillsRemoveBollean) {
                          //   localStorage.removeItem('JobSkills');
                          // }
                        });
                    }
                  });
              }
            } else {
              this.snackBar.open(result['message'], 'Got it!', {

              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {

            });
          }
        )
    }
    else {
      this.snackBar.open('Please select job roles first', 'Got it!', {

      });
    }
  }




  // get role details //
  // getRoleName(item) {

  //   this.jobRoleArray.push(item);
  //   // console.log('this.jobRole',this.jobRoleArray);
  //   return item.jobRoleName
  // }

  //slider event
  onInputChange(event: MatSliderChange) {
    // console.log("event",event);
    this.distanceValue = event.value;

  }


  //industry selected
  selectJobIndustry(industryId) {
    this.jobRoleList = [];
    this.skillsList = [];
    this.selectedRoleWiseSkill = {};
    this.addJobDetailsForm.controls.jobRole.setValue('')  //reseting job role on changing job industry
    this.skillRemove = true;
    // localStorage.setItem('skillRemove',skillRemove);
    this.getJobRoleList(industryId)
  }

  onChecklistChange(checked, skillId, jobRoleId, roleName, skillName) {
    let roleWiseSkill = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId] : {};
    let skillsList = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId].skills : [];
    // let skillsList = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId].skills.id : [];

    if (checked) {
      // const control = <FormArray>this.addSkillsForm.controls['skillDetails'];
      // control.push(new FormControl(skillId));
      skillsList.push({ id: skillId, name: skillName });
      localStorage.setItem('checkedSkilles', JSON.stringify(skillsList));
    }
    else {
      //  skillsList = skillsList.filter(item => item !== skillId)
      skillsList = skillsList.filter(item => item.id !== skillId)
    }

    if (skillsList.length > 0) {
      roleWiseSkill['skills'] = skillsList;
      roleWiseSkill['roleId'] = jobRoleId;
      roleWiseSkill['roleName'] = roleName;
      this.selectedRoleWiseSkill[jobRoleId] = roleWiseSkill;
    }
    else {
      delete this.selectedRoleWiseSkill[jobRoleId];
    }
  }
  saveAddJobDetails() {

    Object.keys(this.industryList).map(
      (key) => {
        if (this.industryList[key].industryId === this.addJobDetailsForm.value.industry) {
          this.industryName = this.industryList[key].industryName;
        }
      }
    );

    let industry;
    let industryName;
    let jobRole;

    if (this.isApiEdit) {
      industry = this.employerJobService.getJobDetails.industry;
      industryName = this.employerJobService.getJobDetails.industryName;
      jobRole = this.employerJobService.getJobDetails.jobRole;
    } else {
      industry = this.addJobDetailsForm.value.industry;
      industryName = this.addJobDetailsForm.value.industryName;
      jobRole = this.guest;
    }

    let jobDetailsObject = {
      industry: industry,
      industryName: this.industryName,
      jobRole: jobRole,
      jobType: this.addJobDetailsForm.value.jobType,
      // payType: this.addJobDetailsForm.value.payType,
      payType: "Hourly",

      pay: this.addJobDetailsForm.value.pay,
      no_of_stuffs: this.addJobDetailsForm.value.no_of_stuffs,
      locationTrackingRadius: this.distanceValue,
      // ni:this.addJobDetailsForm.value.ni,
      // vat:this.addJobDetailsForm.value.vat,
      // fee:this.addJobDetailsForm.value.fee

    }

    // if (this.skillRemove) {
    //   localStorage.removeItem('JobSkills');
    // }
    this.employerJobService.setJobDetails(jobDetailsObject);
    this.saveSkills();
    let getValue = this.employerJobService.getJobDetails;
    // if (getValue) {
    //   this.snackBar.open('JOB DETAILS SAVED', 'Got it!', {

    //   });
    //   //show alert and navigate to job-post component
    //   this.router.navigate([`/employer/job/${this.jobUrlType}`]);
    // }
  }

  //submit skills
  saveSkills() {
    let skillsList = JSON.parse(localStorage.getItem('checkedSkilles'))


    if (Object.keys(this.selectedRoleWiseSkill).length > 0 && this.addJobDetailsForm.value.skillDescription) {
      let skill = {
        //  roleWiseAction: Object.values(this.selectedRoleWiseSkill),
        roleWiseAction: this.selectedRoleWiseSkill,
        description: this.addJobDetailsForm.value.skillDescription
      }
      this.employerJobService.setSkillDetails(skill);
      let getValue = this.employerJobService.getSkillDetails;

      if (getValue) {
        this.snackBar.open('Job Detailks save', 'Got it!', {

        });
        this.skillsRemoveBollean = false;
        //show alert and navigate to job-post component
        this.router.navigate([`/employer/job/${this.jobUrlType}`]);
      }
      else {
        this.snackBar.open('PLEASE SELECT A SKILL', 'Got it!', {

        });
      }
    }
    else {
      this.snackBar.open('Please select skills and enter job description', 'Got it!', {

      });
    }

  }

}
