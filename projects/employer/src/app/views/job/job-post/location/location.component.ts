import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { MatSliderChange, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployerJobService } from '../../../../services';
import { Erros } from 'src/app/models';

// import {} from 'googlemaps';
declare var google: any;
// import { google } from '@agm/core/services/google-maps-types';

@Component({
  selector: 'employer-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  // lat: number = 51.678418;
  // lng: number = 7.809007;
  lng: number;
  lat: number;
  lngs: number;
  lats: number;
  public searchControl: FormControl;
  public zoom: number;
  address: string;
  private geoCoder;
  @ViewChild("search")
  public searchElementRef: ElementRef;
  // autoTicks = false;
  disabled = false;
  max = 30;
  min = 0;
  // showTicks = false;
  step = 1;
  thumbLabel = true;
  distanceValue =0;
  disValue;
  locationName: "";
  public errors: any = Erros;
  jobUrlType;
  geocoder;
  isApiEdit;
  map:FormGroup
  editValue : boolean = false;
  showLocation: boolean = true;
  employerType: any;
  circleColor;
  placess = [];
  getValue;
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private employerJobService: EmployerJobService,
    private router: Router,
    public snackBar: MatSnackBar,
    private activatedRouter: ActivatedRoute,
    public formBuilder : FormBuilder
  ) {

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];

    console.log("location page jobUrlType", this.jobUrlType);

    if (this.jobUrlType === "edit") {
      this.isApiEdit = true;
    } else if (this.jobUrlType === "post") {
      this.isApiEdit = false;
    }

    this.getValue = this.employerJobService.getJobLocation;

    if (this.getValue) {
      //set latitude, longitude and zoom
      if (this.getValue.location.coordinates) {
        this.lng = this.getValue.location.coordinates[0];
        this.lat = this.getValue.location.coordinates[1];
        this.zoom = 12;

      }
      this.distanceValue = (this.getValue.distance)/1609.34;
      this.disValue = this.distanceValue * 1609.34;

    // this.searchControl = new FormControl('getValue.locationName'); 
    // this.map.patchValue({
    //   mapValue:getValue.locationName
    // });                                                                                                                               
      // this.setCurrentPosition();
    }
  }

  ngOnInit() {
    let getValue = this.employerJobService.getJobLocation;
if(getValue){
  this.editValue= true;
  this.searchControl = new FormControl(getValue.locationName, Validators.required);

} else{
  this.searchControl = new FormControl('', Validators.required);


} 
    // set google maps defaults
    this.zoom = 12;
    // create search FormControl
    // set current position
    if(!this.getValue){
    this.setCurrentPosition();

    }

    //==============================================
    // this code is written for hiding the location
    // this.employerType = JSON.parse(localStorage.getItem('currentUser')).employerType ? JSON.parse(localStorage.getItem('currentUser')).employerType : '';
   
    // if (this.employerType == 'COMPANY') {
    //     this.showLocation = false;
    // } else {
    //     this.showLocation = true;
    // } 

    //===============================================

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      // const searchBox = new google.maps.places.SearchBox(this.searchElementRef.nativeElement,{

      // });
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {

      });
      // ,{
      //   center: {lat: this.lat, lng:this.lng},
      //   zoom: 13,
      //   mapTypeId: 'roadmap'
      // });)
      // var map =new google.maps(document.getElementById('map'))
      // var input = document.getElementById('pac-input');
      //   var searchBox = new google.maps.places.SearchBox(input);
      //   map.controls[google.maps.ControlPosition.TOP_LEFT].search(input);
     
      // map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.searchElementRef.nativeElement,{

      // });

      // map.addListener('bounds_changed', function() {
      //   searchBox.setBounds(map.getBounds());
      // });


  //     searchBox.addListener('places_changed', function() {

  // this.placess = searchBox.getPlaces();
  //   console.log("hjhj",this.placess[0].geometry.location.lat());
  //   //  searchBox.setBounds(place.getBounds());
 
  //   this.locationName = this.placess[0].formatted_address;
  //       this.editValue = false;
  //       // verify result
  //       if (this.placess[0].geometry === undefined || this.placess[0].geometry === null) {
  //         return;
  //       }
      
  //       // set latitude, longitude and zoom
  //       this.lat = this.placess[0].geometry.location.lat();
  //       this.lng = this.placess[0].geometry.location.lng();
  //       this.zoom = 12

   
  //     });
      autocomplete.addListener("place_changed", () => {
        console.log("kuhckd");
        this.ngZone.run(() => {
        console.log("sfgfd",autocomplete.getPlace());

          // get the place result
          // let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          const place: typeof google.maps.places.PlaceResult = autocomplete.getPlace();
          // const laces: typeof google.maps.places.PlaceResult = this.searchElementRef.nativeElement;
          // console.log("laces",laces);
          console.log("place",place);

          //  let place = autocomplete.getPlace();
          if (!place.place_id) {
            window.alert('Please select an option from the dropdown list.');
            return;
          }
          // this.locationName = place.name;
          this.locationName = place.formatted_address;

          this.editValue = false;
          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // set latitude, longitude and zoom
        
            this.lat = place.geometry.location.lat();
            this.lng = place.geometry.location.lng();
            this.zoom = 12;
          
        
        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log("position",position);
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;


        // this.mapsAPILoader.load().then(() =>{
        //   this.geocoder = new google.maps.Geocoder;
        //   var latlng = new google.maps.LatLng(this.lat, this.lng)
        //   let currentClassInstance = this;
        //   this.geocoder.geocode({'location': latlng}, function(results, status){
        //     console.log(results[0]);
        //     // if(status === "OK"){
      
        //       currentClassInstance.locationName = results[0].formatted_address;
      
        //     // }
        //   });
        // })
      });
    }
  }
  

  // formatLabel(value: number) {
  //   if (value == 0) {
  //     return value + 'AnyWherer';
  //   }

  //   return value;
  // }

  // // Function to call when the input is touched (when a slider is moved).
  // onTouched = () => {};
  onInputChange(event: MatSliderChange) {
    this.distanceValue = event.value;
    this.disValue = this.distanceValue * 1609.34;
  }

  saveLocationDetails() {
    let getValues = this.employerJobService.getJobLocation;
    
    if (!this.locationName && !getValues) {
      this.snackBar.open('location name is must','got it!',{

      });
      return false;
    }
    if (!this.searchControl.value && getValues){
      this.snackBar.open('location name is must','got it!',{

      });
      return false;
    }
    // setTimeout(()=>{
      let jobLocation
      if(getValues && this.editValue){
  
         jobLocation = {
          location: {
            type: "Point",
            coordinates: [this.lng, this.lat],
          },
          distance: this.distanceValue*1609.34,
          locationName: this.searchControl.value,
        }
      }else{
      jobLocation = {
        location: {
          type: "Point",
          coordinates: [this.lng, this.lat],
        },
        distance: this.distanceValue*1609.34,
        locationName: this.locationName,
      }
    }
      this.employerJobService.setJobLocation(jobLocation);
      let getValue = this.employerJobService.getJobLocation;
      if (getValue) {
        this.snackBar.open('JOB LOCATION SAVED', 'Got it!', {
         
        });
        //show alert and navigate to job-post component
        this.router.navigate([`/employer/job/${this.jobUrlType}`]);
      }
    // },2000)
 
  }

}
