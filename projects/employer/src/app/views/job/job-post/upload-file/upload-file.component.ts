import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { EmployerJobService } from '../../../../services';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectedFileService } from '../../../../services/selected-file.service';
import { GlobalActionsService } from 'src/app/services';


@Component({
  selector: 'employer-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  @ViewChild('imageDropZone') imageDropZone: ElementRef;
  @ViewChild('imageDropZoneFileUpload') imageDropZoneFileUpload: ElementRef;
  public imageFile: File;
  @Input() imageUrl: string;
  // @Output() fileSelected: EventEmitter<any> = new EventEmitter();
  jobUrlType;

  fileName;
  fileIndexForEdit = 'noIndex';
  userDetails: any;
  uploadFileList=[];
  constructor(
    private employerSetGetService: EmployerJobService,
    public router: Router,
    public snackBar: MatSnackBar,
    private activatedRouter: ActivatedRoute,
    private sfs: SelectedFileService,
    private globalActionsService: GlobalActionsService,
  ) {
    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];
  }

  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.imageUrl =  this.employerSetGetService.getFileDetails;
   }

  chooseImage(event) {
    this.imageDropZoneFileUpload.nativeElement.click();
  }
  dropHandler(event) {
    // Prevent default behavior (Prevent file from being opened)
    event.preventDefault();
    console.log("filesssssssa", event.preventDefault());

    if (event.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (let i = 0; i < event.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (event.dataTransfer.items[i].kind === 'file') {
          const file = event.dataTransfer.items[i].getAsFile();
          console.log("file",file);
          this.readFile(file);
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (let i = 0; i < event.dataTransfer.files.length; i++) {
        const file = event.dataTransfer.files[i].getAsFile();
        console.log("file",file);

        this.readFile(file);
      }
    }
    this.imageDropZone.nativeElement.classList.remove('file-dragged');
  }

  readFile(file) {
    console.log("file",file);

    if (
      file.size <= 26214400 // 1 MB = 1,048,576 B , limitation of 25mb
    ) {
      const reader = new FileReader();
      this.imageFile = file;
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.imageUrl = reader.result as string;
        this.fileName = file.name;
      };
    } else {
      this.snackBar.open('File size must be maximum 25mb .', 'OK', {
        
      });
    }
  }

  dragOverHandler(event) {
    event.preventDefault();
  }

  dragEnterHandler(event) {
    this.imageDropZone.nativeElement.classList.add('file-dragged');
  }
  dragLeaveHandler(event) {
    this.imageDropZone.nativeElement.classList.remove('file-dragged');
  }

  resetDropZone(event) {
    this.deleteFile();
    this.imageDropZoneFileUpload.nativeElement.value = '';
    this.imageFile = null;
    this.imageUrl = '';
    this.fileName = '';
    localStorage.removeItem('uploadedFile');
  }

  uploadFile(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      this.readFile(file);
    }
  }


  async saveFile() {

    if(this.imageFile){
      this.snackBar.open('please wait file is uploading', 'Got It!');
    
      await this.globalActionsService.uploadFile(this.imageFile, this.userDetails.userType, 'job-posts', 'job-posts')
      .then(
     
        (res) => {
          console.log("res",res);
          this.uploadFileList.push(res['Location']);
          this.employerSetGetService.setFileDetails(res['Location']);
          let getValue = this.employerSetGetService.getFileDetails;
          if (getValue) {
            this.snackBar.open('FILE SAVED', 'Got it!', {
             
            });
            this.router.navigate([`/employer/job/${this.jobUrlType}`]);
          }
    });
    }else{
      this.snackBar.open('OPPS! NO FILE SELECTED', 'Got it!', {
      
      });
    }
  }

  async deleteFile() {
    console.log('deleteFile');
    if(this.imageUrl){
      await this.globalActionsService.deleteFile(this.imageUrl)
      .then(
        (res) => {
    });
    }else{
      this.snackBar.open('OPPS! NO FILE SELECTED', 'Got it!', {
      
      });
    }
  }
}
