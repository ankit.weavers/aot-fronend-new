import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmployerJobService } from '../../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Erros } from 'src/app/models';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'employer-payment-breakdown',
  templateUrl: './payment-breakdown.component.html',
  styleUrls: ['./payment-breakdown.component.scss']
})
export class PaymentBreakdownComponent implements OnInit {
  private onDestroyUnSubscribe = new Subject<void>();

  public addPaymentBreakdownForm: FormGroup;
  public errors: any = Erros;
  ratePerHour:any;
  noOfStuff:any;
  totalPayment:any;
  totalHour:any;
  ni:any;
  fee:any;
  vat:any;
  formattedPaymentValues:any;
  totalamountsss:any;
  totalNi:any;
  totalVat:any;
  totalFee:any;
  totalCost:any;
  netAmount:any;
  
  jobUrlType;
  constructor(
    private fb: FormBuilder,
    private employerJobService: EmployerJobService,
    private router: Router,
    public snackBar: MatSnackBar,
    private activatedRouter: ActivatedRoute
  ) {

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];
    let getValue = this.employerJobService.getPayment;
    if (getValue) {
      this.addPaymentBreakdownForm = this.fb.group({
        ratePerHour: [getValue.payment.ratePerHour, Validators.required],
        noOfStuff: [getValue.payment.noOfStuff, Validators.required],
        totalHour: [getValue.payment.totalHour, Validators.required],
        ni: [getValue.payment.ni, Validators.required],
        vat: [getValue.payment.vat, [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        fee: [getValue.payment.fee, [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        totalPayment: [getValue.payment.totalPayment]
      });
    }
    else {
      this.addPaymentBreakdownForm = this.fb.group({
        ratePerHour: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        noOfStuff: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        totalHour: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        ni: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        vat: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        fee: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
        totalPayment: ['']
      });
    }
  }

  ngOnInit() {
    let getJobDetails = localStorage.getItem('JobDetails')
    if(getJobDetails){
      let jobDetailsObj = JSON.parse(getJobDetails);
      if(typeof jobDetailsObj == "object"){
        this.addPaymentBreakdownForm.get('ratePerHour').setValue(jobDetailsObj.pay);
        this.ratePerHour = jobDetailsObj.pay;
        this.addPaymentBreakdownForm.get('noOfStuff').setValue(jobDetailsObj.no_of_stuffs);
        this.noOfStuff = jobDetailsObj.no_of_stuffs;
        this.addPaymentBreakdownForm.get('totalPayment').setValue(jobDetailsObj.pay);
       
        // this.addPaymentBreakdownForm.get('ni').setValue(jobDetailsObj.ni);
        // this.addPaymentBreakdownForm.get('vat').setValue(jobDetailsObj.vat);
        // this.addPaymentBreakdownForm.get('fee').setValue(jobDetailsObj.fee);
        
      }
    }
    let getDateTime = localStorage.getItem('Datetime');
;
    if(getDateTime){
      let dateTimeObj = JSON.parse(getDateTime);
      if(typeof dateTimeObj == "object"){
        var totalHours = 0;
        var dates =  dateTimeObj.dateTime;
        for(var i = 0; i< dates.length;i++){
          totalHours+=+dates[i].totalHour;
        }
        this.addPaymentBreakdownForm.get('totalHour').setValue(totalHours);
        this.totalHour = totalHours

      }
    }
    this.getValue();
    
  }
  findTotal(){
  
  
   
  if(this.totalamountsss) {

    this.employerJobService.setPayment({ payment: this.formattedPaymentValues });

    this.snackBar.open('Job Payment saved', 'Got it!', {
     
    });
    //show alert and navigate to job-post component
  }

  this.router.navigate([`/employer/job/${this.jobUrlType}`]);

   
    
  }

  calulationAmount() {
    let totalAmount = (this.ratePerHour*this.totalHour);
    this.totalamountsss = totalAmount;
    let totalFee =(this.fee*totalAmount/100);
    let totalNI = (this.ni*totalAmount/100);
    console.log('totalAmount',totalAmount);
    let totalVat =  (this.vat * (totalAmount+this.ni+this.fee)/100);
    this.totalCost = Math.round(totalAmount).toFixed(2);
    this.totalFee = Math.round(totalFee).toFixed(2);
    this.totalNi = Math.round(totalNI).toFixed(2);
    this.totalVat = Math.round(totalVat).toFixed(2);
    // let totalRate = ratePerHour*totalHour;
    let total = Math.round(totalAmount+totalNI+totalFee+totalVat).toFixed(2);
   this.netAmount = total;
    this.addPaymentBreakdownForm.get('totalPayment').setValue(total);
    this.formattedPaymentValues = {
      ratePerHour:this.ratePerHour,
      noOfStuff:this.noOfStuff,
      totalHour:this.totalHour,
      vat:Math.round(totalVat).toFixed(2),
      ni: Math.round(totalNI).toFixed(2),
      fee:Math.round(totalFee).toFixed(2),
      totalPayment:total
    }
  }
  getValue(){
    let taxValue ={
      ni: "",
      vat:"",
      fee:""
    }
  this.employerJobService.getTaxValue(taxValue)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if(result && !result['isError'] && result['detail']){
          
          let obj = result['detail'];
  
          
          this.addPaymentBreakdownForm.get('ni').setValue(obj.ni);
          this.addPaymentBreakdownForm.get('vat').setValue(obj.vat);
          this.addPaymentBreakdownForm.get('fee').setValue(obj.fee);
          this.ni = obj.ni;
     
          this.vat = obj.vat;
          this.fee = obj.fee;
          this.calulationAmount();
        }
      }
    )
    
  }
  savePaymentBreakdownDetails() {

    //format values to numbers
    let formattedPaymentValues = {
      ratePerHour:Number(this.addPaymentBreakdownForm.value.ratePerHour),
      noOfStuff:Number(this.addPaymentBreakdownForm.value.noOfStuff),
      totalHour:Number(this.addPaymentBreakdownForm.value.totalHour),
      vat:Number(this.addPaymentBreakdownForm.value.vat),
      ni:Number(this.addPaymentBreakdownForm.value.ni),
      fee:Number(this.addPaymentBreakdownForm.value.fee),
      totalPayment:Number(this.addPaymentBreakdownForm.value.totalPayment)
    }

    this.employerJobService.setPayment({ payment: formattedPaymentValues });
    let getValue = this.employerJobService.getPayment;
    if (getValue) {
      this.snackBar.open('JOB PAYMENT SAVED', 'Got it!', {
      
      });
      //show alert and navigate to job-post component
      this.router.navigate([`/employer/job/${this.jobUrlType}`]);
    }
  }

}
