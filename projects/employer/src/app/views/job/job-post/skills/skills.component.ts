import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { GlobalActionsService } from 'src/app/services';
import { EmployerJobService } from '../../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Erros } from 'src/app/models';

@Component({
  selector: 'employer-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  skillsList = [];
  selectedSkills = [];
  skillDescription: FormControl;
  private onDestroyUnSubscribe = new Subject<void>();
  jobRoleIds = [];
  selectedRoleWiseSkill = {};
  addSkillsForm: FormGroup;
  public errors: any = Erros;
  jobUrlType;
  isApiEdit;
  okay = localStorage.getItem('JobSkills');


  // console.log("hmm",this.okay);


  constructor(
    private fb: FormBuilder,
    private employerJobService: EmployerJobService,
    private globalActionsService: GlobalActionsService,
    private router: Router,
    public snackBar: MatSnackBar,
    private activatedRouter: ActivatedRoute
  ) {
    this.skillDescription = new FormControl('', Validators.required);
    this.addSkillsForm = this.fb.group({
      skillDetails: new FormArray([])
    });
    //call all info fucntion for viewing skill details
    this.getInfoDetails();

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];

    if (this.jobUrlType === "edit") {
      this.isApiEdit = true;
    } else if (this.jobUrlType === "post") {
      this.isApiEdit = false;
    }
  }
  ngOnInit() {
    // let skillRemove = localStorage.getItem('skillRemove');
    // if(skillRemove) {
    //   localStorage.removeItem('JobSkills');
    // }
   }


  // function for viewing skill details
  getInfoDetails() {
   
    let getJobDetails = this.employerJobService.getJobDetails;  //get from localstorage for already saved Job data as skills are dependent on Job Roles
    if (getJobDetails) {
      Object.values(getJobDetails.jobRole).map(
        (value) => {
          this.jobRoleIds.push(value['jobRoleId']);
        }
      );
      //get skill details by Job Role Id
      this.globalActionsService.getJobSkillById({ jobRole: this.jobRoleIds })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.skillsList = result['details'];
              console.log('skillsList########################################',)

              

              //check if value is stored in localstorage
              let getValue = this.employerJobService.getSkillDetails; 
              console.log('get skill details',this.employerJobService.getSkillDetails);
              if (getValue) {

                // if (this.isApiEdit) {
                //   this.skillDescription = new FormControl(getValue.description);
                // }
                this.skillDescription = new FormControl(getValue.description);

                this.selectedRoleWiseSkill = getValue.roleWiseAction;
                console.log('skill list', this.skillsList);
                console.log('selectedRoleWiseSkill',this.selectedRoleWiseSkill);
                console.log("okay",this.skillDescription);

                Object.keys(this.skillsList).map(
                  (key) => {
                    if (getValue.roleWiseAction[this.skillsList[key].jobRoleId] && this.skillsList[key].details) {
                      Object.keys(this.skillsList[key].details).map(
                        (skillRow) => {
                          for (let i = 0; i < getValue.roleWiseAction[this.skillsList[key].jobRoleId].skills.length; i++) {
                            if (this.skillsList[key].details[skillRow]['skillId'] === (getValue.roleWiseAction[this.skillsList[key].jobRoleId].skills[i].id)) {
                              this.skillsList[key].details[skillRow].checked = "checked";
                              const control = <FormArray>this.addSkillsForm.controls['skillDetails'];
                              control.push(new FormControl(this.skillsList[key]));

                                if (this.isApiEdit) {
                                  let skillId = this.skillsList[key].details[skillRow]['skillId'];
                                  let jobRoleId = this.skillsList[key].jobRoleId;
                                  let roleName = this.skillsList[key].jobRoleName;
                                  let skillName = this.skillsList[key].details[skillRow]['skillName'];
                                }
                            }

                          }
                        });
                    }
                  });
              }
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
              
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              
            });
          }
        )
    }
    else {
      this.snackBar.open('Please select job roles first', 'Got it!', {
       
      });
    }

  }

  onChecklistChange(checked, skillId, jobRoleId, roleName, skillName) {

    let roleWiseSkill = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId] : {};
    let skillsList = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId].skills : [];
    // let skillsList = this.selectedRoleWiseSkill[jobRoleId] ? this.selectedRoleWiseSkill[jobRoleId].skills.id : [];

    if (checked) { 
      // const control = <FormArray>this.addSkillsForm.controls['skillDetails'];
      // control.push(new FormControl(skillId));
      skillsList.push({ id: skillId, name: skillName });
      localStorage.setItem('checkedSkilles',JSON.stringify(skillsList));
    }
    else {
      //  skillsList = skillsList.filter(item => item !== skillId)
      skillsList = skillsList.filter(item => item.id !== skillId)
    }

    if (skillsList.length > 0) {
      roleWiseSkill['skills'] = skillsList;
      roleWiseSkill['roleId'] = jobRoleId;
      roleWiseSkill['roleName'] = roleName;
      this.selectedRoleWiseSkill[jobRoleId] = roleWiseSkill;
    }
    else {
      delete this.selectedRoleWiseSkill[jobRoleId];
    }
  }


  //submit skills
  saveSkills() {
    let skillsList = JSON.parse( localStorage.getItem('checkedSkilles'))


    if (Object.keys(this.selectedRoleWiseSkill).length > 0 && this.skillDescription.value) {
      console.log('look selectedRoleWiseSkill',this.selectedRoleWiseSkill)
      let skill = {
        //  roleWiseAction: Object.values(this.selectedRoleWiseSkill),
        roleWiseAction: this.selectedRoleWiseSkill,
        description: this.skillDescription.value
      }
      console.log("look",skill);
      this.employerJobService.setSkillDetails(skill);
      let getValue = this.employerJobService.getSkillDetails;

      if (getValue) {
        this.snackBar.open('Skills saved', 'Got it!', {
         
        });
        //show alert and navigate to job-post component
        this.router.navigate([`/employer/job/${this.jobUrlType}`]);
      }
      else {
        this.snackBar.open('PLEASE SELECT A SKILL', 'Got it!', {
       
        });
      }
    }
    else {
      this.snackBar.open('Please select skills and enter job description', 'Got it!', {
       
      });
    }

  }

}
