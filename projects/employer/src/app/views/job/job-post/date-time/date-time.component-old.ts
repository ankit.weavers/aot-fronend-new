import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { EmployerJobService } from '../../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { Erros } from 'src/app/models';
import * as moment from 'moment';

@Component({
  selector: 'employer-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss']
})
export class DateTimeComponent implements OnInit {

  public addDateTimeForm: FormGroup;
  private onDestroyUnSubscribe = new Subject<void>();
  shiftList = [];
  selectOption: Boolean = false;
  startMinDate;
  endMinDate;
  isStartDateSelected: Boolean = false;
  checked: Boolean = false;
  dateRangeControl = new FormControl('');
  savedDateList = [];
  public errors: any = Erros;
  jobUrlType;
  selectedShiftMinStartTime: any;
  selectedShiftMaxTime: any;
  selectedShiftMinEndTime: any;
  minimumTimeRange;
  maximumTimeRange;
  ngStartTime;
  ngEndTime;
  shiftName;
  isValidTime: Boolean = true;
  ismeridian: Boolean = false;
  showSpinners: Boolean = false;
  public hours: any[] = new Array(25);
  public minutes: any[] = new Array(61);
  bsInlineValue = new Date();
  randomDates = [];
  constructor(
    private fb: FormBuilder,
    public globalactionservice: GlobalActionsService,
    public snackBar: MatSnackBar,
    public employerJobService: EmployerJobService,
    public router: Router,
    private activatedRouter: ActivatedRoute
  ) {

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];

    let getDateTimeDetails = this.employerJobService.getDateTimeDetails;
    if (getDateTimeDetails) {
      this.savedDateList = getDateTimeDetails.dateTime;

      console.log("savedDateList", this.savedDateList);

      if (this.jobUrlType === 'edit') {
        this.formatDate(this.savedDateList);
      }
      // this.selectEndDate();
    }
    this.startMinDate = new Date();
    this.addDateTimeForm = this.fb.group({
      shift: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      startHour: ['', Validators.required],
      endHour: ['', Validators.required],
      startMinute: ['', [Validators.required]],
      endMinute: ['', [Validators.required]],
    })

  }

  ngOnInit() {

  }

  //toggle function for sliding option
  sliderOption(event: MatSlideToggleChange) {
    this.checked = event.checked
    if (!this.checked) {
      this.dateRangeControl.reset();
    }
    else {
      this.addDateTimeForm.reset();
    }
  }
  onValueChange(event){
    console.log(event);
    console.log(Date.parse(event));
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    let current_datetime = event
    let formatted_date = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear()   
    this.randomDates.push(formatted_date);
  }
  removeDate(index){
    this.randomDates.splice(index, 1);
  }

  //get start date value to validate end date
  selectEndDate() {
    if (this.addDateTimeForm.controls.startDate.value) {
      this.isStartDateSelected = true;
      this.endMinDate = this.addDateTimeForm.controls.startDate.value;
    }
  }

  //editDays
  editDays(index) {
    this.checked = false;
    this.addDateTimeForm.get('shift').setValue(this.savedDateList[index].shift);
    this.addDateTimeForm.get('startDate').setValue(this.savedDateList[index].startDate);
    this.addDateTimeForm.get('endDate').setValue(this.savedDateList[index].endDate);
    this.addDateTimeForm.get('startHour').setValue(this.savedDateList[index].startTime.substr(0,2));
    this.addDateTimeForm.get('endHour').setValue(this.savedDateList[index].startTime.substr(0,2));
    this.addDateTimeForm.get('endMinute').setValue(this.savedDateList[index].endTime.substr(3));
    this.addDateTimeForm.get('startMinute').setValue(this.savedDateList[index].startTime.substr(3));

    // if (this.jobUrlType === "edit") {
    //   this.endMinDate = this.savedDateList[index].startDate;
    //   this.startMinDate = this.savedDateList[index].startDate;
    // }

    this.savedDateList.splice(index, 1);
    this.dateRangeControl.reset();

  }

  //delete days off
  deleteDays(index) {
    if (this.savedDateList[index]) {
      this.savedDateList.splice(index, 1);
      this.checked = false;
      if (this.savedDateList.length) {

        console.log("save date", this.savedDateList);

        this.employerJobService.setDateTimeDetails({ dateTime: this.savedDateList });
        let getValue = this.employerJobService.getDateTimeDetails;
        if (getValue) {

          // this.addDateTimeForm.reset();
          // this.dateRangeControl.reset();
          // this.endMinDate = '';

          this.snackBar.open('JOB DateTime SAVED', 'Got it!', {
            duration: 3000,
          });
        }
      }
    }
  }

  //process or format date and time
  formatDate(dateArray) {
    for (let i = 0; i < dateArray.length; i++) {
      if (typeof dateArray[i].startDate === "number") {
        this.savedDateList[i].startDate = new Date(dateArray[i].startDate);
      }
      if (typeof dateArray[i].endDate === "number") {
        this.savedDateList[i].endDate = new Date(dateArray[i].endDate);
      }
      if (typeof dateArray[i].startTime === "string") {
        this.savedDateList[i].startTime = moment(dateArray[i].startTime, 'HH:mm a').toDate();
      }
      if (typeof dateArray[i].endTime === "string") {
        this.savedDateList[i].endTime = moment(dateArray[i].endTime, 'HH:mm a').toDate();
      }
    }
  }

  //saving all the date and time details
  saveDateTimeDetails() {

    if (this.checked && this.dateRangeControl.value) {
      
      let timeObj = this.addDateTimeForm.value;
      console.log('range', timeObj.shift);
      var startHour = timeObj.startHour;
      var startMinute = timeObj.startMinute;
      var endHour = timeObj.endHour;
      var endMinute = timeObj.endMinute;
      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;
      this.savedDateList.push({"shift":timeObj.shift,"startDate":this.dateRangeControl.value[0],"endDate":this.dateRangeControl.value[1],"startTime":startTime,"endTime":endTime});
    }
    if (this.addDateTimeForm.valid) {
      
      let timeObj = this.addDateTimeForm.value;
      console.log('range', timeObj.shift);
      var startHour = timeObj.startHour;
      var startMinute = timeObj.startMinute;
      var endHour = timeObj.endHour;
      var endMinute = timeObj.endMinute;
      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;

      this.savedDateList.push({"shift":timeObj.shift,"startDate":timeObj.startDate,"endDate":timeObj.endDate,"startTime":startTime,"endTime":endTime});
    }

    if (this.savedDateList.length) {

      console.log("save date", this.savedDateList);

      this.employerJobService.setDateTimeDetails({ dateTime: this.savedDateList });
      let getValue = this.employerJobService.getDateTimeDetails;
      if (getValue) {

        this.addDateTimeForm.reset();
        this.dateRangeControl.reset();
        this.endMinDate = '';

        this.snackBar.open('JOB DateTime SAVED', 'Got it!', {
          duration: 3000,
        });
        //show alert and navigate to job-post component
        this.router.navigate([`/employer/job/${this.jobUrlType}`]);
      }
    }
    else {
      this.snackBar.open('PLEASE PROVIDE THE DETAILS', 'Got it!', {
        duration: 3000,
      });
    }

  }


  getShiftName() {

    // Define Custom start time
    let timeObj = this.addDateTimeForm.value;

    var startHour = timeObj.startHour;
    var startMinute = timeObj.startMinute;
    var endHour = timeObj.endHour;
    var endMinute = timeObj.endMinute;

    if (startHour != "" && startMinute != "" && endHour != "" && endMinute != "" ) {

      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;

      this.employerJobService.getShiftName({
        "startTime": startTime,
        "endTime": endTime
      })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {

              this.shiftName = result['finalShift'];
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
                duration: 3000,
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
              duration: 3000,
            });
          }
        );
    }
  }
}