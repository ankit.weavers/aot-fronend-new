import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { EmployerJobService } from '../../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { Erros } from 'src/app/models';
import * as moment from 'moment';
import { BsDatepickerConfig,BsDaterangepickerConfig  } from 'ngx-bootstrap/datepicker';
@Component({
  selector: 'employer-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss']
})
export class DateTimeComponent implements OnInit {
  minDate = new Date();
  maxDate;
  totalHour;
  // isOpenRangeDatepicker: Boolean = false;
  // bsConfig: Partial<BsDatepickerConfig>;
  bsConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
  bsConfigs: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
  bsConfigRange: Partial<BsDaterangepickerConfig>;
  public addDateTimeForm: FormGroup;
  private onDestroyUnSubscribe = new Subject<void>();
  // ngclass: Partial<BsDatepickerConfig>;
  shiftList = [];
  selectOption: Boolean = false;
  getJobDetails:any;
  startMinDate;
  endMinDate;
  isStartDateSelected: Boolean = false;
  checked: Boolean = false;
  dateRangeControl = new FormControl('');
  savedDateList = [];
  public errors: any = Erros;
  jobUrlType;
  selectedShiftMinStartTime: any;
  selectedShiftMaxTime: any;
  selectedShiftMinEndTime: any;
  minimumTimeRange;
  maximumTimeRange;
  ngStartTime;
  ngEndTime;
  shiftName;
  isValidTime: Boolean = true;
  ismeridian: Boolean = false;
  showSpinners: Boolean = false;
  public hours: any[] = new Array(24);
  public minutes: any[] = new Array(61);
  bsInlineValue;
  bsInlineRangeValue:Date[];
  datesDisabled=[];
  // maxDate=new Date();

  // bsValue = new Date();
  randomDates = [];
  selectedDates = [];
  colorTheme = ' theme-dark-blue';
  colorTheamRange ='theme-dark-red';
  todayDatecolor = 'today'




  // @ViewChild('r') datePicker:ElementRef;
  constructor(
    
    private fb: FormBuilder,
    public globalactionservice: GlobalActionsService,
    public snackBar: MatSnackBar,
    public employerJobService: EmployerJobService,
    public router: Router,
    private activatedRouter: ActivatedRoute
  ) {
    
    // this.maxDate.setDate(this.maxDate.getDate() + 7);
    // this.bsInlineRangeValue = [this.bsInlineValue, this.maxDate];

    let url = this.activatedRouter.snapshot.url.join().split(',');
    this.jobUrlType = url[0];

    let getDateTimeDetails = this.employerJobService.getDateTimeDetails;
     console.log('constructor',this.savedDateList);
    if (getDateTimeDetails) {
      this.savedDateList = getDateTimeDetails.dateTime;
     
   
     
      let templateDateArr = this.savedDateList;
/*       for(let i =0; i<templateDateArr.length;i++){
        console.log('f', this.employerJobService.getDateTimeDetails);
        this.savedDateList[i].startTime = moment(this.savedDateList[i].startTime).format("hh:mm").replace(':','.')
        this.savedDateList[i].endTime = moment(this.savedDateList[i].endtTime).format("hh:mm").replace(':','.')
        // this.savedDateList[i].startDate = Date.parse(this.savedDateList[i].startDate);
        // this.savedDateList[i].endDate = Date.parse(this.savedDateList[i].endDate);
        
      } */

      if (this.jobUrlType === 'edit') {
        this.formatDate(this.savedDateList);
      }
      // this.selectEndDate();
    }
    this.startMinDate = new Date();
    this.addDateTimeForm = this.fb.group({
      shift: ['', Validators.required],
      // startDate: ['', Validators.required],
      // endDate: ['', Validators.required],
      startHour: ['', Validators.required],
      endHour: ['', Validators.required],
      startMinute: ['', [Validators.required]],
      endMinute: ['', [Validators.required]],
    })

  }

  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.bsConfigs = Object.assign({}, { containerClass: this.colorTheme });
    // this.bsConfig.containerClass = 'theme-dark-blue';
    this.bsConfigRange = Object.assign({},{containerClass:this.colorTheamRange});
    this.bsConfig.customTodayClass = this.todayDatecolor;
    this.bsConfig.dateInputFormat = 'DD-MM-YYYY';
    this.bsConfigs.customTodayClass = this.todayDatecolor;
    this.bsConfigs.rangeInputFormat = 'DD-MM-YYYY';
    
    console.log("date time", this.employerJobService.getDateTimeDetails);









  }

  //toggle function for sliding option
  sliderOption(event: MatSlideToggleChange) {
    this.checked = event.checked
    if (!this.checked) {
      this.dateRangeControl.reset();
    }
    else {
      this.addDateTimeForm.reset();
    }
  }
  onValueChange(event){
    this.datesDisabled.push(new Date(event));
    this.randomDates.push(event);
    console.log("date",this.randomDates,event);
  }
  removeDate(index){
  
    console.log("index",index);
    this.randomDates.splice(index,1);

    this.datesDisabled.splice(index,1);
  }

  //get start date value to validate end date
  selectEndDate() {
    if (this.addDateTimeForm.controls.startDate.value) {
      this.isStartDateSelected = true;
      this.endMinDate = this.addDateTimeForm.controls.startDate.value;
    }
  }

  //editDays
  editDays(index) {
    console.log('savedDateList',this.savedDateList[index]);
    if(this.savedDateList[index].isDateRange ){
      console.log('in');
      this.checked = true;
      //  this.isOpenRangeDatepicker = true;
      //  this.bsInlineRangeValue= new Date(this.savedDateList[index].startDate);
      //  this.bsInlineRangeValue= new Date(this.savedDateList[index].endDate);
    }else{
      this.checked = false
    }
    this.randomDates=[];
  
    // this.addDateTimeForm.get('shift').setValue(this.savedDateList[index].shift);
    this.shiftName = this.savedDateList[index].shift;
    // this.addDateTimeForm.get('startDate').setValue(this.savedDateList[index].startDate);
    // this.addDateTimeForm.get('endDate').setValue(this.savedDateList[index].endDate);
    this.bsInlineValue = new Date(this.savedDateList[index].startDate);
    this.minDate=new Date(this.savedDateList[index].startDate);
    this.maxDate=new Date(this.savedDateList[index].endDate);
    this.bsInlineRangeValue = [this.minDate,this.maxDate];

   



  



    // this.addDateTimeForm.get('startHour').setValue(this.savedDateList[index].startTime.getHours());
    // this.addDateTimeForm.get('endHour').setValue(this.savedDateList[index].startTime.getHours());
    // this.addDateTimeForm.get('endMinute').setValue(this.savedDateList[index].endTime.getMinutes());
    // this.addDateTimeForm.get('startMinute').setValue(this.savedDateList[index].startTime.getMinutes());

    if (this.jobUrlType === "edit") {
      console.log(this.savedDateList[index].startTime.slice(0,-3));
    this.addDateTimeForm.get('startHour').setValue((this.savedDateList[index].startTime).slice(0,-3));
    this.addDateTimeForm.get('endHour').setValue((this.savedDateList[index].endTime).slice(0,-3));
    this.addDateTimeForm.get('endMinute').setValue(("0" + this.savedDateList[index].endTime).slice(-2) );
    this.addDateTimeForm.get('startMinute').setValue( ("0" + this.savedDateList[index].startTime).slice(-2) );
    }else{
    

    //  this.dateRangeControl.setValue([this.savedDateList[index].startDate]) ;
    //  console.log(this.dateRangeControl)
    //  this.dateRangeControl.value[1]  = this.savedDateList[index].endDate;
      this.addDateTimeForm.get('startHour').setValue( this.savedDateList[index].startTime.substr(0,2));
      this.addDateTimeForm.get('endHour').setValue(this.savedDateList[index].endTime.substr(0,2));
      this.addDateTimeForm.get('endMinute').setValue(this.savedDateList[index].endTime.substr(3));
      this.addDateTimeForm.get('startMinute').setValue(this.savedDateList[index].startTime.substr(3));
    }

    this.savedDateList.splice(index, 1);
   
    this.dateRangeControl.reset();
    document.getElementById("calender").scrollIntoView();

  }

  //delete days off
  deleteDays(index) {
    if (this.savedDateList[index]) {
 

      this.savedDateList.splice(index, 1);

      this.checked = false;
    

        console.log("save date", this.savedDateList);

        this.employerJobService.setDateTimeDetails({ dateTime: this.savedDateList });
        let getValue = this.employerJobService.getDateTimeDetails;
        if (getValue) {

          // this.addDateTimeForm.reset();
          // this.dateRangeControl.reset();
          // this.endMinDate = '';

          this.snackBar.open('Job date and time saved', 'Got it!', {
          
          });
        }
 
    }
  }

  //process or format date and time
  formatDate(dateArray) {
    console.log('dateArray',dateArray)
    for (let i = 0; i < dateArray.length; i++) {
      if (typeof dateArray[i].startDate === "number") {
        this.savedDateList[i].startDate = new Date(dateArray[i].startDate);
      }
      if (typeof dateArray[i].endDate === "number") {
        this.savedDateList[i].endDate = new Date(dateArray[i].endDate);
      }
      if (typeof dateArray[i].startTime === "string") {
        console.log('ivalid Date',this.savedDateList[i].startTime)
        this.savedDateList[i].startTime = moment(dateArray[i].startTime, 'HH:mm a').format("HH:mm").replace(':','.');
      }
      if (typeof dateArray[i].endTime === "string") {
        console.log('valid Date',this.savedDateList[i].endTime)
        this.savedDateList[i].endTime = moment(dateArray[i].endTime, 'HH:mm a').format("HH:mm").replace(':','.');
      }
    }
  }

  //saving all the date and time details
  saveDateTimeDetails() {
    this.getJobDetails = JSON.parse(localStorage.getItem('JobDetails'))

    if (this.checked && this.dateRangeControl.value) {
      
      let timeObj = this.addDateTimeForm.value;
    
      console.log('date',typeof(this.dateRangeControl.value[0]));
      var startDate =this.dateRangeControl.value[0];
      var endDate = this.dateRangeControl.value[1];
      var startHour = timeObj.startHour;
      var startMinute = timeObj.startMinute;
      var endHour = timeObj.endHour;
      var endMinute = timeObj.endMinute;
      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;
    

      var difference =  (endDate.setHours(endHour,endMinute) - startDate.setHours(startHour,startMinute));
      console.log("difference",difference);

      var daysDifference = Math.floor(difference/1000/60/60/24);
      difference -= daysDifference*1000*60*60*24

     var hoursDifference = Math.floor(difference/1000/60/60);
      difference -= hoursDifference*1000*60*60


      var minutesDifference = Math.floor(difference/1000/60);
      difference -= minutesDifference*1000*60
      if(hoursDifference>0 || minutesDifference>0 ){
        console.log('if');
        var d = daysDifference+1;
      }
      if(hoursDifference < 1){
        this.snackBar.open('The minimum shift duration is one hour ','Got it!',{

        });
        return false;
      }
      console.log('daysDifference -new',d,'hoursDifference',hoursDifference*this.getJobDetails.no_of_stuffs , 'minutesDifference',minutesDifference);



      this.savedDateList.push({"isDateRange":this.checked,"shift":timeObj.shift, "totalHour":(hoursDifference*d)*this.getJobDetails.no_of_stuffs,"startDate":this.dateRangeControl.value[0],"endDate":this.dateRangeControl.value[1],"startTime":startTime,"endTime":endTime});
    }
    if (this.addDateTimeForm.valid) {
      
      let timeObj = this.addDateTimeForm.value;

      let totalDate = this.randomDates.length;
      var startHour = timeObj.startHour;
      var startMinute = timeObj.startMinute;
      var endHour = timeObj.endHour;
      var endMinute = timeObj.endMinute;
      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;

      for(let i = 0;i<this.randomDates.length;i++){ 
        console.log('start_dte',this.randomDates[i].setHours(startHour,startMinute));
        console.log('end_dte',this.randomDates[i].setHours(endHour,endMinute));
        var difference =  (this.randomDates[i].setHours(endHour,endMinute) - this.randomDates[i].setHours(startHour,startMinute));

        var daysDifference = Math.floor(difference/1000/60/60/24);
        difference -= daysDifference*1000*60*60*24

       var hoursDifference = Math.floor(difference/1000/60/60);
        difference -= hoursDifference*1000*60*60

        var minutesDifference = Math.floor(difference/1000/60);
        difference -= minutesDifference*1000*60
        console.log('hoursDifference',hoursDifference*this.getJobDetails.no_of_stuffs , 'minutesDifference',minutesDifference);
        if(hoursDifference < 1){
          this.snackBar.open('Please insert minimum an hour shift','Got it!',{

          });
          return false;
        }

        this.savedDateList.push({"shift":timeObj.shift, "totalHour":hoursDifference*this.getJobDetails.no_of_stuffs, "startDate":this.randomDates[i],"endDate":this.randomDates[i],"startTime":startTime,"endTime":endTime});
      }

    }

    if (this.savedDateList.length) {

      this.employerJobService.setDateTimeDetails({ dateTime: this.savedDateList });
      let getValue = this.employerJobService.getDateTimeDetails;
      if (getValue) {

        this.addDateTimeForm.reset();
        this.dateRangeControl.reset();
        this.endMinDate = '';

        this.snackBar.open('Job date and time saved', 'Got it!', {
          
        });
        //show alert and navigate to job-post component
        this.router.navigate([`/employer/job/${this.jobUrlType}`]);
      }
    }
    else {
      this.snackBar.open('PLEASE PROVIDE THE DETAILS', 'Got it!', {
        
      });
    }

  }


  getShiftName() {

    // Define Custom start time
    let timeObj = this.addDateTimeForm.value;

    var startHour = timeObj.startHour;
    var startMinute = timeObj.startMinute;
    var endHour = timeObj.endHour;
    var endMinute = timeObj.endMinute;

    if (startHour != "" && startMinute != "" && endHour != "" && endMinute != "" ) {

      let startTime = startHour + '.' + startMinute;
      let endTime = endHour + '.' + endMinute;

      this.employerJobService.getShiftName({
        "startTime": startTime,
        "endTime": endTime
      })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {

              this.shiftName = result['finalShift'];
            } else {
              this.snackBar.open(result['message'], 'Got it!', {
               
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
             
            });
          }
        );
    }
  }
}