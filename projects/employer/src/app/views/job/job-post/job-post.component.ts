
import { Component, OnInit, TemplateRef } from '@angular/core';
import { DetailsComponent } from './details/details.component';
import { EmployerJobService } from '../../../services';
// import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SelectedFileService } from '../../../services/selected-file.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { setHours } from 'ngx-bootstrap/chronos/utils/date-setters';
import * as moment from 'moment';
@Component({
  selector: 'employer-job-post',
  templateUrl: './job-post.component.html',
  styleUrls: ['./job-post.component.scss'],
  providers: [DetailsComponent],

})
export class JobPostComponent implements OnInit {

  private onDestroyUnSubscribe = new Subject<void>();
  saveTemplateForm = new FormControl('', Validators.required);
  templateNameForm = new FormControl('', Validators.required);
  proceedToSummary: Boolean = false;
  saveCurrentJob: Boolean = false;
  deactiveValue:Boolean = false;
  getJobDetails;
  getJobLocation;
  getSkillDetails;
  getTimeDetails;
  getUploadedFile;
  allSeparatedJobs=[];
  getPaymentDetails;
  allJobsPostedList = [];
  fileArray = [];
  isApiEdit: Boolean = false;
  jobUrlType;
  jobSkill=[];
  //job template related variables
  saveAstemplatePermissionModal: BsModalRef;
  templateEvent;
  templateNameModal: BsModalRef
  public templateName: string = ""

  //job template related variables
  modalRef: BsModalRef;
  isSavedTemplate: boolean = false
  constructor(
    public router: Router,
    private employerSetGetService: EmployerJobService,
    public snackBar: MatSnackBar,
    private sfs: SelectedFileService,
    private modalService: BsModalService,
    private activatedRouter: ActivatedRoute
  ) { }

  ngOnInit() {

    this.jobUrlType = this.activatedRouter.snapshot.url.join().split(',');

    if (this.jobUrlType[0] === "edit") {
      this.isApiEdit = true;
   
      if (localStorage.getItem('JobDetailsById')) {   //call job details for edit posted job
        this.getJobDetailsById();
      }
    } else if (this.jobUrlType[0] === "post") {
      this.isApiEdit = false;
      let allJobsList = this.employerSetGetService.getAllJob; 
      console.log('employerSetGetService.getAllJob',allJobsList);
      let jobEdit = localStorage.getItem('jobEdit');
      if(jobEdit) {
        localStorage.removeItem('donotdelete');
        localStorage.removeItem('allJobs');
        localStorage.removeItem('JobDetailsById');
        localStorage.removeItem('editJobId');
        localStorage.removeItem('JobDetails');
        localStorage.removeItem('uploadedFile');
        localStorage.removeItem('Payment');
        localStorage.removeItem('JobSkills');
        localStorage.removeItem('JobLocation');
        localStorage.removeItem('Datetime');
        localStorage.removeItem('SaveTimeJobPost');
        localStorage.removeItem('jobEdit');
      }
      if (allJobsList && allJobsList.length) {
        this.allJobsPostedList = allJobsList;
        this.proceedToSummary = true;
      }
    }

    this.getJobDetails = this.employerSetGetService.getJobDetails;
    this.getJobLocation = this.employerSetGetService.getJobLocation;
    this.getSkillDetails = this.employerSetGetService.getSkillDetails;
    this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
    this.getUploadedFile = this.employerSetGetService.getFileDetails;

    this.getPaymentDetails = this.employerSetGetService.getPayment;
    this.isSavedTemplate = Boolean(this.employerSetGetService.getLocalStorage("isSavedTemplate"))
    if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
      this.saveCurrentJob = true;
    }

  }

  // canDeactivate() {
  //   if(!this.deactiveValue && this.isApiEdit) {
  //     let r = confirm('WARNING: You have unsaved changes. Press Cancel to go back and save these changes, or OK to lose these changes.')
  //     console.log('r',r);
  //     if(r == true) {
  //       localStorage.removeItem('donotdelete');
  //       localStorage.removeItem('allJobs');
  //       localStorage.removeItem('JobDetailsById');
  //       localStorage.removeItem('editJobId');
  //       localStorage.removeItem('JobDetails');
  //       localStorage.removeItem('uploadedFile');
  //       localStorage.removeItem('Payment');
  //       localStorage.removeItem('JobSkills');
  //       localStorage.removeItem('JobLocation');
  //       localStorage.removeItem('Datetime');
  //       localStorage.removeItem('SaveTimeJobPost');
  //     return  true;

  //     }
  //     else {
  //       return false;
  //     }

  //   }
  //   else {
  //     return true;
  //   }
  // }

  // routerChangeIn(status) {
  //   if(status == 1) {
  //     this.router.navigate(["['/employer/job', jobUrlType[0], 'location']"])
  //   }
  // }

  //open modal for save as template
  openTemplatePermissionModal(event, template: TemplateRef<any>) {
    this.templateEvent = event
    if (event.target.checked) {
      console.log("checked")
      this.saveAstemplatePermissionModal = this.modalService.show(template, { backdrop: 'static', keyboard: false });
    }
  }
  //if user does't want to save as template
  backtoUncheckTemplateSaveCheckbox() {
    this.templateEvent.target.checked = false
    this.saveAstemplatePermissionModal.hide()
  }
  //open template name modal
  openTemplateNameModal(template: TemplateRef<any>) {
    this.saveAstemplatePermissionModal.hide()
    this.templateNameModal = this.modalService.show(template, { backdrop: 'static', keyboard: false });
  }
  //save job as template
  saveJobAsTemplate() {

    let postJobDetails = this.getPostJobTemplate();

    this.employerSetGetService.postJobAsTemplate(postJobDetails).subscribe((response) => {
      this.templateNameModal.hide()
      if (response && !response['isError']) {
        this.employerSetGetService.setLocalStorage("isSavedTemplate", true)
        this.isSavedTemplate = true

        this.snackBar.open('Template saved successfully', 'Got it!', {
         
        });
      }
      else {
        this.snackBar.open(response['message'], 'Got it!', {
          
        });
      }
    }, (err) => {
      this.snackBar.open('Network error', 'Got it!', {
       
      });
    })
  }
  //get post job template
  getPostJobTemplate() {
    this.getJobDetails = this.employerSetGetService.getJobDetails;
    this.getJobLocation = this.employerSetGetService.getJobLocation;
    this.getSkillDetails = this.employerSetGetService.getSkillDetails; 
    this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
    this.getUploadedFile = this.employerSetGetService.getFileDetails;
    this.getPaymentDetails = this.employerSetGetService.getPayment;
    let postJobTemplate: any = {}
    postJobTemplate.employerId = JSON.parse(localStorage.getItem('currentUser')).employerId
    // postJobTemplate.roleWiseAction.locationTrackingRadius = this.getJobDetails.locationTrackingRadius,

    postJobTemplate.templateName = this.templateName
    postJobTemplate.jobDetails = {}
    postJobTemplate.jobDetails.industry = this.getJobDetails.industry;
     postJobTemplate.jobDetails.locationTrackingRadius = this.getJobDetails.locationTrackingRadius;


    let newSetTime = [];
    for (let timer = 0; timer < this.getTimeDetails.dateTime.length; timer++) {

      // let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
      // console.log('formattedStartTime',formattedStartTime)
      //new Date 
      // let formattedEndTime =this.getTimeDetails.dateTime[timer].endTime
/*       let formattedStartTime = moment(this.getTimeDetails.dateTime[timer].startTime).format("hh:mm").replace(':','.')
      let formattedEndTime = moment(this.getTimeDetails.dateTime[timer].endTime).format("hh:mm").replace(':','.') */
      let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
      let formattedEndTime = this.getTimeDetails.dateTime[timer].endTime
      console.log('job template endtime',  moment(this.getTimeDetails.dateTime[timer].endTime).format("hh:mm").replace(':','.'))
      //new Date .toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

      newSetTime.push({
        shift: this.getTimeDetails.dateTime[timer].shift,
        startDate:Date.parse(this.getTimeDetails.dateTime[timer].startDate),
        endDate:Date.parse(this.getTimeDetails.dateTime[timer].endDate),
        isDateRange:Date.parse(this.getTimeDetails.dateTime[timer].startDate) != Date.parse(this.getTimeDetails.dateTime[timer].startDate) ? true : false,
        startTime: formattedStartTime,
        endTime: formattedEndTime,
        totalHour:this.getTimeDetails.dateTime[timer].totalHour
      });
    }

    // golam 

    let roleWiseAction = [];
    console.log('getSkillDetails', this.getSkillDetails);

    Object.values(this.getSkillDetails.roleWiseAction).map(
      (element) => {
        let skillIds = [];
        for (let i = 0; i < element['skills'].length; i++) {
          skillIds[i] = element['skills'][i].id;
        }
        console.log('skillIds', skillIds);

        let details = {
          roleId: element["roleId"],
          roleName: element["roleName"],
          skills: skillIds,
          jobType: this.getJobDetails.jobType,
          payType: this.getJobDetails.payType,
          pay: this.getJobDetails.pay,
          noOfStuff: this.getJobDetails.no_of_stuffs,
          locationTrackingRadius:this.getJobDetails.locationTrackingRadius,
          uploadFile: this.getUploadedFile,
          paymentStatus: "UNPAID",
          status: "POSTED",
          payment: this.getPaymentDetails.payment,
          setTime: newSetTime,
          locationName: this.getJobLocation.locationName,
          description: this.getSkillDetails.description,
          distance: this.getJobLocation.distance,
          location: this.getJobLocation.location
        }
        roleWiseAction.push(details);

      }
    );
    postJobTemplate.jobDetails.roleWiseAction = roleWiseAction;
    return postJobTemplate
  }

  // get JobPosted List
  getJobDetailsById() {

    let requiredData = {
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
      jobId: JSON.parse(localStorage.getItem('JobDetailsById')).jobId,
      roleId: JSON.parse(localStorage.getItem('JobDetailsById')).roleId
    }

    this.employerSetGetService.getEditJobDetailsById(requiredData)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            let jobDetailsList = result['details'];
            this.jobSkill =result['details'].skillDetails;
            console.log("lobskill",this.jobSkill);
            console.log('jobDetailsList', jobDetailsList);
            localStorage.setItem('editJobId', result['details'].jobId);
            // localStorage.setItem('alljobs',result['details']);
          // let alljob =  this.employerSetGetService.setAllJobs(jobDetailsList);
  
            localStorage.removeItem('JobDetailsById');

            //***********************************************************************/
            //*          resetting Local storage variables with selected job        */
            //***********************************************************************/

            //*                     1. personal details variables                   */

            let jobRoleObject = {
              // createdAt: "2019-05-22T13:31:41.090Z",
              industryId: jobDetailsList.jobDetails.industryDetails.industryId,
              jobRoleId: jobDetailsList.jobDetails.roleWiseAction.roleId,
              jobRoleName: jobDetailsList.jobDetails.roleWiseAction.roleName,
              skillIds: jobDetailsList.jobDetails.roleWiseAction.skills,
              // updatedAt: "2019-06-04T05:34:46.056Z"
            }

            let jobDetailsObject = {
              industry: jobDetailsList.jobDetails.industryDetails.industryId,
              industryName: jobDetailsList.jobDetails.industryDetails.industryName,
              jobRole: [jobRoleObject],
              jobType: jobDetailsList.jobDetails.roleWiseAction.jobType,
              payType: jobDetailsList.jobDetails.roleWiseAction.payType,
              pay: jobDetailsList.jobDetails.roleWiseAction.pay,
              no_of_stuffs: jobDetailsList.jobDetails.roleWiseAction.noOfStuff,
              locationTrackingRadius: jobDetailsList.jobDetails.roleWiseAction.locationTrackingRadius,

            }

            // console.log(" to set jobDetailsObject", jobDetailsObject);

            this.employerSetGetService.setJobDetails(jobDetailsObject);

            //************************* 2.location variables **************************/
            let jobLocation = {
              location: {
                type: "Point",
                coordinates: [
                  jobDetailsList.jobDetails.roleWiseAction.location.coordinates[0],
                  jobDetailsList.jobDetails.roleWiseAction.location.coordinates[1]
                ],
              },
              distance: jobDetailsList.jobDetails.roleWiseAction.distance,
              locationName: jobDetailsList.jobDetails.roleWiseAction.locationName,
            }

            this.employerSetGetService.setJobLocation(jobLocation);

            //**************************** 3. skill variables ****************************/

            let jobRoleId = jobDetailsList.jobDetails.roleWiseAction.roleId;

            let skillSet = [];
            for (var i = 0; i < this.jobSkill.length; i++) {
              console.log('++++++++++++++++++++',this.jobSkill[i].skillId);
              skillSet.push({ id: this.jobSkill[i].skillId,name: this.jobSkill[i].skillName });
            }
            let roleWiseAction = {
              [jobRoleId]: {
                roleId: jobRoleId,
                roleName: jobDetailsList.jobDetails.roleWiseAction.roleName,
                skills: skillSet
              }
            };


            let setSkill = {
              roleWiseAction: roleWiseAction,
              description: jobDetailsList.jobDetails.roleWiseAction.description
            };


            this.employerSetGetService.setSkillDetails(setSkill);

            //************************** 4. date time variables ***************************/

            let newSetTime = [];
            for (let timer = 0; timer <  jobDetailsList.jobDetails.roleWiseAction.setTime .length; timer++) {
        
              // let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
              // console.log('formattedStartTime',formattedStartTime)
              //new Date 
              //  let formattedEndTime =this.getTimeDetails.dateTime[timer].endTime
              // let formattedStartTime = moment(jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startTime).format("hh:mm").replace(':','.');
              // let formattedEndTime = moment(jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endTime).format("hh:mm").replace(':','.');
              let formattedStartTime =  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startTime
              let formattedEndTime =  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endTime
              console.log('job template endtime',  moment( jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .endTime).format("HHmm"));
              console.log(' template endtime',  moment( jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .endTime));

              //new Date .toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        
              newSetTime.push({
                shift:  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].shift,
                startDate:new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startDate).toISOString(),
                endDate:new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endDate).toISOString(),
                isDateRange:new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startDate).toISOString() != new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endDate).toISOString() ? true : false,
                startTime: formattedStartTime,
                endTime: formattedEndTime,
                totalHour: jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .totalHour
              });
            }

            this.employerSetGetService.setDateTimeDetails({ dateTime: newSetTime });

            //*********************** 5. uploaded files variables ************************/jobDetailsList.jobDetails.roleWiseAction.setTime

            this.employerSetGetService.setFileDetails(jobDetailsList.jobDetails.roleWiseAction.uploadFile);

            //*********************** 6. payment files variables ************************/

            this.employerSetGetService.setPayment({ payment: jobDetailsList.jobDetails.roleWiseAction.payment });

            //***********************************************************************/
            //******** end resetting Local storage variables with selected job **********/
            //***********************************************************************/

            this.saveCurrentJob = true;
            this.getJobDetails = this.employerSetGetService.getJobDetails;
            this.getJobLocation = this.employerSetGetService.getJobLocation;
            this.getSkillDetails = this.employerSetGetService.getSkillDetails;
            this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
            this.getUploadedFile = this.employerSetGetService.getFileDetails;
            this.getPaymentDetails = this.employerSetGetService.getPayment;

          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }


  //save current job to template
  checkSaveTemplate(event) {
    if (!event.target.checked) {
      this.saveTemplateForm.reset();
    }
  }

  //save Job To job List and Local storage updatation
  saveJobToList() {
    this.getDetailsFromLocalStorage();
  this.employerSetGetService.setAllJobs(this.allJobsPostedList);

    this.saveCurrentJob = false;
    if (this.isApiEdit) {


      this.router.navigate(['/employer/job/job-summary']);
   

      this.proceedToSummary = false;
    }
    // else if (!this.isApiEdit) {
   

    //   this.proceedToSummary = true;
    //   this.clearIndividualJobVariablesInLS(); //clear local storage function called
   

    // }
   
  }
  saveJob() {
    this.getDetailsFromLocalStorage();
    

   this.employerSetGetService.setAllJobs(this.allJobsPostedList);
 

    this.saveCurrentJob = false;
    // if (this.isApiEdit) {
  

    //   this.router.navigate(['/employer/job/job-summary']);

    //   this.proceedToSummary = false;
    // }
    if (!this.isApiEdit) {


      this.proceedToSummary = true;
      this.clearIndividualJobVariablesInLS(); //clear local storage function called
  

    }
   
  }

  //setting job list with latest values
  getDetailsFromLocalStorage() {


    // && this.getUploadedFile
    if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {

      let allJobDetails = {
        api: {
          employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
          paymentStatus: "UNPAID",
          status: "POSTED",
          setTime: this.getTimeDetails.dateTime,
          uploadFile: this.getUploadedFile,
          payment: this.getPaymentDetails.payment,
          jobDetails: {
            industry: this.getJobDetails.industry,
            jobType: this.getJobDetails.jobType,
            payType: this.getJobDetails.payType,
            pay: this.getJobDetails.pay,
            noOfStuff: this.getJobDetails.no_of_stuffs,
            roleWiseAction: Object.values(this.getSkillDetails.roleWiseAction),
            locationTrackingRadius: this.getJobDetails.locationTrackingRadius
          },
          description: this.getSkillDetails.description,
          distance: this.getJobLocation.distance,
          location: this.getJobLocation.location,
          locationName: this.getJobLocation.locationName
        },
        jobRole: this.getJobDetails.jobRole,
        allSelectedSkills: this.getSkillDetails.roleWiseAction,
        industryName: this.getJobDetails.industryName,
        locationName: this.getJobLocation.locationName,
        isApiEdit: this.isApiEdit,
        // jobRole: Object.keys(this.getSkillDetails.roleWiseAction),
      }
      this.allJobsPostedList.push(allJobDetails);

      //  this.clearIndividualJobVariablesInLS();
      // if(!this.isApiEdit) {
      //   this.separateJobsByRoles()
      // }
    }
  }

  // separateJobsByRoles() {

  //   for (let index = 0; index < this.allJobsPostedList.length; index++) {

  //     let roleWiseAction = this.allJobsPostedList[index].api.jobDetails.roleWiseAction;
  //     let separatedRoleWiseAction = [];

  //     for (let j = 0; j < roleWiseAction.length; j++) {
  //       let skills = [];
  //       for (let k = 0; k < roleWiseAction[j].skills.length; k++) {
  //         skills.push(roleWiseAction[j].skills[k].id);
  //       }

  //       let newSetTime = [];

  //       for (let timer = 0; timer < this.allJobsPostedList[index].api.setTime.length; timer++) {
  //         let formattedStartTime = this.allJobsPostedList[index].api.setTime[timer].startTime;
  //         let formattedEndTime = this.allJobsPostedList[index].api.setTime[timer].endTime;
  //         newSetTime.push({
  //           shift: this.allJobsPostedList[index].api.setTime[timer].shift,
  //           startDate:Date.parse (this.allJobsPostedList[index].api.setTime[timer].startDate),
           
  //           endDate:Date.parse(this.allJobsPostedList[index].api.setTime[timer].endDate),
            
  //           startTime: formattedStartTime,
  //           endTime: formattedEndTime,
  //           totalHour:this.allJobsPostedList[index].api.setTime[timer].totalHour
  //         });
  //       }
  //       console.log('push uploadfile',this.allJobsPostedList[index].api.uploadFile)
  //       separatedRoleWiseAction.push({
  //         roleId: roleWiseAction[j].roleId,
  //         roleName: roleWiseAction[j].roleName,
  //         skills: skills,
  //         jobType: this.allJobsPostedList[index].api.jobDetails.jobType,
  //         payType: this.allJobsPostedList[index].api.jobDetails.payType,
  //         pay: this.allJobsPostedList[index].api.jobDetails.pay,
  //         noOfStuff: this.allJobsPostedList[index].api.jobDetails.noOfStuff,
  //         uploadFile:this.allJobsPostedList[index].api.uploadFile,
  //         paymentStatus: this.allJobsPostedList[index].api.paymentStatus,
  //         status: this.allJobsPostedList[index].api.status,
  //         payment: this.allJobsPostedList[index].api.payment,
  //         setTime: newSetTime,
  //         locationName: this.allJobsPostedList[index].api.locationName,
  //         description: this.allJobsPostedList[index].api.description,
  //         distance: this.allJobsPostedList[index].api.distance,
  //         location: this.allJobsPostedList[index].api.location,
  //         locationTrackingRadius: this.allJobsPostedList[index].api.jobDetails.locationTrackingRadius
  //       })
  //       console.log('separatedRoleWiseAction',separatedRoleWiseAction)
  //     }

  //     this.allSeparatedJobs.push({
  //       employerId: this.allJobsPostedList[index].api.employerId,
  //       jobDetails: {
  //         industry: this.allJobsPostedList[index].api.jobDetails.industry,
  //         roleWiseAction: separatedRoleWiseAction
  //       }
  //     });

  //   }

  // }

  // editJobs
  
  editJobs(index) {
    // console.log("editJobs", index, this.fileArray, this.allJobsPostedList[index]);

    this.getJobDetails = this.employerSetGetService.getJobDetails;
    this.getJobLocation = this.employerSetGetService.getJobLocation;
    this.getSkillDetails = this.employerSetGetService.getSkillDetails;
    this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
    this.getUploadedFile = this.employerSetGetService.getFileDetails;
    this.getPaymentDetails = this.employerSetGetService.getPayment;

    // && this.getUploadedFile
    if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
      this.snackBar.open('You have an unsaved job! Please save and proceed', 'Got it!', {
        
      });
    }
    else {
      //***********************************************************************/
      //*          resetting Local storage variables with selected job        */
      //***********************************************************************/

      //*                     1. personal details variables                   */
      let jobDetailsObject = {
        industry: this.allJobsPostedList[index].api.jobDetails.industry,
        industryName: this.allJobsPostedList[index].industryName,
        jobRole: this.allJobsPostedList[index].jobRole,
        jobType: this.allJobsPostedList[index].api.jobDetails.jobType,
        payType: this.allJobsPostedList[index].api.jobDetails.payType,
        pay: this.allJobsPostedList[index].api.jobDetails.pay,
        no_of_stuffs: this.allJobsPostedList[index].api.jobDetails.noOfStuff,
        locationTrackingRadius: this.allJobsPostedList[index].api.jobDetails.locationTrackingRadius
      }
      this.employerSetGetService.setJobDetails(jobDetailsObject);

      //************************* 2.location variables **************************/
      let jobLocation = {
        location: {
          type: "Point",
          coordinates: [
            this.allJobsPostedList[index].api.location.coordinates[0],
            this.allJobsPostedList[index].api.location.coordinates[1]
          ],
        },
        distance: this.allJobsPostedList[index].api.distance,
        locationName: this.allJobsPostedList[index].locationName,
      }
      this.employerSetGetService.setJobLocation(jobLocation);

      //**************************** 3. skill variables ****************************/
      let skill = {
        roleWiseAction: this.allJobsPostedList[index].allSelectedSkills,
        description: this.allJobsPostedList[index].api.description
      }
      this.employerSetGetService.setSkillDetails(skill);

      //************************** 4. date time variables ***************************/
      this.employerSetGetService.setDateTimeDetails({ dateTime: this.allJobsPostedList[index].api.setTime });

      //*********************** 5. uploaded files variables ************************/

      this.employerSetGetService.setFileDetails(this.allJobsPostedList[index].api.uploadFile);
      // this.employerSetGetService.setFileDetails({ uploadedFile: index });

      //*********************** 6. payment files variables ************************/
      this.employerSetGetService.setPayment({ payment: this.allJobsPostedList[index].api.payment });

      //***********************************************************************/
      //******** end resetting Local storage variables with selected job **********/
      //***********************************************************************/

      this.allJobsPostedList.splice(index, 1);
      this.employerSetGetService.setAllJobs(this.allJobsPostedList);
      this.router.navigate(['/employer/job/post/details']);
    }

  }

  // deleteJobs from current job list
  deleteJobs(index) {
    if (this.allJobsPostedList[index]) {
      this.allJobsPostedList.splice(index, 1);
      this.employerSetGetService.setAllJobs(this.allJobsPostedList);
    }

    if (this.allJobsPostedList.length < 1) {
      this.proceedToSummary = false;
    }

  }

  // clear localstorage variables for individual job value
  clearIndividualJobVariablesInLS() {
    localStorage.removeItem('JobDetails');
    localStorage.removeItem('uploadedFile');
    localStorage.removeItem('Payment');
    localStorage.removeItem('JobSkills');
    localStorage.removeItem('JobLocation');
    localStorage.removeItem('Datetime');
    localStorage.removeItem('SaveTimeJobPost');
  }

  //save the current job list to local storage and proceed to summary page
  saveAndProceedToSummary() {
    if(this.isApiEdit){
      this.deactiveValue = true;
      let donotdelete = "donotdelete";
      localStorage.setItem('donotdelete',donotdelete);
    this.saveJobToList();
    console.log("edit");

    }else if(!this.isApiEdit){
      this.saveJob();
      console.log("not edit");
      this.getJobDetails = this.employerSetGetService.getJobDetails;
      this.getJobLocation = this.employerSetGetService.getJobLocation;
      this.getSkillDetails = this.employerSetGetService.getSkillDetails;
      this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
      this.getUploadedFile = this.employerSetGetService.getFileDetails;
      this.getPaymentDetails = this.employerSetGetService.getPayment;
      // this.saveJobToList();
  
      //&& this.getUploadedFile
      if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getUploadedFile && this.getPaymentDetails) {
        this.snackBar.open('You have an unsaved job! Please save and proceed', 'Got it!', {
         
        });
        console.log("edit cheack");
      }
      else {
        localStorage.removeItem('allJobs');
        this.employerSetGetService.setAllJobs(this.allJobsPostedList);
        // this.clearIndividualJobVariablesInLS();
        let getValue = this.employerSetGetService.getAllJob;
        if (getValue) {
          // this.snackBar.open('PROCEED TO SUBMIT JOB ', 'Got it!', {
          //   duration: 3000,
          // });
          //show alert and navigate to job-post component
          this.router.navigate(['/employer/job/job-summary']);
        }
      }
    }

  }

  // START JOB TEMPLATE FUNCTIONALITIES



  openTemplateSave(templateSave: TemplateRef<any>) {
    this.modalService.show(templateSave);
  }

}

//======================correct one=================

// import { Component, OnInit, TemplateRef } from '@angular/core';
// import { DetailsComponent } from './details/details.component';
// import { EmployerJobService } from '../../../services';
// // import { Router } from '@angular/router';
// import { MatSnackBar } from '@angular/material';
// import { FormControl, Validators } from '@angular/forms';
// import { takeUntil } from 'rxjs/operators';
// import { Subject } from 'rxjs';
// import { SelectedFileService } from '../../../services/selected-file.service';
// import { Router, ActivatedRoute } from '@angular/router';
// import { BsModalService, BsModalRef } from 'ngx-bootstrap';
// import { setHours } from 'ngx-bootstrap/chronos/utils/date-setters';
// import * as moment from 'moment';
// @Component({
//   selector: 'employer-job-post',
//   templateUrl: './job-post.component.html',
//   styleUrls: ['./job-post.component.scss'],
//   providers: [DetailsComponent],

// })
// export class JobPostComponent implements OnInit {

//   private onDestroyUnSubscribe = new Subject<void>();
//   saveTemplateForm = new FormControl('', Validators.required);
//   templateNameForm = new FormControl('', Validators.required);
//   proceedToSummary: Boolean = false;
//   saveCurrentJob: Boolean = false;
//   getJobDetails;
//   getJobLocation;
//   getSkillDetails;
//   getTimeDetails;
//   getUploadedFile;
//   getPaymentDetails;
//   allJobsPostedList = [];
//   fileArray = [];
//   isApiEdit: Boolean = false;
//   jobUrlType;
//   //job template related variables
//   saveAstemplatePermissionModal: BsModalRef;
//   templateEvent;
//   templateNameModal: BsModalRef
//   public templateName: string = ""

//   //job template related variables
//   modalRef: BsModalRef;
//   isSavedTemplate: boolean = false
//   constructor(
//     public router: Router,
//     private employerSetGetService: EmployerJobService,
//     public snackBar: MatSnackBar,
//     private sfs: SelectedFileService,
//     private modalService: BsModalService,
//     private activatedRouter: ActivatedRoute
//   ) { }

//   ngOnInit() {

//     this.jobUrlType = this.activatedRouter.snapshot.url.join().split(',');

//     if (this.jobUrlType[0] === "edit") {
//       this.isApiEdit = true;
//       if (localStorage.getItem('JobDetailsById')) {   //call job details for edit posted job
//         this.getJobDetailsById();
//       }
//     } else if (this.jobUrlType[0] === "post") {
//       this.isApiEdit = false;
//       let allJobsList = this.employerSetGetService.getAllJob;
//       if (allJobsList && allJobsList.length) {
//         this.allJobsPostedList = allJobsList;
//         this.proceedToSummary = true;
//       }
//     }

//     this.getJobDetails = this.employerSetGetService.getJobDetails;
//     this.getJobLocation = this.employerSetGetService.getJobLocation;
//     this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//     this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//     this.getUploadedFile = this.employerSetGetService.getFileDetails;

//     this.getPaymentDetails = this.employerSetGetService.getPayment;
//     this.isSavedTemplate = Boolean(this.employerSetGetService.getLocalStorage("isSavedTemplate"))
//     if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
//       this.saveCurrentJob = true;
//     }

//   }
//   //open modal for save as template
//   openTemplatePermissionModal(event, template: TemplateRef<any>) {
//     this.templateEvent = event
//     if (event.target.checked) {
//       console.log("checked")
//       this.saveAstemplatePermissionModal = this.modalService.show(template, { backdrop: 'static', keyboard: false });
//     }
//   }
//   //if user does't want to save as template
//   backtoUncheckTemplateSaveCheckbox() {
//     this.templateEvent.target.checked = false
//     this.saveAstemplatePermissionModal.hide()
//   }
//   //open template name modal
//   openTemplateNameModal(template: TemplateRef<any>) {
//     this.saveAstemplatePermissionModal.hide()
//     this.templateNameModal = this.modalService.show(template, { backdrop: 'static', keyboard: false });
//   }
//   //save job as template
//   saveJobAsTemplate() {

//     let postJobDetails = this.getPostJobTemplate();

//     this.employerSetGetService.postJobAsTemplate(postJobDetails).subscribe((response) => {
//       this.templateNameModal.hide()
//       if (response && !response['isError']) {
//         this.employerSetGetService.setLocalStorage("isSavedTemplate", true)
//         this.isSavedTemplate = true

//         this.snackBar.open('Template has been created successfully', 'Got it!', {
//           duration: 3000,
//         });
//       }
//       else {
//         this.snackBar.open(response['message'], 'Got it!', {
//           duration: 3000,
//         });
//       }
//     }, (err) => {
//       this.snackBar.open('Network error', 'Got it!', {
//         duration: 3000,
//       });
//     })
//   }
//   //get post job template
//   getPostJobTemplate() {
//     this.getJobDetails = this.employerSetGetService.getJobDetails;
//     this.getJobLocation = this.employerSetGetService.getJobLocation;
//     this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//     this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//     this.getUploadedFile = this.employerSetGetService.getFileDetails;
//     this.getPaymentDetails = this.employerSetGetService.getPayment;
//     let postJobTemplate: any = {}
//     postJobTemplate.employerId = JSON.parse(localStorage.getItem('currentUser')).employerId
//     // postJobTemplate.roleWiseAction.locationTrackingRadius = this.getJobDetails.locationTrackingRadius,

//     postJobTemplate.templateName = this.templateName
//     postJobTemplate.jobDetails = {}
//     postJobTemplate.jobDetails.industry = this.getJobDetails.industry;
//      postJobTemplate.jobDetails.locationTrackingRadius = this.getJobDetails.locationTrackingRadius;


//     let newSetTime = [];
//     for (let timer = 0; timer < this.getTimeDetails.dateTime.length; timer++) {

//       // let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
//       // console.log('formattedStartTime',formattedStartTime)
//       //new Date 
//       // let formattedEndTime =this.getTimeDetails.dateTime[timer].endTime
// /*       let formattedStartTime = moment(this.getTimeDetails.dateTime[timer].startTime).format("hh:mm").replace(':','.')
//       let formattedEndTime = moment(this.getTimeDetails.dateTime[timer].endTime).format("hh:mm").replace(':','.') */
//       let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
//       let formattedEndTime = this.getTimeDetails.dateTime[timer].endTime
//       console.log('job template endtime',  moment(this.getTimeDetails.dateTime[timer].endTime).format("hh:mm").replace(':','.'))
//       //new Date .toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

//       newSetTime.push({
//         shift: this.getTimeDetails.dateTime[timer].shift,
//         startDate:Date.parse(this.getTimeDetails.dateTime[timer].startDate),
//         endDate:Date.parse(this.getTimeDetails.dateTime[timer].endDate),
//         startTime: formattedStartTime,
//         endTime: formattedEndTime,
//         totalHour:this.getTimeDetails.dateTime[timer].totalHour
//       });
//     }

//     // golam 

//     let roleWiseAction = [];
//     console.log('getSkillDetails', this.getSkillDetails.roleWiseAction);

//     Object.values(this.getSkillDetails.roleWiseAction).map(
//       (element) => {
//         let skillIds = [];
//         let name;
//         let id;
//         for (let i = 0; i < element['skills'].length; i++) {
//           skillIds[i]={id:element['skills'][i].id,name:element['skills'][i].name};
//         }
//         console.log('skillIds', skillIds);

//         let details = {
//           roleId: element["roleId"],
//           roleName: element["roleName"],
//           skills: skillIds,
//           jobType: this.getJobDetails.jobType,
//           payType: this.getJobDetails.payType,
//           pay: this.getJobDetails.pay,
//           noOfStuff: this.getJobDetails.no_of_stuffs,
//           locationTrackingRadius:this.getJobDetails.locationTrackingRadius,
//           uploadFile: this.getUploadedFile,
//           paymentStatus: "UNPAID",
//           status: "POSTED",
//           payment: this.getPaymentDetails.payment,
//           setTime: newSetTime,
//           locationName: this.getJobLocation.locationName,
//           description: this.getSkillDetails.description,
//           distance: this.getJobLocation.distance,
//           location: this.getJobLocation.location
//         }
//         roleWiseAction.push(details);

//       }
//     );
//     postJobTemplate.jobDetails.roleWiseAction = roleWiseAction;
//     return postJobTemplate
//   }

//   // get JobPosted List
//   getJobDetailsById() {

//     let requiredData = {
//       employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
//       jobId: JSON.parse(localStorage.getItem('JobDetailsById')).jobId,
//       roleId: JSON.parse(localStorage.getItem('JobDetailsById')).roleId
//     }

//     this.employerSetGetService.getEditJobDetailsById(requiredData)
//       .pipe(takeUntil(this.onDestroyUnSubscribe))
//       .subscribe(
//         result => {
//           if (result && !result['isError']) {
//             let jobDetailsList = result['details'];
//             console.log('jobDetailsList', jobDetailsList);
//             localStorage.setItem('editJobId', result['details'].jobId);
//             // localStorage.setItem('alljobs',result['details']);
//           // let alljob =  this.employerSetGetService.setAllJobs(jobDetailsList);
  
//             localStorage.removeItem('JobDetailsById');

//             //***********************************************************************/
//             //*          resetting Local storage variables with selected job        */
//             //***********************************************************************/

//             //*                     1. personal details variables                   */

//             let jobRoleObject = {
//               // createdAt: "2019-05-22T13:31:41.090Z",
//               industryId: jobDetailsList.jobDetails.industryDetails.industryId,
//               jobRoleId: jobDetailsList.jobDetails.roleWiseAction.roleId,
//               jobRoleName: jobDetailsList.jobDetails.roleWiseAction.roleName,
//               skillIds: jobDetailsList.jobDetails.roleWiseAction.skills,
//               // updatedAt: "2019-06-04T05:34:46.056Z"
//             }

//             let jobDetailsObject = {
//               industry: jobDetailsList.jobDetails.industryDetails.industryId,
//               industryName: jobDetailsList.jobDetails.industryDetails.industryName,
//               jobRole: [jobRoleObject],
//               jobType: jobDetailsList.jobDetails.roleWiseAction.jobType,
//               payType: jobDetailsList.jobDetails.roleWiseAction.payType,
//               pay: jobDetailsList.jobDetails.roleWiseAction.pay,
//               no_of_stuffs: jobDetailsList.jobDetails.roleWiseAction.noOfStuff,
//               locationTrackingRadius: jobDetailsList.jobDetails.roleWiseAction.locationTrackingRadius,

//             }

//             // console.log(" to set jobDetailsObject", jobDetailsObject);

//             this.employerSetGetService.setJobDetails(jobDetailsObject);

//             //************************* 2.location variables **************************/
//             let jobLocation = {
//               location: {
//                 type: "Point",
//                 coordinates: [
//                   jobDetailsList.jobDetails.roleWiseAction.location.coordinates[0],
//                   jobDetailsList.jobDetails.roleWiseAction.location.coordinates[1]
//                 ],
//               },
//               distance: jobDetailsList.jobDetails.roleWiseAction.distance,
//               locationName: jobDetailsList.jobDetails.roleWiseAction.locationName,
//             }

//             this.employerSetGetService.setJobLocation(jobLocation);

//             //**************************** 3. skill variables ****************************/

//             let jobRoleId = jobDetailsList.jobDetails.roleWiseAction.roleId;

//             let skillSet = [];
//             for (var i = 0; i < jobDetailsList.jobDetails.roleWiseAction.skills.length; i++) {
//               skillSet.push({ id: jobDetailsList.jobDetails.roleWiseAction.skills[i].id,name: jobDetailsList.jobDetails.roleWiseAction.skills[i].name });
            

//             }
//             let roleWiseAction = {
//               [jobRoleId]: {
//                 roleId: jobRoleId,
//                 roleName: jobDetailsList.jobDetails.roleWiseAction.roleName,
//                 skills: skillSet
//               }
//             };


//             let setSkill = {
//               roleWiseAction: roleWiseAction,
//               description: jobDetailsList.jobDetails.roleWiseAction.description
//             };


//             this.employerSetGetService.setSkillDetails(setSkill);

//             //************************** 4. date time variables ***************************/

//             let newSetTime = [];
//             for (let timer = 0; timer <  jobDetailsList.jobDetails.roleWiseAction.setTime .length; timer++) {
        
//               // let formattedStartTime = this.getTimeDetails.dateTime[timer].startTime
//               // console.log('formattedStartTime',formattedStartTime)
//               //new Date 
//               //  let formattedEndTime =this.getTimeDetails.dateTime[timer].endTime
//               // let formattedStartTime = moment(jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startTime).format("hh:mm").replace(':','.');
//               // let formattedEndTime = moment(jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endTime).format("hh:mm").replace(':','.');
//               let formattedStartTime =  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startTime
//               let formattedEndTime =  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endTime
//               console.log('job template endtime',  moment( jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .endTime).format("HHmm"));
//               console.log(' template endtime',  moment( jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .endTime));

//               //new Date .toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        
//               newSetTime.push({
//                 shift:  jobDetailsList.jobDetails.roleWiseAction.setTime[timer].shift,
//                 startDate:new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].startDate).toISOString(),
//                 endDate:new Date( jobDetailsList.jobDetails.roleWiseAction.setTime[timer].endDate).toISOString(),
//                 startTime: formattedStartTime,
//                 endTime: formattedEndTime,
//                 totalHour: jobDetailsList.jobDetails.roleWiseAction.setTime[timer] .totalHour
//               });
//             }

//             this.employerSetGetService.setDateTimeDetails({ dateTime: newSetTime });

//             //*********************** 5. uploaded files variables ************************/jobDetailsList.jobDetails.roleWiseAction.setTime

//             this.employerSetGetService.setFileDetails(jobDetailsList.jobDetails.roleWiseAction.uploadFile);

//             //*********************** 6. payment files variables ************************/

//             this.employerSetGetService.setPayment({ payment: jobDetailsList.jobDetails.roleWiseAction.payment });

//             //***********************************************************************/
//             //******** end resetting Local storage variables with selected job **********/
//             //***********************************************************************/

//             this.saveCurrentJob = true;
//             this.getJobDetails = this.employerSetGetService.getJobDetails;
//             this.getJobLocation = this.employerSetGetService.getJobLocation;
//             this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//             this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//             this.getUploadedFile = this.employerSetGetService.getFileDetails;
//             this.getPaymentDetails = this.employerSetGetService.getPayment;

//           }
//         },
//         error => {
//           this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
//             duration: 3000,
//           });
//         }
//       );
//   }


//   //save current job to template
//   checkSaveTemplate(event) {
//     if (!event.target.checked) {
//       this.saveTemplateForm.reset();
//     }
//   }

//   //save Job To job List and Local storage updatation
//   saveJobToList() {
//     this.getDetailsFromLocalStorage();
//   this.employerSetGetService.setAllJobs(this.allJobsPostedList);

//     this.saveCurrentJob = false;
//     if (this.isApiEdit) {


//       this.router.navigate(['/employer/job/job-summary']);
   

//       this.proceedToSummary = false;
//     }
//     // else if (!this.isApiEdit) {
   

//     //   this.proceedToSummary = true;
//     //   this.clearIndividualJobVariablesInLS(); //clear local storage function called
   

//     // }
   
//   }
//   saveJob() {
//     this.getDetailsFromLocalStorage();
    

//    this.employerSetGetService.setAllJobs(this.allJobsPostedList);
 

//     this.saveCurrentJob = false;
//     // if (this.isApiEdit) {
  

//     //   this.router.navigate(['/employer/job/job-summary']);

//     //   this.proceedToSummary = false;
//     // }
//     if (!this.isApiEdit) {


//       this.proceedToSummary = true;
//       this.clearIndividualJobVariablesInLS(); //clear local storage function called
  

//     }
   
//   }

//   //setting job list with latest values
//   getDetailsFromLocalStorage() {


//     // && this.getUploadedFile
//     if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {

//       let allJobDetails = {
//         api: {
//           employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
//           paymentStatus: "UNPAID",
//           status: "POSTED",
//           setTime: this.getTimeDetails.dateTime,
//           uploadFile: this.getUploadedFile,
//           payment: this.getPaymentDetails.payment,
//           jobDetails: {
//             industry: this.getJobDetails.industry,
//             jobType: this.getJobDetails.jobType,
//             payType: this.getJobDetails.payType,
//             pay: this.getJobDetails.pay,
//             noOfStuff: this.getJobDetails.no_of_stuffs,
//             roleWiseAction: Object.values(this.getSkillDetails.roleWiseAction),
//             locationTrackingRadius: this.getJobDetails.locationTrackingRadius
//           },
//           description: this.getSkillDetails.description,
//           distance: this.getJobLocation.distance,
//           location: this.getJobLocation.location,
//           locationName: this.getJobLocation.locationName
//         },
//         jobRole: this.getJobDetails.jobRole,
//         allSelectedSkills: this.getSkillDetails.roleWiseAction,
//         industryName: this.getJobDetails.industryName,
//         locationName: this.getJobLocation.locationName,
//         isApiEdit: this.isApiEdit,
//         // jobRole: Object.keys(this.getSkillDetails.roleWiseAction),
//       }
//       this.allJobsPostedList.push(allJobDetails);

//       //  this.clearIndividualJobVariablesInLS();
//     }
//   }

//   // editJobs
//   editJobs(index) {
//     // console.log("editJobs", index, this.fileArray, this.allJobsPostedList[index]);

//     this.getJobDetails = this.employerSetGetService.getJobDetails;
//     this.getJobLocation = this.employerSetGetService.getJobLocation;
//     this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//     this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//     this.getUploadedFile = this.employerSetGetService.getFileDetails;
//     this.getPaymentDetails = this.employerSetGetService.getPayment;

//     // && this.getUploadedFile
//     if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
//       this.snackBar.open('YOU HAVE AN UNSAVED JOB...kindly SAVE and PROCEED !', 'Got it!', {
//         duration: 3000,
//       });
//     }
//     else {
//       //***********************************************************************/
//       //*          resetting Local storage variables with selected job        */
//       //***********************************************************************/

//       //*                     1. personal details variables                   */
//       let jobDetailsObject = {
//         industry: this.allJobsPostedList[index].api.jobDetails.industry,
//         industryName: this.allJobsPostedList[index].industryName,
//         jobRole: this.allJobsPostedList[index].jobRole,
//         jobType: this.allJobsPostedList[index].api.jobDetails.jobType,
//         payType: this.allJobsPostedList[index].api.jobDetails.payType,
//         pay: this.allJobsPostedList[index].api.jobDetails.pay,
//         no_of_stuffs: this.allJobsPostedList[index].api.jobDetails.noOfStuff,
//         locationTrackingRadius: this.allJobsPostedList[index].api.jobDetails.locationTrackingRadius
//       }
//       this.employerSetGetService.setJobDetails(jobDetailsObject);

//       //************************* 2.location variables **************************/
//       let jobLocation = {
//         location: {
//           type: "Point",
//           coordinates: [
//             this.allJobsPostedList[index].api.location.coordinates[0],
//             this.allJobsPostedList[index].api.location.coordinates[1]
//           ],
//         },
//         distance: this.allJobsPostedList[index].api.distance,
//         locationName: this.allJobsPostedList[index].locationName,
//       }
//       this.employerSetGetService.setJobLocation(jobLocation);

//       //**************************** 3. skill variables ****************************/
//       let skill = {
//         roleWiseAction: this.allJobsPostedList[index].allSelectedSkills,
//         description: this.allJobsPostedList[index].api.description
//       }
//       this.employerSetGetService.setSkillDetails(skill);

//       //************************** 4. date time variables ***************************/
//       this.employerSetGetService.setDateTimeDetails({ dateTime: this.allJobsPostedList[index].api.setTime });

//       //*********************** 5. uploaded files variables ************************/

//       this.employerSetGetService.setFileDetails(this.allJobsPostedList[index].api.uploadFile);
//       // this.employerSetGetService.setFileDetails({ uploadedFile: index });

//       //*********************** 6. payment files variables ************************/
//       this.employerSetGetService.setPayment({ payment: this.allJobsPostedList[index].api.payment });

//       //***********************************************************************/
//       //******** end resetting Local storage variables with selected job **********/
//       //***********************************************************************/

//       this.allJobsPostedList.splice(index, 1);
//       this.employerSetGetService.setAllJobs(this.allJobsPostedList);
//       this.router.navigate(['/employer/job/post/details']);
//     }

//   }

//   // deleteJobs from current job list
//   deleteJobs(index) {
//     if (this.allJobsPostedList[index]) {
//       this.allJobsPostedList.splice(index, 1);
//       this.employerSetGetService.setAllJobs(this.allJobsPostedList);
//     }

//     if (this.allJobsPostedList.length < 1) {
//       this.proceedToSummary = false;
//     }

//   }

//   // clear localstorage variables for individual job value
//   clearIndividualJobVariablesInLS() {
//     localStorage.removeItem('JobDetails');
//     localStorage.removeItem('uploadedFile');
//     localStorage.removeItem('Payment');
//     localStorage.removeItem('JobSkills');
//     localStorage.removeItem('JobLocation');
//     localStorage.removeItem('Datetime');
//     localStorage.removeItem('SaveTimeJobPost');
//   }

//   //save the current job list to local storage and proceed to summary page
//   saveAndProceedToSummary() {
//     if(this.isApiEdit){

//     this.saveJobToList();
//     console.log("edit");

//     }else if(!this.isApiEdit){
//       this.saveJob();
//       console.log("not edit");
//       this.getJobDetails = this.employerSetGetService.getJobDetails;
//       this.getJobLocation = this.employerSetGetService.getJobLocation;
//       this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//       this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//       this.getUploadedFile = this.employerSetGetService.getFileDetails;
//       this.getPaymentDetails = this.employerSetGetService.getPayment;
//       // this.saveJobToList();
  
//       //&& this.getUploadedFile
//       if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getUploadedFile && this.getPaymentDetails) {
//         this.snackBar.open('YOU HAVE AN UNSAVED JOB...kindly SAVE and PROCEED !', 'Got it!', {
//           duration: 3000,
//         });
//         console.log("edit cheack");
//       }
//       else {
//         localStorage.removeItem('allJobs');
//         this.employerSetGetService.setAllJobs(this.allJobsPostedList);
//         // this.clearIndividualJobVariablesInLS();
//         let getValue = this.employerSetGetService.getAllJob;
//         if (getValue) {
//           this.snackBar.open('PROCEED TO SUBMIT JOB ', 'Got it!', {
//             duration: 3000,
//           });
//           //show alert and navigate to job-post component
//           this.router.navigate(['/employer/job/job-summary']);
//         }
//       }
//     }

//   }

//   // START JOB TEMPLATE FUNCTIONALITIES



//   openTemplateSave(templateSave: TemplateRef<any>) {
//     this.modalService.show(templateSave);
//   }

// }
