
import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { EmployerJobService } from '../../../services';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { SelectedFileService } from '../../../services/selected-file.service';
import { GlobalActionsService } from '../../../../../../../src/app/services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Location,PlatformLocation } from '@angular/common';
@Component({
  selector: 'employer-job-summary',
  templateUrl: './job-summary.component.html',
  styleUrls: ['./job-summary.component.scss']
})
export class JobSummaryComponent implements OnInit {
  @ViewChild('templateJobOffer') templateJobOffer: TemplateRef<any>;
  public termsConditionControl = new FormControl('', Validators.required);
  countOfJobRoles: Number; //variable to get the total payment structure
  private onDestroyUnSubscribe = new Subject<void>();
  selectedFile: any;
  userDetails: any;
  allJobsList = [];
  allSeparatedJobs = [];
  totalAmolunt:number = 0;
  ni:number = 0;
  vat:number = 0;
  fee:number = 0;

  rolewise = [];
  getJobDetails;
  getJobLocation;
  getSkillDetails;
  getTimeDetails;
  getUploadedFile;
  getPaymentDetails;
  proceedToSummary:boolean = true;
  deactiveValue:boolean = false;
  
  saveAstemplatePermissionModal: BsModalRef;
  templateEvent;
  templateNameModal: BsModalRef
  public templateName: string = ""
  modalRef: BsModalRef;
  donotdelete: string;
  constructor(
    private modalService: BsModalService,
    public location:Location, 
    private _location: PlatformLocation,
    private employerSetGetService: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router,
    private sfs: SelectedFileService,
    private globalActionsService: GlobalActionsService,
  ) {
    this.sfs.currentFileArray.subscribe(v => {
      this.selectedFile = v;
    })
   
  }

  ngOnInit() {
    this.donotdelete = localStorage.getItem('donotdelete');

    this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
    let getAllJobs = this.employerSetGetService.getAllJob;
    this.allJobsList = getAllJobs;
    for(let i =0; i<this.allJobsList.length;i++) {
      this.totalAmolunt += parseInt(this.allJobsList[i].api.payment.totalPayment) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.ni += parseInt(this.allJobsList[i].api.payment.ni) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.vat += parseInt(this.allJobsList[i].api.payment.vat) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.fee += parseInt(this.allJobsList[i].api.payment.fee) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)

      this.rolewise[i] = this.allJobsList[i].api.jobDetails.roleWiseAction
    }
    console.log("Amount",this.totalAmolunt);
    console.log("role",this.rolewise)
    if (this.allJobsList) {
      this.separateJobsByRoles();
    }
    else {
      this.router.navigate(['/employer/job/post']);
    }
    this.getJobDetails = this.employerSetGetService.getJobDetails;
    this.getJobLocation = this.employerSetGetService.getJobLocation;
    this.getSkillDetails = this.employerSetGetService.getSkillDetails;
    this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
    this.getUploadedFile = this.employerSetGetService.getFileDetails;
    this.getPaymentDetails = this.employerSetGetService.getPayment;
    console.log("kkkk",this.getJobDetails);


  

  }

  canDeactivate() {
    if(!this.deactiveValue && !this.donotdelete) {
      let r = confirm('WARNING: You have unsaved changes. Press Cancel to go back and save these changes, or OK to lose these changes.')
      console.log('r',r);
      if(r == true) {
        this.allJobsList = [];
        localStorage.removeItem('allJobs');
        this.proceedToSummary = false;
      return  true;

      }
      else {
        return false;
      }

    }
    else {
      return true;
    }
  }


  editJobs(index) {
    // console.log("editJobs", index, this.fileArray, this.allJobsPostedList[index]);
    this.deactiveValue = true;
    this.getJobDetails = this.employerSetGetService.getJobDetails;
    this.getJobLocation = this.employerSetGetService.getJobLocation;
    this.getSkillDetails = this.employerSetGetService.getSkillDetails;
    this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
    this.getUploadedFile = this.employerSetGetService.getFileDetails;
    this.getPaymentDetails = this.employerSetGetService.getPayment;
 console.log("k2",this.getJobDetails);
    // && this.getUploadedFile
    if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
    this.router.navigate(['/employer/job/edit']);

    }
    else {
      //***********************************************************************/
      //*          resetting Local storage variables with selected job        */
      //***********************************************************************/

      //*                     1. personal details variables                   */
      let jobDetailsObject = {
        industry: this.allJobsList[index].api.jobDetails.industry,
        industryName: this.allJobsList[index].industryName,
        jobRole: this.allJobsList[index].jobRole,
        jobType: this.allJobsList[index].api.jobDetails.jobType,
        payType: this.allJobsList[index].api.jobDetails.payType,
        pay: this.allJobsList[index].api.jobDetails.pay,
        no_of_stuffs: this.allJobsList[index].api.jobDetails.noOfStuff,
        locationTrackingRadius: this.allJobsList[index].api.jobDetails.locationTrackingRadius
      }
      this.employerSetGetService.setJobDetails(jobDetailsObject);

      //************************* 2.location variables **************************/
      let jobLocation = {
        location: {
          type: "Point",
          coordinates: [
            this.allJobsList[index].api.location.coordinates[0],
            this.allJobsList[index].api.location.coordinates[1]
          ],
        },
        distance: this.allJobsList[index].api.distance,
        locationName: this.allJobsList[index].locationName,
      }
      this.employerSetGetService.setJobLocation(jobLocation);

      //**************************** 3. skill variables ****************************/
      let skill = {
        roleWiseAction: this.allJobsList[index].allSelectedSkills,
        description: this.allJobsList[index].api.description
      }
      this.employerSetGetService.setSkillDetails(skill);

      //************************** 4. date time variables ***************************/
      this.employerSetGetService.setDateTimeDetails({ dateTime: this.allJobsList[index].api.setTime });

      //*********************** 5. uploaded files variables ************************/

      this.employerSetGetService.setFileDetails(this.allJobsList[index].api.uploadFile);
      // this.employerSetGetService.setFileDetails({ uploadedFile: index });

      //*********************** 6. payment files variables ************************/
      this.employerSetGetService.setPayment({ payment: this.allJobsList[index].api.payment });

      //***********************************************************************/
      //******** end resetting Local storage variables with selected job **********/
      //***********************************************************************/

      this.allJobsList.splice(index, 1);
      this.employerSetGetService.setAllJobs(this.allJobsList);
      this.router.navigate(['/employer/job/post']);
    }

  }
  deleteJobs(index) {
    this.allJobsList.splice(index, 1);
    this.employerSetGetService.setAllJobs(this.allJobsList);
    this.totalAmolunt = 0
    this.ni = 0
    this.vat = 0
    this.fee = 0

      if (this.allJobsList.length == 0) {
      this.proceedToSummary = false;
    }

    for(let i =0; i<this.allJobsList.length;i++) {
      console.log("alljobs",this.allJobsList)
      console.log("Amount",this.totalAmolunt);

      this.totalAmolunt += parseInt(this.allJobsList[i].api.payment.totalPayment) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.ni += parseInt(this.allJobsList[i].api.payment.ni) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.vat += parseInt(this.allJobsList[i].api.payment.vat) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.fee += parseInt(this.allJobsList[i].api.payment.fee) * parseInt(this.allJobsList[i].api.jobDetails.roleWiseAction.length)
      this.rolewise[i] = this.allJobsList[i].api.jobDetails.roleWiseAction
      console.log("role",this.rolewise[i]);
      console.log("Amount2",this.totalAmolunt);

    }
  
   


    // if (this.allJobsList[index] && !this.donotdelete) {
    // }
    // else {
    //   this.snackBar.open('Edit job can not delete','Got it!',{

    //   })
    // }

    // if (this.allJobsList.length < 1) {
    //   this.proceedToSummary = false;
    // }

  }


  //for checking the terms and conditions control
  checkControl(event) {
    if (!event.target.checked) {
      this.termsConditionControl.reset();
    }
  }

  // check profile submited or not //
  checkProfile() {
    let user_details = JSON.parse(localStorage.getItem('currentUser'))
    let data = {
      employerId:user_details.employerId
    }
    this.employerSetGetService._checkProfile(data)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(
      result => {
        if(result && !result['isError'] && result['details']) {
          if(result['details'].isEmployerAuthentic == "True") {
            this.deactiveValue = true;
            this.saveAllJobPostDetails()
          }
          else {
            this.snackBar.open(result['details'].message,'Got it!',{
            
            })
          }
        }
        else {
          this.snackBar.open(result['message'],'Got it!',{
            
          })
        }
      },
      error =>{

      }
      )
  }

  //save all the jobs
  async saveAllJobPostDetails() {

    this.termsConditionControl.reset();
    if(this.allJobsList.length == 0) {
      this.snackBar.open('No job to submit click on add more jobs','Got it!',{

      })
      return false;
    }
    //check if job is coming from api or local storage invoking to separate services for two of these
    if (this.allJobsList[0].isApiEdit) {
      //call post job api
      console.log("call update job api", this.allJobsList[0]);

      let allSkillsToUpdate = [];
      //format skills
      for (let j = 0; j < this.allJobsList[0].api.jobDetails.roleWiseAction.length; j++) {
        let skills = [];
        for (let k = 0; k < this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills.length; k++) {
          if (this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k].id) {
            skills.push(this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k].id);
          }
          else {
            skills.push(this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k]);
          }
        }
        allSkillsToUpdate = skills;
      }
      let newSetTime = [];

      for (let timer = 0; timer < this.allJobsList[0].api.setTime.length; timer++) {
        let formattedStartTime = this.allJobsList[0].api.setTime[timer].startTime;
        let formattedEndTime = this.allJobsList[0].api.setTime[timer].endTime;
        newSetTime.push({
          shift: this.allJobsList[0].api.setTime[timer].shift,
          startDate:Date.parse (this.allJobsList[0].api.setTime[timer].startDate),
          // startDate:(new Date(this.allJobsList[index].api.setTime[timer].startDate)).getTime() / 1000,
          endDate:Date.parse(this.allJobsList[0].api.setTime[timer].endDate),
          // endDate:(new Date(this.allJobsList[index].api.setTime[timer].endDate)).getTime() / 1000,
          startTime: formattedStartTime,
          endTime: formattedEndTime,
          totalHour:this.allJobsList[0].api.setTime[timer].totalHour
        });
      }

      let allJobDetails = {
        roleId: this.allJobsList[0].jobRole[0].jobRoleId,
        jobId: localStorage.getItem('editJobId'),
        jobDetails: {
          location: {
            type: "Point",
            coordinates: [
              this.allJobsList[0].api.location.coordinates[0],
              this.allJobsList[0].api.location.coordinates[1]
            ]
          },
          skills: allSkillsToUpdate,
          roleId: this.allJobsList[0].jobRole[0].jobRoleId,
          roleName: this.allJobsList[0].jobRole[0].jobRoleName,
          jobType: this.allJobsList[0].api.jobDetails.jobType,
          payType: this.allJobsList[0].api.jobDetails.payType,
          pay: this.allJobsList[0].api.jobDetails.pay,
          noOfStuff: this.allJobsList[0].api.jobDetails.noOfStuff,
          uploadFile: this.allJobsList[0].api.uploadFile,
          paymentStatus: "UNPAID",
          status: "POSTED",
          payment: this.allJobsList[0].api.payment,
          locationName: this.allJobsList[0].api.locationName,
          description: this.allJobsList[0].api.description,
          setTime:newSetTime,
          distance: this.allJobsList[0].api.distance,
          locationTrackingRadius: this.allJobsList[0].api.jobDetails.locationTrackingRadius
        },
        localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone
      };

      console.log("update job allJobDetails", allJobDetails);

      //call api and on success remove the local items
      this.employerSetGetService.updateJob(allJobDetails)
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError']) {
              this.snackBar.open('Job details updated', 'Got it!', {
               
              });
              localStorage.removeItem('donotdelete');
              localStorage.removeItem('allJobs');
              localStorage.removeItem('JobDetailsById');
              localStorage.removeItem('editJobId');
              localStorage.removeItem('JobDetails');
              localStorage.removeItem('uploadedFile');
              localStorage.removeItem('Payment');
              localStorage.removeItem('JobSkills');
              localStorage.removeItem('JobLocation');
              localStorage.removeItem('Datetime');
              localStorage.removeItem('SaveTimeJobPost');
              localStorage.removeItem('jobEdit');

              this.modalRef = this.modalService.show(this.templateJobOffer);
              this.router.navigate(['/employer/dashboard/job-posted']);
              
            }
          });
    } else {
      console.log("call post job api");

      let fileArray = [];
      this.sfs.currentFileArray.subscribe(array => {
        if (array) {
          fileArray = array;
        }
      })
      //&& fileArray.length
      if (this.allSeparatedJobs) {

        this.snackBar.open('Please wait for sometime..', 'Got it!', {
         
        });

        this.employerSetGetService.postJob({ jobdetails: this.allSeparatedJobs ,localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone})
          .pipe(takeUntil(this.onDestroyUnSubscribe))
          .subscribe(
            result => {
              this.snackBar.open(result['message'],"Got it!",{
              
              });
              if (result && !result['isError']) {
                this.snackBar.open(result['message'],"Got it!",{
                 
                });
                let jobIds = [];
                let fetchId = [];
                fetchId =result['details'][0].jobId;
                
                for(let i=0; i<result['details'].length;i++){
                  jobIds[i] =result['details'][i].jobId;
                 
                }
                let roleId = [];
                let rolesId =[];
                // roleId =;

                rolesId = result['details'][0].jobDetails.roleWiseAction[0].roleId

                // for(let i=0; i<result['details'].length;i++){
                //   roleId[i] =result['details'][i];

                //   for(let j = 0; j<roleId[i].jobDetails.roleWiseAction.length ; j++) {
                //     rolesId[j] = roleId[i].jobDetails.roleWiseAction[j].roleId
                //     console.log('role',rolesId[j]);
                //   }
                 
                // }
                //changing multiple job id save to one
                this.employerSetGetService.setJobIdToGetRoles(JSON.stringify(jobIds));
                // this.employerSetGetService.setJobIdToGetRoles(JSON.stringify([jobIds]));

                 localStorage.setItem('JobDetailsBId', JSON.stringify({jobId:fetchId,roleId:rolesId}));

                this.snackBar.open('SAVED ALL JOB DETAILS', 'Got it!', {
                  
                });
                localStorage.removeItem('donotdelete');

                localStorage.removeItem('allJobs');
                localStorage.removeItem('JobDetails');
                localStorage.removeItem('uploadedFile');
                localStorage.removeItem('Payment');
                localStorage.removeItem('JobSkills'); 
                localStorage.removeItem('JobLocation');
                localStorage.removeItem('Datetime');
                localStorage.removeItem('SaveTimeJobPost');
                 localStorage.removeItem('jobEdit');

                this.sfs._setSelectedFileArray([]);

                //show alert and navigate to job-post component
                //this.router.navigate(['/employer/dashboard/job-posted']);
              
                this.modalRef = this.modalService.show(this.templateJobOffer);
                this.router.navigate(['/employer/dashboard/job-posted']);

              }
            });
      }
      else { 
        this.snackBar.open('SORRY ! JOB DETAILS COULD NOT BE SAVED', 'Got it!', {
         
        });
      }

    }


  }

  // MODIFICATION FOR MULTIPLE JOB ROLE BASED DETAILING
  separateJobsByRoles() {

    for (let index = 0; index < this.allJobsList.length; index++) {

      let roleWiseAction = this.allJobsList[index].api.jobDetails.roleWiseAction;
      let separatedRoleWiseAction = [];

      for (let j = 0; j < roleWiseAction.length; j++) {
        let skills = [];
        for (let k = 0; k < roleWiseAction[j].skills.length; k++) {
          skills.push(roleWiseAction[j].skills[k].id);
        }

        let newSetTime = [];

        for (let timer = 0; timer < this.allJobsList[index].api.setTime.length; timer++) {
          let formattedStartTime = this.allJobsList[index].api.setTime[timer].startTime;
          let formattedEndTime = this.allJobsList[index].api.setTime[timer].endTime;
          newSetTime.push({
            shift: this.allJobsList[index].api.setTime[timer].shift,
            startDate:Date.parse (this.allJobsList[index].api.setTime[timer].startDate),
            // startDate:(new Date(this.allJobsList[index].api.setTime[timer].startDate)).getTime() / 1000,
            endDate:Date.parse(this.allJobsList[index].api.setTime[timer].endDate),
            // endDate:(new Date(this.allJobsList[index].api.setTime[timer].endDate)).getTime() / 1000,
            startTime: formattedStartTime,
            endTime: formattedEndTime,
            totalHour:this.allJobsList[index].api.setTime[timer].totalHour
          });
        }
        console.log('push uploadfile',this.allJobsList[index].api.uploadFile)
        separatedRoleWiseAction.push({
          roleId: roleWiseAction[j].roleId,
          roleName: roleWiseAction[j].roleName,
          skills: skills,
          jobType: this.allJobsList[index].api.jobDetails.jobType,
          payType: this.allJobsList[index].api.jobDetails.payType,
          pay: this.allJobsList[index].api.jobDetails.pay,
          noOfStuff: this.allJobsList[index].api.jobDetails.noOfStuff,
          uploadFile:this.allJobsList[index].api.uploadFile,
          paymentStatus: this.allJobsList[index].api.paymentStatus,
          status: this.allJobsList[index].api.status,
          payment: this.allJobsList[index].api.payment,
          setTime: newSetTime,
          locationName: this.allJobsList[index].api.locationName,
          description: this.allJobsList[index].api.description,
          distance: this.allJobsList[index].api.distance,
          location: this.allJobsList[index].api.location,
          locationTrackingRadius: this.allJobsList[index].api.jobDetails.locationTrackingRadius
        })
        console.log('separatedRoleWiseAction',separatedRoleWiseAction)
      }

      this.allSeparatedJobs.push({
        employerId: this.allJobsList[index].api.employerId,
        jobDetails: {
          industry: this.allJobsList[index].api.jobDetails.industry,
          roleWiseAction: separatedRoleWiseAction
        }
      });

    }

  }
  addJobPost(){
    this.deactiveValue= true;
    if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
      this.snackBar.open("You Can Not Add Another Job In EditJob","Got it!",{
       
      });
    }else{

    this.router.navigate(['/employer/job/post']);
    }
  }
  navigateToCandidatesList(option){
    this.modalRef.hide();
    this.router.navigate(['/employer/candidate/list',option]);
  }
}

//===========correct one===============

// import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
// import { MatSnackBar } from '@angular/material';
// import { EmployerJobService } from '../../../services';
// import { FormControl, Validators } from '@angular/forms';
// import { takeUntil } from 'rxjs/operators';
// import { Subject } from 'rxjs';
// import { Router } from '@angular/router';
// import { SelectedFileService } from '../../../services/selected-file.service';
// import { GlobalActionsService } from '../../../../../../../src/app/services';
// import { BsModalService, BsModalRef } from 'ngx-bootstrap';

// @Component({
//   selector: 'employer-job-summary',
//   templateUrl: './job-summary.component.html',
//   styleUrls: ['./job-summary.component.scss']
// })
// export class JobSummaryComponent implements OnInit {
//   @ViewChild('templateJobOffer') templateJobOffer: TemplateRef<any>;
//   public termsConditionControl = new FormControl('', Validators.required);
//   countOfJobRoles: Number; //variable to get the total payment structure
//   private onDestroyUnSubscribe = new Subject<void>();
//   apiEdit:boolean=false;
//   selectedFile: any;
//   userDetails: any;
//   allJobsList = [];
//   allSeparatedJobs = [];
//   getJobDetails;
//   getJobLocation;
//   getSkillDetails;
//   getTimeDetails;
//   getUploadedFile;
//   getPaymentDetails;
  
//   saveAstemplatePermissionModal: BsModalRef;
//   templateEvent;
//   templateNameModal: BsModalRef
//   public templateName: string = ""
//   modalRef: BsModalRef;
//   constructor(
//     private modalService: BsModalService,
//     private employerSetGetService: EmployerJobService,
//     public snackBar: MatSnackBar,
//     private router: Router,
//     private sfs: SelectedFileService,
//     private globalActionsService: GlobalActionsService,
//   ) {
//     this.sfs.currentFileArray.subscribe(v => {
//       this.selectedFile = v;
//     })
//   }

//   ngOnInit() {
//     this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
//     let getAllJobs = this.employerSetGetService.getAllJob;
//     this.allJobsList = getAllJobs;
//     console.log("value",getAllJobs);
//     console.log("value 2",this.allJobsList)
//     if (this.allJobsList) {
//       this.separateJobsByRoles();
//     }
//     else {
//       this.router.navigate(['/employer/job/post']);
//     }
//     this.getJobDetails = this.employerSetGetService.getJobDetails;
//     this.getJobLocation = this.employerSetGetService.getJobLocation;
//     this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//     this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//     this.getUploadedFile = this.employerSetGetService.getFileDetails;
//     this.getPaymentDetails = this.employerSetGetService.getPayment;
//     console.log("kkkk",this.getJobDetails);
//   }
//   editJobs(index) {
//     // console.log("editJobs", index, this.fileArray, this.allJobsPostedList[index]);
//     // this.apiEdit=true;
//     this.getJobDetails = this.employerSetGetService.getJobDetails;
//     this.getJobLocation = this.employerSetGetService.getJobLocation;
//     this.getSkillDetails = this.employerSetGetService.getSkillDetails;
//     this.getTimeDetails = this.employerSetGetService.getDateTimeDetails;
//     this.getUploadedFile = this.employerSetGetService.getFileDetails;
//     this.getPaymentDetails = this.employerSetGetService.getPayment;

//     // && this.getUploadedFile
//     if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
//       this.apiEdit=false;
//     this.router.navigate(['/employer/job/edit']);


//     }
    
//     // if(this.apiEdit){
//     //   this.clearIndividualJobVariablesInLS();
//     //   console.log("jfjf");
//     // }
//     else {
//       //***********************************************************************/
//       //*          resetting Local storage variables with selected job        */
//       //***********************************************************************/

//       //*                     1. personal details variables                   */
//       let jobDetailsObject = {
//         industry: this.allJobsList[index].api.jobDetails.industry,
//         industryName: this.allJobsList[index].industryName,
//         jobRole: this.allJobsList[index].jobRole,
//         jobType: this.allJobsList[index].api.jobDetails.jobType,
//         payType: this.allJobsList[index].api.jobDetails.payType,
//         pay: this.allJobsList[index].api.jobDetails.pay,
//         no_of_stuffs: this.allJobsList[index].api.jobDetails.noOfStuff,
//         locationTrackingRadius: this.allJobsList[index].api.jobDetails.locationTrackingRadius
//       }
//       this.employerSetGetService.setJobDetails(jobDetailsObject);

//       //************************* 2.location variables **************************/
//       let jobLocation = {
//         location: {
//           type: "Point",
//           coordinates: [
//             this.allJobsList[index].api.location.coordinates[0],
//             this.allJobsList[index].api.location.coordinates[1]
//           ],
//         },
//         distance: this.allJobsList[index].api.distance,
//         locationName: this.allJobsList[index].locationName,
//       }
//       this.employerSetGetService.setJobLocation(jobLocation);

//       //**************************** 3. skill variables ****************************/
//       let skill = {
//         roleWiseAction: this.allJobsList[index].allSelectedSkills,
//         description: this.allJobsList[index].api.description
//       }
//       this.employerSetGetService.setSkillDetails(skill);

//       //************************** 4. date time variables ***************************/
//       this.employerSetGetService.setDateTimeDetails({ dateTime: this.allJobsList[index].api.setTime });

//       //*********************** 5. uploaded files variables ************************/

//       this.employerSetGetService.setFileDetails(this.allJobsList[index].api.uploadFile);
//       // this.employerSetGetService.setFileDetails({ uploadedFile: index });

//       //*********************** 6. payment files variables ************************/
//       this.employerSetGetService.setPayment({ payment: this.allJobsList[index].api.payment });

//       //***********************************************************************/
//       //******** end resetting Local storage variables with selected job **********/
//       //***********************************************************************/

//       this.allJobsList.splice(index, 1);
//       // this.clearIndividualJobVariablesInLS();
//       this.employerSetGetService.setAllJobs(this.allJobsList);
//       this.router.navigate(['/employer/job/post']);
//     }

//   }
//   deleteJobs(index) {
//     if (this.allJobsList[index]) {
//       this.allJobsList.splice(index, 1);
//       this.employerSetGetService.setAllJobs(this.allJobsList);
//     }

//     // if (this.allJobsList.length < 1) {
//     //   this.proceedToSummary = false;
//     // }

//   }
//   clearIndividualJobVariablesInLS(){
//     localStorage.removeItem('JobDetails');
//     localStorage.removeItem('uploadedFile');
//     localStorage.removeItem('Payment');
//     localStorage.removeItem('JobSkills');
//     localStorage.removeItem('JobLocation');
//     localStorage.removeItem('Datetime');
//     localStorage.removeItem('SaveTimeJobPost');
//     // localStorage.removeItem('allJobs');
//   }
  


//   //for checking the terms and conditions control
//   checkControl(event) {
//     if (!event.target.checked) {
//       this.termsConditionControl.reset();
//     }
//   }

//   //save all the jobs
//   async saveAllJobPostDetails() {

//     this.termsConditionControl.reset();

//     //check if job is coming from api or local storage invoking to separate services for two of these
//     if (this.allJobsList[0].isApiEdit) {
//       //call post job api
//       console.log("call update job api", this.allJobsList[0]);

//       let allSkillsToUpdate = [];
//       //format skills
//       for (let j = 0; j < this.allJobsList[0].api.jobDetails.roleWiseAction.length; j++) {
//         let skills = [];
//         for (let k = 0; k < this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills.length; k++) {
//           if (this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k].id) {
//             skills.push({id:this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k].id,name:this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k].name});

//           }
//           else {
//             skills.push(this.allJobsList[0].api.jobDetails.roleWiseAction[j].skills[k]);
//           }
//         }
//         allSkillsToUpdate = skills;
//       }
//       let newSetTime = [];

//       for (let timer = 0; timer < this.allJobsList[0].api.setTime.length; timer++) {
//         let formattedStartTime = this.allJobsList[0].api.setTime[timer].startTime;
//         let formattedEndTime = this.allJobsList[0].api.setTime[timer].endTime;
//         newSetTime.push({
//           shift: this.allJobsList[0].api.setTime[timer].shift,
//           startDate:Date.parse (this.allJobsList[0].api.setTime[timer].startDate),
//           // startDate:(new Date(this.allJobsList[index].api.setTime[timer].startDate)).getTime() / 1000,
//           endDate:Date.parse(this.allJobsList[0].api.setTime[timer].endDate),
//           // endDate:(new Date(this.allJobsList[index].api.setTime[timer].endDate)).getTime() / 1000,
//           startTime: formattedStartTime,
//           endTime: formattedEndTime,
//           totalHour:this.allJobsList[0].api.setTime[timer].totalHour
//         });
//       }

//       let allJobDetails = {
//         roleId: this.allJobsList[0].jobRole[0].jobRoleId,
//         jobId: localStorage.getItem('editJobId'),
//         jobDetails: {
//           location: {
//             type: "Point",
//             coordinates: [
//               this.allJobsList[0].api.location.coordinates[0],
//               this.allJobsList[0].api.location.coordinates[1]
//             ]
//           },
//           skills: allSkillsToUpdate,
//           roleId: this.allJobsList[0].jobRole[0].jobRoleId,
//           roleName: this.allJobsList[0].jobRole[0].jobRoleName,
//           jobType: this.allJobsList[0].api.jobDetails.jobType,
//           payType: this.allJobsList[0].api.jobDetails.payType,
//           pay: this.allJobsList[0].api.jobDetails.pay,
//           noOfStuff: this.allJobsList[0].api.jobDetails.noOfStuff,
//           uploadFile: this.allJobsList[0].api.uploadFile,
//           paymentStatus: "UNPAID",
//           status: "POSTED",
//           payment: this.allJobsList[0].api.payment,
//           locationName: this.allJobsList[0].api.locationName,
//           description: this.allJobsList[0].api.description,
//           setTime:newSetTime,
//           distance: this.allJobsList[0].api.distance,
//           locationTrackingRadius: this.allJobsList[0].api.jobDetails.locationTrackingRadius
//         }
//       };

//       console.log("update job allJobDetails", allJobDetails);

//       //call api and on success remove the local items
//       this.employerSetGetService.updateJob(allJobDetails)
//         .pipe(takeUntil(this.onDestroyUnSubscribe))
//         .subscribe(
//           result => {
//             if (result && !result['isError']) {
//               this.snackBar.open('UPDATED ALL JOB DETAILS', 'Got it!', {
//                 duration: 3000,
//               });
//               localStorage.removeItem('allJobs');
//               localStorage.removeItem('JobDetailsById');
//               localStorage.removeItem('editJobId');
//               localStorage.removeItem('JobDetails');
//               localStorage.removeItem('uploadedFile');
//               localStorage.removeItem('Payment');
//               localStorage.removeItem('JobSkills');
//               localStorage.removeItem('JobLocation');
//               localStorage.removeItem('Datetime');
//               localStorage.removeItem('SaveTimeJobPost');
//               this.modalRef = this.modalService.show(this.templateJobOffer);
//               this.router.navigate(['/employer/dashboard/job-posted']);
              
//             }
//           });
//     } else {
//       console.log("call post job api");

//       let fileArray = [];
//       this.sfs.currentFileArray.subscribe(array => {
//         if (array) {
//           fileArray = array;
//         }
//       })
//       //&& fileArray.length
//       if (this.allSeparatedJobs) {

//         this.snackBar.open('Please wait for sometime..', 'Got it!', {
//           duration: 3000,
//         });

//         this.employerSetGetService.postJob({ jobdetails: this.allSeparatedJobs })
//           .pipe(takeUntil(this.onDestroyUnSubscribe))
//           .subscribe(
//             result => {
//               this.snackBar.open(result['message'],"Got it!",{
//                 duration: 3000,
//               });
//               if (result && !result['isError']) {
//                 this.snackBar.open(result['message'],"Got it!",{
//                   duration: 3000,
//                 });
//                 let jobIds = [];
//                 for(let i=0; i<result['details'].length;i++){
//                   jobIds[i] =result['details'][i].jobId;
                 
//                 }
//                 this.employerSetGetService.setJobIdToGetRoles(JSON.stringify(jobIds));
//                 this.snackBar.open('SAVED ALL JOB DETAILS', 'Got it!', {
//                   duration: 3000,
//                 });
//                 localStorage.removeItem('allJobs');
//                 localStorage.removeItem('JobDetails');
//                 localStorage.removeItem('uploadedFile');
//                 localStorage.removeItem('Payment');
//                 localStorage.removeItem('JobSkills'); 
//                 localStorage.removeItem('JobLocation');
//                 localStorage.removeItem('Datetime');
//                 localStorage.removeItem('SaveTimeJobPost');
//                 this.sfs._setSelectedFileArray([]);

//                 //show alert and navigate to job-post component
//                 //this.router.navigate(['/employer/dashboard/job-posted']);
              
//                 this.modalRef = this.modalService.show(this.templateJobOffer);
//                 this.router.navigate(['/employer/dashboard/job-posted']);

//               }
//             });
//       }
//       else { 
//         this.snackBar.open('SORRY ! JOB DETAILS COULD NOT BE SAVED', 'Got it!', {
//           duration: 3000,
//         });
//       }

//     }


//   }

//   // MODIFICATION FOR MULTIPLE JOB ROLE BASED DETAILING
//   separateJobsByRoles() {

//     for (let index = 0; index < this.allJobsList.length; index++) {

//       let roleWiseAction = this.allJobsList[index].api.jobDetails.roleWiseAction;
//       let separatedRoleWiseAction = [];

//       for (let j = 0; j < roleWiseAction.length; j++) {
//         let skills = [];
//         for (let k = 0; k < roleWiseAction[j].skills.length; k++) {
//           skills.push({id:roleWiseAction[j].skills[k].id,name:roleWiseAction[j].skills[k].name});
        
//         }

//         let newSetTime = [];

//         for (let timer = 0; timer < this.allJobsList[index].api.setTime.length; timer++) {
//           let formattedStartTime = this.allJobsList[index].api.setTime[timer].startTime;
//           let formattedEndTime = this.allJobsList[index].api.setTime[timer].endTime;
//           newSetTime.push({
//             shift: this.allJobsList[index].api.setTime[timer].shift,
//             startDate:Date.parse (this.allJobsList[index].api.setTime[timer].startDate),
//             // startDate:(new Date(this.allJobsList[index].api.setTime[timer].startDate)).getTime() / 1000,
//             endDate:Date.parse(this.allJobsList[index].api.setTime[timer].endDate),
//             // endDate:(new Date(this.allJobsList[index].api.setTime[timer].endDate)).getTime() / 1000,
//             startTime: formattedStartTime,
//             endTime: formattedEndTime,
//             totalHour:this.allJobsList[index].api.setTime[timer].totalHour
//           });
//         }
//         console.log('push uploadfile',this.allJobsList[index].api.uploadFile)
//         separatedRoleWiseAction.push({
//           roleId: roleWiseAction[j].roleId,
//           roleName: roleWiseAction[j].roleName,
//           skills: skills,
//           jobType: this.allJobsList[index].api.jobDetails.jobType,
//           payType: this.allJobsList[index].api.jobDetails.payType,
//           pay: this.allJobsList[index].api.jobDetails.pay,
//           noOfStuff: this.allJobsList[index].api.jobDetails.noOfStuff,
//           uploadFile:this.allJobsList[index].api.uploadFile,
//           paymentStatus: this.allJobsList[index].api.paymentStatus,
//           status: this.allJobsList[index].api.status,
//           payment: this.allJobsList[index].api.payment,
//           setTime: newSetTime,
//           locationName: this.allJobsList[index].api.locationName,
//           description: this.allJobsList[index].api.description,
//           distance: this.allJobsList[index].api.distance,
//           location: this.allJobsList[index].api.location,
//           locationTrackingRadius: this.allJobsList[index].api.jobDetails.locationTrackingRadius
//         })
//         console.log('separatedRoleWiseAction',separatedRoleWiseAction)
//       }

//       this.allSeparatedJobs.push({
//         employerId: this.allJobsList[index].api.employerId,
//         jobDetails: {
//           industry: this.allJobsList[index].api.jobDetails.industry,
//           roleWiseAction: separatedRoleWiseAction
//         }
//       });

//     }

//   }
//   addJobPost(){
//     if (this.getJobDetails && this.getJobLocation && this.getSkillDetails && this.getTimeDetails && this.getPaymentDetails) {
//       this.snackBar.open("You Can Not Add Another Job In EditJob","Got it!",{
//         duration:3000,
//       });
//     }else{

//     this.router.navigate(['/employer/job/post']);
//     }
//   }
//   navigateToCandidatesList(option){
//     this.modalRef.hide();
//     this.router.navigate(['/employer/candidate/list',option]);
//   }
// }
