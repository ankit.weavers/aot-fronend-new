// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { FormGroup, FormBuilder,FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MapsAPILoader } from '@agm/core';
//import { EmployerJobService } from '../../../services';
import { SelectedFileService ,EmployerJobService} from '../../services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Erros } from 'src/app/models';

@Component({
  selector: 'employer-favourite-candidate',
  templateUrl: './favourite-candidate.component.html',
  styleUrls: ['./favourite-candidate.component.scss']
})
export class FavouriteCandidateComponent implements OnInit {
  modalRef: BsModalRef;
  public searchGroupForm: FormGroup; 
  searchHistoryList = [];
  public roleId;
  public jobId;
  public candidateId;
  public employerId;
  employer=[];
  public rating: any = null;
  public review: any = null;
  public errors: any = Erros;
  public stars: any[] = new Array(5);
  pageNo;
  perPage;
  total;
  totalNoOfPages:number =6;
  dataFetching = false;

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private onDestroyUnSubscribe = new Subject<void>();
  allEmployedCandidateDetails: any = null;
  constructor(    public employerJobService: EmployerJobService,
    private fb: FormBuilder,
    private employerjobservice: EmployerJobService,
    public snackBar: MatSnackBar,
    private router: Router,
    private sfs: SelectedFileService,
    private mapsAPILoader: MapsAPILoader,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,) { }

  ngOnInit() {
    this.getAllEmployedCandidates();
  }
  getAllEmployedCandidates() {
    this.employerjobservice.getAllEmployedCandidateDetail({ employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,pageno: this.pageNo,
    perpage: this.perPage  })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.allEmployedCandidateDetails = result['details'];
            this.snackBar.open("successful","Got it!",{
            
            });
            let obj = result['details'];
            if(Object.entries(obj).length !== 0){
              this.total = obj.total ? obj.total : 0;
              // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
              this.dataFetching = false;
            }          
        
          }else{
            this.dataFetching = false;

            this.snackBar.open('Ops! Something went wrong', 'Got it!', {
              
            });
            
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  viewCandidateProfile(candidateId,rating,profilepic) {
    let JobDetailsById = {
      candidateId: candidateId,
      rating:rating,
      profilepic:profilepic
    }
    localStorage.setItem('Job',JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/candidate/candidate-profile'])
  }
  // saveAsFav(employerId,candidateId){
  //   let userType= JSON.parse(localStorage.getItem('currentUser')).employerId;

  //   this.employerJobService.saveAsFav(userType,candidateId) 
  //     .pipe(takeUntil(this.onDestroyUnSubscribe))
  //     .subscribe(
  //       result => {
  //         if (result && !result['isError']) {
  //           this.getAllEmployedCandidates();
  //           this.snackBar.open(result['message'], 'Got it!', {
  //             duration: 3000,
  //           });
  //         }
  //         else {
  //           this.snackBar.open(result['message'], 'Got it!', {
  //             duration: 3000,
  //           });
  //           // this.sfs._setSearchStatus('');
  //         }
  //       },
  //       error => {
  //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //           duration: 3000,
  //         });
  //       }
  //     );
  // }
  removeFromFav(candidateId){
let userType= JSON.parse(localStorage.getItem('currentUser')).employerId;

    this.employerJobService.removeFromFav(userType,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            this.getAllEmployedCandidates();
            this.snackBar.open('You have marked this candidate as UnFavorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
              
            });
            // this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
        
          });
        }
      );
  }
  getPaginatedData(event) {
    this.pageNo = event.page;
    this.dataFetching = true;
    this.allEmployedCandidateDetails = [];
    this.getAllEmployedCandidates();
  } 

}
