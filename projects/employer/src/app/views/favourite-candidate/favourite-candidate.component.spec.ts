import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteCandidateComponent } from './favourite-candidate.component';

describe('FavouriteCandidateComponent', () => {
  let component: FavouriteCandidateComponent;
  let fixture: ComponentFixture<FavouriteCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
