import { json } from '@angular-devkit/core';
import { Component, OnInit,ViewChild,TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators , FormArray} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { GlobalActionsService } from 'src/app/services';
import { takeUntil } from 'rxjs/operators';
import { EmployerJobService } from '../../services';
// import { CandidateEditProfileService } from '../../../../services';
import { Erros } from 'src/app/models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import * as moment from 'moment';


@Component({
  selector: 'employer-all-candidate',
  templateUrl: './all-candidate.component.html',
  styleUrls: ['./all-candidate.component.scss']
})
export class AllCandidateComponent implements OnInit {
  @ViewChild('informationPopUp') informationPopUp:TemplateRef<any>

  private onDestroyUnSubscribe = new Subject<void>();
  payloadSkill = [];
  skillDetailsObj = []
  industryList = [];
  jobRoleList = [];
  skills =[];
  skillss=[];
  skillsList=[];
  selectedRoleWiseSkill = {};
  selectedRoleWiseSkills={};
  public isSkillAvailable :boolean = false;
  public addJobRolesForm: FormGroup;
  selectedRoles = [];
  modalRef: BsModalRef;

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  public errors: any = Erros;
  selectedSkillsPayload: string[] = [];
   public keywords = [];
  public saveJobIdRoleId =[];
  addSkillsForm: FormGroup;
  public isSlider:boolean=false;
  roles=[];
  rolesss=[];
  text:string="Apply Filter";
  public stars: any[] = new Array(5);
  aplyfilter:boolean =false;
  totalItems;
  itemsPerPage = 20;
  pageNo;
  totalNoOfPages : number = 6;
  dataFetching = false;
  rolesssss=[];
  sources=[];
  pastjob =[];

candidates=[];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar,
    public globalactionservice: GlobalActionsService,
    private employerSetGetService: EmployerJobService,
    private modalService: BsModalService,
    // private candidateEditProfileService: CandidateEditProfileService,
  ) {
    this.addJobRolesForm = this.fb.group({
      industry: ['', Validators.required],
      jobRole: ['', Validators.required],
      // skillDetails: new FormArray([])
      // skillDetails: ['',Validators.required]


     
    });
    this.addSkillsForm = this.fb.group({
      skillDetails: new FormArray([])
    });
    this.getRoleSkills();
  }

  ngOnInit() {
    this.getIndustryList();
    // this.getProfileDetails();
    this.getCandidate();

  }
  toggleCollapsibleDiv() {
    this.isSlider ? this.isSlider = false : this.isSlider = true;
  }
  getCandidate(){
    // console.log('roleIds',this.roleIds);
    let roleIds = [];
   

    for(let i =0; i<this.selectedRoles.length; i++){
      roleIds[i] = this.selectedRoles[i].jobRoleId;
  
    }
    console.log('role',roleIds);
    let payload={
      employerId: JSON.parse(localStorage.getItem('currentUser')).employerId,
     
        roleId:roleIds,
        payloadSkill: this.skillDetailsObj,
        presentDate:moment(new Date()).format('MM/DD/YYYY'),
        presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
        localTimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone,

      pageno: this.pageNo,
      perpage:this.itemsPerPage
    } 
    this.employerSetGetService.getEmployerContactDetails(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result=>{
      if (result && !result['isError'] && result['details']) {
        this.candidates = result['details'].results;
        
        // this.snackBar.open("successful","Got it!",{
        //   duration:5000,
        // });

        this.aplyfilter = true;

        let obj =result['details'];
        if(Object.entries(obj).length!==0)
        {
          this.totalItems = obj.total ? obj.total : 0;
          // this.totalNoOfPages = obj.noofpage ? obj.noofpage : 0;
          this.dataFetching = false;
        }else{
          this.dataFetching = false;
          this.snackBar.open('No data found! Please try again','Got it!',{
         
          });
        }
      }else{
        this.snackBar.open("No Candidate Found","Got it!",{
      
        });
        this.candidates.splice(0,this.candidates.length);

      }
    },
    error => {
      this.dataFetching = false;
      this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
       
      });
    }
  );

    
  }
  removeFromFav(candidateId){
    let userType= JSON.parse(localStorage.getItem('currentUser')).employerId;

    this.employerSetGetService.removeFromFav(userType,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            // this.status=false
            this.getCandidate();
            this.snackBar.open('You have marked this candidate as UnFavorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
           
            });
            // this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }
  saveAsFav(candidateId){
    let userType= JSON.parse(localStorage.getItem('currentUser')).employerId;

    this.employerSetGetService.saveAsFav(userType,candidateId) 
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError']) {
            // this.status=true;
            this.getCandidate();
            this.snackBar.open('You have marked this candidate as Favorite', 'Got it!', {
            
            });
          }
          else {
            this.snackBar.open(result['message'], 'Got it!', {
             
            });
            // this.sfs._setSearchStatus('');
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  resetCandidate() {
    this.addJobRolesForm.reset();
    this.industryList =[];
    this.selectedRoles = [];
    this.jobRoleList = [];
    this.skillsList =[];
    // this.addJobRolesForm.controls.jobRole.reset();  // reseting job role on changing job industry
    // this.addJobRolesForm.controls.industry.reset();
    // this.getRoleSkills() ;
    this.getIndustryList();

    this.getCandidate();
    this.aplyfilter =false;
  
  }
  getPaginateData(event)
  {
    this.pageNo = event.page;
    // console.log("shdsad",this.candidates);
    this.candidates = [];
    // console.log("shdsad",this.candidates);

    this.dataFetching = true;
    this.getCandidate();

  }

  // get Industry List
  getIndustryList() {
    //get industries
    this.globalactionservice.fetchIndustry({employerId:JSON.parse(localStorage.getItem('currentUser')).employerId})
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] &&  result['details']) {
            this.industryList = result['details'].response.industries;
           } else {
             this.snackBar.open('Please select your industry in profile section before post a job','Got it!', {
             
             });
           }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
          });
        }
      );
  }

  //get Job Role List
  getJobRoleList(industryId) {
    this.globalactionservice.getRole({ industryId: [industryId] })
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe(
        result => {
          if (result && !result['isError'] && result['details']) {
            this.jobRoleList = result['details'];
          } 
          else {
            this.snackBar.open(result['message'], 'Got it!', {
            
            });
          }
        },
        error => {
          this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
          
          });
        }
      );
  }

  // industry selected
  selectJobIndustry(industryId) {
    this.jobRoleList = [];
    this.addJobRolesForm.controls.jobRole.setValue('');  // reseting job role on changing job industry
    this.getJobRoleList(industryId);
    this.selectedRoles = [];
    this.skillsList = [];
    // this.skills =[];
  }

  // get selected job roles based on industry selected
  selectedJobRoles() {
    this.selectedRoles = this.addJobRolesForm.value.jobRole;
    console.log('selectedRoles',this.selectedRoles);
   
    for(let i =0; i<this.selectedRoles.length; i++){
      this.rolesssss[i] = this.selectedRoles[i].jobRoleId;

    
    console.log('skills',this.rolesssss)

    }
   
    // console.log('skills',this.skills)
     this.getRoleSkills();
  }
  getRoleSkills() {

    let roleIds = [];
  
    for(let i =0; i<this.selectedRoles.length; i++){
      roleIds[i] = this.selectedRoles[i].jobRoleId;
  
    }
    console.log('roleIds',roleIds);
  
  

    // this.globalactionservice.getJobSkillById({jobRole:roleIds})
    //   .pipe(takeUntil(this.onDestroyUnSubscribe))
    //   .subscribe(
    //     result => {
    //       if (result && !result['isError']) {
    //         let skillsRes =  result['details'];
    //         console.log("skillsRes",typeof skillsRes);
    //         for(let i =0; i<skillsRes.length; i++){
    //           this.skills[i] = skillsRes[i];
    //           console.log('skills',this.skills);
    //         } 
    //         let skillFac =this.skills;
    //         console.log('skillFac',typeof skillFac);

    //         for(let i=0; i<skillFac.length; i++)
    //         {
    //           this.skillss = skillFac[i].details;
    //          console.log('skillss',this.skillss);
    //         } 
      

    //       }
    //     },
    //     error => {
    //       this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
    //         duration: 3000,
    //       });
    //     }
    //   );


      //get skill details by Job Role Id
      // this.globalactionservice.getJobSkillById({ jobRole: roleIds })
      //   .pipe(takeUntil(this.onDestroyUnSubscribe))
      //   .subscribe(
      //     result => {
      //       if (result && !result['isError']) {
      //         this.skillsList = result['details'];
      //         console.log('skillsList########################################', this.skillsList)

      //       } else {
      //         this.snackBar.open(result['message'], 'Got it!', {
      //           duration: 3000,
      //         });
      //       }
      //     },
      //     error => {
      //       this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
      //         duration: 3000,
      //       });
      //     }
      //   )
      this.skillDetailsObj.splice(0,this.skillDetailsObj.length);
      // console.log("kjhdsafkjabfjky",this.skillDetailsObj);
      this.globalactionservice.fetchSkill({ jobRole: roleIds })
        .pipe(takeUntil(this.onDestroyUnSubscribe))
        .subscribe(
          result => {
            if (result && !result['isError'] && result['details']) {
              this.skillsList = result['details'];
              console.log('skillsList########################################', this.skillsList)

            } else {
              this.snackBar.open(result['message'], 'Got it!', {
              
              });
            }
          },
          error => {
            this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
           
            });
          }
        )




  }
  postjob(candidate){
    let payload = {
      // employerId:JSON.parse(localStorage.getItem('currentUser')).employerId,
      candidateId:candidate,
      today:moment(new Date()).format('MM/DD/YYYY'),
      presentTime:String((new Date().getHours() < 10 ? '0' : '') + new Date().getHours()) + '.' + String((new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()),
    }
    this.employerSetGetService.pastJobs(payload)
    .pipe(takeUntil(this.onDestroyUnSubscribe))
    .subscribe(result =>{
      if(result && !result['isError'] && result['details'].hiredJobDetails.length != 0){

        this.pastjob = result['details'].hiredJobDetails[0].results

        this.modalRef = this.modalService.show(this.informationPopUp, { backdrop: 'static', keyboard: false });

      }
      else {
        this.snackBar.open('No data Found', 'Got it!', {
              
        });
      }


    },
    error => {
      this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
     
      });
    }
    )
  }
  dateFormet(startdate,enddate) {
    let startdateFortmet = startdate.toString().split(",")[0]
    let enddateFortmet = enddate.toString().split(",")[0]
    if(startdateFortmet == enddateFortmet) {
      let formeth = moment(startdate,'MM/DD/YYYY').format('DD/MM/YYYY');
      return `${formeth}`;
    } 
    else {
      let stformet =  moment(startdate,'MM/DD/YYYY').format('DD/MM/YYYY')
      let enformet =  moment(enddate,'MM/DD/YYYY').format('DD/MM/YYYY')
      return `${stformet}-${enformet}`
    }
    console.log(startdateFortmet,enddateFortmet);
  }

  viewCandidateProfile(candidateId,rating,profilepic) {
    let JobDetailsById = {
      candidateId: candidateId,
      rating:rating,
      profilepic:profilepic
    }
    localStorage.setItem('Job',JSON.stringify(JobDetailsById));
    this.router.navigate(['/employer/candidate/candidate-profile'])
  }

  onChecklistChange(checked, sId, jobRoleId, roleName, sName,iId,roleIndex,skillIndex) {
    console.log('selected ch',this.skillsList[roleIndex].skillDetails[skillIndex].isSelected)

    this.skillsList[roleIndex].skillDetails[skillIndex].isSelected = checked
   
    if( this.skillsList[roleIndex].skillDetails[skillIndex].isSelected==true ){
      this.skillDetailsObj.push(this.skillsList[roleIndex].skillDetails[skillIndex].skillId);
      console.log("siklllllllll",this.skillDetailsObj);
    }else{
      this.skillDetailsObj.splice(skillIndex,1);
      console.log("siklllllllll",this.skillDetailsObj);

    }
      // this.skillDetailsObj.push({
      //   skillId:sId,
      //   skillName:sName,
      // })
      // this.selectedRoleWiseSkills = {
      //   jobRoleName:roleName,
      //   jobRoleId:jobRoleId,
      //   industryId:iId,
      //   skillDetails:this.skillDetailsObj
      // }
      // this.payloadSkill[roleIndex] =  this.selectedRoleWiseSkills;
      
      console.log('skillDetails',this.skillsList);
  }

  // get candidate profile details
  // getProfileDetails() {
  //   this.candidateEditProfileService.getCandidateProfileDetails({ candidateId: this.userDetails.candidateId })
  //     .pipe(takeUntil(this.onDestroyUnSubscribe))
  //     .subscribe(
  //       result => {
  //         if (result && !result['isError'] ) {

  //           //set value into form
  //           if (result['details'].payloadSkill) {
  //             let industryId = result['details'].payloadSkill[0].industryId;
  //             this.addJobRolesForm.get('industry').setValue(industryId);
  //             this.selectJobIndustry(industryId);
  //             let payloadSkill =  result['details'].payloadSkill;
  //             let selectedRoles = [];
  //             selectedRoles =  payloadSkill.map(({ jobRoleName }) => jobRoleName);
  //             console.log('selectedRoles',selectedRoles)

  //             this.addJobRolesForm.controls['jobRole'].setValue(selectedRoles);
  //             // this.selectedRoles = result['details'].skillDetails;
              
  //             // let jobRoles = [];
  //             // Object.keys(result['details'].skillDetails).map(
  //             //   key => {
  //             //     jobRoles.push(result['details'].skillDetails[key].jobRoleName)
  //             //   }
  //             // )
  //             // this.addJobRolesForm.get('jobRole').setValue(jobRoles);
  //             this.skillsList = result['details'].payloadSkill;
  //           }
  //           // if(result['details'].skillids)
  //           // {
  //           //   let industryId =result['details'].skillids[0].industryId;
  //           //   this.addJobRolesForm.get('industry').setValue(industryId);
  //           //   this.selectJobIndustry(industryId);
  //           //   this.skills = result['details'].skillids;
  //           //   let jobRoles = [];
  //           //   Object.keys(result['detaills'].skillids).map(
  //           //     key =>{
  //           //       jobRoles.push(result['details'].skillids[key].skillName)
  //           //     }
  //           //   )
  //           //   this.addJobRolesForm.get('jobRole').setValue(jobRoles);
  //           // }
  //         }
  //         // else {
  //         //   this.snackBar.open(result['message'], 'Got it!', {
  //         //     duration: 3000,
  //         //   });
  //         // }
  //       },
  //       error => {
  //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //           duration: 3000,
  //         });
  //       }
  //     );
  // }

  // //save job roles 
  // saveJobRoles() {
  //   let roles = [];
  //   Object.keys(this.addJobRolesForm.value.jobRole).map(
  //     key => {
  //       roles.push(this.addJobRolesForm.value.jobRole[key].jobRoleId)
        
  //     }
  //   )
  //   let jobDetailsObject = {
  //     candidateId: this.userDetails.candidateId,
  //     payloadSkill: this.skillsList
  
  //   }

  //   this.addJobRolesForm.reset();
  //   this.selectedRoles = [];
  //   this.jobRoleList = [];
  //   this.skillsList =[];
    

  //   this.candidateEditProfileService.postJobRolesDetails(jobDetailsObject)
  //     .pipe(takeUntil(this.onDestroyUnSubscribe))
  //     .subscribe(
  //       result => {
  //         if (result && !result['isError']) {
  //           this.snackBar.open('ROLES UPDATED', 'Got it!', {
  //             duration: 3000,
  //           });
  //           //show alert and navigate to candidate edit component
  //           this.router.navigate(['/candidate/profile/edit']);
  //         } else {
  //           this.snackBar.open(result['message'], 'Got it!', {
  //             duration: 3000,
  //           });
  //         }
  //       },
  //       error => {
  //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //           duration: 3000,
  //         });
  //       }
  //     );
  // }
  
  // onCheckSkill(checked, skillName, skillId) {
  //   if (checked) {
  //     this.keywords.push(skillName)
  //     this.selectedSkillsPayload.push(skillId)
  //     console.log("data",this.selectedSkillsPayload);
  //     console.log('data 2',this.keywords);
  //   }
  //   else {
  //     for (var i = 0; i < this.keywords.length; i++) {
  //       if (this.keywords[i] === skillName) {
  //         this.keywords.splice(i, 1);
  //         this.selectedSkillsPayload.splice(i, 1)
  //       }
  //     }
  //   }
  // }
  // getFillterCandidate(){
  //   let roles = [];
  //   for(let i =0; i<this.selectedRoles.length; i++){
  //     roles[i] = this.selectedRoles[i].jobRoleId;
  //     console.log("roles",this.roles);
  
  //   }
  //   // Object.keys(this.addJobRolesForm.value.jobRole).map(
  //   //   key => {
  //   //     roles.push(this.addJobRolesForm.value.jobRole[key].jobRoleId)
  //   //     console.log("roleid",this.roles);
        
  //   //   }
  //   // )
  //   let jobDetailsObject = { 
  //     employerId: this.userDetails.employerId,
  //     roleId:this.rolesssss,
  //     payloadSkill: this.skillDetailsObj
  
  //   }

  //   // this.addJobRolesForm.reset();
  //   // this.selectedRoles = [];
  //   // this.jobRoleList = [];
  //   // this.skillsList =[];
    

  //   this.employerSetGetService.getCandidateFillter(jobDetailsObject)
  //     .pipe(takeUntil(this.onDestroyUnSubscribe))
  //     .subscribe(
  //       result => {
  //         if (result && !result['isError']) {
  //           this.snackBar.open(result['message'], 'Got it!', {
  //             duration: 3000,
  //           });
  //           //show alert and navigate to candidate edit component
  //           // this.router.navigate(['/candidate/profile/edit']);
  //         } else {
  //           this.snackBar.open(result['message'], 'Got it!', {
  //             duration: 3000,
  //           });
  //         }
  //       },
  //       error => {
  //         this.snackBar.open('Please Check Your Network Connection', 'Got it!', {
  //           duration: 3000,
  //         });
  //       }
  //     );

  // }
  



}
