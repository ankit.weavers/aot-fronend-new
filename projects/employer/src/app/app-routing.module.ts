import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroductoryComponent } from './views/introductory/introductory.component';
import {FavouriteCandidateComponent} from './views/favourite-candidate/favourite-candidate.component';
import {AllCandidateComponent} from './views/all-candidate/all-candidate.component';
  import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'get-started',
    component: IntroductoryComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: false,
      isEditable: [],
      title: 'Get Started'
    }
  },
  {
    path: 'fav-candidate',
    component: FavouriteCandidateComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Favourite Workers'
    }
  },
  {
    path: 'all-candidate',
    component: AllCandidateComponent,
    data: {
      hasBackBtn: false,
      hasHamburger: true,
      isEditable: [],
      title: 'Workers'
    }
  },
  {
    path: '',
    loadChildren: './views/static/employer-static.module#EmployerStaticModule'
  },
  {
    path: 'candidate',
    loadChildren: './views/candidate/candidate.module#CandidateModule'
  },
  {
    path: 'dashboard',
    loadChildren: './views/user-dashboard/user-dashboard.module#UserDashboardModule'
  },
  {
    path: 'profile',
    loadChildren: './views/profile/employer-profile.module#EmployerProfileModule'
  },
  {
    path: 'job',
    loadChildren: './views/job/employer-job.module#EmployerJobModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
