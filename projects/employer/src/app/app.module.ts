// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployerCoreModule } from './modules/employer-core.module';
import { ModalModule } from 'ngx-bootstrap';
import { FavouriteCandidateComponent } from './views/favourite-candidate/favourite-candidate.component';
import { AllCandidateComponent } from './views/all-candidate/all-candidate.component';

const providers = [];

@NgModule({
  declarations: [
    AppComponent,
    FavouriteCandidateComponent,
    AllCandidateComponent
  ],
  imports: [
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    EmployerCoreModule,
    ModalModule
  ],
  providers,
  bootstrap: [AppComponent]
})
export class EmployerAppModule { }

@NgModule({})
export class EmployerMainModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EmployerAppModule,
      providers
    };
  }
}
